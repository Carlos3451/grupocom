<?php

define('TIMEZONE', 'America/Mazatlan');
date_default_timezone_set(TIMEZONE);

include_once(__DIR__ . '/../../config/Configuracion.php');
$GLOBALS['config'] = new Configuracion();

include_once(__DIR__ . '/../classes/Gasto.php');
$gastoObj = new Gasto();

$gastoObj->process_programados();

?>