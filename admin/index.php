<?php
return;
define('TIMEZONE', 'America/Mazatlan');
date_default_timezone_set(TIMEZONE);

define('BASE_URL', '/admin/');

include_once('../config/Configuracion.php');

$GLOBALS['config'] = new Configuracion();

include_once('classes/Conexion.php');

$GLOBALS['DB'] = new Conexion();

include_once('classes/Login.php');

$pathUrl = str_replace('admin', '', $_SERVER['REQUEST_URI']);

if (strpos($pathUrl, '?') !== false) {
    $pathUrlArray = explode('?', $pathUrl);
    $pathUrl = $pathUrlArray[0];
    $getQueries = explode('&', $pathUrlArray[1]);
    foreach ($getQueries AS $getQuery) {
        $getQueryArray = explode('=', $getQuery);
        $_GET[$getQueryArray[0]] = $getQueryArray[1];
    }
}

$routes = explode('/', trim($pathUrl, '/'));

/* CHECK IF LOGIN ALREADY */
$loginObj = new Login();
$loginData = $loginObj->check_cookie();
if ($loginData['error'] == '') {
    $GLOBALS['usuario'] = $loginData['resultado'];
    route_login();
}
else {
    route_logout();
}

/* LOGOUT */

function route_logout() {
    global $routes;
    switch ($routes[0]) {
        case 'functions':
            route_functions_logout();
            break;
        default:
            include_once('routes/login/login.php');
            break;
    }
}

function route_functions_logout() {
    global $routes;
    if (count($routes)>2) {
        $_GET['function'] = $routes[2];
    }
    switch ($routes[1]) {
        case 'login':
            include_once('functions/login-functions.php');
            break;
        default: 
            if (isset($_COOKIE['PBGUj8WtVXZVGfn2'])) {
                setcookie('PBGUj8WtVXZVGfn2', null, -1, '/');
                echo '{"error":"SESSION_CLOSED"}';
            }
            else {
                echo '{"error":"NOT_LOGIN"}';
            }
            break;
    }
}

/* LOGIN */

function route_login() {
    global $routes;
    switch ($routes[0]) {
        case 'functions':
            route_functions();
            break;
        case 'credenciales':
            route_credenciales();
            break;
        case 'rutas':
            route_rutas();
            break;
        case 'clientes':
            route_clientes();
            break;
        case 'proveedores':
            route_proveedores();
            break;
        case 'productos':
            route_productos();
            break;
        case 'pedidos':
            route_pedidos();
            break;
        case 'ventas':
            route_ventas();
            break;
        case 'secciones':
            route_secciones();
            break;
        case 'inventarios':
            route_inventarios();
            break;
        case 'mermas':
            route_mermas();
            break;
        case 'compras':
            route_compras();
            break;
        case 'notificaciones':
            route_notificaciones();
            break;
        case 'reportes':
            route_reportes();
            break;
        case 'almacenes':
            route_almacenes();
            break;
        case 'entregas':
            route_entregas();
            break;
        case 'gastos':
            route_gastos();
            break;
        case 'facturas':
            route_facturas();
            break;
        case 'complementos':
            route_complementos();
            break;
        case 'facturas-v4':
            route_facturas_v4();
            break;
        case 'complementos-v4':
            route_complementos_v4();
            break;
        default:
            include_once('routes/dashboard/dashboard.php');
            break;
    }
}

function route_functions() {
    global $routes;
    if (count($routes)>2) {
        $_GET['function'] = $routes[2];
        switch ($routes[1]) {
            case 'login':
                include_once('functions/login-functions.php');
                break;
            case 'utils':
                include_once('functions/utils-functions.php');
                break;
            case 'notificacion':
                include_once('functions/notificacion-functions.php');
                break;
            case 'credencial':
                include_once('functions/credencial-functions.php');
                break;
            case 'cliente':
                include_once('functions/cliente-functions.php');
                break;
            case 'proveedor':
                include_once('functions/proveedor-functions.php');
                break;
            case 'producto':
                include_once('functions/producto-functions.php');
                break;
            case 'categoria':
                include_once('functions/categoria-functions.php');
                break;
            case 'ruta':
                include_once('functions/ruta-functions.php');
                break;
            case 'pedido':
                include_once('functions/pedido-functions.php');
                break;
            case 'caja':
                include_once('functions/caja-functions.php');
                break;
            case 'ticket':
                include_once('functions/ticket-functions.php');
                break;
            case 'inventario':
                include_once('functions/inventario-functions.php');
                break;
            case 'merma':
                include_once('functions/merma-functions.php');
                break;
            case 'compra':
                include_once('functions/compra-functions.php');
                break;
            case 'seccion':
                include_once('functions/seccion-functions.php');
                break;
            case 'dashboard':
                include_once('functions/dashboard-functions.php');
                break;
            case 'reporte':
                include_once('functions/reporte-functions.php');
                break;
            case 'almacen':
                include_once('functions/almacen-functions.php');
                break;
            case 'entrega':
                include_once('functions/entrega-functions.php');
                break;
            case 'gasto':
                include_once('functions/gasto-functions.php');
                break;
            case 'factura':
                include_once('functions/factura-functions.php');
                break;
            case 'factura-v4':
                include_once('functions/factura-v4-functions.php');
                break;
        }
    }
}

/* RUTAS */

function route_credenciales() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/credenciales/credenciales-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/credenciales/credenciales-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/credenciales/credenciales-lista.php');
    }
}

function route_clientes() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/clientes/clientes-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/clientes/clientes-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/clientes/clientes-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/clientes/clientes-lista.php');
    }
}

function route_proveedores() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/proveedores/proveedores-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/proveedores/proveedores-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/proveedores/proveedores-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/proveedores/proveedores-lista.php');
    }
}

function route_rutas() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/rutas/rutas-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/rutas/rutas-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/rutas/rutas-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/rutas/rutas-lista.php');
    }
}

function route_productos() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/productos/productos-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/productos/productos-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/productos/productos-form.php');
                }
                break;
            case 'imprimir':
                include_once('routes/productos/productos-imprimir.php');
                break;
        }
    }
    else {
        include_once('routes/productos/productos-lista.php');
    }
}

function route_compras() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/compras/compras-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/compras/compras-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/compras/compras-form.php');
                }
                break;/*
            case 'imprimir':
                if (count($routes)==3) {
                    $_GET['ids'] = $routes[2];
                    include_once('routes/compras/compras-pdf.php');
                }
                break;
                */
        }
    }
    else {
        include_once('routes/compras/compras-lista.php');
    }
}

function route_pedidos() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/pedidos/pedidos-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/pedidos/pedidos-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/pedidos/pedidos-form.php');
                }
                break;
            case 'imprimir':
                if (count($routes)==3) {
                    $_GET['ids'] = $routes[2];
                    include_once('routes/pedidos/pedidos-pdf.php');
                }
                break;
        }
    }
    else {
        include_once('routes/pedidos/pedidos-lista.php');
    }
}

function route_ventas() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/ventas/ventas-form.php');
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    include_once('routes/ventas/ventas-form.php');
                }
                break;
            case 'imprimir':
                if (count($routes)==3) {
                    $_GET['ids'] = $routes[2];
                    include_once('routes/ventas/ventas-pdf.php');
                }
                break;
        }
    }
    else {
        include_once('routes/ventas/ventas-lista.php');
    }
}

function route_secciones() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/secciones/secciones-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/secciones/secciones-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/secciones/secciones-lista.php');
    }
}

function route_inventarios() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/inventarios/inventarios-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/inventarios/inventarios-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/inventarios/inventarios-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/inventarios/inventarios-lista.php');
    }
}

function route_mermas() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/mermas/merma-form.php');
                break;
            case 'editar':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'EDIT';
                    include_once('routes/mermas/merma-form.php');
                }
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/mermas/merma-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/mermas/mermas-lista.php');
    }
}

function route_reportes() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'ventasproductos':
                include_once('routes/reportes/ventasproductos.php');
                break;
            case 'imprimir':
                include_once('routes/reportes/imprimir.php');
                break;
            case 'descargar':
                include_once('routes/reportes/descargar.php');
                break;
        }
    }
    else {
        include_once('routes/reportes/reportes-form.php');
    }
}

function route_notificaciones() {
    global $routes;
    if (count($routes) == 1) {
        include_once('routes/notificaciones/notificaciones.php');
    }
}

function route_almacenes() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    $_GET['mode'] = 'VIEW';
                    include_once('routes/almacenes/almacenes-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/almacenes/almacenes-lista.php');
    }
}

function route_entregas() {
    global $routes;
    if (count($routes)==1) {
        include_once('routes/entregas/entregas-lista.php');
    }
}

function route_gastos() {
    global $routes;
    if (count($routes)==1) {
        include_once('routes/gastos/gastos-lista.php');
    }
}

function route_facturas() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/facturas/factura-form.php');
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    include_once('routes/facturas/factura-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/facturas/facturas-lista.php');
    }
}

function route_complementos() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/complementos/complemento-form.php');
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    include_once('routes/complementos/complemento-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/complementos/complementos-lista.php');
    }
}

function route_facturas_v4() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/facturas-v4/factura-v4-form.php');
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    include_once('routes/facturas-v4/factura-v4-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/facturas-v4/facturas-v4-lista.php');
    }
}

function route_complementos_v4() {
    global $routes;
    if (count($routes)>1) {
        switch ($routes[1]) {
            case 'agregar':
                $_GET['mode'] = 'ADD';
                include_once('routes/complementos-v4/complemento-v4-form.php');
                break;
            case 'ver':
                if (count($routes)>2) {
                    $_GET['id'] = $routes[2];
                    include_once('routes/complementos-v4/complemento-v4-form.php');
                }
                break;
        }
    }
    else {
        include_once('routes/complementos-v4/complementos-v4-lista.php');
    }
}

?>