<div class="editorprecios-overlay">
    <div class="editorprecios-container">
        <h3>Cambiar precios</h3>
        <div class="inputs-containers" style="display:none;">
            <div class="input-container">
                <p>Categoría</p>
                <select name="categoriaID" disabled><option value="">Cargando...</option></select>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Título</p>
                <input type="text" placeholder="Menudeo" name="titulo1">
            </div>
            <div class="input-container">
                <p>Precio</p>
                <input class="inp-icon inp-price" type="text" name="precio1" number>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Título</p>
                <input type="text" placeholder="Medio Mayoreo" name="titulo2">
            </div>
            <div class="input-container">
                <p>Precio</p>
                <input class="inp-icon inp-price" type="text" name="precio2" number>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Título</p>
                <input type="text" placeholder="Mayoreo" name="titulo3">
            </div>
            <div class="input-container">
                <p>Precio</p>
                <input class="inp-icon inp-price" type="text" name="precio3" number>
            </div>
        </div>
        <div class="actions-container">
            <button class="accept-btn">Guardar</button>
            <button class="close-btn">Cerrar</button>
        </div>
    </div>
</div>