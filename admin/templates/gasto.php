<div class="gasto-overlay">
    <div class="gasto-container">
        <h3>Nuevo Gasto</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Fecha</p>
                <input type="text" name="gasto-fecha">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Razón</p>
                <input type="text" name="gasto-razon">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cantidad</p>
                <input class="inp-icon inp-price" type="text" name="gasto-cantidad">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Comentario</p>
                <textarea name="gasto-comentario"></textarea>
            </div>
        </div>
        <div class="actions-container">
            <button class="accept-gasto">Agregar</button>
            <button class="cancel-gasto">Cancelar</button>
        </div>
    </div>
</div>