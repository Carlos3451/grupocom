
<div class="sortcontroller-container">
    <div class="sort-container">
        <div class="method-container">
            <p>Ordenar por</p>
            <div class="method-selector">
                <div class="method-selected">
                    <p></p>
                    <div class="triangle"></div>
                </div>
                <div class="method-options">
                </div>
            </div>
        </div>
        <div class="order-container">
            <img tooltip-hint="Orden descendente" value="desc" class="order-desc" src="/admin/imgs/icons/orden-desc.png">
            <img tooltip-hint="Orden ascendente" value="asc" class="order-asc" src="/admin/imgs/icons/orden-asc.png">
        </div>
    </div>
    <div class="filter-container">
        <img tooltip-hint="Filtros" src="/admin/imgs/icons/filter.png">
    </div>
    <div class="search-container">
        <p>Buscar</p>
        <input class="search" type="text">
    </div>
</div>
<div class="sortcontroller-filter-overlay">
    <div class="sortcontroller-filter-container">
        <h3>Filtros</h3>
        <div class="filters-containers">
        </div>
        <div class="actions-container">
            <button class="filter-close" type="button">Cerrar</button>
        </div>
    </div>
</div>