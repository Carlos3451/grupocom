
<?php
    $sections = $GLOBALS['usuario']['secciones'];
?>
<header>
    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="sections-container">
        <div class="sections-toggle" <?php if (count($sections) == 1 && $GLOBALS['usuario']['id'] != 1) { echo 'noaction'; } ?>>
            <?php if ($GLOBALS['usuario']['seccionActual']['imagen'] != '') { ?>
                <img src="<?php echo $GLOBALS['usuario']['seccionActual']['imagen']; ?>">
            <?php
                } else {
                    $nameArray = explode(' ', $GLOBALS['usuario']['seccionActual']['nombre']);
                    $name = strtoupper(substr($nameArray[0], 0, 1));
            ?>
                <div class="section-abbreviation"><?php echo $name; ?></div>
            <?php } ?>
            <p><?php echo $GLOBALS['usuario']['seccionActual']['nombre']; ?></p>
            <div class="triangle"></div>
        </div>
        <div class="sections">
            <?php
            if (count($sections) >= 1) {
                for ($i=0; $i<count($sections); $i++) {
                    if ($sections[$i]['id'] != $GLOBALS['usuario']['seccionActual']['id']) {
            ?>
            <div class="section section-changer" data-section-id="<?php echo $sections[$i]['id']; ?>">
            <?php if ($sections[$i]['imagen'] != '') { ?>
                <img src="<?php echo $sections[$i]['imagen']; ?>">
            <?php
                } else {
                    $nameArray = explode(' ', $sections[$i]['nombre']);
                    $name = strtoupper(substr($nameArray[0], 0, 1));
            ?>
                <div class="section-abbreviation"><?php echo $name; ?></div>
            <?php } ?>
                <p><?php echo $sections[$i]['nombre']; ?></p>
            </div>
            <?php
                    }
                }
            }
            ?>
            <?php
                if ($GLOBALS['usuario']['id'] == 1) {
            ?>
            <a href="secciones" class="section section-admin">
                <img src="/admin/imgs/icons/config.png">
                <p>Administrar...</p>
            </a>
            <?php
                }
            ?>
        </div>
    </div>
    <div class="menu-fast-container">
        <a href="/admin/" tooltip-hint="Inicio"><img class="menu-icon" src="/admin/imgs/icons/dashboard.png"></a>
        <?php if (in_array('credenciales', $GLOBALS['usuario']['privilegios'])) { ?>
        <a href="/admin/credenciales" tooltip-hint="Credenciales"><img class="menu-icon" src="/admin/imgs/icons/credenciales.png"></a>
        <?php } ?>
        <?php if (in_array('clientes', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/clientes" tooltip-hint="Clientes"><img class="menu-icon" src="/admin/imgs/icons/clientes.png"></a> -->
        <?php } ?>
        <?php if (in_array('proveedores', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/proveedores" tooltip-hint="Proveedores"><img class="menu-icon" src="/admin/imgs/icons/proveedores.png"></a> -->
        <?php } ?>
        <?php if (in_array('rutas', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/rutas" tooltip-hint="Rutas"><img class="menu-icon" src="/admin/imgs/icons/rutas.png"></a> -->
        <?php } ?>
        <?php if (in_array('productos', $GLOBALS['usuario']['privilegios'])) { ?>
        <a href="/admin/productos" tooltip-hint="Productos"><img class="menu-icon" src="/admin/imgs/icons/productos.png"></a>
        <?php } ?>
        <?php if (in_array('productos', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['privilegiosNivel'] > 1) { ?>
        <!-- <a href="/admin/inventarios" tooltip-hint="Inventarios"><img class="menu-icon" src="/admin/imgs/icons/inventario.png"></a> -->
        <?php } ?>
        <?php if (in_array('proveedores', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['privilegiosNivel'] > 1) { ?>
        <!-- <a href="/admin/compras" tooltip-hint="Compras"><img class="menu-icon" src="/admin/imgs/icons/compras.png"></a> -->
        <?php } ?>
        <?php if (in_array('gastos', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/gastos" tooltip-hint="Gastos"><img class="menu-icon" src="/admin/imgs/icons/gastos.png"></a> -->
        <?php } ?>
        <?php if (in_array('pedidos', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/pedidos" tooltip-hint="Pedidos"><img class="menu-icon" src="/admin/imgs/icons/pedidos.png"></a> -->
        <?php } ?>
        <?php if (in_array('ventas', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/ventas" tooltip-hint="Ventas Mostrador"><img class="menu-icon" src="/admin/imgs/icons/ventas.png"></a> -->
        <?php } ?>
        <?php if ((in_array('facturacion', $GLOBALS['usuario']['privilegios']) || $GLOBALS['usuario']['id'] == 1) && $GLOBALS['usuario']['seccionActual']['facturacion'] == 1) { ?>
        <a href="/admin/facturas-v4" tooltip-hint="Facturas V4"><img class="menu-icon" src="/admin/imgs/icons/facturas.png"></a>
        <a href="/admin/complementos-v4" tooltip-hint="Complementos de pago V4"><img class="menu-icon" src="/admin/imgs/icons/complementos.png"></a>
        <?php }?>
        <?php if (in_array('reportes', $GLOBALS['usuario']['privilegios'])) { ?>
        <!-- <a href="/admin/reportes" tooltip-hint="Reportes"><img class="menu-icon" src="/admin/imgs/icons/reportes.png"></a> -->
        <?php } ?>
    </div>
    <a href="notificaciones" class="notifications-container">
        <img src="/admin/imgs/icons/notificacion.png">
        <div class="notifications-counter" <?php echo $GLOBALS['usuario']['notificaciones'] == 0 ? 'style="display:none;"' : ''; ?>>
            <p><?php echo min(99, $GLOBALS['usuario']['notificaciones']); ?></p>
        </div>
    </a>
    <div class="options-container">
        <div class="options-toggle">
            <img src="<?php echo $GLOBALS['usuario']['imagen']; ?>">
            <p><?php echo $GLOBALS['usuario']['nombre']; ?></p>
            <div class="triangle"></div>
        </div>
        <div class="options">
            <div class="option option-logout">
                <img src="/admin/imgs/icons/logout.png">
                <p>Cerrar Sesión</p>
            </div>
        </div>
    </div>
    <div class="menu-container-overlay">
        <div class="menu-container">
            <div class="menu-close">
                <div></div>
                <div></div>
            </div>
            <a href="/admin/"><img class="menu-icon" src="/admin/imgs/icons/dashboard.png"><p>Inicio</p></a>
            <?php if (in_array('credenciales', $GLOBALS['usuario']['privilegios'])) { ?>
            <a href="/admin/credenciales"><img class="menu-icon" src="/admin/imgs/icons/credenciales.png"><p>Credenciales</p></a>
            <?php } ?>
            <?php if (in_array('clientes', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/clientes"><img class="menu-icon" src="/admin/imgs/icons/clientes.png"><p>Clientes</p></a> -->
            <?php } ?>
            <?php if (in_array('proveedores', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/proveedores"><img class="menu-icon" src="/admin/imgs/icons/proveedores.png"><p>Proveedores</p></a> -->
            <?php } ?>
            <?php if (in_array('rutas', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/rutas"><img class="menu-icon" src="/admin/imgs/icons/rutas.png"><p>Rutas</p></a> -->
            <?php } ?>
            <?php if (in_array('productos', $GLOBALS['usuario']['privilegios'])) { ?>
            <a href="/admin/productos"><img class="menu-icon" src="/admin/imgs/icons/productos.png"><p>Productos</p></a>
            <?php } ?>
            <?php if (in_array('productos', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/inventarios"><img class="menu-icon" src="/admin/imgs/icons/inventario.png"><p>Inventarios</p></a> -->
            <?php } ?>
            <?php if (in_array('proveedores', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/compras"><img class="menu-icon" src="/admin/imgs/icons/compras.png"><p>Compras</p></a> -->
            <?php } ?>
            <?php if (in_array('gastos', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/gastos"><img class="menu-icon" src="/admin/imgs/icons/gastos.png"><p>Gastos</p></a> -->
            <?php } ?>
            <?php if (in_array('pedidos', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/pedidos"><img class="menu-icon" src="/admin/imgs/icons/pedidos.png"><p>Pedidos</p></a> -->
            <?php } ?>
            <?php if (in_array('ventas', $GLOBALS['usuario']['privilegios'])) { ?>
            <!-- <a href="/admin/ventas"><img class="menu-icon" src="/admin/imgs/icons/ventas.png"><p>Ventas Mostrador</p></a> -->
            <?php } ?>
            <?php if ($GLOBALS['usuario']['privilegiosNivel'] == 4) { ?>
            <!-- <a href="/admin/reportes"><img class="menu-icon" src="/admin/imgs/icons/reportes.png"><p>Reportes</p></a> -->
            <?php } ?>
            <?php if (in_array('facturacion', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['seccionActual']['facturacion'] == 1) { ?>
            <a href="/admin/facturas-v4"><img class="menu-icon" src="/admin/imgs/icons/facturas.png"><p>Facturas V4</p></a>
            <a href="/admin/complementos-v4"><img class="menu-icon" src="/admin/imgs/icons/complementos.png"><p>Complementos de pago V4</p></a>
            <?php } ?>
        </div>
    </div>
</header>
<div class="header-filler"></div>