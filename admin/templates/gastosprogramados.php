<div class="gastosprogramados-overlay">
    <div class="gastosprogramados-container">
        <div class="title-container">
            <h3>Gastos programados</h3>
            <div class="actions-container">
                <div class="add-gastosprogramados" tooltip-hint="Programar Gasto"><img src="/admin/imgs/icons/actions/add.png"></div>
            </div>
        </div>
        <div class="gastosprogramados-table">
            <div class="gastosprogramados-header">
                <div class="gastosprogramados-column"><p>Razon</p></div>
                <div class="gastosprogramados-column"><p>Comentario</p></div>
                <div class="gastosprogramados-column"><p>Programación</p></div>
                <div class="gastosprogramados-column"><p>Cantidad</p></div>
                <div class="gastosprogramados-column"><p>Acciones</p></div>
            </div>
        </div>
        <button class="gastosprogramados-close-btn">Cerrar</button>
    </div>
</div>
<div class="gastoprogramado-overlay">
    <div class="gastoprogramado-container">
        <h3>Nuevo gasto programado</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Programación</p>
                <select name="programacion">
                    <option value="mensual">Mensual (Primer día de cada mes)</option>
                    <option value="quincenal">Quincenal (Cada quincena)</option>
                    <option value="finesdesemana">Fines de semana (Sábados y Domingos)</option>
                    <option value="entresemana">Entre semana (Lunes a Viernes)</option>
                    <option value="semanal">Semanal</option>
                    <option value="diario">Diario</option>
                </select>
                <select name="dia" style="display:none;">
                    <option value="lunes">Lunes</option>
                    <option value="martes">Martes</option>
                    <option value="miercoles">Miercoles</option>
                    <option value="jueves">Jueves</option>
                    <option value="viernes">Viernes</option>
                    <option value="sabado">Sabado</option>
                    <option value="domingo">Domingo</option>
                </select>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Razón</p>
                <input type="text" name="razon">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cantidad</p>
                <input class="inp-icon inp-price" type="text" name="cantidad">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Comentario</p>
                <textarea name="comentario"></textarea>
            </div>
        </div>
        <div class="actions-container">
            <button class="gastoprogramado-accept">Agregar</button>
            <button class="gastoprogramado-cancel">Cancelar</button>
        </div>
    </div>
</div>