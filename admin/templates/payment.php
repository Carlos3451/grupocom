
<div class="payment-container-overlay">
    <div class="payment-container">
        <p>Metodo de pago</p>
        <div class="methods-container">
            <img class="selected" src="/admin/imgs/icons/pago-efectivo.png" value="efectivo" tooltip-hint="Efectivo">
            <img src="/admin/imgs/icons/pago-cheque.png" value="cheque" tooltip-hint="Cheque">
            <img src="/admin/imgs/icons/pago-tarjeta.png" value="tarjeta" tooltip-hint="Tarjeta">
            <img src="/admin/imgs/icons/pago-transferencia.png" value="transferencia" tooltip-hint="Transferencia">
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cantidad</p>
                <input type="text" class="inp-icon inp-price payment-quantity">
            </div>
        </div>
        <div class="actions-container">
            <button class="payment-accept">Aceptar</button>
            <button class="payment-cancel">Cancelar</button>
        </div>
    </div>
</div>