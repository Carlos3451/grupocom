<div class="repartidor-overlay">
    <div class="repartidor-container">
        <h3>Asignar Repartidor</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Repartidor</p>
                <select name="repartidor"></select>
            </div>
        </div>
        <div class="actions-container">
            <button class="repartidor-accept">Aceptar</button>
            <button class="repartidor-cancel">Cancelar</button>
        </div>
    </div>
</div>