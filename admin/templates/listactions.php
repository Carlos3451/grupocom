
<div class="list-actions-container">
    <div class="selected-counter">
        <p>0 seleccionados</p>
        <div class="actions-container">
            <img tooltip-hint="Quitar selecciones" class="selects-remove" src="/admin/imgs/icons/close.png">
        </div>
    </div>
    <div class="actions-container">
        <img tooltip-hint="Ver" src="/admin/imgs/icons/password-shown.png" class="action-view">
        <img tooltip-hint="Asignar Repartidor" src="/admin/imgs/icons/entregas.png" class="action-delivery">
        <img tooltip-hint="Pedidos" src="/admin/imgs/icons/pedidos.png" class="action-pedidos">
        <img tooltip-hint="Editar" src="/admin/imgs/icons/actions/edit.png" class="action-edit">
        <img tooltip-hint="Imprimir" src="/admin/imgs/icons/actions/printer.png" class="action-print">
        <img tooltip-hint="Enviar Email" src="/admin/imgs/icons/actions/email.png" class="action-email">
        <img tooltip-hint="Descargar" src="/admin/imgs/icons/actions/download.png" class="action-download">
        <img tooltip-hint="Facturar" src="/admin/imgs/icons/facturas.png" class="action-facturar">
        <img tooltip-hint="Cambiar usuario/contraseña" src="/admin/imgs/icons/actions/changepassword.png" class="action-change-password">
        <img tooltip-hint="Agregar existencia" src="/admin/imgs/icons/actions/addexistence.png" class="action-add-existence">
        <img tooltip-hint="Bloquear" src="/admin/imgs/icons/actions/lock.png" class="action-lock">
        <img tooltip-hint="Desbloquear" src="/admin/imgs/icons/actions/unlock.png" class="action-unlock">
        <img tooltip-hint="Agregar pago" src="/admin/imgs/icons/caja.png" class="action-pay">
        <img tooltip-hint="Eliminar" src="/admin/imgs/icons/actions/delete.png" class="action-delete">
        <img tooltip-hint="Cancelar" src="/admin/imgs/icons/actions/cancelar.png" class="action-cancel">
    </div>
</div>