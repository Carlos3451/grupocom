
<div class="claves-sat-overlay">
    <div class="claves-sat-container">
        <div class="searcher-container">
            <input class="inp-icon inp-search" type="text" name="buscadorClavesSAT">
            <select name="tipoClaveSAT">
                <option value="prod">Productos</option>
                <option value="serv">Servicios</option>
            </select>
        </div>
        <div class="claves-overlay">
            <div class="claves-containers"></div>
        </div>
        <button>Cerrar</button>
    </div>
</div>