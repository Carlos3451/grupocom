<div class="complementospago-overlay">
    <div class="complementospago-container">
        <div class="complementospago-title-container">
            <h3>Complementos de Pago</h3>
            <div class="actions-container">
                <img class="complementospago-add" tooltip-hint="Agregar Complemento de Pago" src="imgs/icons/actions/add.png">
            </div>
        </div>
        <div class="complementospago-table">
            <div class="complementospago-header">
                <div class="complementospago-column"><p>Num.</p></div>
                <div class="complementospago-column"><p>Fecha</p></div>
                <div class="complementospago-column"><p>Cantidad</p></div>
                <div class="complementospago-column"><p>Forma de pago</p></div>
                <div class="complementospago-column"><p>Acciones</p></div>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Total:</p>
                <input class="inp-icon inp-price" type="text" name="complementospago-total" disabled>
            </div>
            <div class="input-container">
                <p>Pagado:</p>
                <input class="inp-icon inp-price" type="text" name="complementospago-pagado" disabled>
            </div>
            <div class="input-container">
                <p>Restante:</p>
                <input class="inp-icon inp-price" type="text" name="complementospago-restante" disabled>
            </div>
        </div>
        <div class="actions-container">
            <button type="button" class="complementospago-close">Cerrar</button>
        </div>
    </div>
</div>

<div class="complementopagoadder-overlay">
    <div class="complementopagoadder-container">
        <h3>Agregar Complemento de Pago</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Fecha</p>
                <input type="text" name="complementopagoadder-fecha">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cantidad</p>
                <input class="inp-icon inp-price" type="text" name="complementopagoadder-cantidad">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Forma de pago</p>
                <select type="text" name="complementopagoadder-forma-pago"></select>
            </div>
        </div>
        <div class="actions-container">
            <button type="button" class="complementopagoadder-add">Agregar</button>
            <button type="button" class="complementopagoadder-close">Cerrar</button>
        </div>
    </div>
</div>