<div class="emailfactura-overlay">
    <div class="emailfactura-container">
        <h3>Enviar factura por correo</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Para:</p>
                <input type="text" name="emailfactura-email">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>CCs (separados por comas):</p>
                <input type="text" name="emailfactura-ccs">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Mensaje</p>
                <textarea name="emailfactura-mensaje"></textarea>
            </div>
        </div>
        <div class="actions-container">
            <button class="emailfactura-send">Enviar</button>
            <button class="emailfactura-close">Cerrar</button>
        </div>
    </div>
</div>