<div class="tinyproducto-overlay">
	<form class="tinyproducto-form">
        <h3>Producto</h3>
		<div class="inputs-containers">
			<div class="input-container">
				<p>Nombre</p>
				<input class="inp-required" type="text" name="nombre">
			</div>
		</div>
		<div class="inputs-containers">
			<div class="input-container">
				<p>Codigo de barras</p>
				<input class="inp-icon inp-barcode" type="text" name="codigo">
			</div>
		</div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Unidad</p>
                <select type="text" name="unidad">
                    <option value="pieza">Pieza (pza)</option>
                    <option value="paquete">Paquete (paq)</option>
                    <option value="caja">Caja (caj)</option>
                    <option value="burbuja">Burbuja (burb)</option>
                    <option value="costal">Costal (cost)</option>
                    <option value="maso">Maso (maso)</option>
                    <option value="gramo">Gramo (g)</option>
                    <option value="kilogramo">Kilogramo (kg)</option>
                    <option value="libra">Libra (lb)</option>
                    <option value="mililitro">Mililitro (ml)</option>
                    <option value="litro">Litro (l)</option>
                    <option value="unidad">Unidad (ud)</option>
                </select>
            </div>
        </div>
		<div class="inputs-containers">
			<div class="input-container">
				<p>Costo</p>
				<input class="inp-icon inp-price" type="text" name="costo" price>
			</div>
		</div>
		<div class="inputs-containers">
			<div class="input-container">
				<p>Precio Menudeo</p>
				<input class="inp-icon inp-price" type="text" name="precio1" price>
			</div>
			<div class="input-container">
				<p>Precio Med. May.</p>
				<input class="inp-icon inp-price" type="text" name="precio2" price>
			</div>
			<div class="input-container">
				<p>Precio Mayoreo</p>
				<input class="inp-icon inp-price" type="text" name="precio3" price>
			</div>
		</div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Inventario Minimo</p>
                <input class="inp-required inp-unit inp-pieza" type="text" name="inventarioMinimo">
            </div>
        </div>
        <div class="actions-containers">
            <button class="accept-btn">Agregar</button>
            <button class="cancel-btn">Cancelar</button>
        </div>
	</form>
</div>