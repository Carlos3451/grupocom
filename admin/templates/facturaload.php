<div class="facturaload-overlay">
    <div class="facturaload-container">
        <h3>Cargar datos para facturar</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Tipo</p>
                <select name="facturaload-tipo">
                    <option value="pedido">Pedidos</option>
                    <option value="mostrador">Mostrador</option>
                </select>
            </div>
            <div class="input-container">
                <p>Fecha</p>
                <input type="text" name="facturaload-fecha">
            </div>
        </div>
        <div class="actions-container">
            <button class="facturaload-load">Cargar</button>
            <button class="facturaload-close">Cerrar</button>
        </div>
    </div>
</div>