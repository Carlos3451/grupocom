(function() {

clearForm();

if (formMode=='EDIT') {
    loadingDataMsg = new Wait('Cargando datos.');
}

document.querySelectorAll('[prettycheckbox]').forEach(function(elem, index) {
    let contextData = elem.getAttribute('data-context-data');
    let contextMode = elem.getAttribute('data-context-mode');
    let tooltipHint = elem.getAttribute('data-hint');
    let prettyCheckbox = new PrettyCheckbox(elem, {
        contextMode: contextMode,
        contextData: contextData
    });
    prettyCheckbox.container.setAttribute('tooltip-hint', tooltipHint);
});

ToolTip.update();

if (formMode == 'EDIT') {
    getCredencial();
}

function loadSections(sectionsStr) {
    let sectionsElems = document.querySelectorAll('[data-sectionID]');
    let sections = sectionsStr.split(',');
    sectionsElems.forEach(function(elem, index) {
        let sectionID = elem.getAttribute('data-sectionID');
        if (sections.indexOf(sectionID) != -1) {
            elem.checked = true;
            elem.dispatchEvent(new Event('change'));
        }
    });
}

function getSections() {
    let sectionsElems = document.querySelectorAll('[data-sectionID]');
    let sections = [];
    sectionsElems.forEach(function(elem, index) {
        let checked = elem.checked;
        if (checked) {
            let sectionID = elem.getAttribute('data-sectionID');
            sections.push(sectionID);
        }
    });
    sections.join(',');
    return sections;
}

function getCredencial() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/credencial/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadCredencial(data['resultado']);         
            }
            else {
                new Message('Ocurrio un error al obtener el usuario.<br>' + data['error'], 'error', function() {
                    window.location = 'credenciales';
                });
            }
        }
    });
}

function loadCredencial(credencial) {
    document.querySelector('[name="nombre"]').value = credencial['nombre'];
    document.querySelector('[name="telefono"]').value = credencial['telefono'];
    document.querySelector('[name="email"]').value = credencial['email'];
    document.querySelector('[name="calle"]').value = credencial['calle'];
    document.querySelector('[name="colonia"]').value = credencial['colonia'];
    document.querySelector('[name="codigoPostal"]').value = credencial['codigoPostal'];
    document.querySelector('[name="numeroExterior"]').value = credencial['numeroExterior'];
    document.querySelector('[name="privilegios"]').value = credencial['privilegios'];
    document.querySelector('[name="privilegiosNivel"]').value = credencial['privilegiosNivel'];
    document.querySelector('.image-container > img').setAttribute('src', credencial['imagen']);
    loadPrivilegios();
    loadSections(credencial['secciones']);
}

function loadPrivilegios() {
    let privileges = document.querySelector('[name="privilegios"]').value.split(',');
    for (let i=0; i<privileges.length; i++) {
        document.querySelector('.privileges-containers > .privilege-container[value="' + privileges[i] + '"]').classList.add('selected');
    }
}

document.querySelector('.image-container').addEventListener('click', function(ev) {
    this.querySelector('[name="imagen"]').click();
});

document.querySelector('[name="imagen"]').addEventListener('change', function(ev) {
    Tools.fileToBase64(this.files[0], function(data) {
        document.querySelector('.image-container > img').setAttribute('src', data);
    });
});

document.querySelector('#credencial-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

document.querySelector('.form-submit').addEventListener('click', function(ev) {
    let filled = isFormFilled();
    if (filled) {
        submitForm();
    }
});

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
        window.location = '/admin/credenciales';
    });
});

function submitForm() {
    let formData = new FormData(document.querySelector('#credencial-form'));
    formData.append('secciones', getSections());
    document.querySelector('.form-submit').setAttribute('disabled', '');
    if (formMode == 'ADD') {
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/credencial/add',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Creando creadencial.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Credencial creada con exito.', 'success', function() {
                        location = '/admin/credenciales';
                    });
                }
                else {
                    switch (data['error']) {
                        case 'PASSWORD_NOT_MATCH':
                            new Message('No coinciden las contraseñas.', 'error');
                            break;
                        default:
                            new Message('Ocurrio un error al crear la credencial: "' + data['error'] + '"', 'error');
                            break;
                    }
                }
            }
        }); 
    }
    else if (formMode == 'EDIT') {
        formData.append('id', editID);
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/credencial/edit',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Guardando creadencial.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Credencial guardada con exito.', 'success', function() {
                        location = '/admin/credenciales';
                    });
                }
                else {
                    new Message('Ocurrio un error al crear la credencial: "' + data['error'] + '"', 'error');
                }
            }
        });
    }
}

document.querySelectorAll('.privileges-containers > .privilege-container').forEach(function(elem, index) {
    elem.addEventListener('click', function(ev) {
        this.classList.toggle('selected');
        let privileges = [];
        document.querySelectorAll('.privileges-containers > .privilege-container.selected').forEach(function(elem, index) {
            privileges.push(elem.getAttribute('value'));
        });
        document.querySelector('[name="privilegios"]').value = privileges.join(',');
    });
});

function clearForm() {
    document.querySelectorAll('#credencial-form input').forEach(function(elem, index) {
        elem.value = '';
        elem.checked = false;
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#credencial-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}


})();