(function(){

if (privilegiosNivel > 1) {
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        window.location = "/admin/credenciales/editar/" + dataID;
    }
}

if (privilegiosNivel > 2) {
    Lista.actionDelete['active'] = true;
    Lista.actionDelete['unique'] = false;
    Lista.deleteFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se eliminara la credencial seleccionada.',
            msgWait: 'Eliminando credencial.',
            msgSuccess: 'Credencial eliminada con exito.',
            msgError: 'Ocurrio un error al tratar de eliminar la credencial.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se eliminaran las credenciales seleccionadas.',
                msgWait: 'Eliminando credenciales.',
                msgSuccess: 'Credenciales eliminadas con exito.',
                msgError: 'Ocurrio un error al tratar de eliminar a las credenciales.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Confirm(requestObj.msgQuestion, 'eliminar', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/credencial/delete',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.remove();
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
    Lista.actionChangePassword['active'] = true;
    Lista.actionChangePassword['unique'] = true;
    Lista.changePasswordFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        let userName = selecteds[0].getAttribute('data-usuario');
        ChangePassword.open(userName);
        ChangePassword.dataID = dataID;
    }
}

SortController.setOnMethodChange(function() {

    Lista.load('credencial/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order
    }, function(data) {
        let privilegios = data['privilegios'].split(',');
        let privilegiosFrag = document.createElement('div');
        let privilegiosNivel = '';
        switch (data['privilegiosNivel']) {
            case 1:
                privilegiosNivel = 'Autor';
                break;
            case 2:
                privilegiosNivel = 'Editor';
                break;
            case 3:
                privilegiosNivel = 'Moderador';
                break;
            case 4:
                privilegiosNivel = 'Superusuario';
                break;
        }
        if (data['id'] == 1) {
            privilegiosNivel = 'Administrador';
        }
        for (let i=0; i<privilegios.length; i++) {
            let imageElem = document.createElement('img');
            switch (privilegios[i]) {
                case 'credenciales':
                    imageElem.setAttribute('src', '/admin/imgs/icons/credenciales.png');
                    imageElem.setAttribute('tooltip-hint', 'Credenciales');
                    break;
                case 'clientes':
                    imageElem.setAttribute('src', '/admin/imgs/icons/clientes.png');
                    imageElem.setAttribute('tooltip-hint', 'Clientes');
                    break;
                case 'proveedores':
                    imageElem.setAttribute('src', '/admin/imgs/icons/proveedores.png');
                    imageElem.setAttribute('tooltip-hint', 'Proveedores');
                    break;
                case 'rutas':
                    imageElem.setAttribute('src', '/admin/imgs/icons/rutas.png');
                    imageElem.setAttribute('tooltip-hint', 'Rutas');
                    break;
                case 'productos':
                    imageElem.setAttribute('src', '/admin/imgs/icons/productos.png');
                    imageElem.setAttribute('tooltip-hint', 'Productos');
                    break;
                case 'pedidos':
                    imageElem.setAttribute('src', '/admin/imgs/icons/pedidos.png');
                    imageElem.setAttribute('tooltip-hint', 'Pedidos');
                    break;
                case 'gastos':
                    imageElem.setAttribute('src', '/admin/imgs/icons/gastos.png');
                    imageElem.setAttribute('tooltip-hint', 'Gastos');
                    break;
                case 'facturacion':
                    imageElem.setAttribute('src', '/admin/imgs/icons/facturas.png');
                    imageElem.setAttribute('tooltip-hint', 'Facturacion');
                    break;
                default:
                    imageElem.remove();
                    continue;
            }
            privilegiosFrag.appendChild(imageElem);
        }
        return [
            ('000' + data['id']).slice(-4), data['nombre'],
            '<div class="privileges-container">' + privilegiosFrag.innerHTML + '</div>',
            privilegiosNivel
        ];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']],
            ['data-usuario', data['usuario']]
        ];
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'id'],
    ['Nombre', 'nombre']
]);

/* CHANGE PASSWORD */

let ChangePassword = new class {
    constructor() {
        this.overlayElem = document.querySelector('.changepassword-overlay');
        this.containerElem = document.querySelector('.changepassword-container');
        this.dataID = 0;
        this.userName = '';
        this.overlayElem.addEventListener('animationend', function(ev) {
            if (ev.animationName == 'disappear') {
                this.style.display = 'none';
            }
        });
        this.containerElem.querySelector('.changepassword-cancel').addEventListener('click', function() {
            ChangePassword.close();
        });
        this.containerElem.querySelector('.changepassword-confirm').addEventListener('click', function() {
            ChangePassword.confirm();
        });
    }
    confirm() {
        let usuario = document.querySelector('[name="usuario"]').value;
        let contraseña = document.querySelector('[name="contraseña"]').value;
        let confirmarContraseña = document.querySelector('[name="confirmarContraseña"]').value;
        if (contraseña == confirmarContraseña) {
            Tools.ajaxRequest({
                data: {
                    id: ChangePassword.dataID,
                    usuario: usuario,
                    contraseña: contraseña,
                    confirmarContraseña: confirmarContraseña
                },
                method: 'POST',
                url: 'functions/credencial/change-user-data',
                waitMsg: new Wait('Guardando datos de usuario.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Datos de usuario guardados con exito.', 'success', function() {
                            location.reload();
                        });
                    }
                    else {
                        new Message('Ocurrio un error con la solicitud.<br>' + data['error'], 'error');
                    }
                }
            });
        }
        else {
            new Message('Las contraseñas no coinciden.', 'warning');
        }
    }
    open(userName) {
        this.containerElem.querySelector('[name="usuario"]').value = userName;
        this.overlayElem.style.display = 'flex';
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'appear 0.2s ease-in-out forwards';
    }
    close() {
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'disappear 0.2s ease-in-out forwards';
    }
}

SortController.load();

})();