(function() {

clearForm();

if (formMode=='EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos.');
}

let vendedorAc = new SelectInp(document.querySelector('[name="usuarioID"]'), {
    placeholder: 'S/D',
    defaultValue: 0
});

getClients();

getUsers().then(function(values) {
    if (formMode == 'EDIT' || formMode == 'VIEW') {
        getRuta();
    }
});

function getClients() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'id',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/cliente/get-all-routeless',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    loadClients(result);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function getUsers() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'id',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/credencial/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        options.push({
                            'name': ('000' + result[i]['id']).slice(-4) + ' ' + result[i]['nombre'],
                            'value': result[i]['id'],
                            'keys': [('000' + result[i]['id']).slice(-4), result[i]['nombre']]
                        });
                    }
                    vendedorAc.setOptions(options);
                }
                resolve();
            }
        });
    });
    return promise;
}

function getRuta() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/ruta/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadRuta(data['resultado']);
            }
            else {
                new Message('Ocurrio un error al obtener la ruta.<br>' + data['error'], 'error', function() {
                    window.location = 'rutas';
                });
            }
        }
    });
}

function removeClients() {
    document.querySelectorAll('.searcher-table-row').forEach(function(elem, index) {
        elem.remove();
    });
}

function loadClients(clients) {
    removeClients();
    let clientsFragment = document.createDocumentFragment();
    for (let i=0; i<clients.length; i++) {
        let searchKey = ('000' + clients[i]['folio']).slice(-4) + ' ' + clients[i]['nombre'] + ' ' + clients[i]['direccionCompleta'];
        let row = document.createElement('div');
        row.className = 'searcher-table-row';
        row.setAttribute('data-client-id', clients[i]['id']);
        row.setAttribute('data-client-folio', clients[i]['folio']);
        row.setAttribute('data-client-name', clients[i]['nombre']);
        row.setAttribute('data-client-address', clients[i]['direccionCompleta']);
        row.setAttribute('data-client-route-days', clients[i]['rutaDias']);
        row.setAttribute('data-client-lat', clients[i]['lat']);
        row.setAttribute('data-client-lng', clients[i]['lng']);
        row.setAttribute('data-search-key', searchKey);
        row.innerHTML =
            '<div class="searcher-table-column searcher-table-selectable">'+
                '<div class="checkmark-container">'+
                    '<div class="checkmark"></div>'+
                '</div>'+
            '</div>'+
            '<div class="searcher-table-column">'+
                '<p>' + ('000' + clients[i]['folio']).slice(-4) + '</p>'+
            '</div>'+
            '<div class="searcher-table-column">'+
                '<p>' + clients[i]['nombre'] + '</p>'+
            '</div>'+
            '<div class="searcher-table-column">'+
                '<p>' + clients[i]['direccionCompleta'] + '</p>'+
            '</div>';
        clientsFragment.appendChild(row);
        row.querySelector('.checkmark-container').addEventListener('click', function(ev) {
            let searcherAddElem = document.querySelector('.searcher-add');
            let row = this.closest('.searcher-table-row');
            row.classList.toggle('selected');
            let selecteds = document.querySelectorAll('.searcher-table-row.selected');
            if (selecteds.length > 0) {
                searcherAddElem.removeAttribute('disabled');
                searcherAddElem.textContent = 'Agregar (' + selecteds.length + ')';
            }
            else {
                searcherAddElem.setAttribute('disabled', '');
                searcherAddElem.textContent = 'Agregar';
            }
        });
    }
    document.querySelector('.searcher-table').appendChild(clientsFragment);
}

function loadRuta(ruta) {
    document.querySelector('[name="nombre"]').value = ruta['nombre'];
    document.querySelector('[name="usuarioID"]').value = ruta['usuarioID'];
    document.querySelector('[name="usuarioID"]').dispatchEvent(new Event('change'));
    if (ruta['clientes']['error'] == '') {
        for (let i=0; i<ruta['clientes']['resultado'].length; i++) {
            let cliente = ruta['clientes']['resultado'][i];
            //cliente['rutaDias'] = cliente['rutaDias'].toString(2);
            clientAdd(cliente);
        }
    }
    if (formMode == 'VIEW') {
        disableInputs();
        document.querySelector('.client-filter-route-days').removeAttribute('disabled')
        document.querySelector('.client-filter-route-days').removeAttribute('transparent');
    }
}
    
document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/rutas';
        });
    }
    else {
        window.location = '/admin/rutas';
    }
});

function submitForm() {
    let formData = new FormData(document.querySelector('#data-form'));
    document.querySelector('.form-submit').setAttribute('disabled', '');
    if (formMode == 'ADD') {
        let clientsAdded = getClientsAdded();
        formData.append('clientes', JSON.stringify(clientsAdded));
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/ruta/add',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Creando ruta.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Ruta creada con exito.', 'success', function() {
                        location = '/admin/rutas';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            if (data['error'].indexOf('Duplicate') != -1) {
                                new Message('El usuario ya tiene una ruta asignada.', 'error');
                            }
                            else {
                                new Message('Ocurrio un error al crear la ruta:<br>"' + data['error'] + '"', 'error');
                            }
                            break;
                    }
                }
            }
        });
    }
    else if (formMode == 'EDIT') {
        formData.append('id', editID);
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/ruta/edit',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Guardando ruta.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Ruta guardada con exito.', 'success', function() {
                        location = '/admin/rutas';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            new Message('Ocurrio un error al tratar de guardar la ruta: "' + data['error'] + '"', 'error');
                            break;
                    }
                }
            }
        });
    }
}

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
    document.querySelectorAll('#data-form select').forEach(function(elem, index) {
        elem.selectedIndex = 0;
    });
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#data-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}

/* CLIENTS ADD */
if (formMode != 'VIEW') {
    document.querySelector('.client-add').addEventListener('click', function(ev) {
        let searcherOverlay = document.querySelector('.clients-searcher-overlay');
        searcherOverlay.style.display = 'flex';
        searcherOverlay.style.animation = 'appear 0.2s ease-in-out forwards';
        document.querySelector('.searcher-add').setAttribute('disabled', '');
        searcherReset();
    });
}

document.querySelector('.searcher-cancel').addEventListener('click', function() {
    let searcherOverlay = document.querySelector('.clients-searcher-overlay');
    searcherOverlay.style.animation = 'disappear 0.2s ease-in-out forwards';
    searcherOverlay.addEventListener('animationend', searcherAnimationEnd);
    function searcherAnimationEnd() {
        searcherOverlay.style.display = 'none';
        this.removeEventListener('animationend', searcherAnimationEnd);
    }
});

document.querySelector('.searcher-add').addEventListener('click', function() {
    let clients = [];
    let selecteds = document.querySelectorAll('.searcher-table-row.selected');
    selecteds.forEach(function(elem, index){
        clients.push({
            id: elem.getAttribute('data-client-id'),
            folio: elem.getAttribute('data-client-folio'),
            nombre: elem.getAttribute('data-client-name'),
            direccionCompleta: elem.getAttribute('data-client-address'),
            rutaDias: elem.getAttribute('data-client-route-days'),
            lat: elem.getAttribute('data-client-lat'),
            lng: elem.getAttribute('data-client-lng'),
        });
        elem.remove();
    });
    switch (formMode) {
        case 'ADD':
            for (let i=0; i<clients.length; i++) {
                clientAdd(clients[i]);
            }
            break;
        case 'EDIT':
            let requestObj = {
                msgWait: 'Añadiendo cliente a la ruta.',
                msgSuccess: 'Cliente añadido a la ruta con exito.',
                msgError: 'Ocurrio un error al tratar de añadir al cliente a la ruta.' 
            };
            if (selecteds.length > 1) {
                requestObj = {
                    msgWait: 'Añadiendo clientes a la ruta.',
                    msgSuccess: 'Clientes añadidos a la ruta con exito.',
                    msgError: 'Ocurrio un error al tratar de añadir a los clientes a la ruta.' 
                };
            }
            let ids = [];
            for (let i=0; i<clients.length; i++) {
                ids.push(clients[i]['id']);
            }
            Tools.ajaxRequest({
                data: {
                    rutaID: editID,
                    clientesIDs: ids  
                },
                method: 'POST',
                url: 'functions/ruta/set-clients-route',
                waitMsg: new Wait(requestObj['msgWait']),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        getClients();
                        for (let i=0; i<clients.length; i++) {
                            clientAdd(clients[i]);
                        }
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
            break;
    }
    document.querySelector('.searcher-cancel').click();
});

if (formMode != 'VIEW') {
    document.querySelector('.client-remove').addEventListener('click', function(ev) {
        let selecteds = document.querySelectorAll('.clients-table-row.selected');
        let requestObj = {
            msgQuestion: 'Se quitara de la ruta al cliente seleccionado.',
            msgWait: 'Quitando al cliente de la ruta.',
            msgSuccess: 'Cliente quitado de la ruta con exito.',
            msgError: 'Ocurrio un error al tratar de quitar al cliente de la ruta.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se quitaran de la ruta al clientes seleccionado.',
                msgWait: 'Quitando a los clientes de la ruta.',
                msgSuccess: 'Clientes quitados de la ruta con exito.',
                msgError: 'Ocurrio un error al tratar de quitar a los clientes de la ruta.' 
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-client-id'));
        }
        new Confirm(requestObj['msgQuestion'], 'quitar', function() {
            switch (formMode) {
                case 'ADD':
                    getClients();
                    selecteds.forEach(function(elem, index) {
                        elem.remove();
                    });
                    organizateTable();
                    break;
                case 'EDIT':
                    Tools.ajaxRequest({
                        data: {
                            clientesIDs: ids
                        },
                        method: 'POST',
                        url: 'functions/ruta/unset-clients-route',
                        waitMsg: new Wait(requestObj['msgWait']),
                        success: function(response) {
                            this.waitMsg.destroy();
                            let data = JSON.parse(response);
                            if (data['error'] == '') {
                                new Message(requestObj['msgSuccess'], 'success');
                                getClients();
                                selecteds.forEach(function(elem, index) {
                                    elem.remove();
                                });
                                organizateTable();
                            }
                            else {
                                new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                            }
                        }
                    });
                    break;
            }
        });
    });
}

document.querySelector('.clients-searcher-inp').addEventListener('input', function() {
    let key = this.value;
    searchClient(key);
});

function searcherReset() {
    document.querySelector('.clients-searcher-inp').value = '';
    document.querySelectorAll('.searcher-table-row.selected').forEach(function(elem, index) {
        elem.classList.remove('selected');    
        elem.classList.remove('hidden', 'odd', 'even');
    });
}

function searchClient(key) {
    key = key.trim().toLowerCase();
    let rows = document.querySelectorAll('.searcher-table-row');
    let odd = true;
    rows.forEach(function(elem, index) {
        let name = elem.getAttribute('data-search-key').toLowerCase();
        let nameNormalized = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
        elem.classList.add('hidden');
        elem.classList.remove('odd', 'even');
        if (name.indexOf(key) != -1) {
            if (odd) {
                elem.classList.add('odd');
                odd = false;
            }
            else {
                elem.classList.add('even');
                odd = true;
            }
            elem.classList.remove('hidden');
        }
        else if (nameNormalized.indexOf(key) != -1) {
            if (odd) {
                elem.classList.add('odd');
                odd = false;
            }
            else {
                elem.classList.add('even');
                odd = true;
            }
            elem.classList.remove('hidden');
        }
    });
}

function clientAdd(cliente) {
    let odd = document.querySelectorAll('.clients-table-row').length % 2 ? false : true;
    let row = document.createElement('div');
    let routeDays = ('0000000' + (cliente['rutaDias']).toString(2)).slice(-7).split('').reverse();
    row.className = 'clients-table-row' + (odd ? ' odd' : '');
    row.setAttribute('data-client-id', cliente['id']);
    row.setAttribute('data-client-folio', cliente['folio']);
    row.setAttribute('data-client-name', cliente['nombre']);
    row.setAttribute('data-client-lat', cliente['lat']);
    row.setAttribute('data-client-lng', cliente['lng']);
    row.setAttribute('data-client-route-days', cliente['rutaDias']);
    row.innerHTML = 
        '<div class="clients-table-column clients-table-selectable">'+
            '<div class="checkmark-container">' +
                '<div class="checkmark"></div>' +
            '</div>'+
        '</div>'+
        '<div class="clients-table-column">'+
            '<p>' + ('000' + cliente['folio']).slice(-4) + '</p>'+
        '</div>'+
        '<div class="clients-table-column">'+
            '<p>' + cliente['nombre'] + '</p>'+
        '</div>'+
        '<div class="clients-table-column">'+
            '<p>' + cliente['direccionCompleta'] + '</p>'+
        '</div>'+
        '<div class="clients-table-column">'+
            '<div class="routedays-container">'+
                '<input data-route-day="lunes" data-context="LUN" type="checkbox" prettycheckbox>'+
                '<input data-route-day="martes" data-context="MAR" type="checkbox" prettycheckbox>'+
                '<input data-route-day="miercoles" data-context="MIE" type="checkbox" prettycheckbox>'+
                '<input data-route-day="jueves" data-context="JUE" type="checkbox" prettycheckbox>'+
                '<input data-route-day="viernes" data-context="VIE" type="checkbox" prettycheckbox>'+
                '<input data-route-day="sabado" data-context="SAB" type="checkbox" prettycheckbox>'+
                '<input data-route-day="domingo" data-context="DOM" type="checkbox" prettycheckbox>'+
            '</div>'+
        '</div>';
    let routeDaysInputs = row.querySelectorAll('[data-route-day]');
    for (let i=0; i<routeDays.length; i++) {
        if (routeDays[i] == 1) {
            routeDaysInputs[i].checked = true;
        }
    }
    row.querySelectorAll('[prettycheckbox]').forEach(function(elem, index) {
        let contextData = elem.getAttribute('data-context');
        let routeDayInp = new PrettyCheckbox(elem, {
            contextMode: 'text',
            contextData: contextData
        });
        if (formMode != 'VIEW') {
            routeDayInp.onChange = clientSaveRouteDays;
        }
    });
    if (formMode != 'VIEW') {
        row.querySelector('.checkmark-container').addEventListener('click', function(ev) {
            let rowClosest = this.closest('.clients-table-row');
            rowClosest.classList.toggle('selected');
            updateClientSelecteds();
        });
    }
    document.querySelector('.clients-table').appendChild(row);
    RoutesMap.addMarker(cliente['folio'], cliente['nombre'], cliente['lat'], cliente['lng']);
}

function clientSaveRouteDays() {
    if (formMode == 'EDIT') {
        let routeDays = '';
        let row = this.container.closest('.clients-table-row');
        let clientID = row.getAttribute('data-client-id');
        row.querySelectorAll('[data-route-day]').forEach(function(elem, index) {
            routeDays += elem.checked ? 1 : 0;
        });
        routeDays = parseInt(routeDays.split('').reverse().join(''), 2);
        Tools.ajaxRequest({
            data: {
                rutaID: editID,
                clienteID: clientID,
                rutaDias: routeDays
            },
            method: 'POST',
            url: 'functions/ruta/change-client-route-days',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] != '') {
                    new Message('Ocurrio un error al tratar de cambiar los días de rutas del cliente.<br>' + data['error'], 'error');
                }
            }
        });
    }
}

function getClientsAdded() {
    let clients = []; 
    document.querySelectorAll('.clients-table-row').forEach(function(rowElem, rowIndex) {
        let routeDays = '';
        rowElem.querySelectorAll('[data-route-day]').forEach(function(routeDayElem, routeDayIndex) {
            routeDays += routeDayElem.checked ? 1 : 0;
        });
        routeDays = parseInt(routeDays.split('').reverse().join(''), 2);
        clients.push({
            id: rowElem.getAttribute('data-client-id'),
            rutaDias: routeDays
        });
    });
    return clients;
}

function updateClientSelecteds() {
    let rows = document.querySelectorAll('.clients-table-row.selected');
    if (rows.length > 0) {
        document.querySelector('.client-remove').classList.add('active');
    }
    else {
        document.querySelector('.client-remove').classList.remove('active');
    }
}

/* FILTER CLIENTS */
document.querySelector('.client-filter-route-days').addEventListener('change', function() {
    let dayFilter = parseInt(document.querySelector('.client-filter-route-days').value, 2);
    clientsFilter();
    document.querySelectorAll('.clients-table-row.selected').forEach(function(elem, index){
        elem.classList.remove('selected');
    });
    RoutesMap.clientsToMarkers();
    if (dayFilter != 0 && dayFilter != 127) {
        RoutesMap.route();
    }
    else {
        RoutesMap.clearRoute();
    }
});

function clientsFilter() {
    let dayFilter = document.querySelector('.client-filter-route-days').value;
    let odd = true;
    document.querySelectorAll('.clients-table-row').forEach(function(elem, index) {
        let routeDays = elem.getAttribute('data-client-route-days');
        elem.classList.add('hidden');
        elem.classList.remove('odd');
        if (routeDays & dayFilter) {
            if (odd) {
                elem.classList.add('odd');
                odd = false;
            }
            else {
                odd = true;
            }
            elem.classList.remove('hidden');
        }
        else if (dayFilter == -1) {
            if (odd) {
                elem.classList.add('odd');
                odd = false;
            }
            else {
                odd = true;
            }
            elem.classList.remove('hidden');
        }
    });
}

/* ORGANIZATE TABLE */
function organizateTable() {
    document.querySelectorAll('.clients-table-row').forEach(function(elem, index) {
        let odd = index % 2 ? false : true;
        elem.classList.remove('odd');
        if (odd) {
            elem.classList.add('odd');
        }
    });
}

/* MAP */

RoutesMap = new class {
    constructor() {
        this.clusterGroup = L.markerClusterGroup();
        this.markers = [];
        this.map = L.map(document.querySelector('.map-container > .map'), {
            fullscreenControl: true,
            center: [23.236614, -106.4162317],
            zoom: 12,
            doubleClickZoom: false,
            scrollWheelZoom: false
        });

        this.routePolyline = null;

        this.mapIcon = L.icon({
            iconUrl: '/admin/imgs/markers/shop-marker.png',
            iconAnchor: L.point(16, 41)
        });
        this.matrizIcon = L.icon({
            iconUrl: '/admin/imgs/markers/matriz-marker.png',
            iconAnchor: L.point(16, 41)
        });

        this.matrizMarker = null;

        this.addMatriz();

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
    }
    addMatriz() {
        let latLng = L.latLng(seccionLat, seccionLng);
        let popup = L.popup({
            offset: L.point(0, -28)
        })
            .setLatLng(latLng)
            .setContent('<p>Matriz</p>')
        this.matrizMarker = L.marker([seccionLat, seccionLng], {
            icon: this.matrizIcon,
            title: 'Matriz'
        }).addTo(this.map);
        this.matrizMarker.bindPopup(popup);
    }
    addMarker(folio, name, lat, lng) {
        let latLng = L.latLng(lat, lng);
        let popup = L.popup({
            offset: L.point(0, -28)
        }).setLatLng(latLng).setContent('<p>' + ('000' + folio).slice(-4) + ' - ' + name + '</p>');
        let marker = L.marker([lat, lng], {
            icon: this.mapIcon,
            title: name
        });
        marker.bindPopup(popup);
        this.markers.push(marker);
        this.clusterGroup.addLayer(marker).addTo(this.map);
    }
    clientsToMarkers() {
        let self = this;
        this.clusterGroup.clearLayers();
        this.markers = [];
        document.querySelectorAll('.clients-table-row').forEach(function(elem, index) {
            if (!elem.classList.contains('hidden')) {
                let lat = elem.getAttribute('data-client-lat');
                let lng = elem.getAttribute('data-client-lng');
                let latLng = L.latLng(lat, lng);
                let folio = elem.getAttribute('data-client-folio');
                let name = elem.getAttribute('data-client-name');
                let popup = L.popup({
                    offset: L.point(0, -28)
                }).setLatLng(latLng).setContent('<p>' + ('000' + folio).slice(-4) + ' - ' + name + '</p>');
                let marker = L.marker([lat, lng], {
                    icon: self.mapIcon,
                    title: name
                });
                marker.bindPopup(popup);
                self.markers.push(marker);
                self.clusterGroup.addLayer(marker);
            }
        });
        this.clusterGroup.addTo(this.map);
    }
    clearRoute() {
        if (this.routePolyline) {
            this.routePolyline.remove();
        }
    }
    route() {
        this.clearRoute();
        let self = this;
        let waypoints = [];
        let matrizLatlng = this.matrizMarker.getLatLng();
        waypoints.push(matrizLatlng['lng'] + ',' + matrizLatlng['lat']);
        for (let i=0; i<this.markers.length; i++) {
            let latlng = this.markers[i].getLatLng();
            waypoints.push(latlng['lng'] + ',' + latlng['lat']);
        }
        waypoints = waypoints.join(';');
        Tools.ajaxRequest({
            url: 'https://router.project-osrm.org/trip/v1/driving/' + waypoints + '?source=first&destination=last&geometries=geojson&overview=full',
            success: function(response) {
                let data = JSON.parse(response);
                let trip = data['trips'][0]['geometry'];
                self.routePolyline = L.geoJSON(trip).addTo(self.map);
            }
        });
    }
}();

})();