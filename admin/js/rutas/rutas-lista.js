(function(){

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/rutas/ver/" + dataID;
}

if (privilegiosNivel > 1) {
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        window.location = "/admin/rutas/editar/" + dataID;
    }
}
    
if (privilegiosNivel > 2) {
    Lista.actionDelete['active'] = true;
    Lista.actionDelete['unique'] = false;
    Lista.deleteFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se eliminara la ruta seleccionada.',
            msgWait: 'Eliminando ruta.',
            msgSuccess: 'Ruta eliminada con exito.',
            msgError: 'Ocurrio un error al tratar de elimninar la ruta.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se eliminaran las rutas seleccionadas.',
                msgWait: 'Eliminando rutas.',
                msgSuccess: 'Rutas eliminadas con exito.',
                msgError: 'Ocurrio un error al tratar de eliminar las rutas.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Confirm(requestObj.msgQuestion, 'eliminar', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/ruta/delete',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.remove();
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
}

SortController.setOnMethodChange(function() {

    Lista.load('ruta/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order
    }, function(data) {
        let nombre = data['usuarioNombre'] == null ? 'S/D' : data['usuarioNombre'];
        return ['<a href="/admin/rutas/ver/' + data['id'] + '">' + data['nombre'] + '</a>', nombre];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']]
        ];
        return attrs;
    });

});

SortController.setMethods([
    ['Nombre', 'nombre'],
    ['Vendedor', 'vendedor']
]);

SortController.load();

})();