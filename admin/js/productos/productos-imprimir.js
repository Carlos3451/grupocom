
var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:10, bottom:10, right:10};

var pdf;

let productosList;

let getData = getGetData();

function getGetData() {
    let data = {};
    let dataArr = window.location.search.slice(1).split('&');
    for (let i=0; i<dataArr.length; i++) {
        let tmp = dataArr[i].split('=');
        data[tmp[0]] = decodeURIComponent(tmp[1]);
    }
    return data;
}

Tools.ajaxRequest({
    data: {
        orden: 'asc',
        metodo: 'folio',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/producto/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            productosList = data['resultado'];
            try {
                createPDF();
            }
            catch (err) {
                console.log(err);
            }
        }
        else {
            console.log(data['error']);
        }
    }
});

function createPDF() {
    if (!pdf) {
        pdf = new jsPDF({
            format: 'letter'
        });
    }

    let productsMax = 65;
    let pageMax = Math.ceil(productosList.length / productsMax);

    for (let i=0; i<pageMax; i++) {
        createPage(pdf, i, pageMax, productsMax, productsMax * i);
    }
    
    savePDF('catalogo-productos');
}

function createPage(pdf, page, pageMax, productsMax, productsOffset) {

    let pageSize = {width:215.9, height:279.4};
    
    let lineY = 0;
    let lineHeight = 4;

    let productsTop = margin.top;
    let productsHeight = 4;
    let productsLeft = margin.left;
    let productsSizes = [15, 108, 25, 50];
    let productsTotalSize = productsSizes.reduce(function(total, num) {
        return total + num;
    });
    let productsNames = ['Folio', 'Nombre', 'Costo', 'Precio'];
    let productsKeys = ['folio', 'nombre', 'costo', 'precio'];

    if (getData['costo'] == 0) {
        productsNames = ['Folio', 'Nombre', 'Precio'];
        productsKeys = ['folio', 'nombre', 'precio'];
        productsSizes = [15, 133, 50];
    }

    /* PRODUCTOS */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = productsLeft;
    let columnTextSep = 2;
    //pdf.line(columnSep, productsTop - 4, columnSep, productsTop + 32);
    productsKeys.forEach(function(key, index) {
        //pdf.line(columnSep + productsSizes[index], productsTop - 4, columnSep + productsSizes[index], productsTop + 32);
        if (key == 'precio' || key == 'costo') {
            pdf.text(productsNames[index], columnSep + productsSizes[index] - columnTextSep, productsTop, { align: 'right' });
        }
        else {
            pdf.text(productsNames[index], columnSep + columnTextSep, productsTop);
        }
        columnSep += productsSizes[index];
    });

    pdf.setFontSize(9);
    pdf.setFontStyle('normal');
    
    lineY = productsHeight;
    for (let i=0; i<Math.min(productsMax, productosList.length - productsOffset); i++) {
        let producto = productosList[i + productsOffset];
        columnSep = productsLeft;
        if (i % 2 == 0) {
            pdf.setFillColor('#F1F1F1');
            pdf.rect(columnSep, productsTop + lineY - productsHeight + 1, productsTotalSize, productsHeight, 'F');
        }
        //productosNum++;
        productsKeys.forEach(function(key, index) {
            switch (key) {
                case 'folio':
                    pdf.text(('000' + producto['folio']).slice(-4), columnSep + columnTextSep, productsTop + lineY);
                    //pdf.text(String(i + productsOffset), columnSep + columnTextSep, productsTop + lineY);
                    break;
                case 'nombre':
                    pdf.text(producto['nombre'], columnSep + columnTextSep, productsTop + lineY);
                    break;
                case 'precio':
                    let prices = '$ ' + parseFloat(producto['precio1']).toFixed(2);
                    if (parseFloat(producto['precio2']) != 0) {
                        prices += ' | $ ' + parseFloat(producto['precio2']).toFixed(2);
                    }
                    if (parseFloat(producto['precio3']) != 0) {
                        prices += ' | $ ' + parseFloat(producto['precio3']).toFixed(2);
                    }
                    pdf.text(prices, columnSep + productsSizes[index] - columnTextSep, productsTop + lineY, { align: 'right' });
                    break;
                case 'costo':
                    pdf.text('$ ' + parseFloat(producto['costo']).toFixed(2), columnSep + productsSizes[index] - columnTextSep, productsTop + lineY, { align: 'right' });
                    break;
            }
            columnSep += productsSizes[index];
        });
        lineY += productsHeight;
    }

    pdf.setFontSize(8);

    if (pageMax != 1) {
        pdf.text('Página ' + (page + 1) + '/' + pageMax, pageSize.width - margin.right, pageSize.height - 4, { align: 'right' });
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }

}

function savePDF(name) {
    let file = new File([pdf.output('blob')], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI);
}