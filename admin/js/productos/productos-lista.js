(function(){

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/productos/ver/" + dataID;
}

if (privilegiosNivel > 1) {
    Lista.actionLock['active'] = true;
    Lista.actionLock['unique'] = false;
    Lista.lockFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se bloqueara el producto seleccionado.',
            msgWait: 'Bloqueando producto.',
            msgSuccess: 'Producto bloqueado con exito.',
            msgError: 'Ocurrio un error al tratar de bloquear el producto.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se bloquearan los productos seleccionados.',
                msgWait: 'Bloqueando productos.',
                msgSuccess: 'Productos bloqueados con exito.',
                msgError: 'Ocurrio un error al tratar de bloquear los productos.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Question(requestObj.msgQuestion, '¿Esta de acuerdo?', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/producto/lock',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.setAttribute('bloqueado', '');
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
    Lista.actionUnlock['active'] = true;
    Lista.actionUnlock['unique'] = false;
    Lista.unlockFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se desbloqueara el producto seleccionado.',
            msgWait: 'Desbloqueando producto.',
            msgSuccess: 'Producto desbloqueado con exito.',
            msgError: 'Ocurrio un error al tratar de desbloquear el producto.'
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se desbloquearan los productos seleccionados.',
                msgWait: 'Desbloqueando productos.',
                msgSuccess: 'Productos desbloqueados con exito.',
                msgError: 'Ocurrio un error al tratar de desbloquear los productos.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Question(requestObj.msgQuestion, '¿Esta de acuerdo?', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/producto/unlock',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.removeAttribute('bloqueado');
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        window.location = "/admin/productos/editar/" + dataID;
    }
    Lista.actionAddExistence['active'] = true;
    Lista.actionAddExistence['unique'] = true;
    Lista.addExistenceFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        let nombre = selecteds[0].getAttribute('data-nombre');
        let unit = selecteds[0].getAttribute('data-unidad');
        let unitObj = Tools.unitConverter(unit);
        let existencia = selecteds[0].getAttribute('data-existencia');
        new OpenQuestion('Agregar existencia a ' + nombre, '¿Que cantidad desea agregarle a este producto?', '', function(value) {
            if (!isNaN(value) && value != '' && value > 0) {
                Tools.ajaxRequest({
                    data: {
                        id: dataID,
                        cantidad: value
                    },
                    method: 'POST',
                    url: 'functions/producto/add-existence',
                    success: function(response) {
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            let nuevaExistencia = parseFloat(existencia) + parseFloat(value);
                            selecteds[0].setAttribute('data-existencia', nuevaExistencia);
                            selecteds[0].querySelector('.existence-container').textContent = nuevaExistencia + ' ' + (nuevaExistencia == 1 ? unitObj['singular'] : unitObj['plural']);
                            new Message('Añadido exitosamente ' + value + ' ' + (value == 1 ? unitObj['singular'] : unitObj['plural'])  + ' a ' + nombre + '.', 'success');
                            if (selecteds[0].hasAttribute('inventorylow') && nuevaExistencia > parseFloat(selecteds[0].getAttribute('data-minimo'))) {
                                selecteds[0].removeAttribute('inventorylow');
                            }
                        }
                        else {
                            new Message('Ocurrio un error al tratar de agregar existencia.<br>' + data['error'], 'error');
                        }
                    }
                });
            }
            else {
                new Message('El valor ingresado no es valido. Por favor intente de nuevo.', 'warning');
            }
        });
    }
}
    
if (privilegiosNivel > 2) {
    Lista.actionDelete['active'] = true;
    Lista.actionDelete['unique'] = false;
    Lista.deleteFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se eliminara el producto seleccionada.',
            msgWait: 'Eliminando producto.',
            msgSuccess: 'Producto eliminada con exito.',
            msgError: 'Ocurrio un error al tratar de eliminar el producto.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se eliminaran los productos seleccionadas.',
                msgWait: 'Eliminando productos.',
                msgSuccess: 'Productos eliminados con exito.',
                msgError: 'Ocurrio un error al tratar de eliminar los productos.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Confirm(requestObj.msgQuestion, 'eliminar', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/producto/delete',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.remove();
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
}

SortController.setOnMethodChange(function() {
    Lista.loadByParts('producto/get-all-list', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let existence;
        let unit = Tools.unitConverter(data['unidad']);
        let decimalPlaces = Tools.getDecimals(parseFloat(data['existencia']));
        if (unit['zeros'] && !decimalPlaces) {
            existence = parseFloat(data['existencia']).toFixed(0);
        }
        else {
            existence = parseFloat(data['existencia']).toFixed(unit['zeros'] || decimalPlaces);
        }

        let prices = [data['precio1'], data['precio2'], data['precio3']];
        let titles = ['Menudeo', 'Medio Mayoreo', 'Mayoreo'];

        let pricesElem = '';
        
        for (let i=0; i<prices.length; i++) {
            if (parseFloat(prices[i]) == 0) {
                break;
            }
            pricesElem += '<div class="price-container"><p>' + titles[i] + '</p><p class="price">$ ' + prices[i] + '</p></div>';
        }

        let providerElem = '<a href="/admin/proveedores/ver/' + data['proveedorID'] + '">' + data['proveedorNombre'] + '</a>';
        if (data['proveedorID'] == null) {
            providerElem = 'S/D';
        }

        return [
            '<a href="/admin/productos/ver/' + data.id + '">' + data.id + '</a>',
            data['nombre'],
            providerElem,
            data['categoriaNombre'],
            '<div class="prices-containers">' + pricesElem + '</div><div class="actions-container"><img class="edit-prices-btn" src="imgs/icons/actions/edit.png"></div>',
            '<p class="text-align-right">$ ' + data['costo'] + '</p>',
            '<p class="text-align-right existence-container">' + existence + ' ' + (existence==1 ? unit['singular'] : unit['plural']) + '</p>'
        ];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']],
            ['data-nombre', data['nombre']],
            ['data-unidad', data['unidad']],
            ['data-existencia', data['existencia']],
            ['data-minimo', data['inventarioMinimo']],

            ['data-precio1', data['precio1']],
            ['data-precio2', data['precio2']],
            ['data-precio3', data['precio3']],
        ];
        if (parseFloat(data['existencia']) <= parseFloat(data['inventarioMinimo'])) {
            attrs.push(['inventorylow', '']);
        }
        if (data['bloqueado'] == 1) {
            attrs.push(['bloqueado', '']);
        }
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Codigo', 'codigo'],
    ['Nombre', 'nombre'],
    ['Precio', 'precio'],
    ['Existencia', 'existencia']
]);

SortController.setFilterOptions([
    [
        ['Proveedor', 'select', 'proveedorID', [['Todos', -1], ['Sin Proveedor', 0]]]
    ]
]);

Tools.ajaxRequest({
    data: {
        metodo: 'nombre',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/proveedor/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let frag = document.createDocumentFragment();
            let proveedores = data['resultado'];
            for (let i=0; i<proveedores.length; i++) {
                let option = document.createElement('option');
                option.setAttribute('value', proveedores[i]['id']);
                option.textContent = proveedores[i]['nombre'];
                frag.appendChild(option);
            }
            document.querySelector('[name="proveedorID"]').appendChild(frag);
        }
    }
});

SortController.load();

document.querySelector('.print-products').addEventListener('click', function() {
    let q = new Question('Imprimir Productos', '¿Como desea imprimir la lista?', function(){
        window.open('productos/imprimir');
    }, function() {
        window.open('productos/imprimir?costo=0');
    });
    q.element.querySelector('.message-accept').textContent = 'Con Costos';
    q.element.querySelector('.message-cancel').textContent = 'Sin Costos';
});

document.querySelector('.list-container').addEventListener('click', function(ev) {
    if (ev.target.classList.contains('edit-prices-btn')) {
        let row = ev.target.closest('.list-row');
        let id = row.dataset.id;
        EditorPrecios.open({
            precio1: row.dataset.precio1,
            precio2: row.dataset.precio2,
            precio3: row.dataset.precio3
        });
        EditorPrecios.onAccept = function(pricesData, close) {
            pricesData['id'] = id;
            Tools.ajaxRequest({
                data: pricesData,
                url: 'functions/producto/change-prices',
                method: 'POST',
                waitMsg: new Wait('Actualizando precios...'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        let titles = [];
                        let prices = [];
                        for (let i=1; i<=3; i++) {
                            titles.push(pricesData['titulo' + i]);
                            prices.push(pricesData['precio' + i]);
                            row.dataset['titulo' + i] = pricesData['titulo' + i];
                            row.dataset['precio' + i] = pricesData['precio' + i];
                        }
                        let pricesElem = '';
                        for (let i=0; i<prices.length; i++) {
                            if (parseFloat(prices[i]) != 0) {
                                pricesElem += '<div class="price-container"><p>' + titles[i] + '</p><p class="price">$ ' + parseFloat(prices[i]).toFixed(2) + '</p></div>';
                            }
                        }
                        row.querySelector('.prices-containers').innerHTML = pricesElem;
                        new Message('Precios cambiados con exito.', 'success');
                        close();
                    }
                    else {
                        switch (data['error']) {
                            case 'NOT_CHANGES':
                                new Message('No se modificó ningun dato', 'warning');
                                close();
                                break;
                            default:
                                new Message('Ocurrio un error al tratar de cambiar los precios. Por favor intentelo de nuevo.<br>' + data['error'], 'error');
                                break;
                        }
                    }
                }
            });
        }
    }
});

})();