(function() {

let loadingDataMsg;

let proveedorSI = new SelectInp(document.querySelector('[name="proveedorID"]'), {
    placeholder: 'Sin definir',
    defaultValue: 0
});

document.querySelector('.action-clave-search').addEventListener('click', function() {
    ClavesSAT.onSelect = function(clave, name) {
        document.querySelector('[name="claveSAT"]').value = clave;
    }
    ClavesSAT.open(document.querySelector('[name="claveSAT"]').value);
});

clearForm();

if (formMode=='EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos.');
}

Promise.all([getProviders(), reloadCategories()]).then(function(values) {
    if (formMode == 'EDIT' || formMode == 'VIEW') {
        getProduct();
    }
});

function getProviders() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'folio',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/proveedor/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        options.push({
                            value: result[i]['id'],
                            name: ('000' + result[i]['folio']).slice(-4) + ' ' + result[i]['nombre'],
                            keys: [('000' + result[i]['folio']).slice(-4), result[i]['nombre']]
                        });
                    }
                    proveedorSI.setOptions(options);
                }
                resolve();
            }
        });
    });
    return promise;
}

function getProduct() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/producto/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadProduct(data['resultado']);
            }
        }
    });
}

function loadProduct(producto) {
    document.querySelector('[name="nombre"]').value = producto['nombre'];
    document.querySelector('[name="codigo"]').value = producto['codigo'];
    document.querySelector('[name="costo"]').value = producto['costo'];

    document.querySelector('[name="precio1"]').value = producto['precio1'];
    document.querySelector('[name="precio2"]').value = producto['precio2'];
    document.querySelector('[name="precio3"]').value = producto['precio3'];

    document.querySelector('[name="precio1"]').dispatchEvent(new Event('change'));
    document.querySelector('[name="precio2"]').dispatchEvent(new Event('change'));
    document.querySelector('[name="precio3"]').dispatchEvent(new Event('change'));

    document.querySelector('[name="unidad"]').value = producto['unidad'];
    document.querySelector('[name="categoriaID"]').value = producto['categoriaID'] || 0;
    document.querySelector('[name="inventarioMinimo"]').value = producto['inventarioMinimo'];

    for (let i=0; i<producto.productoUnidades.length; i++) {
        let productoUnidad = producto.productoUnidades[i];
        ProductosUnidades.add(productoUnidad);
    }

    proveedorSI.setValue(producto['proveedorID']);
    if (facturacion == 1) {
        document.querySelector('[name="claveSAT"]').value = producto['claveSAT'];
        document.querySelector('[name="impuesto"]').value = producto['impuesto'];
    }
    changeUnit();
    if (formMode == 'VIEW') {
        disableInputs();
    }
}

if (formMode != 'VIEW') {
    document.querySelector('#producto-form').addEventListener('submit', function(ev) {
        ev.preventDefault();
    });

    document.querySelectorAll('input, textarea, select').forEach(function(elem, index) {
        elem.addEventListener('keydown', function(ev) {
            let keyCode = ev.keyCode;
            let shift = ev.shiftKey;
            let localName = elem.localName;
            if (keyCode == 13) {
                let inputs = document.querySelectorAll('input, textarea, select');
                if (index != inputs.length - 1) {
                    if (localName != 'textarea' || shift) {
                        let inputNext = inputs[index + 1];
                        inputNext.focus();
                    }
                }
            }
        });
    });

    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/productos';
        });
    }
    else {        
        window.location = '/admin/productos';
    }
});

function submitForm() {    
    let formData = new FormData(document.querySelector('#producto-form'));
    document.querySelector('.form-submit').setAttribute('disabled', '');
    let productoUnidades = ProductosUnidades.getAll();
    formData.append('productoUnidades', JSON.stringify(productoUnidades));
    let processForm = {
        url: '',
        waitMessage: '',
        successMsg: '',
        errorMessage: '',
    }
    switch (formMode) {
        case 'ADD':
            processForm = {
                url: 'add',
                waitMessage: 'Añadiendo producto.',
                successMsg: 'Producto añadido con exito.',
                errorMessage: 'Ocurrio un error al tratar de añadir el producto.',
            }
            break;
        case 'EDIT':
            formData.append('id', editID);
            processForm = {
                url: 'edit',
                waitMessage: 'Guardando producto.',
                successMsg: 'Producto guardado con exito.',
                errorMessage: 'Ocurrio un error al tratar de guardar el producto.',
            }
            break;
    }
    Tools.ajaxRequest({
        data: formData,
        url: 'functions/producto/' + processForm['url'],
        method: 'POST',
        contentType: false,
        waitMsg: new Wait(processForm['waitMessage'], true),
        progress: function(progress) {
            this.waitMsg.setProgress(progress * 100);
        },
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                new Message(processForm['successMsg'], 'success', function() {
                    location = '/admin/productos';
                });
            }
            else {
                switch (data['error']) {
                    default:
                        new Message(processForm['errorMessage'] + '<br>"' + data['error'] + '" ', 'error');
                        break;
                }
            }
        }
    });
}

function clearForm() {
    document.querySelectorAll('#producto-form input').forEach(function(elem, index) {
        elem.value = elem.hasAttribute('defaultvalue') ? elem.getAttribute('defaultvalue') : '';
    });
    document.querySelectorAll('#producto-form select').forEach(function(elem, index) {
        elem.selectedIndex = 0;
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#producto-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}

document.querySelectorAll('[numeric]').forEach(function(elem, index){
    elem.addEventListener('keypress', function(ev) {
        let value = this.value;
        if (!Tools.checkNumeric(ev, value)) {
            ev.preventDefault();
        }
    });
});

if (formMode != 'VIEW') {
    document.querySelector('.category-container > .actions-container > .category-add').addEventListener('click', function() {  
        new OpenQuestion('Agregar Categoría', '¿Como desea llamar a la categoría?', '', function(answer) {
            answer = answer.trim();
            if (answer == '') {
                new Message('Por favor rellene el campo.', 'warning');
            }
            else {
                Tools.ajaxRequest({
                    data: {
                        nombre: answer
                    },
                    method: 'POST',
                    url: 'functions/categoria/add',
                    waitMsg: new Wait('Agregando categoría.'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Categoría agregada con exito.', 'success');
                            reloadCategories();
                        }
                        else {
                            new Message('Ocurrio un error al tratar de agregar la categoría: "' + data['error'] + '"');
                        }
                    }
                });
            }
        });
    });

    document.querySelector('.category-container > .actions-container > .category-edit').addEventListener('click', function() {
        let categoryId = document.querySelector('[name="categoriaID"]').value;
        let categoryName = document.querySelector('[name="categoriaID"] > option[value="' + categoryId + '"]').innerHTML;
        new OpenQuestion('Editar Categoría', '¿Como desea reenombrar a la categoría?', categoryName, function(answer) {
            answer = answer.trim();
            if (answer == '') {
                new Message('Por favor rellene el campo.', 'warning');
            }
            else {
                Tools.ajaxRequest({
                    data: {
                        id: categoryId,
                        nombre: answer
                    },
                    method: 'POST',
                    url: 'functions/categoria/edit',
                    waitMsg: new Wait('Editando categoría.'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Categoría editada con exito.', 'success');
                            reloadCategories();
                        }
                        else {
                            new Message('Ocurrio un error al tratar de editar la categoría: "' + data['error'] + '"');
                        }
                    }
                });
            }
        });
    });

    document.querySelector('.category-container > .actions-container > .category-delete').addEventListener('click', function() {
        let categoryId = document.querySelector('[name="categoriaID"]').value;
        let categoryName = document.querySelector('[name="categoriaID"] > option[value="' + categoryId + '"]').innerHTML;
        new Confirm('Eliminar la categoría "' + categoryName + '"', 'eliminar', function(answer) {
            Tools.ajaxRequest({
                data: {
                    id: categoryId
                },
                method: 'POST',
                url: 'functions/categoria/delete',
                waitMsg: new Wait('Eliminando categoría.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Categoría eliminada con exito.', 'success');
                        reloadCategories();
                    }
                    else {
                        new Message('Ocurrio un error al tratar de eliminar la categoría: "' + data['error'] + '"');
                    }
                }
            });
        });
    });
}

function reloadCategories() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            url: 'functions/categoria/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                document.querySelector('[name="categoriaID"]').innerHTML = '<option value="0">Ninguna</option>';
                if (data['error'] == '') {
                    let categorias = data['resultado'];
                    for (let i=0; i<categorias.length; i++) {
                        let option = document.createElement('option');
                        option.value = categorias[i]['id'];
                        option.innerHTML = categorias[i]['nombre'];
                        document.querySelector('[name="categoriaID"]').appendChild(option);
                    }
                }
                resolve();
            }
        });
    });
    return promise;
}

document.querySelector('[name="categoriaID"]').addEventListener('change', function() {
    let value = this.value;
    if (value != 0) {
        document.querySelectorAll('.category-container > .actions-container > .category-edit, .category-container > .actions-container > .category-delete').forEach(function(elem, index) {
            elem.classList.remove('disabled');
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'bounce 0.2s ease-in-out forwards';
        });
    }
    else {
        document.querySelectorAll('.category-container > .actions-container > .category-edit, .category-container > .actions-container > .category-delete').forEach(function(elem, index) {
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'disappear 0.2s ease-in-out forwards';
        });
    }
});

document.querySelectorAll('.category-container > .actions-container > .category-edit, .category-container > .actions-container > .category-delete').forEach(function(elem, index) {
    elem.addEventListener('animationend', function(ev) {
        if (ev.animationName == 'disappear') {
            this.classList.add('disabled');
        }
    });
});

/* CALCULOS DE GANANCIAS */

document.querySelector('[name="costo"]').addEventListener('input', function(ev) {
    document.querySelectorAll('[name="precio1"], [name="precio2"], [name="precio3"]').forEach(function(elem, index) {
        elem.dispatchEvent(new Event('change'));
    });
});

document.querySelector('[name="costo"]').addEventListener('blur', blurInputNumeric);

document.querySelectorAll('[name="margen1"], [name="margen2"], [name="margen3"]').forEach(function(elem, index) {
    elem.addEventListener('input', marginChange);
    elem.addEventListener('change', marginChange);
});

document.querySelectorAll('[name="precio1"], [name="precio2"], [name="precio3"]').forEach(function(elem, index) {
    elem.addEventListener('input', priceChange);
    elem.addEventListener('change', priceChange);
});

function marginChange(ev) {
    let value = this.value;
    if (value != '') {
        let priceInput = this.parentElement.parentElement.querySelector('[name="precio1"], [name="precio2"], [name="precio3"]');
        let cost = parseFloat(document.querySelector('[name="costo"]').value) || 0;
        let price = (cost + (value * 0.01 * cost)).toFixed(2);
        priceInput.value = price;
    }
}

function priceChange(ev) {
    let value = this.value;
    if (value != '') {
        let marginInput = this.parentElement.parentElement.querySelector('[name="margen1"], [name="margen2"], [name="margen3"]');
        let cost = parseFloat(document.querySelector('[name="costo"]').value) || 0;
        if (cost != '' || cost != 0) {
            let margin = (((value / cost) * 100) - 100).toFixed(2);
            marginInput.value = margin;
        }
        else {
            marginInput.value = 0;
        }
        if (ev.type == 'change') {
            this.value = parseFloat(value).toFixed(2);
        }
    }
}

document.querySelector('[name="unidad"]').addEventListener('change', changeUnit);
document.querySelector('[name="inventarioMinimo"]').addEventListener('blur', blurDecimal);

function blurInputNumeric() {
    if (this.value != '') {
        this.value = parseFloat(this.value).toFixed(2);
    }
}

function blurDecimal() {
    let decimals = parseFloat(this.getAttribute('decimals') || 2);
    if (this.value != '') {
        let places = Tools.getDecimals(parseFloat(this.value));
        if (decimals && !places) {
            this.value = parseFloat(this.value).toFixed(0);
        }
        else {
            this.value = parseFloat(this.value).toFixed(decimals || places);
        }
    }
    else {
        this.value = (0).toFixed(decimals);
    }
}

function changeUnit() {
    let unit = document.querySelector('[name="unidad"]').value;
    let unitObj = Tools.unitConverter(unit);
    document.querySelectorAll('[name="inventarioMinimo"]').forEach(function(elem, index) {
        elem.classList.remove('inp-gramo', 'inp-kilogramo', 'inp-libra', 'inp-paquete', 'inp-caja', 'inp-burbuja', 'inp-costal', 'inp-maso', 'inp-pieza', 'inp-mililitro', 'inp-litro', 'inp-unidad');
        elem.classList.add('inp-' + unit);
        elem.setAttribute('decimals', unitObj['zeros']);
        if (elem.value != '') {
            let decimalPlaces = Tools.getDecimals(parseFloat(elem.value));
            if (unitObj['zeros'] && !decimalPlaces) {
                elem.value = parseFloat(elem.value).toFixed(0);
            }
            else {
                elem.value = parseFloat(elem.value).toFixed(unitObj['zeros'] || decimalPlaces);
            }
        }
    });
}

function disableInputs() {
    document.querySelectorAll('input, select, textarea').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

// VENTA POR UNIDADES

const ProductosUnidades = new class {
    constructor() {
        this.container = document.querySelector('.productosunidades-container');
        this.addButton = this.container.querySelector('.add-productounidad-btn');
        this.addButton.addEventListener('click', function() {
            ProductosUnidades.add();
        });
        if (formMode == 'VIEW') {
            this.addButton.style.display = 'none';
        }
    }
    add(data = null) {
        let precioUnidadElem = document.createElement('div');
        precioUnidadElem.className = 'precio-unidad-container';
        precioUnidadElem.innerHTML = (`
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Unidad</p>
                    <select type="text" class="productounidad-unidad">
                        <option value="">Ninguno/Eliminar</option>
                        <option value="pieza">Pieza (pza)</option>
                        <option value="paquete">Paquete (paq)</option>
                        <option value="caja">Caja (caj)</option>
                        <option value="burbuja">Burbuja (burb)</option>
                        <option value="costal">Costal (cost)</option>
                        <option value="maso">Maso (maso)</option>
                        <option value="gramo">Gramo (g)</option>
                        <option value="kilogramo">Kilogramo (kg)</option>
                        <option value="libra">Libra (lb)</option>
                        <option value="mililitro">Mililitro (ml)</option>
                        <option value="litro">Litro (l)</option>
                        <option value="unidad">Unidad (ud)</option>
                    </select>
                </div>
                <div class="input-container">
                    <p class="contenido1-title">¿Cuantas piezas hay por ?</p>
                    <input type="text" class="inp-unit inp-pieza productounidad-contenido" numeric>
                </div>
                <div class="input-container">
                    <p class="contenidocosto-title">Costo por</p>
                    <input type="text" class="inp-icon inp-price productounidad-costo" transparent disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p class="contenidoprecio-title">Precio menudeo</p>
                    <input type="text" class="inp-icon inp-price productounidad-precio1">
                </div>
                <div class="input-container">
                    <p class="contenidoprecio-title">Precio medio mayoreo</p>
                    <input type="text" class="inp-icon inp-price productounidad-precio2">
                </div>
                <div class="input-container">
                    <p class="contenidoprecio-title">Precio mayoreo</p>
                    <input type="text" class="inp-icon inp-price productounidad-precio3">
                </div>
            </div>
        `);

        let unidadSelector = precioUnidadElem.querySelector('.productounidad-unidad');
        let contenidoInp = precioUnidadElem.querySelector('.productounidad-contenido');
        let costoInp = precioUnidadElem.querySelector('.productounidad-costo');

        contenidoInp.addEventListener('keydown', this._evNumberKeydown);
        contenidoInp.addEventListener('focus', this._evNumberFocus);
        contenidoInp.addEventListener('blur', this._evContenidoBlur);

        // costoInp.addEventListener('keydown', this._evNumberKeydown);
        // costoInp.addEventListener('focus', this._evNumberFocus);
        // costoInp.addEventListener('blur', this._evPrecioBlur);

        precioUnidadElem.querySelectorAll('.productounidad-precio1, .productounidad-precio2, .productounidad-precio3').forEach(function(input) {
            input.addEventListener('keydown', ProductosUnidades._evNumberKeydown);
            input.addEventListener('focus', ProductosUnidades._evNumberFocus);
            input.addEventListener('blur', ProductosUnidades._evPrecioBlur);
        });

        contenidoInp.addEventListener('input', ev => {
            let costo = parseFloat(document.querySelector('[name="costo"]').value) || 0;
            costoInp.value = parseFloat(contenidoInp.value) ? (costo * contenidoInp.value).toFixed(2) : '';
        });

        precioUnidadElem.querySelector('.productounidad-unidad').value = 'pieza';
        precioUnidadElem.querySelector('.productounidad-unidad').addEventListener('change', function(ev) {
            if (this.value == '') {
                this.value = this.dataset.backupvalue;
                new Question('Eliminar unidad de venta', 'Se eliminara esta unidad de venta, ¿esta de acuerdo?', function() {
                    precioUnidadElem.remove();
                });
            }
            let unidadMain = Tools.unitConverter(document.querySelector('[name="unidad"]').value);
            let unidadSecondary = Tools.unitConverter(unidadSelector.value);
            contenidoInp.parentElement.children[0].textContent = `¿Cuantos/as ${unidadMain.plural} hay por cada ${unidadSecondary.singular}?`;
            contenidoInp.parentElement.children[1].className = `inp-unit inp-${unidadMain} productounidad-contenido`;
            costoInp.parentElement.children[0].textContent = `Costo por ${unidadSecondary.singular}`;
            this.dataset.backupvalue = this.value;
        });

        unidadSelector.dispatchEvent(new Event('change'));

        this.container.insertBefore(precioUnidadElem, this.addButton);

        if (data) {
            precioUnidadElem.querySelector('.productounidad-unidad').value = data['unidad'];
            precioUnidadElem.querySelector('.productounidad-contenido').value = parseFloat(data['contenido']);
            precioUnidadElem.querySelector('.productounidad-costo').value = data['costo'];
            precioUnidadElem.querySelector('.productounidad-precio1').value = data['precio1'];
            precioUnidadElem.querySelector('.productounidad-precio2').value = data['precio2'];
            precioUnidadElem.querySelector('.productounidad-precio3').value = data['precio3'];
        }
    }
    getAll() {
        let precioUnidades = [];
        document.querySelectorAll('.precio-unidad-container').forEach(function(elem) {
            precioUnidades.push({
                unidad: elem.querySelector('.productounidad-unidad').value,
                contenido: elem.querySelector('.productounidad-contenido').value,
                costo: elem.querySelector('.productounidad-costo').value,
                precio1: elem.querySelector('.productounidad-precio1').value,
                precio2: elem.querySelector('.productounidad-precio2').value,
                precio3: elem.querySelector('.productounidad-precio3').value,
            });
        });
        return precioUnidades;
    }
    _evNumberKeydown(ev) {
        if (!Tools.checkNumeric(ev, this.value)) {
            ev.preventDefault();
        }
    }
    _evNumberFocus() {
        this.select();
    }
    _evContenidoBlur() {
        if (this.value != ''){
            this.value = parseFloat((parseFloat(this.value) || 0).toFixed(3));
        }
    }
    _evPrecioBlur() {
        if (this.value != ''){
            this.value = (parseFloat(this.value) || 0).toFixed(2);
        }
    }
}

})();