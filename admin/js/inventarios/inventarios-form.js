(function() {

let productosList = [];
let inventarioProductosInitial = [];

clearForm();

if (formMode == 'EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos...');
    loadInventario();
}

let productosLoadPromise = loadProductos();
let categoriasLoadPromise = loadCategorias();

function loadCategorias() {
    return new Promise(function(resolve) {
        let selector = document.querySelector('[name="categoriaID"]');
        selector.disabled = true;
        selector.innerHTML = '<option>Cargando...</option>';
        Tools.ajaxRequest({
            url: 'functions/categoria/get-all',
            success: function(response) {
                let result = JSON.parse(response);
                selector.disabled = false;
                selector.children[0].remove();
                if (result['error'] == '') {
                    let data = result['resultado'];
                    let optionsFragment = document.createDocumentFragment();

                    let optionTodos = document.createElement('option');
                    optionTodos.value = '-1';
                    optionTodos.textContent = 'Todos';
                    optionsFragment.appendChild(optionTodos);

                    for (let i=0; i<data.length; i++) {
                        let option = document.createElement('option');
                        option.value = data[i]['id'];
                        option.textContent = data[i]['nombre'];
                        optionsFragment.appendChild(option);
                    }

                    let optionNinguno = document.createElement('option');
                    optionNinguno.value = '0';
                    optionNinguno.textContent = 'Ninguna';
                    optionsFragment.appendChild(optionNinguno);

                    selector.appendChild(optionsFragment);
                }
                resolve();
            }
        });
    });
}

function loadInventario() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/inventario/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                setInventario(data['resultado']);
            }
        }
    });
}

function loadProductos() {
    return new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                metodo: 'folio',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/producto/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    productosList = data['resultado'];
                    for (let i=0; i<productosList.length; i++) {
                        addProduct(productosList[i]);
                    }
                    resolve();
                }
            }
        });
    });
}

function setInventario(inventario) {
    let inventoryProducts = inventario['inventario'];
    productosLoadPromise.then(function() {
        if (inventoryProducts['error'] == '') {
            let productos = inventoryProducts['resultado'];
            for (let i=0; i<productos.length; i++) {
                let producto = productos[i];
                let row = document.querySelector('.inventario-table-row[data-productoid="' + producto['productoID'] + '"]');
                if (row) {
                    row.dataset.id = producto['id'];
                    let existenciaNuevaInput = row.querySelector('[name="existencia-nueva"]');
                    existenciaNuevaInput.disabled = true;
                    existenciaNuevaInput.value = parseFloat(producto['existenciaNueva']);
                    row.querySelector('[name="existencia-anterior"]').value = parseFloat(producto['existenciaAnterior']);
                    setRowDifference(row);
                }
            }
        }
        if (formMode == 'VIEW') {
            disableInputs();
        }
    });
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        submitForm();
    });
    window.onbeforeunload = function() {
        let data = getInventarioProductos();
        if (JSON.stringify(data) != JSON.stringify(inventarioProductosInitial)) {
            return 'Se perderan los datos no guardados. ¿Seguro que deseas abandonar?'
        }
    }
}

function submitForm() {
    let inventarioProductos = getInventarioProductos();
    document.querySelector('.form-submit').setAttribute('disabled', '');
    submitObj = {};
    switch (formMode) {
        case 'ADD':
            submitObj = {
                url: 'functions/inventario/add',
                msgWait: 'Creando inventario.',
                msgSuccess: 'Inventario creado con exito.',
                msgError: 'Ocurrio un error al tratar de crear el inventario.',
            }
            break;
        case 'EDIT':
            submitObj = {
                url: 'functions/inventario/edit',
                msgWait: 'Guardando inventario.',
                msgSuccess: 'Inventario guardado con exito.',
                msgError: 'Ocurrio un error al tratar de guardado el inventario.',
            }
            break;
    }
    Tools.ajaxRequest({
        data: {
            id: editID,
            inventarioProductos: inventarioProductos
        },
        url: submitObj['url'],
        method: 'POST',
        waitMsg: new Wait(submitObj['msgWait']),
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                inventarioProductosInitial = inventarioProductos;
                new Message(submitObj['msgSuccess'], 'success', function() {
                    location = '/admin/inventarios';
                });
            }
            else {
                new Message(submitObj['msgError'] + '<br>' + data['error'], 'error');
            }
        }
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/inventarios';
        });
    }
    else {
        window.location = '/admin/inventarios';
    }
});

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
}

document.querySelector('[name="categoriaID"]').addEventListener('change', updateSearchProductos);
document.querySelector('[name="busqueda"]').addEventListener('input', updateSearchProductos);

function updateSearchProductos() {
    let searchKey = document.querySelector('[name="busqueda"]').value.toLowerCase();
    let categoriaID = document.querySelector('[name="categoriaID"]').value;
    document.querySelectorAll('.inventario-table-row').forEach(function(row) {
        let productoNombre = row.dataset.nombre.toLowerCase();
        row.style.display = 'none';
        if ((categoriaID == -1 || row.dataset.categoriaid == categoriaID) && (searchKey == '' || productoNombre.indexOf(searchKey) != -1)) {
            row.style.display = '';
        }
    });
}

/* AÑADIR PRODUCTO */
function addProduct(producto) {
    let row = document.createElement('div');
    row.className = 'inventario-table-row';
    row.dataset.productoid = producto['id'];
    row.dataset.id = 0;
    row.dataset.categoriaid = producto['categoriaID'] || 0;
    row.dataset.nombre = producto['nombre'];
    row.innerHTML = (
        '<div class="inventario-table-column">'+
            '<input type="text" name="nombre" value="' + producto['nombre'] + '" disabled>'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + producto['unidad'] + ' text-align-right" type="text" name="existencia-nueva" autocomplete="off">'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + producto['unidad'] + ' text-align-right" type="text" name="existencia-anterior" value="' + parseFloat(producto['existencia']) + '" disabled>'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + producto['unidad'] + ' text-align-right" type="text" name="existencia-diferencia" value="' + parseFloat(producto['existencia']) + '" disabled>'+
        '</div>'
    );
    row.querySelector('[name="existencia-anterior"]').value = parseFloat(parseFloat(producto['existencia']).toFixed(3));

    row.querySelector('[name="existencia-nueva"]').addEventListener('keydown', keydownProductoRow);
    row.querySelector('[name="existencia-nueva"]').addEventListener('input', updateRow);
    row.querySelector('[name="existencia-nueva"]').addEventListener('focus', selectInput);
    row.querySelector('[name="existencia-nueva"]').addEventListener('blur', blurDecimal);
    row.querySelector('[name="existencia-nueva"]').addEventListener('blur', updateRow);

    ToolTip.update();    
    document.querySelector('.inventario-table').appendChild(row);
}

function keydownProductoRow(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
    let row = this.parentElement.parentElement;
    let iRow = row;
    switch (ev.key) {
        case 'Enter': case 'ArrowDown':
            while (iRow.nextElementSibling) {
                iRow = iRow.nextElementSibling;
                let column = iRow.children[1];
                if (!column) {
                    return;
                }
                let input = column.children[0];
                if (!input) {
                    return;
                }
                if (iRow.style.display != 'none') {
                    input.focus();
                    return;
                }
            }
            break;
        case 'ArrowUp':
            while (iRow.previousElementSibling) {
                iRow = iRow.previousElementSibling;
                let column = iRow.children[1];
                if (!column) {
                    return;
                }
                let input = column.children[0];
                if (!input) {
                    return;
                }
                if (iRow.style.display != 'none') {
                    input.focus();
                    return;
                }
            }
            break;
    }
}

function selectInput() {
    this.select();
}

function blurDecimal() {
    if (this.value != '') {
        this.value = parseFloat(parseFloat(this.value).toFixed(3));
    }
}

function updateRow(row) {
    row = this.closest('.inventario-table-row');
    setRowDifference(row);
}

function setRowDifference(row) {
    let actual = parseFloat(row.querySelector('[name="existencia-anterior"]').value) || 0;
    let nueva = parseFloat(row.querySelector('[name="existencia-nueva"]').value) || 0;
    let diferencia = nueva - actual;
    let diferenciaElem = row.querySelector('[name="existencia-diferencia"]');
    diferenciaElem.value = (diferencia > 0 ? '+' : '') + parseFloat(diferencia.toFixed(3));
    if (diferencia > 0) {
        diferenciaElem.removeAttribute('quantity-negative');
        diferenciaElem.setAttribute('quantity-positive', '');
    }
    else if (diferencia < 0) {
        diferenciaElem.removeAttribute('quantity-positive');
        diferenciaElem.setAttribute('quantity-negative', '');
    }
    else {
        diferenciaElem.removeAttribute('quantity-negative');            
        diferenciaElem.removeAttribute('quantity-positive');
    }
}

/* GET ALL DATA */

function getInventarioProductos() {
    let productsData = [];
    document.querySelectorAll('.inventario-table-row').forEach(function(row) {
        let existenciaNueva = row.querySelector('[name="existencia-nueva"]').value;
        if (row.dataset.id == 0 && existenciaNueva != '') {
            productsData.push({
                id: row.dataset.id, 
                productoID: row.dataset.productoid,
                existenciaAnterior: row.querySelector('[name="existencia-anterior"]').value,
                existenciaNueva: existenciaNueva
            });
        }
    });
    return productsData;
}

function disableInputs() {
    document.querySelectorAll('input').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

/* NUMERIC */

function keypressIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

document.querySelectorAll('[numeric]').forEach(function(elem, index) {
    elem.addEventListener('keypress', keypressIsNumber);
});
    
})();