var Lista = new class {

    constructor() {

        this.selectJustOne = false;

        this.partObj = {
            url: '',
            data: {},
            columnsFn: null,
            attrFn: null,
            offset: 0
        };

        this.customActions = [];

        this.loadTimeouts = [];

        this.xhr = null;

        this.viewFn = function(ev) {
            console.log('VIEW_FN');
        }
        this.deliveryFn = function(ev) {
            console.log('DELIVERY_FN');
        }
        this.editFn = function(ev) {
            console.log('EDIT_FN');
        }
        this.printFn = function(ev) {
            console.log('PRINT_FN');
        }
        this.payFn = function(ev) {
            console.log('PAY_FN');
        }
        this.deleteFn = function(ev) {
            console.log('DELETE_FN');
        }
        this.cancelFn = function(ev) {
            console.log('CANCEL_FN');
        }
        this.changePasswordFn = function(ev) {
            console.log('CHANGE_PASSWORD_FN');
        }
        this.addExistenceFn = function(ev) {
            console.log('ADD_EXISTENCE_FN');
        }
        this.lockFn = function(ev) {
            console.log('ADD_EXISTENCE_FN');
        }
        this.unlockFn = function(ev) {
            console.log('ADD_EXISTENCE_FN');
        }
        this.pedidosFn = function(ev) {
            console.log('GOTO_PEDIDOS');
        }
        this.facturarFn = function(ev) {
            console.log('FACTURAR_FN');
        }
        this.downloadFn = function(ev) {
            console.log('DOWNLOAD_FN');
        }
        this.emailFn = function(ev) {
            console.log('EMAIL_FN');
        }

        this.actionView = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-view')
        }
        this.actionDelivery = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-delivery')
        }
        this.actionEdit = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-edit')
        }
        this.actionPay = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-pay')
        }
        this.actionPrint = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-print')
        }
        this.actionDelete = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-delete')
        }
        this.actionCancel = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-cancel')
        }
        this.actionChangePassword = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-change-password')
        }
        this.actionAddExistence = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-add-existence')
        }
        this.actionLock = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-lock')
        }
        this.actionUnlock = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-unlock')
        }
        this.actionPedidos = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-pedidos')
        }
        this.actionFacturar = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-facturar')
        }
        this.actionDownload = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-download')
        }
        this.actionEmail = {
            active: false,
            unique: false,
            element: document.querySelector('.list-actions-container > .actions-container > .action-email')
        }

        document.querySelector('.list-actions-container > .selected-counter > .actions-container > .selects-remove').addEventListener('click', this.removeSelections);

        this.activateActions();
    }

    addAction(tooltip, icon, isUnique, callback) {
        let action = document.createElement('img');
        action.setAttribute('tooltip-hint', tooltip);
        if (isUnique) {
            action.setAttribute('unique', 'true');
        }
        action.src = icon;
        action.addEventListener('click', function() {
            let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
            callback(selecteds);
        });
        document.querySelector('.list-actions-container > .actions-container').appendChild(action);
        ToolTip.update();
        this.customActions.push(action);
    }

    removeSelections() {
        let rows = document.querySelectorAll('.list-container > .list-row');
        rows.forEach(function(elem, index) {
            elem.classList.remove('selected');
        });
        Lista.updateSelections();
    }
    
    load(path, data, columnsFn, attrFn, callback = null) {
        Lista.clear();
        Lista.createLoading();
        if (this.xhr != null) {
            this.xhr.abort();
        }
        this.xhr = Tools.ajaxRequest({
            data: data,
            method: 'POST',
            url: 'functions/' + path,
            success: function(response) {
                Lista.deleteLoading();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    for (let l=0; l<Math.ceil(result.length / 100); l++) {
                        Lista.loadTimeouts.push(setTimeout(function(){
                            let frag = document.createDocumentFragment();
                            for (let i=l*100; i<Math.min((l * 100) + 100, result.length); i++) {
                                let columns = columnsFn(result[i]);
                                let attrs = attrFn(result[i]);
                                Lista.createRow(frag, columns, attrs);
                            }
                            document.querySelector('.list-container').appendChild(frag);
                            ToolTip.update();
                        }, l * 100));
                    }
                }
                if (callback) {
                    callback(response);
                } 
            }
        });
    }

    loadByParts(path, data, columnsFn, attrFn, offset = 0) {
        if (offset == 0) {            
            document.querySelector('.list-overflow').removeEventListener('scroll', Lista._evScroll);
            Lista.clear();
            this.partObj = {
                path: path,
                data: data,
                columnsFn: columnsFn,
                attrFn: attrFn,
                offset: 0
            };
        }
        Lista.createLoading();
        if (this.xhr != null) {
            this.xhr.abort();
        }
        data['offset'] = offset;
        this.xhr = Tools.ajaxRequest({
            data: data,
            method: 'POST',
            url: 'functions/' + path,
            success: function(response) {
                Lista.deleteLoading();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    let frag = document.createDocumentFragment();
                    for (let i=0; i<result.length; i++) {
                        let columns = columnsFn(result[i]);
                        let attrs = attrFn(result[i]);
                        Lista.createRow(frag, columns, attrs);
                    }
                    document.querySelector('.list-container').appendChild(frag);
                    document.querySelector('.list-overflow').addEventListener('scroll', Lista._evScroll);
                    ToolTip.update();
                }
            }
        });
    }

    _evScroll(offset) {
        let scrollBottom = this.scrollTop + this.offsetHeight;
        let height = document.querySelector('.list-container').offsetHeight;
        if (scrollBottom >= height) {
            this.removeEventListener('scroll', Lista._evScroll);
            Lista.partObj['offset'] += 100;
            Lista.loadByParts(Lista.partObj['path'], Lista.partObj['data'], Lista.partObj['columnsFn'], Lista.partObj['attrFn'], Lista.partObj['offset']);
        }
    }

    clear() {
        for (let i=0; i<this.loadTimeouts.length; i++) {
            clearTimeout(this.loadTimeouts[i]);
        }
        this.loadTimeouts = [];
        let lists = document.querySelectorAll('.list-container > .list-row');
        lists.forEach(function(elem, index) {
            elem.remove();
        });
    }
    
    createRow(fragment, columns, attrs) {
        let rowElem = document.createElement('div');
        rowElem.className = 'list-row';

        for (let i=0; i<attrs.length; i++) {
            rowElem.setAttribute(attrs[i][0], attrs[i][1]);
        }

        let columnSelectElem = document.createElement('div');
        columnSelectElem.className = 'list-column list-selectable';
        columnSelectElem.innerHTML =
            '<div class="checkmark-container">' +
                '<div class="checkmark"></div>' +
            '</div>';
        rowElem.appendChild(columnSelectElem);

        columnSelectElem.querySelector('.checkmark-container').addEventListener('click', function(ev) {
            if (Lista.selectJustOne) {
                Lista.removeSelections();
            }
            let row = this.closest('.list-row');
            row.classList.toggle('selected');
            Lista.updateSelections();
        });

        for (let i=0; i<columns.length; i++) {
            let columnElem = document.createElement('div');
            columnElem.className = 'list-column';
            columnElem.innerHTML = columns[i];
            rowElem.appendChild(columnElem);
        }
        fragment.appendChild(rowElem);
        return rowElem;
    }
    
    createLoading() {
        let elem = document.createElement('div');
        elem.className = 'list-row loading-list';
        elem.innerHTML =
            '<div class="list-column">'+
                '<div class="loader"></div>'+
                '<p>Cargando...</p>'+
            '</div>';
        document.querySelector('.list-container').appendChild(elem);
    }
    
    deleteLoading() {
        document.querySelector('.list-row.loading-list').remove();
    }

    updateSelections() {
        let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
        let counterElem = document.querySelector('.list-actions-container > .selected-counter > p');
        let actions = [this.actionView, this.actionDelivery, this.actionEdit, this.actionPrint, this.actionChangePassword, this.actionPay, this.actionDelete, this.actionCancel, this.actionAddExistence, this.actionLock, this.actionUnlock, this.actionPedidos, this.actionFacturar, this.actionDownload, this.actionEmail];
        counterElem.textContent = selecteds.length + ' ' + (selecteds.length==1 ? 'seleccionado' : 'seleccionados');
        counterElem.style.animation = 'none';
        counterElem.offsetHeight;
        counterElem.style.animation = 'bounceSmall 0.2s ease-in-out';
        if (selecteds.length>0) {
            document.querySelector('.list-actions-container .selects-remove').classList.add('active');
        }
        else {
            document.querySelector('.list-actions-container .selects-remove').classList.remove('active');
        }
        for (let i=0; i<actions.length; i++) {
            if (actions[i]['active'] && ((actions[i]['unique'] && selecteds.length == 1) || (!actions[i]['unique'] && selecteds.length > 0))) {
                actions[i]['element'].classList.add('active');
                actions[i]['element'].style.animation = 'none';
                actions[i]['element'].offsetHeight;
                actions[i]['element'].style.animation = 'bounceSmall 0.2s ease-in-out';
            }
            else {
                actions[i]['element'].classList.remove('active');
            }
        }
        for (let i=0; i<this.customActions.length; i++) {
            let action = this.customActions[i];
            if ((action.hasAttribute('unique') && selecteds.length == 1) || (!action.hasAttribute('unique') && selecteds.length > 0)) {
                action.classList.add('active');
                action.style.animation = 'none';
                action.offsetHeight;
                action.style.animation = 'bounceSmall 0.2s ease-in-out';
            }
            else {
                action.classList.remove('active');
            }
        }
    }
    
    activateActions() {
        document.querySelectorAll('.list-actions-container > .actions-container > .action-view').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.viewFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-delivery').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.deliveryFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-delete').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.deleteFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-edit').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.editFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-pay').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.payFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-print').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.printFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-change-password').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.changePasswordFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-add-existence').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.addExistenceFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-lock').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.lockFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-unlock').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.unlockFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-cancel').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.cancelFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-pedidos').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.pedidosFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-facturar').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.facturarFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-download').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.downloadFn(selecteds)
            });
        });
        document.querySelectorAll('.list-actions-container > .actions-container > .action-email').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                let selecteds = document.querySelectorAll('.list-container > .list-row.selected');
                Lista.emailFn(selecteds)
            });
        });
    }
}