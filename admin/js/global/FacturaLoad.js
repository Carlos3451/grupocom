
var FacturaLoad = new class {
    constructor() {
        let self = this;
        this.overlay = document.querySelector('.facturaload-overlay');
        this.container = this.overlay.querySelector('.facturaload-container');
        new DatePicker(this.container.querySelector('[name="facturaload-fecha"]'), {
            rangeActived: true,
            yearSelect: true,
            monthSelect: true
        })
        this.container.querySelector('.facturaload-load').addEventListener('click', function() {
            let type = self.container.querySelector('[name="facturaload-tipo"]').value;
            let date = self.container.querySelector('[name="facturaload-fecha"]').value.replace(/ /g, '+');
            window.location = 'facturas/agregar?tipo=' + type + '&fecha=' + encodeURIComponent(date);
        });
        this.container.querySelector('.facturaload-close').addEventListener('click', function() {
            self.close();
        });
    }
    open() {
        this.overlay.style.display = 'flex';
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'appear 0.2s ease-in-out';
    }
    close() {
        let self = this;
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'disappear 0.2s ease-in-out';
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
}