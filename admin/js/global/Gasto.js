var Gasto = new class {
    constructor() {
        let self = this;

        this.onAccept = null;

        this.overlayElem = document.querySelector('.gasto-overlay');
        this.containerElem = this.overlayElem.querySelector('.gasto-container');
        this.fechaInp = this.containerElem.querySelector('[name="gasto-fecha"]');
        this.cantidadInp = this.containerElem.querySelector('[name="gasto-cantidad"]');
        this.razonInp = this.containerElem.querySelector('[name="gasto-razon"]');
        this.comentarioInp = this.containerElem.querySelector('[name="gasto-comentario"]');
        
        this.fechaDP = new DatePicker(this.fechaInp);

        this.cantidadInp.addEventListener('keydown', function(ev) {
            if (!Tools.checkNumeric(ev, this.value)) {
                ev.preventDefault();
            }
        });
        this.cantidadInp.addEventListener('keypress', function(ev) {
            if (!Tools.checkNumeric(ev, this.value)) {
                ev.preventDefault();
            }
        });
        this.cantidadInp.addEventListener('focus', function() {
            this.select();
        });
        this.cantidadInp.addEventListener('blur', function() {
            if (this.value != '') {
                this.value = (parseFloat(this.value) || 0).toFixed(2);
            }
        });

        this.overlayElem.addEventListener('animationend', function(ev) {
            if (ev.animationName == 'disappear') {
                this.style.display = 'none';
            }
        });

        this.containerElem.querySelector('.accept-gasto').addEventListener('click', function() {
            this.setAttribute('disabled', '');
            let data = self.getValues();
            if (data['razon'] == '') {
                new Message('El campo razón no puede estar vacío.', 'warning', function() {
                    self.containerElem.querySelector('.accept-gasto').removeAttribute('disabled');
                });
                return;
            }
            if (self.onAccept) {
                self.onAccept(data);
            }
            self.close();
        });

        this.containerElem.querySelector('.cancel-gasto').addEventListener('click', function() {
            self.close();
        });

    }

    clear() {
        this.fechaDP.changeDate(new Date());
        this.cantidadInp.value = '';
        this.razonInp.value = '';
        this.comentarioInp.value = '';
    }

    open(data = null) {
        this.clear();
        if (data) {
            for (let key in data) {
                let input = this.containerElem.querySelector('[name="gasto-' + key + '"]');
                if (input) {
                    input.value = data[key];
                    input.dispatchEvent(new Event('input'));
                }
            }
        }
        this.overlayElem.style.display = 'flex';
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'appear 0.2s ease-in-out';
    }

    close() {
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'disappear 0.2s ease-in-out';
    }

    getValues() {
        let data = {
            fecha: this.fechaInp.value,
            cantidad: this.cantidadInp.value,
            razon: this.razonInp.value,
            comentario: this.comentarioInp.value
        }
        return data;
    }

}