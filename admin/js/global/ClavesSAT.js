
var ClavesSAT = new class {
    constructor() {
        let self = this;
        this.clavesList = null;
        this.onSelect = null;
        this.overlay = document.querySelector('.claves-sat-overlay');
        this.container = this.overlay.querySelector('.claves-sat-container');
        this.clavesContainer = this.container.querySelector('.claves-containers');
        this.searcher = this.container.querySelector('[name="buscadorClavesSAT"]');
        this.type = this.container.querySelector('[name="tipoClaveSAT"]');
        this.searcher.addEventListener('input', function() {
            self.search(this.value);
        });
        this.type.addEventListener('change', function() {
            self.search(self.searcher.value);
        });
        this.container.querySelector('button').addEventListener('click', function() {
            self.close();
        });
        this.loadClavesSAT();
    }
    close() {
        let self = this;
        this.onSelect = null;
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'disappear 0.2s ease-in-out';
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
    open(key = '') {
        Tools.removeChilds(this.clavesContainer);
        this.overlay.style.display = 'flex';
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'appear 0.2s ease-in-out';
        this.searcher.value = '';
        this.type.value = 'prod';
        if (key != '') {
            this.searcher.value = key;
            this.search(key);
        }
    }
    search(key) {
        let self = this;
        let type = this.type.value;
        Tools.removeChilds(this.clavesContainer);
        key = key.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
		if (key!='') {
			var match = this.clavesList[type].filter(function(concept) {
				return concept['searchKey'].match(new RegExp(key, 'gi')) || concept['key'].match(new RegExp(key, 'gi'));
            });
            let frag = document.createDocumentFragment();
			for (var i=0; i<Math.min(match.length, 100); i++) {
                let elem = document.createElement('div');
                elem.className = 'clave-container';
                elem.setAttribute('data-key', match[i]['key']);
                elem.setAttribute('data-name', match[i]['name']);
                elem.innerHTML = '<p class="clave">[' + match[i]['key'] + ']</p><p class="name">' + match[i]['name'] + '</p>';
                elem.addEventListener('click', function() {
                    if (self.onSelect) {
                        let clave = this.dataset.key;
                        let name = this.dataset.name;
                        self.onSelect(clave, name);
                    }
                    self.close();
                });
                frag.appendChild(elem);
			}
            this.clavesContainer.append(frag);
		}
    }
    loadClavesSAT() {
        let self = this;
        Tools.ajaxRequest({
            method: 'POST',
            url: 'functions/factura/claves-sat-get-all',
            success: function(response) {
                let data = JSON.parse(response);
                self.clavesList = {
                    'prod' : [],
                    'serv' : []
                };
                for (let division in data['prod']) {
                    for (let grupo in data['prod'][division]) {
                        for (let clase in data['prod'][division][grupo]) {
                            for (let clave in data['prod'][division][grupo][clase]) {
                                self.clavesList['prod'].push(data['prod'][division][grupo][clase][clave]);
                            }
                        }
                    }
                }
                for (let division in data['serv']) {
                    for (let grupo in data['serv'][division]) {
                        for (let clase in data['serv'][division][grupo]) {
                            for (let clave in data['serv'][division][grupo][clase]) {
                                self.clavesList['serv'].push(data['serv'][division][grupo][clase][clave]);
                            }
                        }
                    }
                }
                for (let i=0; i<self.clavesList['prod'].length; i++) {
                    self.clavesList['prod'][i]['searchKey'] = self.clavesList['prod'][i]['name'].normalize('NFD').replace(/[\u0300-\u036f]/g, "");
                }
                for (let i=0; i<self.clavesList['serv'].length; i++) {
                    self.clavesList['serv'][i]['searchKey'] = self.clavesList['serv'][i]['name'].normalize('NFD').replace(/[\u0300-\u036f]/g, "");
                }
            }
        });
    }
}