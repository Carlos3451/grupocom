const EditorPrecios = new class {
    constructor() {
        let self = this;

        this.mode = 'productos';
        this.onAccept = null;

        this.overlay = document.querySelector('.editorprecios-overlay');
        this.container = this.overlay.querySelector('.editorprecios-container');

        this.container.querySelectorAll('[number]').forEach(function(input) {
            input.addEventListener('keydown', function(ev) {
                if (!Tools.checkNumeric(ev, this.value)) {
                    ev.preventDefault();
                }
            });
            input.addEventListener('focus', function(ev) {
                this.select();
            });
            input.addEventListener('blur', function(ev) {
                if (this.value != '') {
                    this.value = (parseFloat(this.value) || 0).toFixed(2);
                }
            });
        });

        this.container.querySelector('.accept-btn').addEventListener('click', function() {
            if (self.onAccept) {
                let data = {};
                if (self.container.querySelector('[name="categoriaID"]').parentElement.parentElement.style.display != 'none') {
                    data['categoriaID'] = self.container.querySelector('[name="categoriaID"]').value;
                }
                self.container.querySelectorAll('input').forEach(function(input) {
                    data[input.name] = input.value;
                });
                self.onAccept(data, function() { self.close(); });
            }
        });

        this.container.querySelector('[name="categoriaID"]').setAttribute('disabled', '');
        this.container.querySelector('.close-btn').addEventListener('click', function() {
            self.close();
        });
    }

    clear() {
        this.container.querySelector('[name="categoriaID"]').parentElement.parentElement.style.display = 'none';
        this.container.querySelectorAll('input').forEach(function(input) {
            input.value = '';
        }); 
    }

    open(precioNormales, categoriesActive = false) {
        console.log(precioNormales);
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
        this.clear();

        if (categoriesActive) {
            this.container.querySelector('[name="categoriaID"]').parentElement.parentElement.style.display = 'block';
        }
        for (let key in precioNormales) {
            this.container.querySelector('input[name="' + key + '"]').value = precioNormales[key];
        }
    }

    close() {
        let self = this;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }

    setCategorias(data) {
        let selector = this.container.querySelector('[name="categoriaID"]');
        selector.innerHTML = '';
        selector.removeAttribute('disabled');
        let optionsFragment = document.createDocumentFragment();
        for (let i=0; i<data.length; i++) {
            let option = document.createElement('option');
            option.textContent = data[i]['nombre'];
            option.value = data[i]['id'];
            optionsFragment.appendChild(option);
        }
        selector.appendChild(optionsFragment);
    }

    _inputKeydown(ev) {
        let value = this.value;
        if (!Tools.checkNumeric(ev, value)) {
            ev.preventDefault();
        }
    }

    _inputBlur(ev) {
        if (this.value != '') {
            this.value = (parseFloat(this.value) || 0).toFixed(2);
        }
    }
}