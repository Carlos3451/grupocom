
const GastoProgramado = new class {
    constructor() {
        let self = this;
        this.editID = 0;

        this.overlay = document.querySelector('.gastoprogramado-overlay');
        this.container = this.overlay.querySelector('.gastoprogramado-container');

        this.container.querySelector('.gastoprogramado-cancel').addEventListener('click', function() {
            self.close();
        });

        this.container.querySelector('[name="programacion"]').addEventListener('change', function(ev) {
            if (this.value == 'semanal') {
                self.container.querySelector('[name="dia"]').removeAttribute('disabled');
                self.container.querySelector('[name="dia"]').style.display = '';
            }
            else {
                self.container.querySelector('[name="dia"]').setAttribute('disabled', '');
                self.container.querySelector('[name="dia"]').style.display = 'none';
            }
        });

        this.container.querySelector('[name="cantidad"]').addEventListener('keydown', function(ev) {
            if (!Tools.checkNumeric(ev, this.value)) {
                ev.preventDefault();
            }
        });
        this.container.querySelector('[name="cantidad"]').addEventListener('blur', function(ev) {
            this.value = (parseFloat(this.value) || 0).toFixed(2);
        });
        this.container.querySelector('[name="cantidad"]').addEventListener('focus', function(ev) {
            this.select();
        });

        this.container.querySelector('.gastoprogramado-accept').addEventListener('click', function() {
            let programacionValue = self.container.querySelector('[name="programacion"]').value;
            if (self.container.querySelector('[name="razon"]').value == '') {
                new Message('El campo razón no puede estar vacío.', 'warning');
                return;
            }
            if (programacionValue == 'semanal') {
                programacionValue = self.container.querySelector('[name="dia"]').value;
            }
            if (self.editID == 0) {
                Tools.ajaxRequest({
                    data: {
                        programacion: programacionValue,
                        razon: self.container.querySelector('[name="razon"]').value,
                        comentario: self.container.querySelector('[name="comentario"]').value,
                        cantidad: self.container.querySelector('[name="cantidad"]').value
                    },
                    url: 'functions/gasto/add-gastoprogramado',
                    method: 'POST',
                    waitMsg: new Wait('Agregando gasto programado...'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Gasto programado agregado con exito.', 'success');
                            GastosProgramados.addRow(data['resultado']['id'], this.data['programacion'], this.data['razon'], this.data['comentario'], this.data['cantidad'] || '0.00');
                            self.close();
                        }
                        else {
                            new Message('Ocurrio un error al tratar de agregar el gasto programado.<br>' + data['error'], 'error');
                        }
                    }
                });
            }
            else {
                Tools.ajaxRequest({
                    data: {
                        id: self.editID,
                        programacion: programacionValue,
                        razon: self.container.querySelector('[name="razon"]').value,
                        comentario: self.container.querySelector('[name="comentario"]').value,
                        cantidad: self.container.querySelector('[name="cantidad"]').value
                    },
                    url: 'functions/gasto/edit-gastoprogramado',
                    method: 'POST',
                    waitMsg: new Wait('Guardando gasto programado...'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Gasto programado guardado con exito.', 'success');
                            GastosProgramados.updateRow(data['resultado']['id'], this.data['programacion'], this.data['razon'], this.data['comentario'], this.data['cantidad'] || '0.00');
                            self.close();
                        }
                        else {
                            new Message('Ocurrio un error al tratar de agregar el gasto programado.<br>' + data['error'], 'error');
                        }
                    }
                });
            }
        });
    }
    clear() {
        this.container.querySelector('.gastoprogramado-accept').textContent = 'Agregar';
        this.container.querySelectorAll('select').forEach(function(elem) {
            elem.selectedIndex = 0;
        });
        this.container.querySelectorAll('input, textarea').forEach(function(elem) {
            elem.value = '';
        });
    }
    open(id = 0, programacion = 'mensual', razon = '', comentario = '', cantidad = '0.00') {
        this.clear();;
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;

        this.container.querySelector('.gastoprogramado-accept').textContent = 'Agregar';

        this.editID = id;

        if (programacion == 'lunes' || programacion == 'martes' || programacion == 'miercoles' || programacion == 'jueves' || programacion == 'viernes' || programacion == 'sabado' || programacion == 'domingo') {
            this.container.querySelector('[name="programacion"]').value = 'semanal';
            this.container.querySelector('[name="dia"]').value = programacion;
        }
        else {
            this.container.querySelector('[name="programacion"]').value = programacion;
        }
        this.container.querySelector('[name="razon"]').value = razon;
        this.container.querySelector('[name="comentario"]').value = comentario;
        this.container.querySelector('[name="cantidad"]').value = cantidad;
        
        this.container.querySelector('[name="programacion"]').dispatchEvent(new Event('change'));

        if (this.editID != 0) {
            this.container.querySelector('.gastoprogramado-accept').textContent = 'Guardar';
        }
    }
    close() {
        let self = this;
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = 'none';          
        }, 200)
    }
}

const GastosProgramados = new class {
    constructor() {
        let self = this;
        this.overlay = document.querySelector('.gastosprogramados-overlay');
        this.container = this.overlay.querySelector('.gastosprogramados-container');
        this.container.querySelector('.gastosprogramados-close-btn').addEventListener('click', function() {
            self.close();
        });
        this.container.querySelector('.add-gastosprogramados').addEventListener('click', function() {
            GastoProgramado.open();
        });
        this.load();
    }
    open() {
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
    }
    close() {
        let self = this;
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = 'none';          
        }, 200)
    }
    load() {
        let self = this;
        Tools.ajaxRequest({
            url: 'functions/gasto/get-all-gastosprogramados',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let gastos = data['resultado'];
                    for (let i=0; i<gastos.length; i++) {
                        let gasto = gastos[i];
                        self.addRow(gasto['id'], gasto['programacion'], gasto['razon'], gasto['comentario'], gasto['cantidad']);
                    }
                }
            }
        });
    }
    addRow(id, programacion, razon, comentario, cantidad) {
        let row = document.createElement('div');
        row.className = 'gastosprogramados-row';

        row.dataset.id = id;
        row.dataset.razon = razon;
        row.dataset.comentario = comentario;
        row.dataset.programacion = programacion;
        row.dataset.cantidad = cantidad;
        
        row.innerHTML = (
            '<div class="gastosprogramados-column"><p class="razon">' + razon + '</p></div>' +
            '<div class="gastosprogramados-column"><p class="comentario">' + comentario + '</p></div>' +
            '<div class="gastosprogramados-column"><p class="programacion">' + programacion + '</p></div>' +
            '<div class="gastosprogramados-column"><p class="cantidad">$' + cantidad + '</p></div>' +
            '<div class="gastosprogramados-column">' +
                '<div class="actions-container">' +
                    '<img class="gasto-programado-edit" tooltip-hint="Editar" src="/admin/imgs/icons/actions/edit.png">' +
                    '<img class="gasto-programado-delete" tooltip-hint="Eliminar" src="/admin/imgs/icons/actions/delete.png">' +
                '</div>' +
            '</div>'
        );
        row.querySelector('.gasto-programado-edit').addEventListener('click', function() {
            GastoProgramado.open(row.dataset.id, row.dataset.programacion, row.dataset.razon, row.dataset.comentario, row.dataset.cantidad);
        });
        row.querySelector('.gasto-programado-delete').addEventListener('click', function() {
            new Question('Eliminar gasto programado', 'Se eliminara este gasto programado, ¿Seguro que desea hacerlo?', function() {
                Tools.ajaxRequest({
                    data: {
                        id: id
                    },
                    url: 'functions/gasto/delete-gastoprogramado',
                    method: 'POST',
                    waitMsg: new Wait('Eliminando gasto programado...'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Gasto programado eliminado con exito.', 'success');
                            row.remove();
                        }
                        else {
                            new Message('Ocurrio un erro al tratar de eliminar el gasto programado.<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        });
        this.container.querySelector('.gastosprogramados-table').appendChild(row);
        ToolTip.update();
    }
    updateRow(id, programacion, razon, comentario, cantidad) {
        let row = this.container.querySelector('.gastosprogramados-table > .gastosprogramados-row[data-id="' + id + '"]');
        if (!row) {
            return;
        }
        row.dataset.id = id;
        row.dataset.razon = razon;
        row.dataset.comentario = comentario;
        row.dataset.programacion = programacion;
        row.dataset.cantidad = cantidad;

        row.querySelector('.razon').textContent = razon;
        row.querySelector('.comentario').textContent = comentario;
        row.querySelector('.programacion').textContent = programacion;
        row.querySelector('.cantidad').textContent = '$' + cantidad;
    }
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = false;
    Lista.cancelFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se cancelara el gasto seleccionada.',
            msgWait: 'Cancelando gasto.',
            msgSuccess: 'Gasto cancelado con exito.',
            msgError: 'Ocurrio un error al tratar de cancelar el gasto.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se cancelaran los gastos seleccionadas.',
                msgWait: 'Cancelando gastos.',
                msgSuccess: 'Gastos canceladas con exito.',
                msgError: 'Ocurrio un error al tratar de cancelar los gastos.'
            };
        }
        let ids = [];
        let cancelleds = 0;
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
            if (selecteds[i].hasAttribute('cancelled')) {
                cancelleds += 1;
            }
        }
        if (cancelleds == 0) {
            new Confirm(requestObj.msgQuestion, 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        ids: ids
                    },
                    method: 'POST',
                    url: 'functions/gasto/cancel',
                    waitMsg: new Wait(requestObj['msgWait'], true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message(requestObj['msgSuccess'], 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        }
        else {
            if (selecteds.length == 1) {
                new Message('No se puede cancelar un gasto ya cancelado.', 'warning');
            }
            else if (selecteds.length == cancelleds) {
                new Message('No se pueden cancelar gastos ya cancelados.', 'warning');
            }
            else {
                new Message('Existen algunos gastos seleccionadas que ya estan cancelados.', 'warning');                
            }
        }
    }
}
