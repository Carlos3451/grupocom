
var Repartidor = new class {

    constructor() {
        let self = this;

        this.onAccept = null;
        this.ids = [];

        this.overlayElem = document.querySelector('.repartidor-overlay');
        this.containerElem = this.overlayElem.querySelector('.repartidor-container');
        this.repartidorElem = this.containerElem.querySelector('[name="repartidor"]');
        this.acceptBtn = this.containerElem.querySelector('.repartidor-accept');
        this.cancelBtn = this.containerElem.querySelector('.repartidor-cancel');
        this.overlayElem.addEventListener('animationend', function(ev) {
            if (ev.animationName == 'disappear') {
                this.style.display = 'none';
            }
        });
        this.acceptBtn.addEventListener('click', function() {
            if (self.onAccept != null) {
                self.onAccept(self.ids, self.repartidorElem.value);
            }
            this.setAttribute('disabled', '');
            self.close();
        });
        this.cancelBtn.addEventListener('click', function() {
            self.close();
        });
        this._loadDealers();
    }

    _loadDealers() {
        let self = this;
        Tools.ajaxRequest({
            url: 'functions/credencial/get-dealers',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let repartidores = data['resultado'];
                    let optionsFragment = document.createDocumentFragment();
                    for (let i=0; i<repartidores.length; i++) {
                        let optionElem = document.createElement('option');
                        optionElem.setAttribute('value', repartidores[i]['id']);
                        optionElem.textContent = ('000' + repartidores[i]['id']).slice(-4) + ' ' + repartidores[i]['nombre'];
                        optionsFragment.appendChild(optionElem);
                    }
                    self.repartidorElem.appendChild(optionsFragment);
                }
            }
        });
    }

    open(ids) {
        this.ids = ids;
        this.overlayElem.style.display = 'flex';
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'appear 0.2s ease-in-out forwards';
        this.acceptBtn.removeAttribute('disabled');
    }

    close() {
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'disappear 0.2s ease-in-out forwards';
    }

}