var EmailFactura = new class {
    constructor() {
        let self = this;
        this.uid = '';
        this.callback = null;
        this.accesoID = 0;
        this.folio = '';
        this.overlay = document.querySelector('.emailfactura-overlay');
        this.container = this.overlay.querySelector('.emailfactura-container');
        this.emailInp = this.container.querySelector('[name="emailfactura-email"]');
        this.ccsInp = this.container.querySelector('[name="emailfactura-ccs"]');
        this.mensajeInp = this.container.querySelector('[name="emailfactura-mensaje"]');
        this.container.querySelector('.emailfactura-send').addEventListener('click', function() {
            self.send();
        });
        this.container.querySelector('.emailfactura-close').addEventListener('click', function() {
            self.close();
        });
    }
    open(folio, email, uid, accesoID, callback = null) {
        this.callback = callback;
        this.folio = folio;
        this.uid = uid;
        this.accesoID = accesoID;
        this.emailInp.value = email;
        this.ccsInp.value = '';
        this.mensajeInp.value = '';
        this.overlay.style.display = 'flex';
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'appear 0.2s ease-in-out';
    }
    close() {
        let self = this;
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'disappear 0.2s ease-in-out';
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
    send() {
        let self = this;
        Tools.ajaxRequest({
            data: {
                folio: self.folio,
                accesoID: self.accesoID,
                UID: self.uid,
                destinatarios: self.emailInp.value,
                ccs: self.ccsInp.value,
                mensaje: self.mensajeInp.value
            },
            url: 'functions/factura-v4/email',
            method: 'POST',
            waitMsg: new Wait('Enviando correo...'),
            success: function(response) {
                self.close();
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Correo enviado con exito.', 'success', function() {                        
                        if (self.callback) {
                            self.callback();
                        }
                    });
                }
                else {
                    new Message('Ocurrio un erro al enviar el correo.<br>' + data['error'], 'error');
                }
            }
        })
    }
}