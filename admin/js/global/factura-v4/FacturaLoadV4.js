
var FacturaLoad = new class {
    constructor() {
        let self = this;
        this.overlay = document.querySelector('.facturaload-overlay');
        this.container = this.overlay.querySelector('.facturaload-container');

        this.clienteSI = new SelectInp(document.querySelector('[name="facturaload-cliente"]'), {
            placeholder: 'Todos',
            defaultValue: 0
        });

        let desdeDP = new DatePicker(this.container.querySelector('[name="facturaload-fecha-desde"]'), {
            dateMax: new Date()
        });
        let hastaDP = new DatePicker(this.container.querySelector('[name="facturaload-fecha-hasta"]'), {
            dateMin: new Date()
        });
        
        desdeDP.onChange = function() {
            let dateArr = desdeDP.input.value.split('/');
            let date = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
            hastaDP.setDateMin(date);
        }
        
        hastaDP.onChange = function() {
            let dateArr = hastaDP.input.value.split('/');
            let date = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
            desdeDP.setDateMax(date);
        }

        this.container.querySelector('.facturaload-load').addEventListener('click', function() {
            let type = self.container.querySelector('[name="facturaload-tipo"]').value;
            let dateStart = self.container.querySelector('[name="facturaload-fecha-desde"]').value;
            let dateEnd = self.container.querySelector('[name="facturaload-fecha-hasta"]').value;
            let date = dateStart;
            if (dateStart != dateEnd) {
                date += ' - ' + dateEnd;
            }
            date = date.replace(/ /g, '+');
            let clienteID = self.container.querySelector('[name="facturaload-cliente"]').value;
            window.location = 'facturas-v4/agregar?tipo=' + type + '&fecha=' + encodeURIComponent(date) + (clienteID != '' ? ('&cliente=' + clienteID) : '');
        });
        this.container.querySelector('.facturaload-close').addEventListener('click', function() {
            self.close();
        });

        this.loadClientes();
    }
    loadClientes() {
        let selector = this.container.querySelector('[name="facturaload-cliente"]');
        selector.innerHTML = '<option>Cargando...</option>';
        selector.disabled = true;
        Tools.ajaxRequest({
            data: {
                metodo: 'folio',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/cliente/get-all-min',
            success: function(response) {
                selector.innerHTML = '<option value="">Todos</option>';
                selector.disabled = false;
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let options = [];
                    let clientes = data['resultado'];
                    for (let i=0; i<clientes.length; i++) {
                        options.push({
                            'name': ('000' + clientes[i]['folio']).slice(-4) + ' ' + clientes[i]['nombre'],
                            'value': clientes[i]['id'],
                            'keys': [('000' + clientes[i]['folio']).slice(-4), clientes[i]['nombre']]
                        });
                    }
                    FacturaLoad.clienteSI.setOptions(options);
                }
            }
        });
    }
    open() {
        this.overlay.style.display = 'flex';
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'appear 0.2s ease-in-out';
    }
    close() {
        let self = this;
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'disappear 0.2s ease-in-out';
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
}