const TinyProducto = new class {
	constructor() {
		let self = this;

		this.onSuccess = null;

		this.overlay = document.querySelector('.tinyproducto-overlay');
		this.form = this.overlay.querySelector('.tinyproducto-form');

		this.acceptBtn = this.form.querySelector('.accept-btn');
		this.cancelBtn = this.form.querySelector('.cancel-btn');

		this.form.querySelector('[name="inventarioMinimo"]').addEventListener('keydown', this._keydownFn);
		this.form.querySelector('[name="inventarioMinimo"]').addEventListener('blur', this._blurFn);

		this.form.querySelectorAll('input[price]').forEach(function(input) {
			input.addEventListener('keydown', self._keydownFn);
			input.addEventListener('blur', self._priceBlurFn);
		});

		this.form.querySelector('[name="unidad"]').addEventListener('change',function() {
			self.form.querySelector('[name="inventarioMinimo"]').className = 'inp-required inp-unit inp-' + this.value;
		});

		this.form.addEventListener('submit', function(ev) {
			ev.preventDefault();
		});

		this.acceptBtn.addEventListener('click', function() {
			this.setAttribute('disabled', '');
			self.submit();
		});
		this.cancelBtn.addEventListener('click', function() {
			self.close();
		});
	}
	submit() {
        if (!this._isFormFilled()) {
			this.acceptBtn.removeAttribute('disabled');
			return;
		}
		let self = this;
		let formData = {};

		this.form.querySelectorAll('input[name], select[name]').forEach(function(elem) {
			formData[elem.getAttribute('name')] = elem.value;
		});

		Tools.ajaxRequest({
			data: formData,
			url: 'functions/producto/add-tiny',
			method: 'POST',
			waitMsg: new Wait('Dando de alta producto...'),
			success: function(response) {
				this.waitMsg.destroy();
				let result = JSON.parse(response);
				if (result['error'] == '') {
					formData['id'] = result['resultado']['id'];
					formData['folio'] = result['resultado']['folio'];
					new Message('Producto dado de alta con exito', 'success', function() {
						self.onSuccess(formData);
						self.close();
					});
				}
				else {
					self.acceptBtn.removeAttribute('disabled');
					switch (result['error']) {
						default:
							new Message('Ocurrio un error al tratar de dar de alta el producto. Por favor intentelo de nuevo.<br>' + data['error'], 'error');
							break;
					}
				}
			}
		});
	}
	clear() {
		this.acceptBtn.removeAttribute('disabled');
		this.form.querySelector('[name="inventarioMinimo"]').className = 'inp-required inp-unit inp-pieza';
		this.form.querySelectorAll('input').forEach(function(input) {
			input.value = '';
		});
		this.form.querySelectorAll('select').forEach(function(select) {
			select.selectedIndex = 0;
		});
	}
	open(name = '') {
		this.clear();
		if (name != '') {
			this.form.querySelector('[name="nombre"]').value = name;
		}
		this.overlay.style.display = 'flex';
		this.overlay.offsetHeight;
		this.overlay.style.opacity = 1;
	}
	close() {
		let self = this;
		this.overlay.style.opacity = 0;
		setTimeout(function() {
			self.overlay.style.display = 'none';
		}, 200);
	}

	_isFormFilled() {
		let self = this;
		let filled = true;
		let requiredInputs = this.form.querySelectorAll('.inp-required');
		requiredInputs.forEach(function(elem, index) {
			if (elem.value == '') {
				filled = false;
				if (!elem.hasAttribute('invalid')) {
					elem.setAttribute('invalid', '');
					elem.addEventListener('keydown', self._clearInvalid);
				}
			}
		});
		if (!filled) {
			new Message('Por favor rellene los campos marcados para continuar.', 'warning');
		}
		return filled;
	}

	_clearInvalid() {
		this.removeAttribute('invalid');
		this.removeEventListener('keydown', this._clearInvalid)
	}

	_keydownFn(ev) {
		if (!Tools.checkNumeric(ev, this.value)) {
			ev.preventDefault();
		}
	}
	_blurFn(ev) {
		if (this.value != '') {
			this.value = parseFloat(parseFloat(this.value).toFixed(3));
		}
	}
	_priceBlurFn(ev) {
		if (this.value != '') {
			this.value = parseFloat(this.value).toFixed(2);
		}
	}
}