(function(){

sessionActive = true;

document.querySelector('header > .options-container > .options-toggle').addEventListener('click', function(){
    let optionsContainer = this.parentNode;
    let isOpen = optionsContainer.classList.contains('open');
    if (!isOpen) {
        optionsContainer.classList.add('open');
        document.body.addEventListener('click', evCloseOptions);
    }
    else {
        document.body.removeEventListener('click', evCloseOptions);
        optionsContainer.classList.remove('open');
    }
});

document.querySelector('header > .options-container > .options > .option').addEventListener('click', function() {
    document.querySelector('header > .options-container').classList.remove('open');
    document.body.removeEventListener('click', evCloseOptions);
});

document.querySelector('header > .options-container > .options > .option-logout').addEventListener('click', function() {
    let question = new Question('Se cerrara la sesión', '¿Desea cerrar la sesión actual?', function() {
        Tools.ajaxRequest({
            url: 'functions/login/logout',
            waitMsg: new Wait('Cerrando sesión.'),
            success: function(response) {
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Se ha cerrado la sesión con exito.', 'success', function() {
                        location.reload();
                    });
                }
                else {
                    new Message('Ocurrio un error al cerrar la sesión. Error: ' + data['error'], 'error');
                }
            }
        })
    });
});

/* SECCIONES */

document.querySelector('header > .sections-container > .sections-toggle').addEventListener('click', function(){
    let sectionsContainer = this.parentNode;
    if (sectionsContainer.querySelectorAll('.sections > .section-changer, .sections > .section-admin').length != 0) {
        let isOpen = sectionsContainer.classList.contains('open');
        if (!isOpen) {
            sectionsContainer.classList.add('open');
            document.body.addEventListener('click', evCloseSections);
        }
        else {
            document.body.removeEventListener('click', evCloseSections);
            sectionsContainer.classList.remove('open');
        }
    }
});

document.querySelector('header > .menu-toggle').addEventListener('click', function(){
    let menuOverlay = document.querySelector('header > .menu-container-overlay');
    let menuContainer = document.querySelector('header > .menu-container-overlay > .menu-container');
    menuOverlay.style.display = 'block';
    menuOverlay.style.animation = 'appear 0.2s ease-in-out forwards';
    menuContainer.style.animation = 'menuAppear 0.2s ease-in-out forwards';
});

document.querySelector('header > .menu-container-overlay > .menu-container > .menu-close').addEventListener('click', function(){
    let menuOverlay = document.querySelector('header > .menu-container-overlay');
    let menuContainer = document.querySelector('header > .menu-container-overlay > .menu-container');
    menuOverlay.style.animation = 'disappear 0.2s ease-in-out forwards';
    menuContainer.style.animation = 'menuDisappear 0.2s ease-in-out forwards';
});

document.querySelector('header > .menu-container-overlay').addEventListener('animationend', function(ev) {
    if (ev.animationName == 'disappear') {
        this.style.display = 'none';
    }
});

selectModule();

function selectModule() {
    let pathArray = window.location.pathname.split('/');
    let pathSearch = [pathArray[0], pathArray[1], pathArray[2]].join('/');
    let curModuleElem = document.querySelector('header > .menu-fast-container > a[href="' + pathSearch + '"]');
    if (curModuleElem) {
        curModuleElem.classList.add('selected');
    }
    if (pathSearch == '/admin/notificaciones') {
        document.querySelector('.notifications-container').classList.add('selected');
    }
}

function evCloseOptions(ev) {
    let optionsContainer = document.querySelector('header > .options-container');
    let target = ev.target;
    if (!optionsContainer.contains(target)) {
        optionsContainer.classList.remove('open');
        this.removeEventListener('click', evCloseOptions);
    }
}

function evCloseSections(ev) {
    let sectionsContainer = document.querySelector('header > .sections-container');
    let target = ev.target;
    if (!sectionsContainer.contains(target)) {
        sectionsContainer.classList.remove('open');
        this.removeEventListener('click', evCloseSections);
    }
}

/* CAMBIAR SECCIONES */

document.querySelectorAll('header .section-changer').forEach(function(elem, index) {
    elem.addEventListener('click', function(ev) {
        let sectionID = this.getAttribute('data-section-id');
        Tools.ajaxRequest({
            data: {
                id: sectionID
            },
            method: 'POST',
            url: 'functions/login/change-section',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    location.reload();
                }
                else {
                    new Message('Ocurrio un error al tratar de cambiar de sección.<br>' + data['error'], 'error');
                }
            }
        });
    });
});

})();