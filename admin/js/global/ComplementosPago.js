const ComplementoPagoAdder = new class {
    constructor() {
        let self = this;
        this.callback = null;
        this.overlay = document.querySelector('.complementopagoadder-overlay');
        this.container = this.overlay.querySelector('.complementopagoadder-container');
        this.container.querySelector('.complementopagoadder-add').addEventListener('click', function() {
            if (self.callback != null) {
                self.callback(self.container.querySelector('[name="complementopagoadder-fecha"]').value, self.container.querySelector('[name="complementopagoadder-cantidad"]').value, self.container.querySelector('[name="complementopagoadder-forma-pago"]').value)
            }
            self.close();
        });
        this.container.querySelector('.complementopagoadder-close').addEventListener('click', function() {
            self.close();
        });
        this.container.querySelector('[name="complementopagoadder-cantidad"]').addEventListener('keydown', function(ev) {
            if (!Tools.checkNumeric(ev, this.value)) {
                ev.preventDefault();
            }
        });
        this.container.querySelector('[name="complementopagoadder-cantidad"]').addEventListener('blur', function(ev) {
            this.value = (parseFloat(this.value) || 0).toFixed(2);
        });
        let dateNow = new Date();
        this.datepicker = new DatePicker(this.container.querySelector('[name="complementopagoadder-fecha"]'), {            
            dateMin: new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 3),
            dateMax: new Date()
        });
    }
    clear() {
        this.datepicker.changeDate(new Date());
        this.container.querySelector('[name="complementopagoadder-cantidad"]').value = '';
    }
    close() {
        let self = this;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
    open() {
        this.clear();
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
    }
}

const ComplementosPago = new class {
    constructor() {
        let self = this;
        
        this.facturaUID = '';
        this.facturaUUID = '';
        this.clienteUID = '';
        this.clienteData = null;
        this.accesoID = 0;
        this.formasPago = [];
        this.formasPagoPromise;

        this.overlay = document.querySelector('.complementospago-overlay');
        this.container = this.overlay.querySelector('.complementospago-container');
        this.container.querySelector('.complementospago-add').addEventListener('click', function() {
            ComplementoPagoAdder.open();
            ComplementoPagoAdder.callback = function(fecha, cantidad, formaPago) {
                if (cantidad == '' || parseFloat(cantidad) == 0) {
                    new Message('Cantidad ingresada no valida.', 'warning');
                }
                else {
                    Tools.ajaxRequest({
                        data: {
                            facturaUUID: self.facturaUUID,
                            facturaUID: self.facturaUID,
                            cantidad: cantidad,
                            fecha: fecha,
                            accesoID: self.accesoID,
                            formaPago: formaPago,
                            clienteUID: self.clienteUID,
                            numeroParcialidad: self.container.querySelectorAll('.complementospago-row').length + 1,
                            restoAnterior: self.container.querySelector('[name="complementospago-restante"]').value
                        },
                        url: 'functions/factura/add-complemento',
                        method: 'POST',
                        waitMsg: new Wait('Agregando Complemento de Pago'),
                        success: function(response) {
                            this.waitMsg.destroy();
                            let data = JSON.parse(response);
                            if (data['error'] == '') {
                                new Message('Complemento de Pago creado con exito.', 'success');
                                let row = document.createElement('div');
                                self.container.querySelector('[name="complementospago-pagado"]').value = (parseFloat(self.container.querySelector('[name="complementospago-pagado"]').value) + parseFloat(cantidad)).toFixed(2);
                                self.container.querySelector('[name="complementospago-restante"]').value = (parseFloat(self.container.querySelector('[name="complementospago-total"]').value) - parseFloat(self.container.querySelector('[name="complementospago-pagado"]').value)).toFixed(2);
                                row.className = 'complementospago-row';
                                row.dataset.uid = data['resultado']['uid'];
                                row.innerHTML = (
                                    '<div class="complementospago-column"><p>' + (self.container.querySelectorAll('.complementospago-row').length + 1) + '</p></div>'+
                                    '<div class="complementospago-column"><p>' + fecha + '</p></div>'+
                                    '<div class="complementospago-column"><p>$' + parseFloat(cantidad).toFixed(2) + '</p></div>'+
                                    '<div class="complementospago-column"><p>' + self.getFormaPago(formaPago) + '</p></div>'+
                                    '<div class="complementospago-column"><div class="actions-container">'+
                                        '<img class="action-email" tooltip-hint="Enviar por correo" src="imgs/icons/actions/email.png">'+
                                        '<img class="action-print" tooltip-hint="Imprimir" src="imgs/icons/actions/printer.png">'+
                                        '<img class="action-cancel" tooltip-hint="Cancelar" src="imgs/icons/actions/cancelar.png">'+
                                    '</div></div>'
                                );
                                self.activateActions(row);
                                self.container.querySelector('.complementospago-table').appendChild(row);
                            }
                            else {
                                if (data['errorLog'] && data['errorLog']['message'] && data['errorLog']['message']['message']) {
                                    new Message('Ocurrio un error al tratar de crear el complemento de pago. Por favor intentelo de nuevo.<br>' + data['errorLog']['message']['message'], 'error');                                    
                                }
                                else if (data['errorLog'] && data['errorLog']['message'] && data['errorLog']['message']) {
                                    new Message('Ocurrio un error al tratar de crear el complemento de pago. Por favor intentelo de nuevo.<br>' + data['errorLog']['message'], 'error');                                    
                                }
                                else {
                                    new Message('Ocurrio un error al tratar de crear el complemento de pago. Por favor intentelo de nuevo.<br>' + data['error'], 'error');
                                }
                            }
                        }
                    });
                }
            }
        });
        this.container.querySelector('.complementospago-close').addEventListener('click', function() {
            self.close();
        });
    }
    close() {
        let self = this;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
    open(facturaUID, facturaUUID, clienteData, clienteUID, total, accesoID) {
        this.clear();
        this.facturaUID = facturaUID;
        this.facturaUUID = facturaUUID;
        this.clienteUID = clienteUID;
        this.accesoID = accesoID;
        this.clienteData = clienteData;

        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
        this.container.querySelector('[name="complementospago-total"]').value = parseFloat(total).toFixed(2);
        this.formasPagoPromise = this.loadFormasPagos(this.accesoID);
        this.load();
    }
    clear() {
        this.facturaUID = '';
        this.facturaUUID = '';
        this.clienteUID = '';
        this.clienteData = null;
        this.container.querySelector('[name="complementospago-total"]').value = '';
        this.container.querySelector('[name="complementospago-pagado"]').value = '';
        this.container.querySelector('[name="complementospago-restante"]').value = '';
    }
    clearRows() {
        this.container.querySelectorAll('.complementospago-row').forEach(function(row) {
            row.remove();
        });
    }
    load() {
        let self = this;
        this.clearRows();
        this.container.querySelector('.complementospago-table').innerHTML += (
            '<div class="complementospago-row loading"><div class="complementospago-column"><p>Cargando Complementos de Pago...</p></div></div>'
        );
        Tools.ajaxRequest({
            data: {
                facturaUID: this.facturaUID
            },
            url: 'functions/factura/get-complementos',
            method: 'POST',
            success: function(response) {
                self.clearRows();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let complementos = data['resultado'];
                    let rowsFragment = document.createDocumentFragment();
                    let pagado = 0;
                    for (let i=0; i<complementos.length; i++) {
                        let complemento = complementos[i];
                        let row = document.createElement('div');
                        row.className = 'complementospago-row';
                        row.dataset.uid = complemento['complementoUID'];

                        row.innerHTML = (
                            '<div class="complementospago-column"><p>' + (i + 1) + '</p></div>'+
                            '<div class="complementospago-column"><p>' + complemento['fecha'].split('-').reverse().join('/') + '</p></div>'+
                            '<div class="complementospago-column"><p>$' + parseFloat(complemento['cantidad']).toFixed(2) + '</p></div>'+
                            '<div class="complementospago-column"><p class="forma-pago">' + complemento['formaPago'] + '</p></div>'+
                            '<div class="complementospago-column"><div class="actions-container">'+
                                '<img class="action-email" tooltip-hint="Enviar por correo" src="imgs/icons/actions/email.png">'+
                                '<img class="action-print" tooltip-hint="Imprimir" src="imgs/icons/actions/printer.png">'+
                                '<img class="action-cancel" tooltip-hint="Cancelar" src="imgs/icons/actions/cancelar.png">'+
                            '</div></div>'
                        );
                        
                        self.getFormaPago(row.querySelector('.forma-pago'), complemento['formaPago']);
                        self.activateActions(row);

                        if (complemento['cancelado'] == 1) {
                            row.querySelector('.action-cancel').remove();
                            row.setAttribute('cancelado', 'true');
                        }
                        else {
                            pagado += parseFloat(complemento['cantidad']);
                        }

                        rowsFragment.appendChild(row);
                    }
                    self.container.querySelector('[name="complementospago-pagado"]').value = parseFloat(pagado).toFixed(2);
                    self.container.querySelector('[name="complementospago-restante"]').value = (parseFloat(self.container.querySelector('[name="complementospago-total"]').value) - parseFloat(pagado)).toFixed(2);
                    self.container.querySelector('.complementospago-table').appendChild(rowsFragment);
                    ToolTip.update();
                }
                else {
                    if (data['error'] == 'EMPTY') {
                        self.container.querySelector('[name="complementospago-pagado"]').value = '0.00';
                        self.container.querySelector('[name="complementospago-restante"]').value = self.container.querySelector('[name="complementospago-total"]').value;
                    }
                    else {
                        new Message('Ocurrio un error al cargar los complementos de pago. Por favor intentelo de nuevo.<br>' + data['error'], 'error', function() {
                            self.close();
                        });
                    }
                }
            }
        });
    }
    loadFormasPagos(accesoID) {
        let self = this;
        let promise = new Promise(function(resolve, reject) {
            let selectInp = ComplementoPagoAdder.container.querySelector('[name="complementopagoadder-forma-pago"]');
            selectInp.setAttribute('disabled', '');
            selectInp.innerHTML = '<option value="">Cargando...</option>';
            Tools.ajaxRequest({
                data: {
                    accesoID: accesoID
                },
                method: 'POST',
                url: 'functions/factura/get-formas-pago',
                success: function(response) {
                    Tools.removeChilds(selectInp);
                    selectInp.removeAttribute('disabled');
                    selectInp.innerHTML = '';
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        self.formasPago = data['resultado'];
                        let optionsFrag = document.createDocumentFragment();
                        for (let i=0; i<data['resultado'].length; i++) {
                            let option = document.createElement('option');
                            option.textContent = data['resultado'][i]['name'];
                            option.setAttribute('value', data['resultado'][i]['key']);
                            optionsFrag.appendChild(option);
                        }                
                        selectInp.appendChild(optionsFrag);
                    }
                    resolve();
                }
            });
        });
        return promise;
    }
    getFormaPago(elem, formaPagoID) {
        let self = this;
        this.formasPagoPromise.then(function(value) {
            for (let i=0; i<self.formasPago.length; i++) {
                if (self.formasPago[i]['key'] == formaPagoID) {
                    elem.textContent = self.formasPago[i]['name'];
                }
            }
        });
    }
    activateActions(row) {
        let self = this;
        
        row.querySelector('.action-email').addEventListener('click', function() {
            let uid = row.dataset.uid;
            let email = '';
            if (self.clienteData) {
                email = self.clienteData['Contacto']['Email'];
            }
            EmailFactura.open(email, uid, self.accesoID);
        });

        row.querySelector('.action-print').addEventListener('click', function() {
            Tools.ajaxRequest({
                data: {
                    accesoID: self.accesoID,
                    UID: row.dataset.uid
                },
                method: 'POST',
                url: 'functions/factura/pdf',
                responseType: 'blob',
                waitMsg: new Wait('Obteniendo PDF.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    window.open(window.URL.createObjectURL(response));
                }
            });
        });

        row.querySelector('.action-cancel').addEventListener('click', function() {
            new Confirm('Cancelar Complemento de Pago', 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        uid: row.dataset.uid,
                        accesoID: self.accesoID,
                    },
                    waitMsg: new Wait('Cancelando Complemento de Pago...'),
                    url: 'functions/factura/complemento-cancel',
                    method: 'POST',
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Complemento de pago cancelado con exito.', 'success');
                            self.container.querySelector('[name="complementospago-pagado"]').value = (parseFloat(self.container.querySelector('[name="complementospago-pagado"]').value) - parseFloat(row.dataset.cantidad)).toFixed(2);
                            self.container.querySelector('[name="complementospago-restante"]').value = (parseFloat(self.container.querySelector('[name="complementospago-total"]').value) - parseFloat(self.container.querySelector('[name="complementospago-pagado"]').value)).toFixed(2);
                            row.setAttribute('cancelado', '');
                            row.querySelector('.action-cancel').remove();
                        }
                        else {
                            if (data['errorLog'] && data['errorLog']['message'] && data['errorLog']['message']['message']) {
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago. Por favor intentelo de nuevo.<br>' + data['errorLog']['message']['message'], 'error');
                            }
                            else if (data['errorLog'] && data['errorLog']['message']) {
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago. Por favor intentelo de nuevo.<br>' + data['errorLog']['message'], 'error');
                            }
                            else {
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago. Por favor intentelo de nuevo.<br>' + data['error'], 'error');
                            }
                        }
                    }
                });
            });
        });
    }
}