(function() {

clearInputs();

let getObj = getGET();

if (getObj['accesoid'] && getObj['copiauid']) {
    Tools.ajaxRequest({
        data: {
            accesoID: getObj['accesoid'],
            UID: getObj['copiauid']
        },
        url: 'functions/factura/xml',
        method: 'POST',
        waitMsg: new Wait('Cargando datos de la factura a copiar...'),
        success: function(response) {
            this.waitMsg.destroy();
            let xml = ( new DOMParser() ).parseFromString( response, "application/xml" );
            parseXML(xml);
        }
    });
}

function parseXML(xml) {
    let conceptosElem = xml.getElementsByTagName('cfdi:Conceptos');
    if (conceptosElem && conceptosElem.length == 1) {
        console.log(conceptosElem[0]);
        for (let i=0; i<conceptosElem[0].children.length; i++) {
            let conceptoElem = conceptosElem[0].children[i];
            let unidad = satUnitToLocalUnit(conceptoElem.getAttribute('ClaveUnidad'));
            let precio = conceptoElem.getAttribute('ValorUnitario');
            let trasladosElems = conceptoElem.getElementsByTagName('cfdi:Traslado');
            let impuesto = 'tasacero';
            if (trasladosElems.length == 1) {
                impuesto = satImpuestoToLocalImpuesto(trasladosElems[0].getAttribute('Impuesto'));
                switch (impuesto) {
                    case 'iva':
                        precio = Math.round(precio * 1.16 * 100000) / 100000;
                        break;
                    case 'ieps8':
                        precio = Math.round(precio * 1.08 * 100000) / 100000;
                        break;
                    default:
                        new Message('Error al cargar los conceptos.<br>Error: Traslado no encontrado.', 'error', function() {
                            window.location = 'facturas';
                        });
                        return;
                }
            }
            else if (trasladosElems.length == 2) {
                let impuesto1 = trasladosElems[0].getAttribute('Impuesto');
                let impuesto2 = trasladosElems[1].getAttribute('Impuesto');
                if ((impuesto1 == '002' && impuesto2 == '003') || (impuesto1 == '003' && impuesto2 == '002')) {
                    impuesto = 'ieps160iva';
                    precio = Math.round(precio * 2.6 * 1.16 * 100000) / 100000;
                }
                else {
                    new Message('Error al cargar los conceptos.<br>Error: Traslado IEPS 160 IVA esperado, pero otro devuelto.', 'error', function() {
                        window.location = 'facturas';
                    });
                    return;
                }
            }
            else if (trasladosElems.length != 0) {
                console.log(trasladosElems);
                new Message('Error al cargar los conceptos.<br>Error: Más traslados de los necesarios.', 'error', function() {
                    window.location = 'facturas';
                });
                return;
            }
            addConcept({
                claveSAT: conceptoElem.getAttribute('ClaveProdServ'),
                nombre: conceptoElem.getAttribute('Descripcion'),
                unidad: unidad,
                precio1: precio,
                cantidad: conceptoElem.getAttribute('Cantidad'),
                impuesto: impuesto,
                id: 0
            }, true);
        }
    }
}

function satImpuestoToLocalImpuesto(satImpuesto) {
    switch (satImpuesto) {
        case '002': return 'iva';
        case '003': return 'ieps8';
    }
}

function satUnitToLocalUnit(satUnit) {
    switch (satUnit) {
        case 'GRM':
            return 'gramo';
        case 'KGM':
            return 'kilogramo';
        case 'MLT':
            return 'mililitro';
        case 'LTR':
            return 'litro';
        case 'H87':
            return 'pieza';
        case 'XPK':
            return 'paquete';
        case 'XBX':
            return 'caja';
        case 'LBR':
            return 'libra';
        case 'XBH':
            return 'maso';
    }
    return 'pieza';
}

let clientToggle = new class {
    constructor() {
        let self = this;
        this.toggle = document.querySelector('.client-toggle');
        this.container = document.querySelector('.client-data-container');
        this.toggle.addEventListener('click', function() {
            this.classList.toggle('open');
            self.toggleClientContainer();
        });
    }
    toggleClientContainer() {
        this.container.classList.toggle('open');
    }
}

let preloadClientUID = null;
let clientsLoaded = false;

let getData = {};
let productsList = null;
let clientsList = null;

let productoSI = new SelectInp(document.querySelector('[name="producto-buscador"]'), {
    openEmpty: false,
    selectOnHover: false
});
let clienteSI = new SelectInp(document.querySelector('[name="clienteUID"]'), {
    selectOnHover: false
});

let dateNow = new Date();

let fechaFacturacionDP = new DatePicker(document.querySelector('[name="fechaFacturacion"]'), {
    dateMin: new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 3),
    dateMax: new Date()
});

productoSI.onChange = function() {
    let productID = this.value;    
    let product = findProductByID(productID);
    addConcept(product);
    this.clear();
}

clienteSI.searchInput.setAttribute('enterfocus', 1);
productoSI.searchInput.setAttribute('enterfocus', 1);

loadFacturaAccesos();
loadProducts();

function getGET() {
    let getArray = window.location.search.substr(1).split('&');
    let getObj = {};
    for (let i=0; i<getArray.length; i++) {
        let arr = getArray[i].split('=');
        getObj[arr[0]] = decodeURIComponent(arr[1]);
    }
    console.log(getObj);
    return getObj;
}

function clearFacturaInputs() {
    clienteSI.searchInput.value = 'Cargando clientes...';
    clienteSI.searchInput.setAttribute('disabled', '');

    Tools.removeChilds(document.querySelector('[name="usoCFDI"]'));
    document.querySelector('[name="usoCFDI"]').innerHTML = '<option>Cargando datos...</option>';
    document.querySelector('[name="usoCFDI"]').setAttribute('disabled', '');

    Tools.removeChilds(document.querySelector('[name="formaPago"]'));
    document.querySelector('[name="formaPago"]').innerHTML = '<option>Cargando datos...</option>';
    document.querySelector('[name="formaPago"]').setAttribute('disabled', '');

    Tools.removeChilds(document.querySelector('[name="metodoPago"]'));
    document.querySelector('[name="metodoPago"]').innerHTML = '<option>Cargando datos...</option>';
    document.querySelector('[name="metodoPago"]').setAttribute('disabled', '');

    Tools.removeChilds(document.querySelector('[name="tipoRelacion"]'));
    document.querySelector('[name="tipoRelacion"]').innerHTML = '<option>Cargando datos...</option>';
    document.querySelector('[name="tipoRelacion"]').setAttribute('disabled', '');
}

function loadFacturaAccesos() {
    let emisorSelect = document.querySelector('[name="emisorID"]');
    emisorSelect.innerHTML = '<option>Cargando accesos...</option>';
    emisorSelect.setAttribute('disabled', '');
    clearFacturaInputs();
    Tools.ajaxRequest({
        url: 'functions/factura/get-all-access',
        method: 'GET',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                Tools.removeChilds(emisorSelect);
                emisorSelect.removeAttribute('disabled');
                let result = data['resultado'];
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<result.length; i++) {
                    let option = document.createElement('option');
                    option.value = result[i]['id'];
                    option.textContent = result[i]['nombre'];
                    optionsFrag.appendChild(option);
                }
                emisorSelect.appendChild(optionsFrag);
                emisorSelect.dispatchEvent(new Event('change'));
            }
        }
    });
}

document.querySelector('[name="emisorID"]').addEventListener('change', function() {
    clearFacturaInputs();
    loadClients(this.value);
    loadSATData(this.value);
});

function loadClients(accessID) {
    let inp = clienteSI.searchInput;
    clientsList = [];
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura/get-all-clients',
        success: function(response) {
            inp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                clientsList = data['resultado'];
                setClients();
            }
        }
    });
}

function setClients() {
    let options = [];
    for (let i=0; i<clientsList.length; i++) {
        let client = clientsList[i];
        let option = {
            name: '[' + client['RFC'] + '] ' + client['RazonSocial'],
            value: client['UID'],
            keys: [client['RFC'], client['RazonSocial'], '[' + client['RFC'] + '] ' + client['RazonSocial']]
        }
        options.push(option);
    }
    clienteSI.setOptions(options);
    clienteSI.searchInput.value = '';
    clienteSI.searchInput.removeAttribute('disabled');
    clientsLoaded = true;
    if (preloadClientUID != null) {
        if (preloadClientUID == 1) {
            let uid = clienteSI.container.querySelector('.selectinp-option').getAttribute('value');
            clienteSI.setValue(uid);
            clienteSI.ghostInput.dispatchEvent(new Event('change'));
        }
        else {
            clienteSI.setValue(preloadClientUID);
            clienteSI.ghostInput.dispatchEvent(new Event('change'));
        }
    }
}

clienteSI.onChange = function() {
    let value = clienteSI.value;
    let client = clientsList.find(function(obj) {
        return obj['UID'] == value;
    });
    if (client) {
        document.querySelectorAll('.client-actions-container > .actions-container > .client-edit').forEach(function(elem, index) {
            elem.classList.remove('disabled');
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'bounce 0.2s ease-in-out forwards';
        });
        document.querySelector('[name="clienteNombres"]').value = client['Contacto']['Nombre'];
        document.querySelector('[name="clienteApellidos"]').value = client['Contacto']['Apellidos'];
        document.querySelector('[name="clienteEmails"]').value = client['datosExtras'] ? client['datosExtras']['emails'] : client['Contacto']['Email'];
        document.querySelector('[name="clienteTelefono"]').value = client['Contacto']['Telefono'];
        document.querySelector('[name="clienteRazonSocial"]').value = client['RazonSocial'];
        document.querySelector('[name="clienteRFC"]').value = client['RFC'];
        document.querySelector('[name="clienteCalle"]').value = client['Calle'];
        document.querySelector('[name="clienteCodigoPostal"]').value = client['CodigoPostal'];
        document.querySelector('[name="clienteNumeroExterior"]').value = client['Numero'];
        document.querySelector('[name="clienteNumeroInterior"]').value = client['Interior'];
        document.querySelector('[name="clienteColonia"]').value = client['Colonia'];
        document.querySelector('[name="clienteCiudad"]').value = client['Ciudad'];
        document.querySelector('[name="clienteEstado"]').value = client['Estado'];
        FacturaAdder.loadFacturas(client['RFC']);
    }
    else {
        document.querySelectorAll('.client-actions-container > .actions-container > .client-edit').forEach(function(elem, index) {
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'disappear 0.2s ease-in-out forwards';
        });
        document.querySelector('[name="clienteNombres"]').value = '';
        document.querySelector('[name="clienteApellidos"]').value = '';
        document.querySelector('[name="clienteEmails"]').value = '';
        document.querySelector('[name="clienteTelefono"]').value = '';
        document.querySelector('[name="clienteRazonSocial"]').value = '';
        document.querySelector('[name="clienteRFC"]').value = '';
        document.querySelector('[name="clienteCalle"]').value = '';
        document.querySelector('[name="clienteCodigoPostal"]').value = '';
        document.querySelector('[name="clienteNumeroExterior"]').value = '';
        document.querySelector('[name="clienteNumeroInterior"]').value = '';
        document.querySelector('[name="clienteColonia"]').value = '';
        document.querySelector('[name="clienteCiudad"]').value = '';
        document.querySelector('[name="clienteEstado"]').value = '';
    }
}

/* FACTURA DATA */

function loadSATData(accesoID) {
    let usoCFDISelector = document.querySelector('[name="usoCFDI"]');
    let formaPagoSelector = document.querySelector('[name="formaPago"]');
    let metodoPagoSelector = document.querySelector('[name="metodoPago"]');
    let tipoRelacionSelector = document.querySelector('[name="tipoRelacion"]');

    Tools.ajaxRequest({
        data: {
            accesoID: accesoID
        },
        method: 'POST',
        url: 'functions/factura/get-sat-data',
        success: function(response) {
            Tools.removeChilds(usoCFDISelector);
            Tools.removeChilds(formaPagoSelector);
            Tools.removeChilds(metodoPagoSelector);
            Tools.removeChilds(tipoRelacionSelector);
            usoCFDISelector.disabled = false;
            formaPagoSelector.disabled = false;
            metodoPagoSelector.disabled = false;
            tipoRelacionSelector.disabled = false;

            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (data['resultado']['usoCfdi']['error'] == '') {
                    let result = data['resultado']['usoCfdi']['resultado'];
                    let optionsFrag = document.createDocumentFragment();
                    for (let i=0; i<result.length; i++) {
                        let option = document.createElement('option');
                        option.textContent = result[i]['name'];
                        option.setAttribute('value', result[i]['key']);
                        optionsFrag.appendChild(option);
                    }                
                    usoCFDISelector.appendChild(optionsFrag);
                }
                if (data['resultado']['formaPago']['error'] == '') {
                    let result = data['resultado']['formaPago']['resultado'];
                    let optionsFrag = document.createDocumentFragment();
                    for (let i=0; i<result.length; i++) {
                        let option = document.createElement('option');
                        option.textContent = result[i]['name'];
                        option.setAttribute('value', result[i]['key']);
                        optionsFrag.appendChild(option);
                    }                
                    formaPagoSelector.appendChild(optionsFrag);
                }
                if (data['resultado']['metodoPago']['error'] == '') {
                    let result = data['resultado']['metodoPago']['resultado'];
                    let optionsFrag = document.createDocumentFragment();
                    for (let i=0; i<result.length; i++) {
                        let option = document.createElement('option');
                        option.textContent = result[i]['name'];
                        option.setAttribute('value', result[i]['key']);
                        optionsFrag.appendChild(option);
                    }                
                    metodoPagoSelector.appendChild(optionsFrag);
                }
                if (data['resultado']['tipoRelacion']['error'] == '') {
                    let result = data['resultado']['tipoRelacion']['resultado'];
                    let optionsFrag = document.createDocumentFragment();
                    for (let i=0; i<result.length; i++) {
                        let option = document.createElement('option');
                        option.textContent = result[i]['name'];
                        option.setAttribute('value', result[i]['key']);
                        optionsFrag.appendChild(option);
                    }                
                    tipoRelacionSelector.appendChild(optionsFrag);
                }
            }
        }
    });

}

/*
function loadUsoCFDI(accessID) {
    let selectInp = document.querySelector('[name="usoCFDI"]');
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura/get-usos-cfdi',
        success: function(response) {
            Tools.removeChilds(selectInp);
            selectInp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<data['resultado'].length; i++) {
                    let option = document.createElement('option');
                    option.textContent = data['resultado'][i]['name'];
                    option.setAttribute('value', data['resultado'][i]['key']);
                    optionsFrag.appendChild(option);
                }                
                selectInp.appendChild(optionsFrag);
            }
        }
    });
}

function loadFormaPago(accessID) {
    let selectInp = document.querySelector('[name="formaPago"]');
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura/get-formas-pago',
        success: function(response) {
            Tools.removeChilds(selectInp);
            selectInp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<data['resultado'].length; i++) {
                    let option = document.createElement('option');
                    option.textContent = data['resultado'][i]['name'];
                    option.setAttribute('value', data['resultado'][i]['key']);
                    optionsFrag.appendChild(option);
                }                
                selectInp.appendChild(optionsFrag);
            }
        }
    });
}

function loadMetodosPago(accessID) {
    let selectInp = document.querySelector('[name="metodoPago"]');
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura/get-metodos-pago',
        success: function(response) {
            Tools.removeChilds(selectInp);
            selectInp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<data['resultado'].length; i++) {
                    let option = document.createElement('option');
                    option.textContent = data['resultado'][i]['name'];
                    option.setAttribute('value', data['resultado'][i]['key']);
                    optionsFrag.appendChild(option);
                }                
                selectInp.appendChild(optionsFrag);
            }
        }
    });
}
*/

function loadProducts() {
    productoSI.searchInput.value = 'Cargando productos...';
    productoSI.searchInput.setAttribute('disabled', '');
    Tools.ajaxRequest({
        data: {
            busqueda: '',
            orden: 'asc',
            metodo: 'folio'
        },
        method: 'POST',
        url: 'functions/producto/get-all-facturar',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                productsList = data['resultado'];
                setProducts();
            }
        }
    });
}

function setProducts() {
    let options = [];
    for (let i=0; i<productsList.length; i++) {
        let product = productsList[i];
        let option = {
            name: '[' + ('000' + product['folio']).slice(-4) + '] ' + product['nombre'],
            value: product['id'],
            keys: [product['folio'].toString(), product['nombre']]
        }
        options.push(option);
    }
    productoSI.setOptions(options);
    productoSI.searchInput.value = '';
    productoSI.searchInput.removeAttribute('disabled');
}

function addConcept(product, loaded = false) {
    if (product != null) {
        let row = document.createElement('div');
        row.className = 'factura-table-row';
        row.innerHTML = (
            '<div class="factura-table-column">'+
                '<div class="clave-input-container">'+
                    '<input name="conceptoClaveSAT" type="text" value="' + product['claveSAT'] + '">' +
                    '<img class="action-search-clave" src="imgs/icons/buscador-azul.png">' +
                '</div>' +
            '</div>' +
            '<div class="factura-table-column"><input name="conceptoNombre" type="text" value="' + product['nombre'] + '"></div>' +
            '<div class="factura-table-column"><select name="conceptoUnidad">'+
                '<option value="pieza">Pieza (pza)</option>'+
                '<option value="paquete">Paquete</option>'+
                '<option value="caja">Caja</option>'+
                '<option value="burbuja">Burbuja</option>'+
                '<option value="saco">Saco</option>'+
                '<option value="maso">Maso</option>'+
                '<option value="gramo">Gramo</option>'+
                '<option value="kilogramo">Kilogramo</option>'+
                '<option value="libra">Libra</option>'+
                '<option value="mililitro">Mililitro</option>'+
                '<option value="litro">Litro</option>'+
                '<option value="unidad">Unidad</option>'+
            '</select></div>' +
            '<div class="factura-table-column"><input class="inp-unit inp-' + product['unidad'] + '" name="conceptoCantidad" type="text" decimals="3" autocomplete="off" numbers></div>' +
            '<div class="factura-table-column"><input class="inp-icon inp-price" name="conceptoPrecio" type="text" value="' + product['precio1'] + '" autocomplete="off" numbers></div>' +
            '<div class="factura-table-column"><input class="inp-icon inp-price" name="conceptoTotal" type="text" value="0" numbers disabled></div>' +
            '<div class="factura-table-column"><select name="conceptoImpuesto">'+
                '<option value="tasacero">Tasa Cero (0%)</option>'+
                '<option value="iva">IVA (16%)</option>'+
                '<option value="ieps8">IEPS (8%)</option>'+
                '<option value="ieps160iva">IEPS (160%) e IVA (16%)</option>'+
            '</select></div>' +
            '<div class="factura-table-column"><div class="actions-container"><img class="action-delete-row" src="imgs/icons/actions/delete.png"></div></div>'
        );

        row.setAttribute('data-unidad', product['unidad']);
        row.setAttribute('data-id', product['id']);
        
        row.querySelector('[name="conceptoImpuesto"]').value = product['impuesto'];

        row.querySelector('[name="conceptoUnidad"]').value = product['unidad'];
        row.querySelector('[name="conceptoUnidad"]').addEventListener('change', function() {
            row.querySelector('[name="conceptoCantidad"]').className = 'inp-unit inp-' + this.value;
        });

        row.querySelectorAll('[numbers]').forEach(function(elem) {
            elem.addEventListener('keydown', evKeydownIsNumber);
            elem.addEventListener('focus', function() { this.select() });
        });

        row.querySelectorAll('input, select').forEach(function(elem) {
            elem.addEventListener('keydown', function(ev) {
                let columnIdx = -1;
                let column = this.closest('.factura-table-column');
                let row = column.parentElement;
                let found = false;
                switch (ev.key) {
                    case 'Enter':
                        ev.preventDefault();
                        while (column.nextElementSibling) {
                            let input = column.nextElementSibling.querySelector('input, select');
                            if (input && !input.disabled) {
                                found = true;
                                input.focus();
                                if (input.tagName != 'SELECT') {
                                    setTimeout(function() {
                                        input.select();
                                    });
                                }
                                break;
                            }
                            else {
                                column = column.nextElementSibling;
                            }
                        }
                        if (!found) {
                            productoSI.searchInput.focus();
                        }
                        break;
                    case 'ArrowLeft':
                        ev.preventDefault();
                        while (column.previousElementSibling) {
                            let input = column.previousElementSibling.querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                if (input.tagName != 'SELECT') {
                                    setTimeout(function() {
                                        input.select();
                                    });
                                }
                                break;
                            }
                            else {
                                column = column.previousElementSibling;
                            }
                        }
                        break;
                    case 'ArrowRight':
                        ev.preventDefault();
                        while (column.nextElementSibling) {
                            let input = column.nextElementSibling.querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                if (input.tagName != 'SELECT') {
                                    setTimeout(function() {
                                        input.select();
                                    });
                                }
                                break;
                            }
                            else {
                                column = column.nextElementSibling;
                            }
                        }
                        break;
                    case 'ArrowDown':
                        if (this.tagName == 'SELECT') {
                            return
                        }
                        ev.preventDefault();
                        columnIdx = -1;
                        for (let i=0; i<row.children.length; i++) {
                            if (row.children[i].isEqualNode(column)) {
                                columnIdx = i;
                                break;
                            }
                        }
                        if (columnIdx == -1) {
                            return;
                        }
                        while (row.nextElementSibling) {
                            let input = row.nextElementSibling.children[columnIdx].querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                if (input.tagName != 'SELECT') {
                                    setTimeout(function() {
                                        input.select();
                                    });
                                }
                                break;
                            }
                            else {
                                row = row.nextElementSibling;
                            }
                        }
                        break;
                    case 'ArrowUp':
                        if (this.tagName == 'SELECT') {
                            return
                        }
                        ev.preventDefault();
                        columnIdx = -1;
                        for (let i=0; i<row.children.length; i++) {
                            if (row.children[i].isEqualNode(column)) {
                                columnIdx = i;
                                break;
                            }
                        }
                        if (columnIdx == -1) {
                            return;
                        }
                        while (row.previousElementSibling) {
                            let input = row.previousElementSibling.children[columnIdx].querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                setTimeout(function() {
                                    input.select();
                                });
                                break;
                            }
                            else {
                                row = row.previousElementSibling;
                            }
                        }
                        break;
                }
            });
        });

        row.querySelector('[name="conceptoCantidad"]').addEventListener('blur', evBlurDecimal);
        row.querySelector('[name="conceptoPrecio"]').addEventListener('blur', evBlurPrice);
        row.querySelector('[name="conceptoTotal"]').addEventListener('blur', function() {
            if (this.value != '') {
                this.value = parseFloat(parseFloat(this.value).toFixed(6));
            } 
        });

        row.querySelector('[name="conceptoImpuesto"]').addEventListener('change', function() {
            updateConceptRow(row);
        });
        
        row.querySelector('[name="conceptoCantidad"]').addEventListener('blur', function() {
            updateConceptRow(row);
        });
        row.querySelector('[name="conceptoCantidad"]').addEventListener('input', function(){
            updateConceptRow(row)
        });
        
        row.querySelector('[name="conceptoPrecio"]').addEventListener('blur', function() {
            updateConceptRow(row);
        });
        row.querySelector('[name="conceptoPrecio"]').addEventListener('input', function(){
            updateConceptRow(row)
        });

        row.querySelector('.action-search-clave').addEventListener('click', function() {
            ClavesSAT.open(row.querySelector('[name="conceptoClaveSAT"]').value);
            ClavesSAT.onSelect = function(clave, name) {
                row.querySelector('[name="conceptoClaveSAT"]').value = clave;
            }
        });

        row.querySelector('.action-delete-row').addEventListener('click', function() {
            new Question('Se eliminara este concepto.', '¿Esta de acuerdo?', function() {
                row.remove();
                updateGranTotal();
            });
        });

        document.querySelector('.factura-table').appendChild(row);

        if (!loaded) {
            setTimeout(function(){
                row.querySelector('[name="conceptoCantidad"]').focus();
            }, 1);
        }
        else {
            row.querySelector('[name="conceptoCantidad"]').value = product['cantidad'];
            row.querySelector('[name="conceptoCantidad"]').dispatchEvent(new Event('blur'));
            updateConceptRow(row);
        }
    }
}

function findProductByID(productID) {
    for (let i=0; i<productsList.length; i++) {
        if (productsList[i]['id'] == productID) {
            return productsList[i];
        }
    }
    return null;
}

function updateConceptRow(row, propagate = true) {
    let cantidad = parseFloat(row.querySelector('[name="conceptoCantidad"]').value);
    let precio = parseFloat(row.querySelector('[name="conceptoPrecio"]').value);
    let total = (cantidad * precio) || 0;
    row.querySelector('[name="conceptoTotal"]').value = total;
    row.querySelector('[name="conceptoTotal"]').dispatchEvent(new Event('blur'));
    updateGranTotal(propagate);
}

function updateGranTotal(propagate = true) {
    let aumentarImpuesto = document.querySelector('[name="aumentarImpuesto"]').checked;
    let granTotal = 0;
    let descuentoPorcentaje = parseFloat(document.querySelector('[name="descuentoPorcentaje"]').value) || 0;
    document.querySelectorAll('.factura-table-row').forEach(function(row) {
        if (aumentarImpuesto) {
            granTotal += parseFloat(row.querySelector('[name="conceptoTotal"]').value);
        }
        else {
            let total = parseFloat(row.querySelector('[name="conceptoTotal"]').value) * (1 - (descuentoPorcentaje * 0.01));
            let impuesto = 0;
            let tipoImpuesto = row.querySelector('[name="conceptoImpuesto"]').value;
            switch (tipoImpuesto) {
                case 'ieps8': impuesto = 0.08; break;
                case 'ieps160iva': impuesto = (2.6 * 1.16) - 1; break;
                case 'iva': impuesto = 0.16; break;
            }
            granTotal += total / (1 + impuesto);
        }
    });
    document.querySelector('[name="granTotal"]').value = granTotal.toFixed(2);
    updateImpuesto(propagate);
}

document.querySelector('[name="descuentoPorcentaje"]').addEventListener('input', updateImpuesto);
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('focus', function() {
    this.select();
});
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('blur', evBlurDecimal);
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('blur', updateImpuesto);

function updateImpuesto(propagate = true) {
    let aumentarImpuesto = document.querySelector('[name="aumentarImpuesto"]').checked;
    let granTotal = parseFloat(document.querySelector('[name="granTotal"]').value);
    let descuentoPorcentaje = parseFloat(document.querySelector('[name="descuentoPorcentaje"]').value) || 0;
    let descuentoCantidad = granTotal * descuentoPorcentaje * 0.01;
    let subtotal = granTotal - descuentoCantidad;
    let impuestos = 0;
    document.querySelectorAll('.factura-table-row').forEach(function(row) {
        if (aumentarImpuesto) {
            let total = parseFloat(row.querySelector('[name="conceptoTotal"]').value) * (1 - (descuentoPorcentaje * 0.01));
            let impuesto = 0;
            let tipoImpuesto = row.querySelector('[name="conceptoImpuesto"]').value;
            switch (tipoImpuesto) {
                case 'ieps8': impuesto = 0.08; break;
                case 'ieps160iva': impuesto = (2.6 * 1.16) - 1; break;
                case 'iva': impuesto = 0.16; break;
            }
            impuestos += total * impuesto;
        }
        else {
            let total = parseFloat(row.querySelector('[name="conceptoTotal"]').value) * (1 - (descuentoPorcentaje * 0.01));
            let impuesto = 0;
            let tipoImpuesto = row.querySelector('[name="conceptoImpuesto"]').value;
            switch (tipoImpuesto) {
                case 'ieps8': impuesto = 0.08; break;
                case 'ieps160iva': impuesto = (2.6 * 1.16) - 1; break;
                case 'iva': impuesto = 0.16; break;
            }
            impuestos += (total / (1 + impuesto)) * impuesto;
        }
    });
    document.querySelector('[name="descuentoCantidad"]').value = descuentoCantidad.toFixed(2);
    document.querySelector('[name="subtotal"]').value = subtotal.toFixed(2);
    document.querySelector('[name="impuestos"]').value = impuestos.toFixed(2);
    if (propagate) {
        updateTotal();
    }
}

function updateTotal() {
    let subtotal = parseFloat(document.querySelector('[name="subtotal"]').value);
    let impuestos = parseFloat(document.querySelector('[name="impuestos"]').value);
    document.querySelector('[name="total"]').value = (subtotal + impuestos).toFixed(2);
}

function clearInputs() {
    document.querySelector('[name="aumentarImpuesto"]').checked = false;
    document.querySelectorAll('input').forEach(function(inp) {
        inp.value = '';
    });
    document.querySelector('[name="granTotal"]').value = '0.00';
    document.querySelector('[name="descuentoPorcentaje"]').value = '0';
    document.querySelector('[name="descuentoCantidad"]').value = '0.00';
    document.querySelector('[name="subtotal"]').value = '0.00';
    document.querySelector('[name="impuestos"]').value = '0.00';
    document.querySelector('[name="total"]').value = '0.00';

    document.querySelector('[name="activarRelacionados"]').checked = false;
}

/* NUMERIC */

function evKeydownIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

function evBlurDecimal() {
    let decimals = parseFloat(this.getAttribute('decimals') || 2);
    if (this.value != '') {
        let places = Tools.getDecimals(parseFloat(this.value));
        if (decimals && !places) {
            this.value = parseFloat(this.value).toFixed(0);
        }
        else {
            this.value = parseFloat(this.value).toFixed(decimals || places);
        }
    }
}

function evBlurPrice() {
    if (this.value != '') {
        this.value = parseFloat(this.value).toFixed(2);
    }
    else {
        this.value = (0).toFixed(2);
    }
}

function getConcepts() {
    let aumentarImpuesto = document.querySelector('[name="aumentarImpuesto"]').checked;
    let concepts = [];
    document.querySelectorAll('.factura-table-row').forEach(function(row) {
        if (aumentarImpuesto) {
            let concept = {
                id: row.dataset.id,
                unidad: row.querySelector('[name="conceptoUnidad"]').value,
                clave: row.querySelector('[name="conceptoClaveSAT"]').value,
                nombre: row.querySelector('[name="conceptoNombre"]').value,
                cantidad: row.querySelector('[name="conceptoCantidad"]').value,
                precio: row.querySelector('[name="conceptoPrecio"]').value,
                total: row.querySelector('[name="conceptoTotal"]').value,
                impuesto: row.querySelector('[name="conceptoImpuesto"]').value
            };
            concepts.push(concept);
        }
        else {
            let impuesto = 0;
            let tipoImpuesto = row.querySelector('[name="conceptoImpuesto"]').value;
            switch (tipoImpuesto) {
                case 'ieps8': impuesto = 0.08; break;
                case 'ieps160iva': impuesto = (2.6 * 1.16) - 1; break;
                case 'iva': impuesto = 0.16; break;
            }
            let precioReal = (parseFloat(row.querySelector('[name="conceptoPrecio"]').value) / (1 + impuesto)).toFixed(6);
            let totalReal = (parseFloat(row.querySelector('[name="conceptoTotal"]').value) / (1 + impuesto)).toFixed(6);
            let concept = {
                id: row.dataset.id,
                unidad: row.querySelector('[name="conceptoUnidad"]').value,
                clave: row.querySelector('[name="conceptoClaveSAT"]').value,
                nombre: row.querySelector('[name="conceptoNombre"]').value,
                cantidad: row.querySelector('[name="conceptoCantidad"]').value,
                precio: precioReal,
                total: totalReal,
                impuesto: row.querySelector('[name="conceptoImpuesto"]').value
            };
            concepts.push(concept);
        }
    });
    console.log(concepts);
    return concepts;
}

function getRelacionados() {
    let relacionados = [];
    document.querySelectorAll('.relacionados-table-row').forEach(function(row) {
        relacionados.push(row.dataset.uuid);
    });
    return relacionados;
}

function getFormData() {
    let data = {
        accesoID: document.querySelector('[name="emisorID"]').value,
        clienteUID: document.querySelector('[name="clienteUID"]').value,
        fechaFacturacion: document.querySelector('[name="fechaFacturacion"]').value,
        usoCFDI: document.querySelector('[name="usoCFDI"]').value,
        formaPago: document.querySelector('[name="formaPago"]').value,
        metodoPago: document.querySelector('[name="metodoPago"]').value,
        comentarios: document.querySelector('[name="comentarios"]').value,
        descuentoPorcentaje: document.querySelector('[name="descuentoPorcentaje"]').value,
        conceptos: getConcepts()
    }
    if (document.querySelector('[name="activarRelacionados"]').checked) {
        data['tipoRelacion'] = document.querySelector('[name="tipoRelacion"]').value;
        data['relacionados'] = getRelacionados();
    }
    return data;
}

/* GETS */

getGetValues();

function getGetValues() {
    let getArr = window.location.search.replace('?', '').split('&');
    for (let i=0; i<getArr.length; i++) {
        let get = getArr[i].split('=');
        getData[get[0]] = decodeURIComponent(get[1]).replace(/\+/g, ' ');
    }
    if (getData['tipo']) {
        switch (getData['tipo']) {
            case 'mostrador':
                if (getData['id']) {
                    loadTicketFromID(getData['id']);
                }
                else if (getData['fecha']) {
                    loadTicketFromDate(getData['fecha']);
                }
                break;
            case 'pedido':
                if (getData['id']) {
                    loadPedidoFromID(getData['id']);
                }
                else if (getData['fecha']) {
                    loadPedidoFromDate(getData['fecha']);
                }
                break;
        }
    }
}

/* Load Data */

function loadTicketFromID(ticketID) {
    Tools.ajaxRequest({
        data: {
            id: ticketID
        },
        url: 'functions/ticket/get-facturar',
        method: 'POST',
        waitMsg: new Wait('Cargando datos.'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (data['resultado']['clienteID'] == 1) {
                    if (clientsLoaded) {
                        let uid = clienteSI.container.querySelector('.selectinp-option').getAttribute('value');
                        clienteSI.setValue(uid);
                        clienteSI.ghostInput.dispatchEvent(new Event('change'));
                    }
                    else {
                        preloadClientUID = 1;
                    }
                }
                else if (data['resultado']['clienteUID'] != null) {
                    if (clientsLoaded) {
                        clienteSI.setValue(data['resultado']['clienteUID']);
                        clienteSI.ghostInput.dispatchEvent(new Event('change'));
                    }
                    else {
                        preloadClientUID = data['resultado']['clienteUID'];
                    }
                }
                if (data['resultado']['productos']['error'] == '') {
                    let concepts = data['resultado']['productos']['resultado'];
                    for (let i=0; i<concepts.length; i++) {
                        let concept = concepts[i];
                        let product = {
                            id: concept['productoID'],
                            nombre: concept['nombre'],
                            precio1: concept['precio'],
                            cantidad: concept['cantidad'],
                            unidad: concept['unidad'],
                            claveSAT: concept['claveSAT'],
                            impuesto: concept['impuesto']
                            
                        }
                        addConcept(product, true);
                    }
                }
            }
            else {
                new Message('Ocurrio un error al cargar el ticket.<br>' + data['error'], 'error');
            }
        }
    });
}

function loadPedidoFromID(pedidoID) {
    Tools.ajaxRequest({
        data: {
            id: pedidoID
        },
        url: 'functions/pedido/get-facturar',
        method: 'POST',
        waitMsg: new Wait('Cargando datos.'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (data['resultado']['clienteID'] == 1) {
                    if (clientsLoaded) {
                        let uid = clienteSI.container.querySelector('.selectinp-option').getAttribute('value');
                        clienteSI.setValue(uid);
                        clienteSI.ghostInput.dispatchEvent(new Event('change'));
                    }
                    else {
                        preloadClientUID = 1;
                    }
                }
                else if (data['resultado']['clienteUID'] != null) {
                    if (clientsLoaded) {
                        clienteSI.setValue(data['resultado']['clienteUID']);
                        clienteSI.ghostInput.dispatchEvent(new Event('change'));
                    }
                    else {
                        preloadClientUID = data['resultado']['clienteUID'];
                    }
                }
                if (data['resultado']['pedidos']['error'] == '') {
                    let concepts = data['resultado']['pedidos']['resultado'];
                    for (let i=0; i<concepts.length; i++) {
                        let concept = concepts[i];
                        let product = {
                            id: concept['productoID'],
                            nombre: concept['nombre'],
                            precio1: concept['precio'],
                            cantidad: concept['cantidad'],
                            unidad: concept['unidad'],
                            claveSAT: concept['claveSAT'],
                            impuesto: concept['impuesto']
                        }
                        addConcept(product, true);
                    }
                }
            }
            else {
                new Message('Ocurrio un error al cargar el ticket.<br>' + data['error'], 'error');
            }
        }
    });
}

function loadTicketFromDate(date) {
    Tools.ajaxRequest({
        data: {
            fecha: date
        },
        url: 'functions/ticket/get-all-facturar',
        method: 'POST',
        waitMsg: new Wait('Cargando datos.'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                let promise = new Promise(function(resolve) {
                    let products = {};
                    for (let i=0; i<result.length; i++) {
                        if (result[i]['productos']['error'] == '') {
                            let concepts = result[i]['productos']['resultado'];
                            for (let c=0; c<concepts.length; c++) {
                                let concept = concepts[c];
                                if (products[concept['productoID']]) {
                                    products[concept['productoID']]['cantidad'] += parseFloat(concept['cantidad']);
                                    products[concept['productoID']]['total'] += parseFloat(parseFloat(concept['total']).toFixed(2));
                                }
                                else {
                                    products[concept['productoID']] = {
                                        id: concept['productoID'],
                                        nombre: concept['nombre'],
                                        precio1: 0,
                                        cantidad: parseFloat(parseFloat(concept['cantidad']).toFixed(3)),
                                        total: parseFloat(parseFloat(concept['total']).toFixed(2)),
                                        unidad: concept['unidad'],
                                        claveSAT: concept['claveSAT'],
                                        impuesto: concept['impuesto']
                                    }
                                }
                            }
                        }
                    }
                    resolve(products);
                }).then(function(products){
                    for (let i in products) {
                        let product = products[i];
                        product['total'] = parseFloat(parseFloat(product['total']).toFixed(2));
                        product['cantidad'] = parseFloat(parseFloat(product['cantidad']).toFixed(3));
                        let price = product['total'] / product['cantidad'];
                        product['precio1'] = parseFloat(parseFloat(price).toFixed(6));
                        addConcept(product, true);
                    }
                });
            }
            else {
                new Message('Ocurrio un error al cargar el ticket.<br>' + data['error'], 'error');
            }
        }
    });
}

function loadPedidoFromDate(date) {
    Tools.ajaxRequest({
        data: {
            fecha: date
        },
        url: 'functions/pedido/get-all-facturar',
        method: 'POST',
        waitMsg: new Wait('Cargando datos.'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                let promise = new Promise(function(resolve) {
                    let products = {};
                    for (let i=0; i<result.length; i++) {
                        if (result[i]['pedidos']['error'] == '') {
                            let concepts = result[i]['pedidos']['resultado'];
                            for (let c=0; c<concepts.length; c++) {
                                let concept = concepts[c];
                                if (products[concept['productoID']]) {
                                    products[concept['productoID']]['cantidad'] += parseFloat(concept['cantidad']);
                                    products[concept['productoID']]['total'] += parseFloat(parseFloat(concept['total']).toFixed(2));
                                }
                                else {
                                    products[concept['productoID']] = {
                                        id: concept['productoID'],
                                        nombre: concept['nombre'],
                                        precio1: 0,
                                        cantidad: parseFloat(parseFloat(concept['cantidad']).toFixed(3)),
                                        total: parseFloat(parseFloat(concept['total']).toFixed(2)),
                                        unidad: concept['unidad'],
                                        claveSAT: concept['claveSAT'],
                                        impuesto: concept['impuesto']
                                    }
                                }
                            }
                        }
                    }
                    resolve(products);
                }).then(function(products){
                    for (let i in products) {
                        let product = products[i];
                        product['total'] = parseFloat(parseFloat(product['total']).toFixed(2));
                        product['cantidad'] = parseFloat(parseFloat(product['cantidad']).toFixed(3));
                        let price = product['total'] / product['cantidad'];
                        product['precio1'] = parseFloat(parseFloat(price).toFixed(6));
                        addConcept(product, true);
                    }
                });
            }
            else {
                new Message('Ocurrio un error al cargar el ticket.<br>' + data['error'], 'error');
            }
        }
    });
}

/* SUBMIT */
    
document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

document.querySelector('.form-submit').addEventListener('click', function(ev) {
    let filled = isFormFilled();
    if (filled) {
        submitForm();
    }
});

function submitForm() {
    document.querySelector('.form-submit').setAttribute('disabled', '');
    Tools.ajaxRequest({
        data: getFormData(),
        url: 'functions/factura/facturar',
        method: 'POST',
        waitMsg: new Wait('Facturando.'),
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                new Question('Facturado con exito.', '¿Desea mandarla por correo?', function() {
                    EmailFactura.open(document.querySelector('[name="clienteEmails"]').value, result['uid'], document.querySelector('[name="emisorID"]').value, function() {
                        location = '/admin/facturas';
                    });
                }, function() {
                    location = '/admin/facturas';
                });
            }
            else {
                console.log(data);
                switch (data['error']) {
                    case 'FACTURA_ERROR':
                        if (data['errorLog']['message']) {
                            if (data['errorLog']['message']['message']) {
                                new Message('Ocurrio un error al facturar<br>"' + data['errorLog']['message']['message'] + '"', 'error');
                            }
                            else {
                                new Message('Ocurrio un error al facturar<br>"' + data['errorLog']['message'] + '"', 'error');
                            }
                        }
                        else if (data['errorLog']) {
                            new Message('Ocurrio un error al facturar<br>' + JSON.stringify(data['errorLog']), 'error');
                        }
                        else {
                            new Message('Ocurrio un error al facturar<br>"' + data['error'] + '"', 'error');
                        }
                        break;
                    default:
                        new Message('Ocurrio un error al facturar<br>"' + data['error'] + '"', 'error');
                        break;
                }
            }
        }
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#cliente-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function propagateTotal() {
    let aumentarImpuesto = document.querySelector('[name="aumentarImpuesto"]').checked;
    let descuentoPorcentaje = parseFloat(document.querySelector('[name="descuentoPorcentaje"]').value);
    let total = parseFloat(parseFloat(document.querySelector('[name="total"]').value).toFixed(2)) / (1 - (descuentoPorcentaje * 0.01));
    let rows = document.querySelectorAll('.factura-table-row');
    let equalTotal = total / rows.length;
    rows.forEach(function(row) {
        if (aumentarImpuesto) {
            let impuesto = 1;
            let tipoImpuesto = row.querySelector('[name="conceptoImpuesto"]').value;
            switch (tipoImpuesto) {
                case 'ieps8':
                    impuesto = 1.08;
                    break;
                case 'ieps160iva':
                    impuesto = 2.6 * 1.16;
                    break;
                case 'iva':
                    impuesto = 1.16;
                    break;
            }
            let rowQuantity = parseFloat(row.querySelector('[name="conceptoCantidad"]').value);
            let price = parseFloat(((equalTotal / impuesto) / rowQuantity).toFixed(6));
            row.querySelector('[name="conceptoPrecio"]').value = price;
            row.querySelector('[name="conceptoTotal"]').value = parseFloat((rowQuantity * price).toFixed(6));
        }
        else {
            let rowQuantity = parseFloat(row.querySelector('[name="conceptoCantidad"]').value);
            let price = parseFloat((equalTotal / rowQuantity).toFixed(6));
            row.querySelector('[name="conceptoPrecio"]').value = price;
            row.querySelector('[name="conceptoTotal"]').value = parseFloat((rowQuantity * price).toFixed(6));
        }
    });     
    updateGranTotal(false);
}

document.querySelector('[name="total"]').addEventListener('input', function() {
    //propagateTotal();
});
document.querySelector('[name="total"]').addEventListener('focus', function() {
    this.select();
});
document.querySelector('[name="total"]').addEventListener('blur', evBlurPrice);
document.querySelector('[name="total"]').addEventListener('blur', updateTotal);
document.querySelector('[name="total"]').addEventListener('keydown', evKeydownIsNumber);

let ClientAdder = new class {
    constructor() {
        let self = this;
        this.uid = '';
        this.overlay = document.querySelector('.client-adder-overlay');
        this.form = this.overlay.querySelector('.client-adder-form');
        this.form.querySelector('.client-adder-accept').addEventListener('click', function() {
            self.submit();
        });
        this.form.querySelector('.client-adder-close').addEventListener('click', function() {
            self.close();
        });
    }
    getData() {
        let data = new FormData(this.form);
        data.append('uid', this.uid);
        data.append('accesoID', document.querySelector('[name="emisorID"]').value);
        return data;
    }
    submit() {
        let self = this;
        this.form.querySelector('.client-adder-accept').setAttribute('disabled', '');
        this.form.querySelector('.client-adder-close').setAttribute('disabled', '');
        if (!Tools.checkFormFilled(this.form)) {
            self.form.querySelector('.client-adder-accept').removeAttribute('disabled');
            self.form.querySelector('.client-adder-close').removeAttribute('disabled');
            new Message('Por favor rellene los campos marcados para continuar.', 'warning');
            return;
        }
        if (this.uid == '') {
            Tools.ajaxRequest({
                data: this.getData(),
                url: 'functions/factura/add-client',
                method: 'POST',
                contentType: false,
                waitMsg: new Wait('Registrando cliente'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Cliente registrado con exito.', 'success', function() {
                            clienteSI.searchInput.value = 'Cargando clientes...';
                            clienteSI.searchInput.setAttribute('disabled', '');
                            loadClients(document.querySelector('[name="emisorID"]').value);
                            self.close();
                        });
                    }
                    else {
                        self.form.querySelector('.client-adder-accept').removeAttribute('disabled');
                        self.form.querySelector('.client-adder-close').removeAttribute('disabled');
                        if (data['error'] == 'GET_DATA_ERROR') {
                            new Message('Ocurrio un error al tratar de dar de alta al cliente. Por favor intente de nuevo.<br>' + JSON.stringify(data['errorLog']['message']), 'error');
                        }
                        else {
                            new Message('Ocurrio un error al tratar de dar de alta al cliente. Por favor intente de nuevo.<br>' + data['error'], 'error');
                        }
                    }
                }
            });
        }
        else {
            Tools.ajaxRequest({
                data: this.getData(),
                url: 'functions/factura/edit-client',
                method: 'POST',
                contentType: false,
                waitMsg: new Wait('Guardando cliente'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Cliente guardado con exito.', 'success', function() {
                            clienteSI.searchInput.value = 'Cargando clientes...';
                            clienteSI.searchInput.setAttribute('disabled', '');
                            loadClients(document.querySelector('[name="emisorID"]').value);
                            self.close();
                        });
                    }
                    else {
                        self.form.querySelector('.client-adder-accept').removeAttribute('disabled');
                        self.form.querySelector('.client-adder-close').removeAttribute('disabled');
                        if (data['error'] == 'GET_DATA_ERROR') {
                            new Message('Ocurrio un error al tratar de guardar el cliente. Por favor intente de nuevo.<br>' + JSON.stringify(data['errorLog']['message']), 'error');
                        }
                        else {
                            new Message('Ocurrio un error al tratar de guardar el cliente. Por favor intente de nuevo.<br>' + data['error'], 'error');
                        }
                    }
                }
            });
        }
    }
    clear() {
        this.form.querySelector('.client-adder-accept').textContent = 'Registrar';
        this.form.querySelector('[name="nombres"]').value = '';
        this.form.querySelector('[name="apellidos"]').value = '';
        this.form.querySelector('[name="emails"]').value = '';
        this.form.querySelector('[name="telefono"]').value = '';
        this.form.querySelector('[name="razonSocial"]').value = '';
        this.form.querySelector('[name="rfc"]').value = '';
        this.form.querySelector('[name="calle"]').value = '';
        this.form.querySelector('[name="codigoPostal"]').value = '';
        this.form.querySelector('[name="numeroExterior"]').value = '';
        this.form.querySelector('[name="numeroInterior"]').value = '';
        this.form.querySelector('[name="colonia"]').value = '';
        this.form.querySelector('[name="ciudad"]').value = '';
        this.form.querySelector('[name="estado"]').value = '';
    }
    open(uid = '') {
        let self = this;
        this.uid = uid;
        this.clear();
        if (this.uid != '') {
            let client = clientsList.find(function(obj) {
                return obj['UID'] == self.uid;
            });
            if (client) {
                this.form.querySelector('[name="nombres"]').value = client['Contacto']['Nombre'];
                this.form.querySelector('[name="apellidos"]').value = client['Contacto']['Apellidos'];
                this.form.querySelector('[name="emails"]').value = client['datosExtras'] ? client['datosExtras']['emails'] : client['Contacto']['Email'];
                this.form.querySelector('[name="telefono"]').value = client['Contacto']['Telefono'];
                this.form.querySelector('[name="razonSocial"]').value = client['RazonSocial'];
                this.form.querySelector('[name="rfc"]').value = client['RFC'];
                this.form.querySelector('[name="calle"]').value = client['Calle'];
                this.form.querySelector('[name="codigoPostal"]').value = client['CodigoPostal'];
                this.form.querySelector('[name="numeroExterior"]').value = client['Numero'];
                this.form.querySelector('[name="numeroInterior"]').value = client['Interior'];
                this.form.querySelector('[name="colonia"]').value = client['Colonia'];
                this.form.querySelector('[name="ciudad"]').value = client['Ciudad'];
                this.form.querySelector('[name="estado"]').value = client['Estado'];
                this.form.querySelector('.client-adder-accept').textContent = 'Guardar';
            }
        }
        this.form.querySelector('.client-adder-accept').removeAttribute('disabled');
        this.form.querySelector('.client-adder-close').removeAttribute('disabled');
        this.overlay.style.display = 'flex';
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'appear 0.2s ease-in-out';
    }
    close() {
        let self = this;
        this.overlay.style.animation = '';
        this.overlay.offsetHeight;
        this.overlay.style.animation = 'disappear 0.2s ease-in-out';
        setTimeout(function() {
            self.overlay.style.display = 'none';
        }, 200);
    }
}

document.querySelector('.client-add').addEventListener('click', function() {
    ClientAdder.open();
});
document.querySelector('.client-edit').addEventListener('click', function() {
    ClientAdder.open(clienteSI.value);
});

new CheckSwitch(document.querySelector('[name="aumentarImpuesto"]'), 'SI', 'NO');
document.querySelector('[name="aumentarImpuesto"]').addEventListener('change', function() {
    updateGranTotal();
});


new CheckSwitch(document.querySelector('[name="activarRelacionados"]'), 'SI', 'NO');
document.querySelector('[name="activarRelacionados"]').addEventListener('change', function() {
    if (this.checked) {
        document.querySelector('.relacionados-container').style.display = '';
    }
    else {
        document.querySelector('.relacionados-container').style.display = 'none';
    }
});

// TINY PRODUCTO

document.querySelector('.new-producto-btn').addEventListener('click', function() {    
    TinyProducto.open('');
});

TinyProducto.onSuccess = function(producto) {
    console.log(producto);
    
    producto['proveedorID'] = null;
    producto['proveedorNombre'] = 'S/D';
    producto['categoriaID'] = null;
    producto['categoriaNombre'] = null;
    producto['existencia'] = 0;
    producto['bloqueado'] = 0;
    producto['facturar'] = 1;
    producto['claveSAT'] = '';
    producto['impuesto'] = 'tasacero';

    productsList.push(producto);

    let option = {
        'name': '[' + ('000' + producto['folio']).slice(-4) + '] ' + producto['nombre'],
        'value': producto['id'],
        'keys': [producto['folio'].toString(), producto['nombre']]
    };

    productoSI.addOption(option);

    addConcept(producto);
}

// FACTURAS RELACIONADAS



function addRelacionado(factura) {
    if (factura != null) {
        let row = document.createElement('div');
        row.className = 'relacionados-table-row';
        row.dataset.uid = factura['uid'];
        row.dataset.uuid = factura['uuid'];
        row.innerHTML = (
            '<div class="relacionados-table-column"><input name="facturaFecha" type="text" value="' + factura['fecha'] + '" disabled></div>' +
            '<div class="relacionados-table-column"><input name="facturaFolio" type="text" value="' + factura['folio'] + '" disabled></div>' +
            '<div class="relacionados-table-column"><input name="facturaUUID" type="text" value="' + factura['uuid'] + '" disabled></div>' +
            '<div class="relacionados-table-column"><div class="actions-container"><img class="action-delete-row" src="imgs/icons/actions/delete.png"></div></div>'
        );

        row.querySelector('.action-delete-row').addEventListener('click', function() {
            new Question('Se eliminara este CFDI de los relacionados', '¿Esta de acuerdo?', function() {
                row.remove();
            });
        });

        document.querySelector('.relacionados-table').appendChild(row);
    }
}

/* AGREAGAR FACTURAS */

const FacturaAdder = new class {
    constructor() {
        let self = this;
        this.facturasAjax = null;
        this.overlay = document.querySelector('.facturadder-overlay');
        this.container = this.overlay.querySelector('.facturadder-container');
        this.table = this.container.querySelector('.facturadder-table');
        this.fechaInput = this.container.querySelector('.facturadder-fecha');
        this.fechaPicker = new DatePicker(this.fechaInput, {
            selectUntil: 'month'
        });

        this.fechaInput.addEventListener('change', function() {
            let cliente = clientsList.find(function(obj) {
                return obj['UID'] == clienteSI.value;
            });
            FacturaAdder.loadFacturas(cliente['RFC']);
        });

        this.container.querySelector('.facturadder-table').addEventListener('click', function(ev) {
            if (ev.target.classList.contains('facturadder-selector')) {
                ev.target.parentElement.parentElement.classList.toggle('selected');
                let selectedsCount = self.table.querySelectorAll('.selected').length;
                if (selectedsCount != 0) {                    
                    self.container.querySelector('.facturadder-add-btn').disabled = false;
                    self.container.querySelector('.facturadder-add-btn').textContent = 'Agregar (' + selectedsCount + ')';
                }
                else {                    
                    self.container.querySelector('.facturadder-add-btn').disabled = true;
                    self.container.querySelector('.facturadder-add-btn').textContent = 'Agregar';
                }
            }
        });
        this.container.querySelector('.facturadder-add-btn').addEventListener('click', function() {
            let facturas = self.getSelecteds();
            for (let i=0; i<facturas.length; i++) {
                if (document.querySelector('.relacionados-table [data-uid="' + facturas[i]['uid'] + '"]')) {
                    continue;
                }
                addRelacionado(facturas[i]);
            }
            self.close();
        });
        this.container.querySelector('.facturadder-close-btn').addEventListener('click', function() {
            self.close();
        });
    }
    loadFacturas(rfc) {
        let self = this;
        if (rfc == '') {
            return;
        }
        if (this.facturasAjax) {
            this.facturasAjax.abort();
        }
        this.clearTable();
        let loadingRow = this.createLoading();
        this.facturasAjax = Tools.ajaxRequest({
            data: {
                filtros: {
                    accesoID: document.querySelector('[name="emisorID"]').value,
                    rfc: rfc,
                    fecha: this.fechaInput.value,
                    limite: 1000
                }
            },
            url: 'functions/factura/get-all',
            method: 'POST',
            success: function(response) {
                loadingRow.remove();
                let result = JSON.parse(response);
                if (result['error'] == '') {
                    self.addFacturas(result['resultado']);
                }
            }
        });
    }
    createLoading() {
        let row = document.createElement('div');
        row.className = 'facturadder-table-loading';
        row.innerHTML = (
            '<div class="loading"></div>'+
            '<p>Cargando...</p>'
        );
        this.table.appendChild(row);
        return row;
    }
    clearTable() {
        this.table.querySelectorAll('.facturadder-table-loading').forEach(function(row) {
            row.remove();
        });
        this.table.querySelectorAll('.facturadder-table-row').forEach(function(row) {
            row.remove();
        });
    }
    addFacturas(facturas) {
        let rowsFragment = document.createDocumentFragment();
        for (let i=0; i<facturas.length; i++) {
            let factura = facturas[i];
            if (factura['datosExtra']) {
                let row = this.generateRow(factura);
                rowsFragment.appendChild(row);
            }
        }
        this.container.querySelector('.facturadder-table').appendChild(rowsFragment);
    }
    generateRow(factura) {
        let row = document.createElement('div');
        let saldoAnterior = parseFloat(factura['Total']) - parseFloat(factura['datosExtra']['pagoAcumulado']);
        row.className = 'facturadder-table-row';
        row.dataset.uid = factura['UID'];
        row.dataset.uuid = factura['UUID'];
        row.dataset.fecha = factura['FechaTimbrado'].split('-').reverse().join('/');
        row.dataset.folio = factura['Folio'];
        row.innerHTML = (
            '<div class="facturadder-table-column"><div class="facturadder-selector"></div></div>'+
            '<div class="facturadder-table-column">'+
                '<p>' + row.dataset.fecha + '</p>'+
            '</div>'+
            '<div class="facturadder-table-column">'+
                '<p>' + factura['Folio'] + '</p>'+
            '</div>'+
            '<div class="facturadder-table-column">'+
                '<p>' + factura['UUID'] + '</p>'+
            '</div>'
        );
        return row;
    }
    getSelecteds() {
        let facturas = [];
        this.table.querySelectorAll('.selected').forEach(function(row) {
            facturas.push({
                folio: row.dataset.folio,
                fecha: row.dataset.fecha,
                uid: row.dataset.uid,
                uuid: row.dataset.uuid
            });
        });
        return facturas;
    }
    disableDuplicateds() {
        let uids = [];
        document.querySelectorAll('.relacionados-table > .relacionados-table-row').forEach(function(row) {
            uids.push(row.dataset.uid);
        });
        this.table.querySelectorAll('.facturadder-table-row').forEach(function(row) {
            row.style.display = '';
        });
        for (let i=0; i<uids.length; i++) {
            let row = this.table.querySelector('[data-uid="' + uids[i] + '"]');
            if (row) {
                row.style.display = 'none';
            }
        }
    }
    open() {
        this.table.querySelectorAll('.selected').forEach(function(row) {
            row.classList.remove('selected');
        });
        this.disableDuplicateds();
        this.container.querySelector('.facturadder-add-btn').textContent = 'Agregar';
        this.container.querySelector('.facturadder-add-btn').disabled = true;
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
    }
    close() {
        let self = this;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = '';
        }, 200);
    }
}

document.querySelector('.add-relacionado-btn').addEventListener('click', function() {
    let clienteUID = document.querySelector('[name="clienteUID"]').value;
    if (clienteUID == '') {
        new Message('Seleccione un cliente primero', 'warning');
        return;
    }
    FacturaAdder.open();
});

// PASOS CON ENTER

document.querySelectorAll('[enterfocus]').forEach(function(elem) {
    elem.addEventListener('keydown', function(ev) {
        if (ev.key == 'Enter') {
            ev.preventDefault();
            let enabledInputs = document.querySelectorAll('[enterfocus="' + this.getAttribute('enterfocus') +'"]');
            let idx = -1;
            for (let i=0; i<enabledInputs.length; i++) {
                if (enabledInputs[i].isEqualNode(this)) {
                    idx = i;
                    break;
                }
            }
            if (idx == -1) {
                return;
            }
            if (idx == enabledInputs.length - 1) {
                return;
            }
            enabledInputs[idx + 1].focus();
            if (enabledInputs[idx + 1].tagName == 'INPUT') {
                setTimeout(function() {
                    enabledInputs[idx + 1].select();
                });
            }
        }
    });
});

})();