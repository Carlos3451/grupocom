const { degrees, PDFDocument, rgb, StandardFonts } = PDFLib;

(function(){

let facturasList = [];
let clientsList = [];

function removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, '');
}

Lista.actionPrint['active'] = true;
Lista.actionPrint['unique'] = true;
Lista.printFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value,
            UID: uid
        },
        method: 'POST',
        url: 'functions/factura/pdf',
        responseType: 'blob',
        waitMsg: new Wait('Obteniendo PDF...'),
        success: function(response) {
            this.waitMsg.destroy();
            //window.open(window.URL.createObjectURL(response));
            modidyPDF(response);
        }
    });
}

async function modidyPDF(data) {
    let dataBytes = await data.arrayBuffer();

    let pdf = await PDFDocument.load(dataBytes);
    let helveticaFont = await pdf.embedFont(StandardFonts.HelveticaBold);

    let pages = pdf.getPages();
    let pagesCopiesNumbers = [];
    for (let i=0; i<pages.length; i++) {
        pagesCopiesNumbers.push(i);
    }

    let pagesCopies = await pdf.copyPages(pdf, pagesCopiesNumbers);

    for (let i=0; i<pagesCopies.length; i++) {
        pdf.addPage(pagesCopies[i]);
    }

    pages = pdf.getPages();

    let xx = -70;
    let yy = 130;

    for (let i=pages.length / 2; i<pages.length; i++) {
        pages[i].drawRectangle({
            x: 0 + xx,
            y: 505 + yy,
            rotate: degrees(45),
            width: 400,
            height: 40,
            color: rgb(0.070, 0.043, 0.458),
        });
        pages[i].drawText('COPIA', {
            x: 100 + xx,
            y: 615 + yy,
            size: 32,
            rotate: degrees(45),
            font: helveticaFont,
            color: rgb(1.0, 1.0, 1.0)
        });
    }

    let pdfBytes = await pdf.save();
    window.open(window.URL.createObjectURL(new Blob([pdfBytes], {type: 'application/pdf'})));
}

Lista.actionDownload['active'] = true;
Lista.actionDownload['unique'] = true;
Lista.downloadFn = function(selecteds) {
    let waitMsg = new Wait('Obteniendo datos de la factura.');
    let uid = selecteds[0].dataset.uid;
    let promises = [];
    let folio = selecteds[0].dataset.folio.replace('F ', '');
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura/xml',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura/pdf',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    Promise.all(promises).then(function(result) {
        let zip = new JSZip();
        zip.file('factura-' + folio + '.pdf', result[1]);
        zip.file('factura-' + folio + '.xml', result[0]);
        zip.generateAsync({type: 'blob'}).then(function(content) {
            waitMsg.destroy();
            let downloadElem = document.createElement('a');
            downloadElem.setAttribute('href', window.URL.createObjectURL(content));
            downloadElem.setAttribute('download', 'factura-' + folio + '.zip');
            downloadElem.style.display = 'none';
            document.querySelector('body').appendChild(downloadElem);
            downloadElem.click();
            downloadElem.remove();
        });
    });
}

Lista.actionEmail['active'] = true;
Lista.actionEmail['unique'] = true;
Lista.emailFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    let email = '';
    let client = getClientByRFC(selecteds[0].dataset.rfc);
    if (client) {
        email = client['datosExtras'] ? client['datosExtras']['emails'] : client['Contacto']['Email'];
    }
    EmailFactura.open(email, uid, document.querySelector('[name="accesoID"]').value);
}

Lista.addAction('Copiar', 'imgs/icons/actions/copy.png', true, function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    let uuid = selecteds[0].dataset.uuid;
    let accesoID = document.querySelector('[name="accesoID"]').value;
    window.location = 'facturas-v4/agregar?version=3&accesoid=' + accesoID + '&copiauid=' + encodeURIComponent(uid);
});

/*
Lista.addAction('Complementos de Pago', 'imgs/icons/pago-efectivo.png', true, function(selecteds) {
    if (selecteds[0].dataset.tipo != 'PPD') {
        new Message('Complementos de Pago solo disponible para Pago en Parcialidades o Diferidos', 'warning');
        return;
    }
    let uid = selecteds[0].dataset.uid;
    let uuid = selecteds[0].dataset.uuid;
    let clienteUID = selecteds[0].dataset.clienteuid;
    let total = selecteds[0].dataset.total;
    let clientData = getClientByRFC(selecteds[0].dataset.rfc);
    ComplementosPago.open(uid, uuid, clientData, clienteUID, total, document.querySelector('[name="accesoID"]').value);
});
*/
/*
Lista.actionPay['active'] = true;
Lista.actionPay['unique'] = true;
Lista.payFn = function(selecteds) {
    if (selecteds[0].hasAttribute('cancelled')) {
        new Message('El pedido esta cancelado.', 'warning');
    }
    else if (selecteds[0].hasAttribute('payed')) {
        new Message('Este pedido ya esta completamente pagado.', 'warning');
    }
    else {
        let dataID = selecteds[0].getAttribute('data-id');
        let total = selecteds[0].getAttribute('data-total');
        let payed = selecteds[0].getAttribute('data-pagado');
        let rest = parseFloat(total - payed);
        Payment.open(rest);        
        Payment.onAccept = function(method, quantity) {
            let date = new Date();
            Tools.ajaxRequest({
                data: {
                    pedidoID: dataID,
                    fecha: Math.floor(date.getTime() / 1000),
                    metodo: method,
                    cantidad: quantity
                },
                method: 'POST',
                url: 'functions/pedido/add-payment',
                waitMsg: new Wait('Agregando pago.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Pago agregado con exito.', 'success');
                        payed = parseFloat(payed) + parseFloat(quantity);
                        rest = parseFloat(total - payed);
                        selecteds[0].setAttribute('data-pagado', payed);
                        if (rest <= 0) {
                            selecteds[0].setAttribute('payed', '');
                            selecteds[0].querySelectorAll('.list-column')[7].querySelector('p').textContent = 'Pagado';
                        }
                        else {
                            selecteds[0].querySelectorAll('.list-column')[7].querySelector('p').textContent = '$' + rest.toFixed(2);
                        }
                    }
                    else {
                        new Message('Ocurrio un error al tratar de agregar el pago.<br>' + data['error'], 'success');
                    }
                }
            });
        }
    }
}
*/

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = true;
    Lista.cancelFn = function(selecteds) {
        let facturaUID = selecteds[0].dataset.uid;
        if (!selecteds[0].hasAttribute('cancelled')) {
            new OptionsQuestion('Cancelar factura', 'Elija el motivo de la cancelación.', [
                {
                    'text': '02 - Comprobante emitido con errores sin relación',
                    'value': '02'
                },
                {
                    'text': '03 - No se llevó a cabo la operación',
                    'value': '03'
                },
                {
                    'text': '04 - Operación nominativa relacionada en la factura global',
                    'value': '04'
                }
            ], function(motivo) {
                Tools.ajaxRequest({
                    data: {
                        accesoID: document.querySelector('[name="accesoID"]').value,
                        uid: facturaUID,
                        motivo: motivo
                    },
                    method: 'POST',
                    url: 'functions/factura/cancel',
                    waitMsg: new Wait('Cancelando factura.', true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Factura cancelada con exito.', 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            if (data['errorLog'] && data['errorLog']['message']) {                                    
                                new Message('Ocurrio un error al tratar de cancelar la factura.' + '<br>' + data['errorLog']['message'], 'error');
                            }
                            else {
                                new Message('Ocurrio un error al tratar de cancelar la factura.', 'error');
                            }
                        }
                    }
                });
            });
        }
        else {
            new Message('No se puede cancelar una factura ya cancelada.', 'warning');
        }
    }
}

Lista.selectJustOne = true;

SortController.setOnMethodChange(function() {});

function reloadFacturas() {
    SortController.load();
    Lista.load('factura/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let emisor = document.querySelector('[name="accesoID"]').children[document.querySelector('[name="accesoID"]').selectedIndex].textContent;
        let date = data['FechaTimbrado'].split('-').reverse().join('/');
        return [
            data['Folio'],
            date,
            data['RazonSocialReceptor'],
            emisor,
            (data['datosExtra'] ? data['datosExtra']['usuarioNombre'] : '-'),
            data['Status'],
            (data['datosExtra'] ? ('<div class="paystatus-container"><input type="checkbox" ' + (data['datosExtra']['pagado'] == 1 ? 'checked' : '') + ' name="filaEstadoPago"><p>' + (data['datosExtra']['pagado'] == 1 ? 'Pagado' : 'Sin pagar') + '</p></div>') : '-'),
            (data['datosExtra'] ? data['datosExtra']['metodoPago'] : '-'),
            '$' + parseFloat(data['Total']).toFixed(2)
        ];
    }, function(data) {
        let payed = data['total'] - data['pagado'];
        let attrs = [
            ['data-uid', data['UID']],
            ['data-uuid', data['UUID']],
            ['data-folio', data['Folio']],
            ['data-rfc', data['Receptor']],
            ['data-total', data['Total']],
            ['data-email', data['datosExtra'] ? (data['datosExtra']['clienteEmail'] != null ? data['datosExtra']['clienteEmail'] : '') : ''],
            ['data-clienteuid', data['datosExtra'] ? data['datosExtra']['clienteUID'] : ''],
            ['data-tipo', data['datosExtra'] ? data['datosExtra']['metodoPago'] : '-']
        ];
        if (data['Status'] == 'cancelada') {
            attrs.push(['cancelled', '']);
        }
        if (data['datosExtra'] && data['datosExtra']['pagado'] == 1) {
            attrs.push(['pagado', '']);
        }
        if (data['datosExtra'] && data['datosExtra']['id']) {
            attrs.push(['data-id', data['datosExtra']['id']]);
        }
        return attrs;
    });
}

document.querySelector('.list-container').addEventListener('change', function(ev) {
    if (ev.target.hasAttribute('name') && ev.target.getAttribute('name') == 'filaEstadoPago') {
        let row = ev.target.closest('.list-row');
        let id = row.dataset.id;
        let checked = ev.target.checked;
        if (checked) {
            ev.target.parentElement.querySelector('p').textContent = 'Pagado';
            row.setAttribute('pagado', '');
        }
        else {
            ev.target.parentElement.querySelector('p').textContent = 'Sin Pagar';
            row.removeAttribute('pagado');
        }
        Tools.ajaxRequest({
            data: {
                id: id,
                pagado: checked ? 1 : 0
            },
            url: 'functions/factura/change-pay-status',
            method: 'POST',
            waitMsg: new Wait('Cambiando estado de pago.'),
            success: function(response) {
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Estado de pago cambiado con exito.', 'success');
                }
                else {
                    new Message('Ocurrio un error al tratar de cambiar el estado de pago.<br>' + data['error'], 'error');
                }
            }
        });
    }
});

SortController.setMethods([
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['RFC', 'select', 'accesoID', []],
        ['Cliente', 'select', 'clienteRFC', [
            ['Todos', 0]
        ]]
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    selectUntil: 'month'
});

function loadClients() {
    clientsList = [];
    Lista.clear();
    Lista.createLoading();
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value
        },
        method: 'POST',
        url: 'functions/factura/get-all-clients',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                clientsList = data['resultado'];
                let optionsFragment = document.createDocumentFragment();
                for (let i=0; i<clientsList.length; i++) {
                    let cliente = clientsList[i];
                    let option = document.createElement('option');
                    option.value = cliente['RFC'];
                    option.textContent = cliente['RazonSocial'];
                    optionsFragment.appendChild(option);
                }
                document.querySelector('[name="clienteRFC"]').appendChild(optionsFragment);
            }
            reloadFacturas();
        }
    });
}

function getClientByRFC(rfc) {
    return clientsList.find(function(value) {
        return value['RFC'] == rfc;
    });
}

Tools.ajaxRequest({
    url: 'functions/factura/get-all-access',
    method: 'GET',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let result = data['resultado'];
            let optionsFrag = document.createDocumentFragment();
            for (let i=0; i<result.length; i++) {
                let option = document.createElement('option');
                option.value = result[i]['id'];
                option.textContent = result[i]['nombre'];
                optionsFrag.appendChild(option);
            }
            document.querySelector('[name="accesoID"]').appendChild(optionsFrag);
            if (window.localStorage.getItem('factura-acceso-id')) {
                document.querySelector('[name="accesoID"]').value = window.localStorage.getItem('factura-acceso-id');
            }
            loadClients();
        }
    }
});

document.querySelector('[name="fecha"]').addEventListener('change', function() {
    reloadFacturas();
});

document.querySelector('[name="accesoID"]').addEventListener('change', function() {
    window.localStorage.setItem('factura-acceso-id', this.value);
    loadClients();
});

document.querySelector('[name="clienteRFC"]').addEventListener('change', function() {
    let rfc = this.value;
    document.querySelectorAll('.list-row').forEach(function(row) {
        row.style.display = 'none';
        if (row.dataset.rfc == rfc || rfc == 0) {
            row.style.display = '';
        }
    });
});

})();