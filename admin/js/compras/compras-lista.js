(function(){

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/compras/ver/" + dataID;
}

Lista.actionPay['active'] = true;
Lista.actionPay['unique'] = true;
Lista.payFn = function(selecteds) {
    if (selecteds[0].hasAttribute('cancelled')) {
        new Message('El compra esta cancelado.', 'warning');
    }
    else if (selecteds[0].hasAttribute('payed')) {
        new Message('Este compra ya esta completamente pagado.', 'warning');
    }
    else {
        let dataID = selecteds[0].getAttribute('data-id');
        let total = selecteds[0].getAttribute('data-total');
        let payed = selecteds[0].getAttribute('data-pagado');
        let rest = parseFloat(total - payed);
        Payment.open(rest);        
        Payment.onAccept = function(method, quantity) {
            let date = new Date();
            Tools.ajaxRequest({
                data: {
                    compraID: dataID,
                    fecha: Math.floor(date.getTime() / 1000),
                    metodo: method,
                    cantidad: quantity
                },
                method: 'POST',
                url: 'functions/compra/add-payment',
                waitMsg: new Wait('Agregando pago.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Pago agregado con exito.', 'success');
                        payed = parseFloat(payed) + parseFloat(quantity);
                        rest = parseFloat(total - payed);
                        selecteds[0].setAttribute('data-pagado', payed);
                        if (rest <= 0) {
                            selecteds[0].setAttribute('payed', '');
                            selecteds[0].querySelectorAll('.list-column')[6].querySelector('p').textContent = 'Pagado';
                        }
                        else {
                            selecteds[0].querySelectorAll('.list-column')[6].querySelector('p').textContent = '$' + rest.toFixed(2);
                        }
                    }
                    else {
                        new Message('Ocurrio un error al tratar de agregar el pago.<br>' + data['error'], 'success');
                    }
                }
            });
        }
    }
}

if (privilegiosNivel > 1) {
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        if (!selecteds[0].hasAttribute('cancelled')) {
            window.location = "/admin/compras/editar/" + dataID;
        }
        else {
            new Message('No se puede editar un compra cancelado.', 'warning');
        }
    }
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = false;
    Lista.cancelFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se cancelara la compra seleccionada.',
            msgWait: 'Cancelando compra.',
            msgSuccess: 'Compra cancelada con exito.',
            msgError: 'Ocurrio un error al tratar de cancelar la compra.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se cancelaran las compras seleccionadas.',
                msgWait: 'Cancelando compras.',
                msgSuccess: 'Compras canceladas con exito.',
                msgError: 'Ocurrio un error al tratar de cancelar las compras.'
            };
        }
        let ids = [];
        let cancelleds = 0;
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
            if (selecteds[i].hasAttribute('cancelled')) {
                cancelleds += 1;
            }
        }
        if (cancelleds == 0) {
            new Confirm(requestObj.msgQuestion, 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        ids: ids
                    },
                    method: 'POST',
                    url: 'functions/compra/cancel',
                    waitMsg: new Wait(requestObj['msgWait'], true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message(requestObj['msgSuccess'], 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        }
        else {
            if (selecteds.length == 1) {
                new Message('No se puede cancelar una compra ya cancelada.', 'warning');
            }
            else if (selecteds.length == cancelleds) {
                new Message('No se pueden cancelar compras ya canceladas.', 'warning');
            }
            else {
                new Message('Existen algunas compras seleccionadas que ya estan canceladas.', 'warning');                
            }
        }
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('compra/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let payed = data['total'] - data['pagado'];
        return [
            '<a href="/admin/compras/ver/' + data['id'] + '">' + ('000' + data['folio']).slice(-4) + '</a>',
            data['fechaCreate'].split(' ')[0].split('-').reverse().join('/'),
            '<a href="/admin/proveedores/ver/' + data['proveedorID'] + '">' +  data['proveedorNombre'] + '</a>',
            data['usuarioNombre'],
            '<p class="text-align-right">$ ' + data['total'] + '</p>',
            '<p class="text-align-right">' + (payed <= 0 ? 'Pagado' : '$' + (payed).toFixed(2)) + '</p>'
        ];
    }, function(data) {
        let payed = data['total'] - data['pagado'];
        let attrs = [
            ['data-id', data['id']],
            ['data-total', data['total']],
            ['data-pagado', data['pagado']]
        ];
        if (payed <= 0) {
            attrs.push(['payed', '']);
        }
        if (data['cancelado'] == 1) {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['Proveedor', 'input', 'proveedor', 'text'],
        ['Usuario', 'input', 'usuario', 'text']
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    selectUntil: 'month'
});
let proveedorSI = new SelectInp(document.querySelector('[name="proveedor"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});
let usuarioSI = new SelectInp(document.querySelector('[name="usuario"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});

Tools.ajaxRequest({
    data: {
        metodo: 'folio',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/proveedor/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let proveedores = data['resultado'];
            for (let i=0; i<proveedores.length; i++) {
                options.push({
                    'name': ('000' + proveedores[i]['folio']).slice(-4) + ' ' + proveedores[i]['nombre'],
                    'value': proveedores[i]['id'],
                    'keys': [('000' + proveedores[i]['folio']).slice(-4), proveedores[i]['nombre']]
                });
            }
            proveedorSI.setOptions(options);
        }
    }
});
Tools.ajaxRequest({
    data: {
        metodo: 'id',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            usuarioSI.setOptions(options);
        }
    }
});

SortController.load();

})();