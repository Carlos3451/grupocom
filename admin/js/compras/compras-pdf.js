
var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:8, bottom:10, right:8};

var logoData = '';
var logoSize = {width:0, height:0};

var pdf;

var originalIDs = idsStr.split('+');
var comprasIDs = idsStr.split('+');

if (originalIDs.length > 1) {
    document.title = "Imprimir compra";
}

toDataURL('/config/logotipo.png', function(dataUrl) {
    let image = new Image();
    image.src = dataUrl;
    logoData = dataUrl;
    image.onload = function() {
        logoSize.width = this.width;
        logoSize.height = this.height;
        if (printMode == 'desglozadas') {
            generatePDFs(comprasIDs.shift());
        }
        else if (printMode == 'unidas') {
            generateJoinPDF();
        }
    }
});

function toDataURL(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function() {
        let reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

function generateJoinPDF() {
    Tools.ajaxRequest({
        data: {
            ids: comprasIDs
        },
        method: 'POST',
        url: 'functions/compra/get-all-ids-print',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                document.title = 'entradas';
                console.log(data['resultado']);
                try {
                    createPDF(data['resultado']);
                }
                catch (err) {
                    console.log(err);
                }
            }
            else {
                console.log(data['error']);
            }
        }
    });
}

function generatePDFs(compraID) {    
    Tools.ajaxRequest({
        data: {
            id: compraID
        },
        method: 'POST',
        url: 'functions/compra/get',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (originalIDs.length == 1) {
                    document.title = 'F-' + ('000' + compraData['folio']).slice(-4);
                }
                try {
                    createPDF(data['resultado']);
                }
                catch (err) {
                    console.log(err);
                }
            }
            else {
                console.log(data['error']);
            }
        }
    });
}

function createPDF(compraData) {
    if (!pdf) {
        pdf = new jsPDF({
            format: 'letter'
        });
    }

    let pageMax = 1;
    let productsMax = 38;

    if (!compraData['compras']['resultado']) {
        compraData['compras']['resultado'] = [];
    }

    /*
    console.log(compraData['compras']['resultado']);

    compraData['compras']['resultado'] = [];

    for (let i=0; i<64; i++) {
        compraData['compras']['resultado'][i] = {
            nombre: 'CERILLOS',
            unidad: 'caja',
            piezaUnidad: 'pieza',
            cantidadEntero: 10,
            cantidadUnitario: 100
        }
    }
    */
    
    if (compraData['compras']['error'] == '') {
        pageMax = Math.ceil(compraData['compras']['resultado'].length / productsMax);
    }

    for (let i=0; i<pageMax; i++) {
        createPage(compraData, pdf, i, pageMax, productsMax, productsMax * i);
    }

    //console.log(productosNum, compraData['compras']['resultado'].length);
    if (comprasIDs.length != 0 && printMode == 'desglozadas') {
        pdf.addPage();
        generatePDFs(comprasIDs.shift());
    }
    else {
        let name;
        if (originalIDs.length == 1) {
            name = 'F-' + ('000' + compraData['folio']).slice(-4);
        }
        else {
            name = "compras";
        }
        savePDF(name);
    }
}

function createPage(compraData, pdf, page, pageMax, productsMax, productsOffset) {

    let pageSize = {width:215.9, height:279.4};
    
    let lineY = 0;
    let lineHeight = 4;

    let logoY = 8;
    let logoHeight = 16;
    let logoTextLeft = 48;

    let compraDataLeft = 200;
    let compraDataFecha = printMode == 'desglozadas' ? new Date(compraData['fechaCreate']) : new Date();
    let compraFecha = ('00' + compraDataFecha.getDate()).slice(-2) + '/' + ('00' + (compraDataFecha.getMonth() + 1)).slice(-2) + '/' + compraDataFecha.getFullYear();

    let compraProductsTop = 32;
    let compraProductsHeight = 6;
    let compraProductsLeft = margin.left;
    let compraProductsSizes = [160, 20, 20];
    let compraProductsNames = ['Nombre', 'Cant. Entero', 'Cant. Unit.'];
    let compraProductsKeys = ['nombre', 'cantidadEntero', 'cantidadUnitario'];
    
    let stepsTop = 214;

    /* LOGOTIPO */

    pdf.addImage(logoData, 'PNG', margin.left, logoY, logoHeight * (logoSize.width / logoSize.height), logoHeight);

    lineY = logoY + 4;

    /*
    pdf.setFontSize(10);
    pdf.setFontStyle('normal');
    pdf.text('Fruteria San Carlos', logoTextLeft, lineY);
    lineY += lineHeight;
    pdf.text('Culiacán - Mazatlán, El Venadillo', logoTextLeft, lineY);
    lineY += lineHeight;
    pdf.text('Tel. 980 9322', logoTextLeft, lineY);
    lineY += lineHeight;
    */

    pdf.setFontSize(16);
    pdf.setFontStyle('bold');
    if (printMode == 'desglozadas') {
        pdf.text('Entrada', margin.left + 38, 18);
    }
    else {
        pdf.text('Entradas (Unidas)', margin.left + 38, 18);
    }

    lineY = logoY + 4;

    pdf.setFontSize(10);

    if (printMode == 'desglozadas') {
        pdf.text('F-' + ('0000' + compraData['folio']).slice(-5), compraDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        pdf.text(compraFecha, compraDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
    }

    if (pageMax > 1) {
        lineY += lineHeight;
        pdf.text('Página ' + (page + 1) + ' / ' + pageMax, compraDataLeft, lineY, { align: 'right' });
    }

    /* PRODUCTOS */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = compraProductsLeft;
    let columnTextSep = 2;
    
    //pdf.line(columnSep, compraProductsTop - 4, columnSep, compraProductsTop + 32);
    compraProductsKeys.forEach(function(key, index) {
        if (key == 'nombre') {
            pdf.text(compraProductsNames[index], columnSep + columnTextSep, compraProductsTop);
        }
        else {
            pdf.text(compraProductsNames[index], columnSep + compraProductsSizes[index] - columnTextSep, compraProductsTop, { align: 'right' });
        }
        columnSep += compraProductsSizes[index];
    });

    pdf.setFontSize(8);
    pdf.setFontStyle('normal');

    pdf.setDrawColor('#666');

    if (compraData['compras']['error'] == '') {
        let productos = compraData['compras']['resultado'];
        lineY = compraProductsHeight;
        for (let i=0; i<Math.min(productsMax, productos.length - productsOffset); i++) {
            if (i != 0) {
                let ly = Math.floor(compraProductsTop + lineY - (compraProductsHeight / 2) - 1);
                pdf.line(margin.left, ly, pageSize.width - margin.right, ly)
            }
            let producto = productos[i + productsOffset];
            //productosNum++;
            columnSep = compraProductsLeft;
            compraProductsKeys.forEach(function(key, index) {
                switch (key) {
                    case 'nombre':
                        if (producto['nombre'] != null) {
                            pdf.text(producto['nombre'].slice(0, 46), columnSep + columnTextSep, compraProductsTop + lineY);
                        }
                        else {
                            pdf.text('[PRODUCTO ELIMINADO]', columnSep + columnTextSep, compraProductsTop + lineY);
                        }
                        break;
                    default: {
                        let unitObj = Tools.unitConverter(producto['unidad']);
                        if (key == 'cantidadUnitario') {
                            unitObj = Tools.unitConverter(producto['piezaUnidad']);
                        }
                        if (parseFloat(producto[key]) != -1) {
                            pdf.text(parseFloat(parseFloat(producto[key]).toFixed(3)).toString() + ' ' + (producto[key] == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + compraProductsSizes[index] - columnTextSep - 0, compraProductsTop + lineY, { align: 'right' });
                        }
                        break;
                    }
                }
                columnSep += compraProductsSizes[index];
            });
            lineY += compraProductsHeight;
        }
    }

    pdf.setDrawColor('#000');

    if (page < pageMax - 1) {
        pdf.addPage();
    }

}

function savePDF(name) {
    let file = new File([pdf.output('blob')], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI);
}