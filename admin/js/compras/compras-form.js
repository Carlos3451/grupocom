(function() {

let productList = [];

let compraProductsDeleted = [];

let proveedorAc;
let productosAc;

clearForm();

new DatePicker(document.querySelector('[name="fecha"]'));

if (formMode != 'VIEW') {
    productosAc = new SelectInp(document.querySelector('[name="producto-buscador"]'), {
        placeholder: 'Producto',
        openEmpty: false,
        selectOnHover: false
    });
}

if (formMode == 'EDIT' || formMode == 'VIEW') {
    document.querySelector('[name="proveedorID"]').setAttribute('disabled', '');
    loadingDataMsg = new Wait('Cargando datos...');
    getCompra();
}
else {
    proveedorAc = new SelectInp(document.querySelector('[name="proveedorID"]'), {
        placeholder: 'S/D',
        openEmpty: false,
        selectOnHover: false
    });
    proveedorAc.searchInput.addEventListener('keydown', function(ev) {
        let key = ev.key.toLowerCase();
        if (key == 'enter') {
            productosAc.searchInput.focus();
        }
    });
    getProviders();
}

TinyProducto.onSuccess = function(producto) {
    console.log(producto);
    
    producto['proveedorID'] = null;
    producto['proveedorNombre'] = 'S/D';
    producto['categoriaID'] = null;
    producto['categoriaNombre'] = null;
    producto['existencia'] = 0;
    producto['bloqueado'] = 0;
    producto['facturar'] = 1;
    producto['claveSAT'] = '';
    producto['impuesto'] = 'tasacero';

    productList.push(producto);

    let option = {
        'name': '[' + producto['folio'] + '] ' + producto['nombre'],
        'value': producto['id'],
        'keys': [producto['folio'].toString(), producto['nombre']]
    };

    productosAc.addOption(option);

    let productObj = {
        productoID: producto['id'],
        nombre: producto['nombre'],
        unidad: producto['unidad'],
        costo: producto['costo'],
        precio1: producto['precio1'],
        precio2: producto['precio2'],
        precio3: producto['precio3'],
        impuesto: producto['impuesto'],
        claveSAT: producto['claveSAT']
    };

    addProduct(productObj);
}

getProducts();

function getCompra() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/compra/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadCompra(data['resultado']);
            }
        }
    });
}

function getProviders() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/proveedor/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        options.push({
                            'name': ('000' + result[i]['folio']).slice(-4) + ' ' + result[i]['nombre'],
                            'value': result[i]['id'],
                            'keys': [('000' + result[i]['folio']).slice(-4), result[i]['nombre']]
                        });
                    }
                    proveedorAc.setOptions(options);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function getProducts() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/producto/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    productList = result;
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        options.push({
                            'name': '[' + result[i]['folio'] + '] ' + result[i]['nombre'],
                            'value': result[i]['id'],
                            'keys': [result[i]['folio'].toString(), result[i]['nombre']]
                        });
                    }
                    if (formMode != 'VIEW') {
                        productosAc.setOptions(options);
                    }
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadCompra(compra) {
    document.querySelector('[name="fecha"]').value = compra['fechaCreate'].split(' ')[0].split('-').reverse().join('/');
    document.querySelector('[name="fecha"]').dispatchEvent(new Event('input'));
    document.querySelector('[name="proveedorID"]').value = ('000' + compra['proveedorFolio']).slice(-4) + ' ' + compra['proveedorNombre'];
    document.querySelector('[name="proveedorID"]').dispatchEvent(new Event('change'));
    document.querySelector('[name="total"]').value = compra['total'];
    let productosCompras = compra['compras'];
    let pagosData = compra['pagos'];
    if (productosCompras['error'] == '') {
        let productos = productosCompras['resultado'];
        for (let i=0; i<productos.length; i++) {
            addProduct(productos[i]);
        }
    }
    if (pagosData['error'] == '') {
        let pagos = pagosData['resultado'];
        for (let i=0; i<pagos.length; i++) {
            let pago = pagos[i];
            addPayment(new Date(pago['fechaCreate']), pago['metodo'], pago['cantidad'], pago['cancelado'], pago['id']);
        }
    }
    if (formMode == 'VIEW') {
        disableInputs();
    }
    updatePaymentsTotal();
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
}

function submitForm() {
    let providerID = document.querySelector('[name="proveedorID"]').value;
    let compraProductos = getCompraProductsData();
    let pagos = getPaymentsData();
    document.querySelector('.form-submit').setAttribute('disabled', '');
    submitObj = {};
    switch (formMode) {
        case 'ADD':
            submitObj = {
                url: 'functions/compra/add',
                msgWait: 'Creando compra.',
                msgSuccess: 'Compra creada con exito.',
                msgError: 'Ocurrio un error al tratar de crear la compra.',
            }
            break;
        case 'EDIT':
            submitObj = {
                url: 'functions/compra/edit',
                msgWait: 'Guardando compra.',
                msgSuccess: 'Compra guardada con exito.',
                msgError: 'Ocurrio un error al tratar de guardar la compra.',
            }
            break;
    }
    Tools.ajaxRequest({
        data: {
            id: editID,
            fecha: document.querySelector('[name="fecha"]').value,
            proveedorID: providerID,
            compraProductos: compraProductos,
            compraProductosEliminados: compraProductsDeleted,
            pagos: pagos,
            total: document.querySelector('[name="total"]').value
        },
        url: submitObj['url'],
        method: 'POST',
        waitMsg: new Wait(submitObj['msgWait']),
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                new Message(submitObj['msgSuccess'], 'success', function() {
                    window.location = '/admin/compras';
                });
            }
            else {
                new Message(submitObj['msgError'] + '<br>' + data['error'], 'error');
            }
        }
    });
}

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
    document.querySelectorAll('#data-form .accounting-container input').forEach(function(elem, index) {
        elem.value = '0.00';
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#data-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('click', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
    this.removeEventListener('click', clearInvalid);
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/compras';
        });
    }
    else {        
        window.location = '/admin/compras';
    }
});

/* AÑADIR PRODUCTO */
if (formMode != 'VIEW') {
    productosAc.searchInput.addEventListener('keydown', function(ev) {
        if (ev.key == 'Enter' && this.value != "") {
            let name = this.value;
            new Question('Dar de alta producto', 'El producto "' + name + '" no se encontro, ¿Desea darlo de alta?', function() {
                TinyProducto.open(name);
            });
        }
    });
    productosAc.onChange = function() {
        if (!this.value) {
            return;
        }
        let exists = false;
        let productID = this.value;
        let producto = productList.find(function(value) {
            return value['id'] == productID;
        });
        let productObj = {
            productoID: producto['id'],
            nombre: producto['nombre'],
            unidad: producto['unidad'],
            costo: producto['costo'],
            precio1: producto['precio1'],
            precio2: producto['precio2'],
            precio3: producto['precio3'],
            impuesto: producto['impuesto'],
            claveSAT: producto['claveSAT']
        };
        document.querySelectorAll('.compra-table-row').forEach(function(elem, index) {
            let dataID = elem.getAttribute('data-producto-id');
            if (productID == dataID) {
                exists = true;
            }
        });
        if (!exists) {
            addProduct(productObj);
        }
        else {
            let msg = new Message('Este producto ya esta añadido en la compra.', 'warning', function() {
                productosAc.searchInput.focus();
            });
            setTimeout(function(){
                msg.element.querySelector('.message-accept').focus();
            });
        }
        this.clear();
    }
}

function addProduct(productObj, focusInput = true) {
    console.log(productObj);
    let rowElem = document.createElement('div');
    let unitObj = Tools.unitConverter(productObj['unidad']);
    rowElem.className = 'compra-table-row';
    rowElem.setAttribute('data-producto-id', productObj['productoID']);
    rowElem.setAttribute('data-compra-producto-id', productObj['id'] || 0);
    rowElem.setAttribute('data-producto-nombre', productObj['nombre']);
    rowElem.setAttribute('data-producto-unidad', productObj['unidad']);
    rowElem.setAttribute('data-producto-costo', productObj['costo']);
    rowElem.setAttribute('data-impuesto', productObj['impuesto']);
    rowElem.innerHTML = (
        '<div class="compra-table-column">'+
            '<div class="clave-input-container">'+
                '<input name="compraClaveSAT" type="text" value="' + productObj['claveSAT'] + '">' +
                '<img class="action-search-clave" src="imgs/icons/buscador-azul.png">' +
            '</div>' +
        '</div>' +
        '<div class="compra-table-column">'+
            '<input type="text" name="producto-nombre" disabled>'+
        '</div>'+
        '<div class="compra-table-column"><select name="compraImpuesto">'+
            '<option value="tasacero">Tasa Cero (0%)</option>'+
            '<option value="iva">IVA (16%)</option>'+
            '<option value="ieps8">IEPS (8%)</option>'+
            '<option value="ieps160iva">IEPS (160%) e IVA (16%)</option>'+
            '<option value="exento">Exento</option>'+
        '</select></div>' +
        '<div class="compra-table-column">'+
            '<input class="inp-unit inp-' + productObj['unidad'] + ' text-align-right" type="text" name="producto-cantidad" decimals="' + unitObj['zeros'] + '" autocomplete="off">'+
        '</div>'+
        '<div class="compra-table-column prices-container">'+
            '<input class="inp-icon inp-price" type="text" name="producto-costo" value="0.00">'+
        '</div>'+
        '<div class="compra-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-precio1" value="0.00">'+
        '</div>'+
        '<div class="compra-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-precio2" value="0.00">'+
        '</div>'+
        '<div class="compra-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-precio3" value="0.00">'+
        '</div>'+
        '<div class="compra-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-total" value="0.00" disabled>'+
        '</div>'+
        '<div class="compra-table-column">'+
            '<div class="actions-container">'+
                '<img tooltip-hint="Subir" class="product-up" src="/admin/imgs/icons/actions/up.png">'+
                '<img tooltip-hint="Bajar" class="product-down" src="/admin/imgs/icons/actions/down.png">'+
                '<img tooltip-hint="Eliminar" class="product-delete" src="/admin/imgs/icons/actions/delete.png">'+
            '</div>'+
        '</div>'
    );
    
    rowElem.querySelector('[name="compraImpuesto"]').value = productObj['impuesto'];

    rowElem.querySelector('.action-search-clave').addEventListener('click', function() {
        ClavesSAT.open(rowElem.querySelector('[name="compraClaveSAT"]').value);
        ClavesSAT.onSelect = function(clave, name) {
            rowElem.querySelector('[name="compraClaveSAT"]').value = clave;
        }
    });

    if (!productObj['id']) {
        rowElem.querySelector('[name="producto-cantidad"]').value = 1;
        rowElem.querySelector('[name="producto-costo"]').value = productObj['costo'];
        rowElem.querySelector('[name="producto-precio1"]').value = productObj['precio1'];
        rowElem.querySelector('[name="producto-precio2"]').value = productObj['precio2'];
        rowElem.querySelector('[name="producto-precio3"]').value = productObj['precio3'];
        if (privilegesLevel > 1) {
            rowElem.querySelector('[name="compraImpuesto"]').addEventListener('keydown', productoInputKeydown);

            rowElem.querySelector('[name="producto-costo"]').addEventListener('keydown', productoInputKeydown);
            rowElem.querySelector('[name="producto-costo"]').addEventListener('keypress', keypressIsNumber);
            rowElem.querySelector('[name="producto-costo"]').addEventListener('blur', blurInputNumeric);
            rowElem.querySelector('[name="producto-costo"]').addEventListener('focus', selectInput);
            rowElem.querySelector('[name="producto-costo"]').addEventListener('change', updateCompraRowTotal);
            rowElem.querySelector('[name="producto-costo"]').addEventListener('input', updateCompraRowTotal);

            rowElem.querySelector('[name="producto-precio1"]').addEventListener('keydown', productoInputKeydown);
            rowElem.querySelector('[name="producto-precio1"]').addEventListener('keypress', keypressIsNumber);
            rowElem.querySelector('[name="producto-precio1"]').addEventListener('blur', blurInputNumeric);
            rowElem.querySelector('[name="producto-precio1"]').addEventListener('focus', selectInput);

            rowElem.querySelector('[name="producto-precio2"]').addEventListener('keydown', productoInputKeydown);
            rowElem.querySelector('[name="producto-precio2"]').addEventListener('keypress', keypressIsNumber);
            rowElem.querySelector('[name="producto-precio2"]').addEventListener('blur', blurInputNumeric);
            rowElem.querySelector('[name="producto-precio2"]').addEventListener('focus', selectInput);

            rowElem.querySelector('[name="producto-precio3"]').addEventListener('keydown', productoInputKeydown);
            rowElem.querySelector('[name="producto-precio3"]').addEventListener('keypress', keypressIsNumber);
            rowElem.querySelector('[name="producto-precio3"]').addEventListener('blur', blurInputNumeric);
            rowElem.querySelector('[name="producto-precio3"]').addEventListener('focus', selectInput);

        }
        else {
            rowElem.querySelector('[name="compraImpuesto"]').setAttribute('disabled', '');
            rowElem.querySelector('[name="producto-costo"]').setAttribute('disabled', '');
            rowElem.querySelector('[name="producto-precio1"]').setAttribute('disabled', '');
            rowElem.querySelector('[name="producto-precio2"]').setAttribute('disabled', '');
            rowElem.querySelector('[name="producto-precio3"]').setAttribute('disabled', '');
        }
    }
    else {
        rowElem.querySelector('[name="compraImpuesto"]').setAttribute('disabled', '');
        rowElem.querySelector('[name="producto-costo"]').setAttribute('disabled', '');
        rowElem.querySelector('[name="producto-precio1"]').setAttribute('disabled', '');
        rowElem.querySelector('[name="producto-precio2"]').setAttribute('disabled', '');
        rowElem.querySelector('[name="producto-precio3"]').setAttribute('disabled', '');
        rowElem.querySelector('[name="producto-costo"]').value = productObj['costo'] || '0.00';
        rowElem.querySelector('[name="producto-precio1"]').value = productObj['precio1'] || '0.00';
        rowElem.querySelector('[name="producto-precio2"]').value = productObj['precio2'] || '0.00';
        rowElem.querySelector('[name="producto-precio3"]').value = productObj['precio3'] || '0.00';
        
        rowElem.querySelector('[name="producto-cantidad"]').value = parseFloat(parseFloat(productObj['cantidad']).toFixed(3));

        rowElem.querySelector('[name="producto-total"]').value = productObj['total'];
    }

    rowElem.querySelector('[name="producto-nombre"]').value = productObj['nombre'];

    document.querySelector('.compra-table').appendChild(rowElem);

    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('keydown', productoInputKeydown);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('keypress', keypressIsNumber);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('input', updateCompraRowTotal);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('focus', selectInput);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('blur', blurDecimal);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('blur', updateCompraRowTotal);

    if (formMode == 'VIEW') {
        rowElem.querySelector('.actions-container').remove();
    }
    else {
        rowElem.querySelector('.product-up').addEventListener('click', animateMoveUpCompraRow);
        rowElem.querySelector('.product-down').addEventListener('click', animateMoveDownCompraRow);
        rowElem.querySelector('.product-delete').addEventListener('click', deleteCompraRow);
    }

    ToolTip.update();

    if (!productObj['id'] && focusInput) {
        setTimeout(function(){
            rowElem.querySelector('[name="producto-cantidad"]').focus();
            rowElem.querySelector('[name="producto-cantidad"]').dispatchEvent(new Event('input'));
        }, 10);
    }
}

function productoPrecioKeydown(ev) {
    let value = this.value;
    switch (ev.key.toLowerCase()) {
        case 'enter':
            let rect = productosAc.searchInput.getBoundingClientRect();
            if (rect.top < 48) {
                window.scrollBy(0, rect.top - 80);
            }
            productosAc.searchInput.focus();
            break;
    }
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

function productoCantidadKeydown(ev) {
    let value = this.value;
    switch (ev.key.toLowerCase()) {
        case 'enter':
            let row = this.closest('.compra-table-row');
            let costoInp = row.querySelector('[name="producto-costo"]');
            if (costoInp) {
                costoInp.focus();
            }
            else {
                productosAc.searchInput.focus();
            }
            break;
    }
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

function productoInputKeydown(ev) {
    let value = this.value;
    let column = this.parentElement;
    let row = column.parentElement;
    switch (ev.key.toLowerCase()) {
        case 'enter':
            productosAc.searchInput.focus();
            break;
        case 'arrowright':
            if (column.nextElementSibling) {
                ev.preventDefault();
                setTimeout(function() {
                    column.nextElementSibling.children[0].focus();
                }, 1);
            }
            break;
        case 'arrowleft':
            if (column.previousElementSibling) {
                ev.preventDefault();
                setTimeout(function() {
                    column.previousElementSibling.children[0].focus();
                }, 1);
            }
            break;
        case 'arrowdown':
            if (this.tagName != 'SELECT' && row.nextElementSibling) {
                console.log(this.tagName);
                ev.preventDefault();
                let idx = Array.from(row.children).indexOf(column);
                setTimeout(function() {
                    row.nextElementSibling.children[idx].children[0].focus();
                }, 1);
            }
            break;
        case 'arrowup':
            if (this.tagName != 'SELECT' && row.previousElementSibling && row.previousElementSibling.classList.contains('compra-table-row')) {
                console.log(this.tagName);
                ev.preventDefault();
                let idx = Array.from(row.children).indexOf(column);
                setTimeout(function() {
                    row.previousElementSibling.children[idx].children[0].focus();
                }, 1);
            }
            break;
    }
    if (this.tagName != 'SELECT' && !Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

/* FUNCIONES PRODUCTOS */

function animateMoveUpCompraRow() {
    let productRow = this.closest('.compra-table-row');
    previousProductRow = productRow.previousElementSibling;
    if (previousProductRow.classList.contains('compra-table-row')) {
        productRow.style.animation = '';
        previousProductRow.style.animation = '';

        productRow.offsetHeight;

        productRow.style.animation = 'moveUpCompraRow 0.2s ease-in-out';
        previousProductRow.style.animation = 'moveDownCompraRow 0.2s ease-in-out';

        productRow.addEventListener('animationend', moveUpCompraRow);
    }
}

function animateMoveDownCompraRow() {
    let productRow = this.closest('.compra-table-row');
    nextProductRow = productRow.nextElementSibling;
    if (nextProductRow) {
        productRow.style.animation = '';
        nextProductRow.style.animation = '';

        productRow.offsetHeight;

        productRow.style.animation = 'moveDownCompraRow 0.2s ease-in-out';
        nextProductRow.style.animation = 'moveUpCompraRow 0.2s ease-in-out';

        productRow.addEventListener('animationend', moveDownCompraRow);
    }
}

function moveUpCompraRow() {
    let productRow = this.closest('.compra-table-row');
    previousProductRow = productRow.previousElementSibling;

    productRow.style.animation = '';
    previousProductRow.style.animation = '';

    document.querySelector('.compra-table').insertBefore(productRow, previousProductRow);

    productRow.removeEventListener('animationend', moveUpCompraRow);
}

function moveDownCompraRow() {
    let productRow = this.closest('.compra-table-row');
    nextProductRow = productRow.nextElementSibling;
    
    productRow.style.animation = '';
    nextProductRow.style.animation = '';
    
    document.querySelector('.compra-table').insertBefore(productRow, nextProductRow.nextElementSibling);

    productRow.removeEventListener('animationend', moveDownCompraRow);
}

function deleteCompraRow() {
    let row = this.closest('.compra-table-row');
    let compraProductID = row.getAttribute('data-compra-producto-id');
    new Question('Eliminar producto', 'Se eliminara el producto de la compra', function(){
        if (compraProductID != 0) {                
            compraProductsDeleted.push(compraProductID);
        }
        row.remove();
        updateCompraTotal();
    });
}

function selectInput() {
    this.select();
}

function blurInputNumeric() {
    if (this.value != '') {
        this.value = parseFloat(this.value).toFixed(2);
    }
    else {
        this.value = (0).toFixed(2);
    }
}

function blurDecimal() {
    let decimals = parseFloat(this.getAttribute('decimals') || 2);
    if (this.value != '') {
        let places = Tools.getDecimals(parseFloat(this.value));
        if (decimals && !places) {
            this.value = parseFloat(this.value).toFixed(0);
        }
        else {
            this.value = parseFloat(this.value).toFixed(decimals || places);
        }
    }
    else {
        this.value = (0).toFixed(decimals);
    }
}

function updateCompraRowTotal() {
    let row = this.closest('.compra-table-row');
    let quantity = row.querySelector('[name="producto-cantidad"]').value || 0;
    let cost = row.querySelector('[name="producto-costo"]').value || 0;
    row.querySelector('[name="producto-total"]').value = (quantity * cost).toFixed(2);
    updateCompraTotal();
}

/* FUNCIONES PEDIDO TOTALES */

function updateCompraTotal() {
    let rows = document.querySelectorAll('.compra-table-row');
    let total = 0;
    rows.forEach(function(elem, index) {
        total += parseFloat(elem.querySelector('[name="producto-total"]').value);
    });
    total = total.toFixed(2);
    document.querySelector('[name="total"]').value = total;
    updatePaymentsTotal();
}

/* PAGOS */

Payment.onAccept = function(method, quantity) {
    let date = new Date();
    addPayment(date, method, quantity);
}

function addPayment(date, method, quantity, cancelled = false, paymentID = 0) {
    let dateString = ('00' + date.getDate()).slice(-2) + '/' + ('00' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
    let dateUnix = Math.floor(date.getTime() / 1000)
    let paymentRow = document.createElement('div');
    paymentRow.setAttribute('data-id', paymentID);
    paymentRow.setAttribute('data-fecha', dateUnix);
    paymentRow.setAttribute('data-metodo', method);
    paymentRow.setAttribute('data-cantidad', quantity);
    if (cancelled) {
        paymentRow.setAttribute('cancelled', '');
    }
    paymentRow.className = 'payments-table-row';
    paymentRow.innerHTML =
        '<div class="payments-table-column">'+
            '<input type="text" value="' + dateString + '" disabled>'+
        '</div>'+
        '<div class="payments-table-column">'+
            '<div class="quantity-container">'+
                '<img tooltip-hint="' + (method[0].toUpperCase() + method.slice(1)) + '" class="method" src="/admin/imgs/icons/pago-' + method + '.png">'+
                '<p class="quantity">$' + quantity + '</p>'+
            '</div>'+
        '</div>'+
        '<div class="payments-table-column">'+
            '<div class="actions-container">'+
                '<img class="payment-delete" src="/admin/imgs/icons/actions/delete.png">'+
            '</div>'+
        '</div>';
    if (formMode == 'VIEW') {
        paymentRow.querySelector('.actions-container').remove();
    }
    else {
        if (paymentID == 0) {
            paymentRow.querySelector('.payment-delete').addEventListener('click', deletePaymentRow);
            paymentRow.querySelector('.payment-delete').setAttribute('tooltip-hint', 'Eliminar');
        }
        else {
            if (!cancelled) {
                paymentRow.querySelector('.payment-delete').setAttribute('src', '/admin/imgs/icons/actions/cancelar.png');
                paymentRow.querySelector('.payment-delete').addEventListener('click', cancelPaymentRow);
                paymentRow.querySelector('.payment-delete').setAttribute('tooltip-hint', 'Cancelar');
            }
            else {
                paymentRow.querySelector('.payment-delete').remove();
            }
        }
    }
    document.querySelector('.payments-table').appendChild(paymentRow);
    ToolTip.update();
    updatePaymentsTotal();
}

if (formMode != 'VIEW') {
    document.querySelector('.payment-add').addEventListener('click', function() {
        let resto = parseFloat(document.querySelector('[name="resto"]').value);
        Payment.open(resto);
    });
}

function updatePaymentsTotal() {
    let pagado = 0;
    let total = document.querySelector('[name="total"]').value;
    document.querySelectorAll('.payments-table-row').forEach(function(elem, index) {
        if (!elem.hasAttribute('cancelled')) {
            pagado += parseFloat(elem.getAttribute('data-cantidad'));
        }
    });
    document.querySelector('[name="pagado"]').value = pagado.toFixed(2);
    document.querySelector('[name="resto"]').value = (total - pagado).toFixed(2);
}

function deletePaymentRow() {
    let paymentRow = this.closest('.payments-table-row');
    new Question('Se eliminara este pago', '¿Esta de acuerdo?', function() {
        paymentRow.remove();
        updatePaymentsTotal();
    });
}

function cancelPaymentRow() {
    let self = this;
    let paymentRow = this.closest('.payments-table-row');
    new Question('Se cancelara este pago', '¿Esta de acuerdo?', function() {
        paymentRow.setAttribute('cancelled', '');
        updatePaymentsTotal();
        self.remove();
    });
}

/* GET ALL DATA */

function getCompraProductsData() {
    let productsRows = document.querySelectorAll('.compra-table-row');
    let productsData = [];
    productsRows.forEach(function(elem, index) {
        productsData.push({
            id: elem.getAttribute('data-compra-producto-id'), 
            productoID: elem.getAttribute('data-producto-id'),
            nombre: elem.querySelector('[name="producto-nombre"]').value,
            cantidad: elem.querySelector('[name="producto-cantidad"]').value,
            costo: elem.querySelector('[name="producto-costo"]').value,
            precio1: elem.querySelector('[name="producto-precio1"]').value,
            precio2: elem.querySelector('[name="producto-precio2"]').value,
            precio3: elem.querySelector('[name="producto-precio3"]').value,
            total: elem.querySelector('[name="producto-total"]').value,
            unidad: elem.getAttribute('data-producto-unidad'),
            impuesto: elem.querySelector('[name="compraImpuesto"]').value,
            claveSAT: elem.querySelector('[name="compraClaveSAT"]').value
        });
    });
    return productsData;
}

function getPaymentsData() {
    let paymentsRows = document.querySelectorAll('.payments-table-row');
    let paymentsData = [];
    paymentsRows.forEach(function(elem, index) {
        paymentsData.push({
            id: elem.getAttribute('data-id'),
            fecha: elem.getAttribute('data-fecha'),
            metodo: elem.getAttribute('data-metodo'),
            cantidad: elem.getAttribute('data-cantidad'),
            cancelado: elem.hasAttribute('cancelled') ? 1 : 0
        });
    });
    return paymentsData;
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });
}

/* NUMERIC */

function keypressIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

document.querySelectorAll('[numeric]').forEach(function(elem, index) {
    elem.addEventListener('keypress', keypressIsNumber);
    elem.addEventListener('keydown', keypressIsNumber);
});

document.querySelector('.btn-new-product').addEventListener('click', function() {
    TinyProducto.open('');
});
    
})();