const { degrees, PDFDocument, rgb, StandardFonts } = PDFLib;

(function(){

let clientsList = [];

function removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, '');
}

Lista.actionPrint['active'] = true;
Lista.actionPrint['unique'] = true;
Lista.printFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value,
            UID: uid
        },
        method: 'POST',
        url: 'functions/factura/pdf',
        responseType: 'blob',
        waitMsg: new Wait('Obteniendo PDF...'),
        success: function(response) {
            this.waitMsg.destroy();
            //window.open(window.URL.createObjectURL(response));
            modidyPDF(response);
        }
    });
}

async function modidyPDF(data) {
    let dataBytes = await data.arrayBuffer();

    let pdf = await PDFDocument.load(dataBytes);
    let helveticaFont = await pdf.embedFont(StandardFonts.HelveticaBold);

    let pages = pdf.getPages();
    let pagesCopiesNumbers = [];
    for (let i=0; i<pages.length; i++) {
        pagesCopiesNumbers.push(i);
    }

    let pagesCopies = await pdf.copyPages(pdf, pagesCopiesNumbers);

    for (let i=0; i<pagesCopies.length; i++) {
        pdf.addPage(pagesCopies[i]);
    }

    pages = pdf.getPages();

    let xx = -70;
    let yy = 130;

    for (let i=pages.length / 2; i<pages.length; i++) {
        pages[i].drawRectangle({
            x: 0 + xx,
            y: 505 + yy,
            rotate: degrees(45),
            width: 400,
            height: 40,
            color: rgb(0.070, 0.043, 0.458),
        });
        pages[i].drawText('COPIA', {
            x: 100 + xx,
            y: 615 + yy,
            size: 32,
            rotate: degrees(45),
            font: helveticaFont,
            color: rgb(1.0, 1.0, 1.0)
        });
    }

    let pdfBytes = await pdf.save();
    window.open(window.URL.createObjectURL(new Blob([pdfBytes], {type: 'application/pdf'})));
}

Lista.actionDownload['active'] = true;
Lista.actionDownload['unique'] = true;
Lista.downloadFn = function(selecteds) {
    let waitMsg = new Wait('Obteniendo datos del complemento.');
    let uid = selecteds[0].dataset.uid;
    let promises = [];
    let folio = selecteds[0].dataset.folio.replace('F ', '');
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura/xml',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura/pdf',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    Promise.all(promises).then(function(result) {
        let zip = new JSZip();
        zip.file('complementopago-' + folio + '.pdf', result[1]);
        zip.file('complementopago-' + folio + '.xml', result[0]);
        zip.generateAsync({type: 'blob'}).then(function(content) {
            waitMsg.destroy();
            let downloadElem = document.createElement('a');
            downloadElem.setAttribute('href', window.URL.createObjectURL(content));
            downloadElem.setAttribute('download', 'factura-' + folio + '.zip');
            downloadElem.style.display = 'none';
            document.querySelector('body').appendChild(downloadElem);
            downloadElem.click();
            downloadElem.remove();
        });
    });
}

Lista.actionEmail['active'] = true;
Lista.actionEmail['unique'] = true;
Lista.emailFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    let email = '';
    let client = getClientByRFC(selecteds[0].dataset.rfc);
    if (client) {
        email = client['Contacto']['Email'];
    }
    EmailFactura.open(email, uid, document.querySelector('[name="accesoID"]').value);
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = true;
    Lista.cancelFn = function(selecteds) {
        let facturaUID = selecteds[0].dataset.uid;
        if (!selecteds[0].hasAttribute('cancelled')) {
            new OptionsQuestion('Cancelar complemento', 'Elija el motivo de la cancelación.', [
                {
                    'text': '02 - Comprobante emitido con errores sin relación',
                    'value': '02'
                },
                {
                    'text': '03 - No se llevó a cabo la operación',
                    'value': '03'
                },
                {
                    'text': '04 - Operación nominativa relacionada en la factura global',
                    'value': '04'
                }
            ], function(motivo) {
                Tools.ajaxRequest({
                    data: {
                        accesoID: document.querySelector('[name="accesoID"]').value,
                        uid: facturaUID,
                        motivo: motivo
                    },
                    method: 'POST',
                    url: 'functions/factura/complemento-cancel',
                    waitMsg: new Wait('Cancelando complemento de pago...', true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Complemento de pago cancelado con exito.', 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            if (data['errorLog'] && data['errorLog']['message']) {                                    
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago.' + '<br>' + data['errorLog']['message'], 'error');
                            }
                            else {
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago.', 'error');
                            }
                        }
                    }
                });
            });
        }
        else {
            new Message('No se puede cancelar un complemento de pago ya cancelado.', 'warning');
        }
    }
}

Lista.selectJustOne = true;

SortController.setOnMethodChange(function() {});

function reloadComplementos() {
    SortController.load();
    let filtros = SortController.filters;
    filtros['tipo'] = 'pago';
    Lista.load('factura/get-all-complementos', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: filtros
    }, function(data) {
        let emisor = document.querySelector('[name="accesoID"]').children[document.querySelector('[name="accesoID"]').selectedIndex].textContent;
        let date = data['FechaTimbrado'].split('-').reverse().join('/');
        let cantidad = '-';
        if (data['datosExtra']) {
            cantidad = '$' + data['datosExtra'].reduce((a, c) => a + c['cantidad'], 0);
        }
        return [
            data['Folio'],
            date,
            data['RazonSocialReceptor'],
            emisor,
            (data['datosExtra'] ? data['datosExtra'][0]['usuarioNombre'] : '-'),
            data['Status'],
            cantidad
        ];
    }, function(data) {
        let attrs = [
            ['data-uid', data['UID']],
            ['data-uuid', data['UUID']],
            ['data-folio', data['Folio']],
            ['data-rfc', data['Receptor']],
            ['data-total', data['Total']]
        ];
        if (data['Status'] == 'cancelada') {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    });
}

SortController.setMethods([
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['RFC', 'select', 'accesoID', []],
        ['Cliente', 'select', 'clienteRFC', [
            ['Todos', 0]
        ]]
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    selectUntil: 'month'
});

function loadClients() {
    clientsList = [];
    Lista.clear();
    Lista.createLoading();
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value
        },
        method: 'POST',
        url: 'functions/factura/get-all-clients',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                clientsList = data['resultado'];
                let optionsFragment = document.createDocumentFragment();
                for (let i=0; i<clientsList.length; i++) {
                    let cliente = clientsList[i];
                    let option = document.createElement('option');
                    option.value = cliente['RFC'];
                    option.textContent = cliente['RazonSocial'];
                    optionsFragment.appendChild(option);
                }
                document.querySelector('[name="clienteRFC"]').appendChild(optionsFragment);
            }
            reloadComplementos();
        }
    });
}

function getClientByRFC(rfc) {
    return clientsList.find(function(value) {
        return value['RFC'] == rfc;
    });
}

Tools.ajaxRequest({
    url: 'functions/factura/get-all-access',
    method: 'GET',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let result = data['resultado'];
            let optionsFrag = document.createDocumentFragment();
            for (let i=0; i<result.length; i++) {
                let option = document.createElement('option');
                option.value = result[i]['id'];
                option.textContent = result[i]['nombre'];
                optionsFrag.appendChild(option);
            }
            document.querySelector('[name="accesoID"]').appendChild(optionsFrag);
            if (window.localStorage.getItem('factura-acceso-id')) {
                document.querySelector('[name="accesoID"]').value = window.localStorage.getItem('factura-acceso-id');
            }
            loadClients();
        }
    }
});

document.querySelector('[name="fecha"]').addEventListener('change', function() {
    reloadComplementos();
});

document.querySelector('[name="accesoID"]').addEventListener('change', function() {
    window.localStorage.setItem('factura-acceso-id', this.value);
    loadClients();
});

document.querySelector('[name="clienteRFC"]').addEventListener('change', function() {
    let rfc = this.value;
    document.querySelectorAll('.list-row').forEach(function(row) {
        row.style.display = 'none';
        if (row.dataset.rfc == rfc || rfc == 0) {
            row.style.display = '';
        }
    });
});

})();