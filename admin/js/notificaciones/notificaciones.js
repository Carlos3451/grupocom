(function() {

lastID = 0;
loadedAll = false;

createLoading();

Tools.ajaxRequest({
    data: {
        limite: 100,
        offset: 0
    },
    method: 'POST',
    url: 'functions/notificacion/get-all',
    success: function(response) {
        deleteLoading();
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let notificaciones = data['resultado'];
            let fragment = document.createDocumentFragment();
            for (let i=0; i<notificaciones.length; i++) {
                addNotificacion(fragment, notificaciones[i]);
            }
            lastID = notificaciones[notificaciones.length - 1]['id'];
            document.querySelector('.notificaciones-containers').appendChild(fragment);
            document.querySelector('.notificaciones-containers').scrollTop = 10;
            document.querySelector('.notificaciones-containers').scrollTop = 0;
        }
        else if (data['error'] == 'EMPTY') {
            loadedAll = true;
        }
    }
});

function loadNotificaciones() {
    Tools.ajaxRequest({
        data: {
            id: lastID,
            limite: 100,
            offset: 0
        },
        method: 'POST',
        url: 'functions/notificacion/get-all',
        success: function(response) {
            deleteLoading();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let notificaciones = data['resultado'];
                let fragment = document.createDocumentFragment();
                for (let i=0; i<notificaciones.length; i++) {
                    addNotificacion(fragment, notificaciones[i]);
                }
                lastID = notificaciones[notificaciones.length - 1]['id'];
                document.querySelector('.notificaciones-containers').appendChild(fragment);
            }
            else if (data['error'] == 'EMPTY') {
                loadedAll = true;
            }
        }
    });
}

document.querySelector('.notificaciones-containers').addEventListener('scroll', function() {
    let height = this.offsetHeight;
    let scrollTop = this.scrollTop;
    let scrollHeight = this.scrollHeight;
    if ((scrollTop + height) > (scrollHeight - 32)) {
        console.log(loadedAll);
        if (!loadingExists() && !loadedAll) {
            createLoading();
            loadNotificaciones();
        }
    }
});

function loadingExists() {
    return document.querySelector('.notificacion-loading') ? true : false;
}

function createLoading() {
    let elem = document.createElement('div');
    elem.className = 'notificacion-loading';
    elem.innerHTML = '<div class="loader"></div>';
    document.querySelector('.notificaciones-containers').appendChild(elem);
}

function deleteLoading() {
    document.querySelector('.notificacion-loading').remove();
}

function addNotificacion(fragment, notificacion) {
    let notificacionContainer = document.createElement('div');
    notificacionContainer.className = 'notificacion-container' + (notificacion['visto'] ? '' : ' notificacion-unread');
    notificacionContainer.setAttribute('data-id', notificacion['id']);
    notificacionContainer.innerHTML =
        '<div class="notificacion-icon-container">'+
            '<img class="notificacion-icon" src="/admin/imgs/icons/productos.png">'+
            '<img class="notificacion-subicon" src="/admin/imgs/icons/actions/edit.png">'+
        '</div>'+
        '<div class="notificacion-data">'+
            '<p class="notificacion-description"></p>'+
            '<p class="notificacion-date"></p>'+
        '</div>';
    let description = getNotificacionDescription(notificacion);
    let fecha = getNotificacionFecha(notificacion['fechaCreate']);
    let iconObj = getNotificacionIconObj(notificacion);
    notificacionContainer.querySelector('.notificacion-description').innerHTML = description;
    notificacionContainer.querySelector('.notificacion-date').textContent = fecha;
    notificacionContainer.querySelector('.notificacion-icon').setAttribute('src', iconObj['icon']);
    notificacionContainer.querySelector('.notificacion-subicon').setAttribute('src', iconObj['subicon']);
    fragment.appendChild(notificacionContainer);
}

function getNotificacionIconObj(notificacion) {
    let type = notificacion['tipo'];
    let parameters = JSON.parse(notificacion['parametros']);
    let typeArray = type.split('.');
    let icon = '';
    let subicon = '';
    switch (typeArray[0]) {
        case 'producto':
            icon = '/admin/imgs/icons/productos.png';
            break;
        case 'credencial':
            icon = '/admin/imgs/icons/credenciales.png';
            break;
        case 'cliente':
            icon = '/admin/imgs/icons/clientes.png';
            break;
        case 'existencia':
            icon = '/admin/imgs/icons/productos.png';
            break;
        case 'ruta':
            icon = '/admin/imgs/icons/rutas.png';
            break;
        case 'pedido':
            icon = '/admin/imgs/icons/pedidos.png';
            break;
        case 'compra':
            icon = '/admin/imgs/icons/compras.png';
            break;
        case 'pedidopago': case 'comprapago':
            switch (parameters['metodo']) {
                case 'efectivo':
                    icon = '/admin/imgs/icons/pago-efectivo.png';
                    break;
                case 'deposito':
                    icon = '/admin/imgs/icons/pago-deposito.png';
                    break;
                case 'cheque':
                    icon = '/admin/imgs/icons/pago-cheque.png';
                    break;
                case 'transferencia':
                    icon = '/admin/imgs/icons/pago-transferencia.png';
                    break;
                case 'tarjeta':
                    icon = '/admin/imgs/icons/pago-tarjeta.png';
                    break;
            }
            break;
        case 'inventario':
            icon = '/admin/imgs/icons/inventario.png';
            break;
        case 'venta':
            icon = '/admin/imgs/icons/ventas.png';
            break;
        case 'caja':
            icon = '/admin/imgs/icons/caja.png';
            break;
        case 'proveedor':
            icon = '/admin/imgs/icons/proveedores.png';
            break;
    }
    switch (typeArray[1]) {
        case 'add':
            subicon = '/admin/imgs/icons/actions/add.png';
            break;
        case 'open':
            subicon = '/admin/imgs/icons/actions/open.png';
            break;
        case 'edit':
            subicon = '/admin/imgs/icons/actions/edit.png';
            break;
        case 'delete':
            subicon = '/admin/imgs/icons/actions/delete.png';
            break;
        case 'changepassword':
            subicon = '/admin/imgs/icons/actions/changepassword.png';
            break;
        case 'low':
            subicon = '/admin/imgs/icons/existencia.png';
            break;
        case 'cancel':
            subicon = '/admin/imgs/icons/actions/cancelar.png';
            break;
        case 'routed':
        case 'unrouted':
            subicon = '/admin/imgs/icons/rutas.png';
            break;
        case 'assign':
        case 'unassign':
            subicon = '/admin/imgs/icons/credenciales.png';
            break;
        case 'lock':
            subicon = '/admin/imgs/icons/actions/lock.png';
            break;
        case 'unlock':
            subicon = '/admin/imgs/icons/actions/unlock.png';
            break;
        case 'delivery':
            subicon = '/admin/imgs/icons/entregas.png';
            break;
    }
    let iconObj = {
        icon: icon,
        subicon: subicon
    };
    return iconObj;
}

function getNotificacionDescription(notificacion) {
    let type = notificacion['tipo'];
    let parameters = JSON.parse(notificacion['parametros']);
    let fromSelf = userID == notificacion['remitenteID'];
    let remitente = notificacion['remitenteNombre'];
    let description = '';
    switch (type) {
        case 'producto.add':
            description = (fromSelf ? 'Añadiste' : remitente + ' añadio') + 'el producto con folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="productos/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'producto.edit':
            description = (fromSelf ? 'Editaste ' : remitente + ' edito') + ' el producto con folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="productos/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'producto.delete':
            description = (fromSelf ? 'Eliminaste ' : remitente + ' elimino') + 'el producto con folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado ' + parameters['id'] + '">' + parameters['nombre'];
            break;
        case 'producto.low':
            description = 'El producto con folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="productos/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a> esta bajo de inventario';
            break;
        case 'existencia.add': {
            let unitObj = Tools.unitConverter(parameters['unidad']);
            description = (fromSelf ? 'Añadiste' : remitente + ' añadio') + ' ' + parameters['cantidad'] + ' ' + (parameters['cantidad']==1 ? unitObj['singular'] : unitObj['plural']) + ' al producto con folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="productos/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        }
        case 'credencial.add':
            description = (fromSelf ? 'Creaste' : remitente + ' creo') + ' al usuario con el folio ' + ('000' + parameters['id']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
        case 'credencial.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' al usuario con el folio ' + ('000' + parameters['id']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
        case 'credencial.delete':
            description = (fromSelf ? 'Eliminaste' : remitente + ' elimino') + ' al usuario con el folio ' + ('000' + parameters['id']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
        case 'credencial.changepassword':
            description = (fromSelf ? 'Cambiaste el usuario/contraseña' : remitente + ' cambio el usuario/contraseña') + ' del usuario con el folio ' + ('000' + parameters['id']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
        case 'cliente.add':
            description = (fromSelf ? 'Agregaste' : remitente + ' agrego') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'cliente.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'cliente.delete':
            description = (fromSelf ? 'Eliminaste' : remitente + ' elimino') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
        case 'cliente.routed':
            description = (fromSelf ? 'Asignaste' : remitente + ' asigno') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a> a la Ruta llamada <a href="rutas/ver/' + parameters['rutaID'] + '">' + parameters['rutaNombre'] + '</a>';
            break;
        case 'cliente.unrouted':
            description = (fromSelf ? 'Quitaste' : remitente + ' quito') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a> de su Ruta';
            break;
        case 'cliente.lock':
            description = (fromSelf ? 'Bloqueaste' : remitente + ' bloqueo') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '.';
            break;
        case 'cliente.unlock':
            description = (fromSelf ? 'Desbloqueaste' : remitente + ' desbloqueo') + ' al cliente con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="clientes/ver/' + parameters['id'] + '">' + parameters['nombre'] + '.';
            break;
        case 'ruta.add':
            description = (fromSelf ? 'Creaste' : remitente + ' creo') + ' la Ruta llamada <a href="rutas/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'ruta.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' la Ruta llamada <a href="rutas/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'ruta.delete':
            description = (fromSelf ? 'Eliminaste' : remitente + ' elimino') + ' la Ruta llamada ' + parameters['nombre'];
            break;
        case 'ruta.assign':
            description = (fromSelf ? 'Asignaste' : remitente + ' asigno') + ' al usuario con el folio ' + ('000' + parameters['usuarioID']).slice(-4) + ' llamado ' + parameters['usuarioNombre'] + ' a la Ruta llamada <a href="rutas/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'ruta.unassign':
            description = (fromSelf ? 'Quitaste' : remitente + ' quito') + ' al usuario asignado a la Ruta llamada <a href="rutas/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'pedido.add':
            description = (fromSelf ? 'Creaste' : remitente + ' creo') + ' el Pedido con el folio <a href="pedidos/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> para el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'pedido.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' el Pedido con el folio <a href="pedidos/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> de el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'pedido.cancel':
            description = (fromSelf ? 'Cancelaste' : remitente + ' cancelo') + ' el Pedido con el folio <a href="pedidos/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> de el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'pedido.delivery':
            description = (fromSelf ? 'Asignaste' : remitente + ' asigno') + ' a ' + parameters['repartidorNombre'] + ' con el folio ' + ('000' + parameters['repartidorID']).slice(-4) + ' como repartidor al Pedido con el folio <a href="pedidos/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> de el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'pedidopago.add':
            description = (fromSelf ? 'Agregaste' : remitente + ' agrego') + ' un Pago por la cantidad de $ ' + parameters['cantidad'] + ' (' + parameters['metodo'] + ') en el pedido con el folio <a href="pedidos/ver/' + parameters['pedidoID'] + '">' + ('000' + parameters['pedidoFolio']).slice(-4) + '</a> de el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'pedidopago.cancel':
            description = (fromSelf ? 'Cancelaste' : remitente + ' cancelo') + ' un Pago por la cantidad de $ ' + parameters['cantidad'] + ' (' + parameters['metodo'] + ') en el pedido con el folio <a href="pedidos/ver/' + parameters['pedidoID'] + '">' + ('000' + parameters['pedidoFolio']).slice(-4) + '</a> de el cliente <a href="clientes/ver/' + parameters['clienteID'] + '">' + ('000' + parameters['clienteFolio']).slice(-4) + ' ' + parameters['clienteNombre'] + '</a>';
            break;
        case 'inventario.add':
            description = (fromSelf ? 'Realizaste' : remitente + ' realizo') + ' un inventario con el folio <a href="inventarios/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a>';
            break;
        case 'inventario.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' el inventario con el folio <a href="inventarios/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']) + '</a>';
            break;
        case 'inventario.delete':
            description = (fromSelf ? 'Eliminaste' : remitente + ' elimino') + ' el inventario con el folio ' + ('000' + parameters['folio']).slice(-4);
            break;
        case 'caja.open':
            description = (fromSelf ? 'Abriste' : remitente + ' abrio') + ' caja con el folio ' + ('000' + parameters['folio']).slice(-4) + ' y con $' + parseFloat(parameters['dineroApertura']).toFixed(2);
            break;
        case 'compra.add':
            description = (fromSelf ? 'Creaste' : remitente + ' creo') + ' una Compra con el folio <a href="compras/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> para el proveedor <a href="proveedores/ver/' + parameters['proveedorID'] + '">' + ('000' + parameters['proveedorFolio']).slice(-4) + ' ' + parameters['proveedorNombre'] + '</a>';
            break;
        case 'compra.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' una Compra con el folio <a href="compras/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> de el proveedor <a href="proveedores/ver/' + parameters['proveedorID'] + '">' + ('000' + parameters['proveedorFolio']).slice(-4) + ' ' + parameters['proveedorNombre'] + '</a>';
            break;
        case 'compra.cancel':
            description = (fromSelf ? 'Cancelaste' : remitente + ' cancelo') + ' una Compra con el folio <a href="compras/ver/' + parameters['id'] + '">' + ('000' + parameters['folio']).slice(-4) + '</a> de el proveedor <a href="proveedores/ver/' + parameters['proveedorID'] + '">' + ('000' + parameters['proveedorFolio']).slice(-4) + ' ' + parameters['proveedorNombre'] + '</a>';
            break;
        case 'comprapago.add':
            description = (fromSelf ? 'Agregaste' : remitente + ' agrego') + ' un Pago por la cantidad de $ ' + parameters['cantidad'] + ' (' + parameters['metodo'] + ') en la compra con el folio <a href="compras/ver/' + parameters['compraID'] + '">' + ('000' + parameters['compraFolio']).slice(-4) + '</a> de el proveedor <a href="clientes/ver/' + parameters['proveedorID'] + '">' + ('000' + parameters['proveedorFolio']).slice(-4) + ' ' + parameters['proveedorNombre'] + '</a>';
            break;
        case 'comprapago.cancel':
            description = (fromSelf ? 'Cancelaste' : remitente + ' cancelo') + ' un Pago por la cantidad de $ ' + parameters['cantidad'] + ' (' + parameters['metodo'] + ') en la compra con el folio <a href="compras/ver/' + parameters['compraID'] + '">' + ('000' + parameters['compraFolio']).slice(-4) + '</a> de el proveedor <a href="clientes/ver/' + parameters['proveedorID'] + '">' + ('000' + parameters['proveedorFolio']).slice(-4) + ' ' + parameters['proveedorNombre'] + '</a>';
            break;
        case 'proveedor.add':
            description = (fromSelf ? 'Agregaste' : remitente + ' agrego') + ' al proveedor con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="proveedores/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'proveedor.edit':
            description = (fromSelf ? 'Editaste' : remitente + ' edito') + ' al proveedor con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado <a href="proveedores/ver/' + parameters['id'] + '">' + parameters['nombre'] + '</a>';
            break;
        case 'proveedor.delete':
            description = (fromSelf ? 'Eliminaste' : remitente + ' elimino') + ' al proveedor con el folio ' + ('000' + parameters['folio']).slice(-4) + ' llamado ' + parameters['nombre'];
            break;
    }
    return description;
}

function getNotificacionFecha(dateStr) {
    let date = new Date(dateStr);
    let curDate = new Date();
    let dateDiff = Math.floor((curDate.getTime() - date.getTime()) / 1000);
    if (dateDiff < 60) {
        return 'Hace unos segundos';
    }
    else if (dateDiff < 3600) {
        let minutes = Math.floor(dateDiff / 60);
        return 'Hace ' + minutes + ' ' + (minutes == 1 ? 'minuto' : 'minutos');
    }
    else if (dateDiff < 86400) {
        let hours = Math.floor(dateDiff / 3600);
        return 'Hace ' + hours + ' ' + (hours == 1 ? 'hora' : 'horas');
    }
    else if (dateDiff < 172800) {
        return 'Ayer';
    }
    else {
        let days = Math.floor(dateDiff / 172800);
        return 'Hace ' + days + ' ' + (days == 1 ? 'día' : 'dias');
    }
}

})();