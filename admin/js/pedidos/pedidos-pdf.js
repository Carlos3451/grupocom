const { degrees, PDFDocument, rgb, StandardFonts } = PDFLib;

var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:10, bottom:10, right:10};

var logoData = '';
var logoSize = {width:0, height:0};

var pdf;

var originalIDs = idsStr.split('+');
var pedidosIDs = idsStr.split('+');

if (originalIDs.length > 1) {
    document.title = "Imprimir Pedidos";
}

console.log(originalIDs);

toDataURL('/config/logotipo.png', function(dataUrl) {
    let image = new Image();
    image.src = dataUrl;
    logoData = dataUrl;
    image.onload = function() {
        logoSize.width = this.width;
        logoSize.height = this.height;
        generatePDFs(pedidosIDs.shift());
    }
});

function toDataURL(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function() {
        let reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

function generatePDFs(pedidoID) {
    let pedidoData;

    let productosNum = 0;

    let pagado = 0;

    Tools.ajaxRequest({
        data: {
            id: pedidoID
        },
        method: 'POST',
        url: 'functions/pedido/get-print',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                pedidoData = data['resultado'];
                if (originalIDs.length == 1) {
                    document.title = 'F-' + ('000' + pedidoData['folio']).slice(-4);
                }
                try {
                    createPDF();
                }
                catch (err) {
                    console.log(err);
                }
            }
            else {
                console.log(data['error']);
            }
        }
    });

    function createPDF() {
        if (!pdf) {
            pdf = new jsPDF({
                format: 'letter'
            });
        }

        let pageMax = 1;
        let productsMax = 30;

        if (!pedidoData['pedidos']['resultado']) {
            pedidoData['pedidos']['resultado'] = [];
        }

        /*
        for (let i=0; i<productsMax; i++) {
            pedidoData['pedidos']['resultado'][i] = pedidoData['pedidos']['resultado'][0];
        }

        console.log(pedidoData['pedidos']['resultado']);
        */

        if (pedidoData['pagos']['error'] == '') {
            pedidoData['pagos']['resultado'].forEach(function(pago, index) {
                if (!pago['cancelado']) {
                    pagado += parseFloat(pago['cantidad']);
                }
            });
        }

        if (pedidoData['pedidos']['error'] == '') {
            pageMax = Math.ceil(pedidoData['pedidos']['resultado'].length / productsMax);
        }

        let productOffset = 0;

        for (let i=0; i<pageMax; i++) {
            createPage(pdf, i, pageMax, productsMax, productsMax * i);
        }

        /*

        let pageMode = 0;

        if (pedidoData['pedidos']['error'] == '') {
            pageMax = Math.ceil(pedidoData['pedidos']['resultado'].length / productsMax);
            let productsClampeds = ((pedidoData['pedidos']['resultado'].length / (productsMax + emptyPageProductsMax)) % 1) * (productsMax + emptyPageProductsMax);
            if (pageMax > 2) {
                if (productsClampeds > productsMax) {
                    pageMax = Math.ceil((pedidoData['pedidos']['resultado'].length - productsMax) / emptyPageProductsMax) + 1;
                    pageMode = 1;
                }
            }
        }

        for (let i=0; i<pageMax; i++) {
            if (pageMode == 0) {
                let pageProductsMax = Math.ceil(pedidoData['pedidos']['resultado'].length / pageMax);
                createPage(pdf, i, pageMax, pageProductsMax, pageProductsMax * i);
            }
            else {
                let pageProductsMax = Math.ceil((pedidoData['pedidos']['resultado'].length - productsMax) / (pageMax - 1));
                console.log(pedidoData['pedidos']['resultado'].length);
                if (i != pageMax - 1) {
                    createPage(pdf, i, pageMax, pageProductsMax, pageProductsMax * i);
                }
                else {
                    createPage(pdf, i, pageMax, productsMax, pageProductsMax * (pageMax - 1));
                }
            }
        }
        */

        //console.log(productosNum, pedidoData['pedidos']['resultado'].length);
        if (pedidosIDs.length != 0) {            
            pdf.addPage();
            generatePDFs(pedidosIDs.shift());
        }
        else {
            let name;
            if (originalIDs.length == 1) {
                name = 'F-' + ('000' + pedidoData['folio']).slice(-4);
            }
            else {
                name = "Pedidos";
            }
            savePDF(name);
        }
    }

    function createPage(pdf, page, pageMax, productsMax, productsOffset) {

        let pageSize = {width:215.9, height:279.4};
        
        let lineY = 0;
        let lineHeight = 4;

        let pedidoDataLeft = 200;
        let pedidoDataFecha = new Date(pedidoData['fechaCreate']);
        let pedidoFecha = ('00' + pedidoDataFecha.getDate()).slice(-2) + '/' + ('00' + (pedidoDataFecha.getMonth() + 1)).slice(-2) + '/' + pedidoDataFecha.getFullYear();
        let pedidoHora = ('0' + pedidoDataFecha.getHours()).slice(-2) + ':' + ('0' + pedidoDataFecha.getMinutes()).slice(-2) + ':' + ('0' + pedidoDataFecha.getSeconds()).slice(-2);
        
        let clientName = pedidoData['clienteNombre'];

        let logoHeight = 24;
        let logoX = (pageSize.width / 2) - ((logoHeight * (logoSize.width / logoSize.height)) / 2);
        let logoY = 0;

        let informacionY = 28;
        let informacionX = (pageSize.width / 2);

        let clientDataY = 56;
        let clientDataSep = 18;

        let pedidoProductsTop = 72;
        let pedidoProductsHeight = 4;
        let pedidoProductsLeft = margin.left;
        let pedidoProductsSizes = [15, 25, 110, 20, 20];
        let pedidoProductsNames = ['Folio', 'Cantidad', 'Nombre', 'Precio', 'Total'];
        let pedidoProductsKeys = ['folio', 'cantidad', 'nombre', 'precio', 'total'];

        let totalsTop = 200;
        let totalsRight = 18;
        let totalsLeft = 175;
        
        let stepsTop = 214;

        /* LOGOTIPO */

        pdf.addImage(logoData, 'PNG', logoX, logoY, logoHeight * (logoSize.width / logoSize.height), logoHeight);

        lineY = informacionY;

        pdf.setFontSize(24);
        pdf.setFontStyle('bold');
        pdf.text('GRUPO FRONTERA', informacionX, lineY, {align: 'center'});

        pdf.setFontSize(8);
        pdf.setFontStyle('bold');
        lineY += lineHeight;
        pdf.text('BRAULIO PAEZ TAPIA', informacionX, lineY, {align: 'center'});
        lineY += lineHeight;
        pdf.text('RFC: PATB530812RWA', informacionX, lineY, {align: 'center'});
        lineY += lineHeight;
        pdf.text('AV. GUTIERREZ NAJERA, Num. Ext. S/N, Num. Int. A 35, COL. REFORMA', informacionX, lineY, {align: 'center'});
        lineY += lineHeight;
        pdf.text('C.P. 82030 MAZATLÁN, SINALOA, MÉXICO', informacionX, lineY, {align: 'center'});
        lineY += lineHeight;
        pdf.text('TEL. 982-78-92', informacionX, lineY, {align: 'center'});

        pdf.line(margin.left, lineY + 2, 200, lineY + 2);

        /* CLIENTE */
        lineY = clientDataY;

        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        pdf.text('CLIENTE:', margin.left, lineY);
        lineY += lineHeight;
        pdf.text('DIRECCIÓN:', margin.left, lineY);
        lineY += lineHeight;
        pdf.text('TELÉFONO:', margin.left, lineY);

        lineY = clientDataY;

        pdf.setFontSize(9);
        pdf.setFontStyle('normal');
        
        pdf.text('[' + ('0000' + (pedidoData['clienteFolio'] || '')).slice(-4) + '] ' + (clientName || '').toUpperCase(), margin.left + clientDataSep, lineY);
        lineY += lineHeight;
        pdf.text(pedidoData['clienteDireccionCompleta'], margin.left + clientDataSep, lineY);
        lineY += lineHeight;
        pdf.text((pedidoData['clienteTelefono'] || '').toUpperCase(), margin.left + clientDataSep, lineY);

        //DATOS

        let pedidoDatosLeft = 140;
        let pedidosDatosSep = 15;

        lineY = clientDataY;

        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        pdf.text('FOLIO:', pedidoDatosLeft, lineY);
        lineY += lineHeight;
        pdf.text('FECHA:', pedidoDatosLeft, lineY);
        lineY += lineHeight;
        pdf.text('EMISOR:', pedidoDatosLeft, lineY);

        lineY = clientDataY;

        pdf.setFontSize(9);
        pdf.setFontStyle('normal');
        
        pdf.text('F-' + ('0000' + (pedidoData['folio'])).slice(-1 * Math.max(4, pedidoData['folio'].toString().length)), pedidoDatosLeft + pedidosDatosSep, lineY);
        lineY += lineHeight;
        pdf.text(pedidoFecha, pedidoDatosLeft + pedidosDatosSep, lineY);
        lineY += lineHeight;
        pdf.text((pedidoData['usuarioNombre']).toUpperCase(), pedidoDatosLeft + pedidosDatosSep, lineY);

        /*
        pdf.text('F-' + ('000' + pedidoData['folio']).slice(-4), pedidoDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        pdf.text(pedidoFecha + ' ' + pedidoHora, pedidoDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        switch (pedidoData['tipo']) {
            case 'contado':
                pdf.text('Preventa al Contado', pedidoDataLeft, lineY, { align: 'right' });
                break;
            case 'credito':
                pdf.text('Preventa a Credito', pedidoDataLeft, lineY, { align: 'right' });
                break;
        }
        lineY += lineHeight;
        pdf.text(pedidoData['usuarioNombre'], pedidoDataLeft, lineY, { align: 'right' });
        */

        if (pageMax) {
            pdf.text('Página ' + (page + 1) + ' / ' + pageMax, pedidoDataLeft, 15, { align: 'right' });
        }

        pdf.line(margin.left, lineY + 3, 200, lineY + 3);

        /* PRODUCTOS */
        
        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        let columnSep = pedidoProductsLeft;
        let columnTextSep = 2;
        //pdf.line(columnSep, pedidoProductsTop - 4, columnSep, pedidoProductsTop + 32);
        pedidoProductsKeys.forEach(function(key, index) {
            //pdf.line(columnSep + pedidoProductsSizes[index], pedidoProductsTop - 4, columnSep + pedidoProductsSizes[index], pedidoProductsTop + 32);
            if (key == 'precio' || key == 'total' || key == 'cantidad') {
                pdf.text(pedidoProductsNames[index], columnSep + pedidoProductsSizes[index] - columnTextSep, pedidoProductsTop, { align: 'right' });
            }
            else {
                pdf.text(pedidoProductsNames[index], columnSep + columnTextSep, pedidoProductsTop);
            }
            columnSep += pedidoProductsSizes[index];
        });

        pdf.setFontSize(9);
        pdf.setFontStyle('normal');

        if (pedidoData['pedidos']['error'] == '') {
            let productos = pedidoData['pedidos']['resultado'];
            lineY = pedidoProductsHeight;
            for (let i=0; i<Math.min(productsMax, productos.length - productsOffset); i++) {
                let producto = productos[i + productsOffset];
                if ((i % 2) == 1) {
                    pdf.setFillColor('#EEE');
                    pdf.rect(pedidoProductsLeft, pedidoProductsTop + lineY - 3, 190, 4, 'F');
                }
                //productosNum++;
                columnSep = pedidoProductsLeft;
                pedidoProductsKeys.forEach(function(key, index) {
                    switch (key) {
                        case 'cantidad': {
                            let unitObj = Tools.unitConverter(producto['unidad']);
                            let decimalPlaces =  Tools.getDecimals(parseFloat(producto['cantidad']));
                            if (unitObj['zeros'] && !decimalPlaces) {
                                pdf.text(parseFloat(producto['cantidad']).toFixed(0), columnSep + pedidoProductsSizes[index] - columnTextSep - 8, pedidoProductsTop + lineY, { align: 'right' });
                            }
                            else {
                                pdf.text(parseFloat(producto['cantidad']).toFixed(unitObj['zeros'] || decimalPlaces), columnSep + pedidoProductsSizes[index] - columnTextSep - 8, pedidoProductsTop + lineY, { align: 'right' });
                            }
                            pdf.text(producto['cantidad'] == 1 ? unitObj['singular'] : unitObj['plural'], columnSep + pedidoProductsSizes[index] - columnTextSep - 6, pedidoProductsTop + lineY);
                            break;
                        }
                        case 'folio':
                            pdf.text(('000' + producto['folio']).slice(-4), columnSep + columnTextSep, pedidoProductsTop + lineY);
                            //pdf.text(String(i + productsOffset), columnSep + columnTextSep, pedidoProductsTop + lineY);
                            break;
                        case 'nombre':
                            pdf.text(producto['nombre'], columnSep + columnTextSep, pedidoProductsTop + lineY);
                            break;
                        case 'precio':
                        case 'total':
                            pdf.text('$ ' + parseFloat(producto[key]).toFixed(2), columnSep + pedidoProductsSizes[index] - columnTextSep, pedidoProductsTop + lineY, { align: 'right' });
                            break;
                    }
                    columnSep += pedidoProductsSizes[index];
                });
                lineY += pedidoProductsHeight;
            }
        }

        if (page == pageMax - 1) {
            /* TOTAL */

            let pedidoSubtotal = 0;
            
            let productos = pedidoData['pedidos']['resultado'];

            for (let i=0; i<productos.length; i++) {
                pedidoSubtotal += parseFloat(productos[i]['total']);
            }

            let pedidoResto = pedidoSubtotal - pagado;
            let pedidoTotal = pedidoSubtotal - parseFloat(pedidoData['descuentoCantidad']);

            pdf.setFontSize(8);
            pdf.setFontStyle('normal');

            lineY = totalsTop;
            pdf.text('$ ' + parseFloat(pedidoSubtotal).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('$ ' + parseFloat(pedidoData['descuentoCantidad']).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('$ ' + parseFloat(pedidoTotal).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            lineY += lineHeight;
            //pdf.text('$ ' + pagado.toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            //lineY += lineHeight;
            //pdf.text('$ ' + pedidoResto.toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            
            pdf.setFontStyle('bold');

            lineY = totalsTop;
            pdf.text('Subotal', totalsLeft, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('Descuento (0%)', totalsLeft + 2, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('Total', totalsLeft, lineY, { align: 'right' });
            lineY += lineHeight;
            //pdf.text('Pagado', totalsLeft, lineY, { align: 'right' });
            //lineY += lineHeight;
            //pdf.text('Resto', totalsLeft, lineY, { align: 'right' });

            lineY = stepsTop;

            pdf.setFontStyle('normal');

            pdf.text('1 - CHECAR BIEN SU MERCANCIA AL MOMENTO DE RECIBIRLA', margin.left, lineY);
            lineY += lineHeight;
            pdf.text('2 - NO REALIZAR PAGOS NI ABONOS SIN FACTURA ORIGINAL', margin.left, lineY);
            lineY += lineHeight;
            pdf.text('3 - VERIFIQUE QUE SU ABONO SEA ANOTADO EN LA FACTURA', margin.left, lineY);
            lineY += lineHeight;
            
            //PAGARE

            pdf.setFontStyle('bold');
            
            pdf.line(margin.left, lineY - 1, pageSize.width - margin.right, lineY - 1);
            lineY += lineHeight;
            pdf.text('PAGARE', margin.left, lineY);
            //pdf.text('BUENO POR: $ ' + parseFloat(pedidoData['total']).toFixed(2), margin.left + 50, lineY);
            pdf.text('BUENO POR: $ ' + parseFloat(pedidoTotal).toFixed(2), margin.left + 50, lineY);
            pdf.text('F-' + ('000' + pedidoData['folio']).slice(-4), margin.left + 100, lineY);
            lineY += lineHeight;
            lineY += lineHeight;
            pdf.setFontStyle('normal');
            pdf.text('Debe(mos) y pagare(mos) incondicionalmente por este pagare a la orden de GRUPO FRONTERA en la ciudad de Mazatlán, Sin. el ' + pedidoFecha + ' o en cualquier otra que se me requiera la cantidad de ' + Tools.numeroALetras(pedidoTotal) + ' Valor recibido a mi (nuestra) entera satisfacción. Se solventaran las obligaciones adquiridas en este pagare entregando su equivalente en moneda nacional a la fecha de pago. De no verificarse el pago de la cantidad que este pagare expresa el día de su vencimiento causara interés moratorios al tipo de 5% mensuales (cinco por ciento) pagadero en esta ciudad. Este pagare es mercantil y esta regido por la Ley General de Títulos y Operaciones de Crédito en su articulo 173 y artículos correlativos, por no ser pagare domiciliado.', margin.left, lineY, { maxWidth: (pageSize.width - margin.left - margin.right) });
            pdf.line(80, lineY + 30, pageSize.width - 80, lineY + 30);
            pdf.setFontStyle('bold');
            pdf.text(clientName || '', pageSize.width / 2, lineY + 34, { align: 'center' });
        }

        if (page < pageMax - 1) {
            pdf.addPage();
        }

    }
}

async function savePDF(name) {
    let dataBytes = pdf.output('arraybuffer');

    let pdfDoc = await PDFDocument.load(dataBytes);
    let helveticaFont = await pdfDoc.embedFont(StandardFonts.HelveticaBold);

    let pages = pdfDoc.getPages();
    let pagesCopiesNumbers = [];
    for (let i=0; i<pages.length; i++) {
        pagesCopiesNumbers.push(i);
    }

    let pagesCopies = await pdfDoc.copyPages(pdfDoc, pagesCopiesNumbers);

    for (let i=0; i<pagesCopies.length; i++) {
        pdfDoc.addPage(pagesCopies[i]);
    }

    pages = pdfDoc.getPages();

    let xx = -70;
    let yy = 80;

    for (let i=pages.length / 2; i<pages.length; i++) {
        console.log(i);
        pages[i].drawRectangle({
            x: 0 + xx,
            y: 505 + yy,
            rotate: degrees(45),
            width: 400,
            height: 40,
            color: rgb(0.070, 0.043, 0.458),
        });
        pages[i].drawText('COPIA', {
            x: 100 + xx,
            y: 615 + yy,
            size: 32,
            rotate: degrees(45),
            font: helveticaFont,
            color: rgb(1.0, 1.0, 1.0)
        });
    }

    let pdfBytes = await pdfDoc.save();
    
    let pdfBlob = new Blob([pdfBytes], {type: 'application/pdf'});

    let file = new File([pdfBytes], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI); 
}