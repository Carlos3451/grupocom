(function(){

let newsTimeout = null;

if (privilegios.indexOf('facturacion') != -1) {

    Lista.actionFacturar['active'] = true;
    Lista.actionFacturar['unique'] = true;
    Lista.facturarFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        window.location = '/admin/facturas-v4/agregar/?tipo=pedido&id=' + dataID;
    }

}

Lista.actionPrint['active'] = true;
Lista.actionPrint['unique'] = false;
Lista.printFn = function(selecteds) {
    let ids = [];
    cancelleds = 0;
    for (let i=0; i<selecteds.length; i++) {
        ids.push(selecteds[i].getAttribute('data-id'));
    }
    Tools.ajaxRequest({
        data: {
            ids: ids
        },
        method: 'POST',
        url: 'functions/pedido/printed',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                selecteds.forEach(function(elem, index) {
                    elem.querySelector('.printed-status').classList.add('printed');
                    elem.querySelector('.printed-status').setAttribute('tooltip-ready-hint', 'Impreso');
                });
            }
            else {
                console.log(data['error']);
            }
        }
    });
    window.open('/admin/pedidos/imprimir/' + ids.join('+'));
}

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/pedidos/ver/" + dataID;
}

if (privilegiosNivel > 1) {
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        if (!selecteds[0].hasAttribute('cancelled')) {
            window.location = "/admin/pedidos/editar/" + dataID;
        }
        else {
            new Message('No se puede editar un pedido cancelado.', 'warning');
        }
    }
}

Lista.actionPay['active'] = true;
Lista.actionPay['unique'] = true;
Lista.payFn = function(selecteds) {
    if (selecteds[0].hasAttribute('cancelled')) {
        new Message('El pedido esta cancelado.', 'warning');
    }
    else if (selecteds[0].hasAttribute('payed')) {
        new Message('Este pedido ya esta completamente pagado.', 'warning');
    }
    else {
        let dataID = selecteds[0].getAttribute('data-id');
        let total = selecteds[0].getAttribute('data-total');
        let payed = selecteds[0].getAttribute('data-pagado');
        let rest = parseFloat(total - payed);
        Payment.open(rest);        
        Payment.onAccept = function(method, quantity) {
            let date = new Date();
            Tools.ajaxRequest({
                data: {
                    pedidoID: dataID,
                    fecha: Math.floor(date.getTime() / 1000),
                    metodo: method,
                    cantidad: quantity
                },
                method: 'POST',
                url: 'functions/pedido/add-payment',
                waitMsg: new Wait('Agregando pago.'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message('Pago agregado con exito.', 'success');
                        payed = parseFloat(payed) + parseFloat(quantity);
                        rest = parseFloat(total - payed);
                        selecteds[0].setAttribute('data-pagado', payed);
                        if (rest <= 0) {
                            selecteds[0].setAttribute('payed', '');
                            selecteds[0].querySelectorAll('.list-column')[7].querySelector('p').textContent = 'Pagado';
                        }
                        else {
                            selecteds[0].querySelectorAll('.list-column')[7].querySelector('p').textContent = '$' + rest.toFixed(2);
                        }
                    }
                    else {
                        new Message('Ocurrio un error al tratar de agregar el pago.<br>' + data['error'], 'success');
                    }
                }
            });
        }
    }
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = false;
    Lista.cancelFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se cancelara el pedido seleccionada.',
            msgWait: 'Cancelando pedido.',
            msgSuccess: 'Pedido cancelado con exito.',
            msgError: 'Ocurrio un error al tratar de cancelar el pedido.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se cancelaran los pedidos seleccionadas.',
                msgWait: 'Cancelando pedidos.',
                msgSuccess: 'Pedidos cancelados con exito.',
                msgError: 'Ocurrio un error al tratar de cancelar los pedidos.'
            };
        }
        let ids = [];
        let cancelleds = 0;
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
            if (selecteds[i].hasAttribute('cancelled')) {
                cancelleds += 1;
            }
        }
        if (cancelleds == 0) {
            new Confirm(requestObj.msgQuestion, 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        ids: ids
                    },
                    method: 'POST',
                    url: 'functions/pedido/cancel',
                    waitMsg: new Wait(requestObj['msgWait'], true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message(requestObj['msgSuccess'], 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        }
        else {
            if (selecteds.length == 1) {
                new Message('No se puede cancelar un pedido ya cancelado.', 'warning');
            }
            else if (selecteds.length == cancelleds) {
                new Message('No se pueden cancelar pedidos ya cancelados.', 'warning');
            }
            else {
                new Message('Existen algunos pedidos seleccionados que ya estan cancelados.', 'warning');                
            }
        }
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('pedido/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let dateTimeArray = data['fechaCreate'].split(' ');
        let dateArray = dateTimeArray[0].split('-');
        let timeArray = dateTimeArray[1].split(':');
        let payed = data['total'] - data['pagado'];
        return [
            '<a href="/admin/pedidos/ver/' + data['id'] + '">' + ('000' + data['folio']).slice(-4) + '</a>',
            dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0] + ' ' + dateTimeArray[1],
            '<a href="/admin/clientes/ver/' + data['clienteID'] + '">' + data['clienteNombre'] + '</a>',
            data['usuarioNombre'],
            data['tipo'][0].toUpperCase() + data['tipo'].slice(1),
            '<p class="text-align-right">$ ' + data['total'] + '</p>',
            '<p class="text-align-right">' + (data['cancelado'] == 0 ? (payed <= 0 ? 'Pagado' : '$' + (payed).toFixed(2)) : 'CANCELADO' ) + '</p>',
            '<div tooltip-hint="' + (data['impreso'] == 1 ? 'Impreso' : 'Sin imprimir') + '" class="printed-status' + (data['impreso'] == 1 ? ' printed' : '') + '"></div>'
        ];
    }, function(data) {
        let payed = data['total'] - data['pagado'];
        let attrs = [
            ['data-id', data['id']],
            ['data-total', data['total']],
            ['data-pagado', data['pagado']]
        ];
        if (payed <= 0) {
            attrs.push(['payed', '']);
        }
        if (data['cancelado'] == 1) {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    }, function(response) {
        getNewsPedidos(0);
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['Cliente', 'input', 'cliente', 'text'],
        ['Usuario', 'input', 'usuario', 'text']
    ],
    [
        ['Estado', 'select', 'estado', [
                ['Todos', ''],
                ['Pendientes', 'pendientes'],
                ['Pagados', 'pagados'],
                ['Cancelados', 'cancelados']
            ]
        ],
        ['Tipo', 'select', 'tipo', [
                ['Todos', ''],
                ['Contado', 'contado'],
                ['Credito', 'credito']
            ]
        ]
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    monthSelect: true,
    yearSelect: true
});
let clienteSI = new SelectInp(document.querySelector('[name="cliente"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});
let usuarioSI = new SelectInp(document.querySelector('[name="usuario"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});

Tools.ajaxRequest({
    data: {
        metodo: 'folio',
        orden: 'asc',
        busqueda: ''
    },
        method: 'POST',
        url: 'functions/cliente/get-all-min',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let clientes = data['resultado'];
            for (let i=0; i<clientes.length; i++) {
                options.push({
                    'name': ('000' + clientes[i]['folio']).slice(-4) + ' ' + clientes[i]['nombre'],
                    'value': clientes[i]['id'],
                    'keys': [('000' + clientes[i]['folio']).slice(-4), clientes[i]['nombre']]
                });
            }
            clienteSI.setOptions(options);
        }
    }
});
Tools.ajaxRequest({
    data: {
        metodo: 'id',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            usuarioSI.setOptions(options);
        }
    }
});

SortController.load();

function getNewsPedidos(idMax) {
    Tools.ajaxRequest({
        data: {
            busqueda: SortController.search,
            metodo: SortController.method,
            orden: SortController.order,
            filtros: SortController.filters,
            ultimoID: idMax
        },
        method: 'POST',
        url: 'functions/pedido/get-news',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (idMax != 0) {
                    document.querySelector('.list-news').classList.add('open');
                    if (data['resultado']['nuevos'] == 1) {
                        document.querySelector('.list-news > a').textContent = '1 pedido nuevo. Haz click aquí para recargar.';
                    }
                    else {
                        document.querySelector('.list-news > a').textContent = data['resultado']['nuevos'] + ' pedidos nuevos. Haz click aquí para recargar.';
                    }
                }
                else {
                    idMax = data['resultado']['idMax'];
                }
            }
            if (newsTimeout) {
                clearTimeout(newsTimeout);
            }
            newsTimeout = setTimeout(function() {
                getNewsPedidos(idMax);
            }, 60000);
        }
    });
}

})();