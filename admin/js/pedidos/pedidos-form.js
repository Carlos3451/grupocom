(function() {

let productList = [];

let pedidoProductsDeleted = [];

let clienteAc;
let productosAc;

let clientesPromise;

clearForm();

if (formMode != 'VIEW') {
    productosAc = new SelectInp(document.querySelector('[name="producto-buscador"]'), {
        placeholder: 'Producto',
        openEmpty: false,
        selectOnHover: false
    });
    clienteAc = new SelectInp(document.querySelector('[name="clienteID"]'), {
        placeholder: 'S/D',
        openEmpty: false,
        selectOnHover: false,
        clearSearcher: false,
        defaultValue: 0
    });
    clienteAc.searchInput.addEventListener('keydown', function(ev) {
        let key = ev.key.toLowerCase();
        if (key == 'enter') {
            productosAc.searchInput.focus();
        }
    })
    clientesPromise = getClients();
}
else {
    document.querySelector('[name="clienteID"]').disabled = true;
}

if (formMode == 'EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos...');
    getPedido();
}

let fechaDP = new DatePicker(document.querySelector('[name="fecha"]'));

let productosPromise = getProducts();

function getPedido() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/pedido/get',
        success: function(response) {
            productosPromise.then(() => {
                loadingDataMsg.destroy();
            });
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadPedido(data['resultado']);
            }
        }
    });
}

function getClients() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            url: 'functions/cliente/get-all-min',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        options.push({
                            'name': result[i]['nombre'],
                            'value': result[i]['id'],
                            'keys': [result[i]['nombre']]
                        });
                    }
                    clienteAc.setOptions(options);
                }
                resolve();
            }
        });
    });
    return promise;
}

function getProducts() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'folio',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/producto/get-all-min',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    productList = result;
                    let options = [];
                    for (let i=0; i<result.length; i++) {
                        if (result[i]['bloqueado'] == 0) {
                            options.push({
                                'name': result[i]['nombre'],
                                'value': result[i]['id'],
                                'keys': [result[i]['nombre']]
                            });
                        }
                    }
                    if (formMode != 'VIEW') {
                        productosAc.setOptions(options);
                    }
                }
                resolve();
            }
        });
    });
    return promise;
}

function loadPedido(pedido) {
    document.querySelector('[name="subtotal"]').value = pedido['subtotal'];
    document.querySelector('[name="descuentoPorcentaje"]').value = pedido['descuentoPorcentaje'];
    document.querySelector('[name="descuentoCantidad"]').value = pedido['descuentoCantidad'];
    document.querySelector('[name="total"]').value = pedido['total'];

    if (clientesPromise) {
        clientesPromise.then(() => {
            clienteAc.searchInput.value = pedido['clienteNombre'];
            clienteAc.searchInput.dispatchEvent(new Event('change'));
            clienteAc.ghostInput.value = pedido.clienteID;
        });
    }
    else {
        document.querySelector('[name="clienteID"]').value = pedido['clienteNombre'];
    }
    
    let fechaArr = pedido['fechaCreate'].split('-');
    fechaDP.changeDate(new Date(parseFloat(fechaArr[0]), parseFloat(fechaArr[1]) - 1, parseFloat(fechaArr[2])));
    
    productosPromise.then(() => {
        let productosPedidos = pedido['pedidos'];
        if (productosPedidos['error'] == '') {
            let productos = productosPedidos['resultado'];
            for (let i=0; i<productos.length; i++) {
                addProduct(productos[i]);
            }
        }
    });
    let pagosData = pedido['pagos'];
    if (pagosData['error'] == '') {
        let pagos = pagosData['resultado'];
        for (let i=0; i<pagos.length; i++) {
            let pago = pagos[i];
            addPayment(new Date(pago['fechaCreate']), pago['metodo'], pago['cantidad'], pago['cancelado'], pago['id']);
        }
    }
    if (formMode == 'VIEW') {
        disableInputs();
    }
    updatePaymentsTotal();
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
}

function submitForm(nombreCliente = '') {
    let clientID = nombreCliente == '' ? document.querySelector('[name="clienteID"]').value : 1;
    document.querySelector('.form-submit').setAttribute('disabled', '');
    if (clientID == 0 && nombreCliente == '') {
        let clientName = clienteAc.searchInput.value;
        new Question('No existe el cliente llamado ' + clientName, '¿Desea registrar como Publico en General?', function(){
            submitForm(clientName);
        }, function() {
            document.querySelector('.form-submit').removeAttribute('disabled');
        });
        /*
        if (privileges.indexOf('clientes') != -1) {
            new Question('No existe el cliente llamado ' + clientName, '¿Desea darlo de alta?', function(){
                Tools.ajaxRequest({
                    data: {
                        nombre: clientName,
                        telefono: '',
                        email: '',
                        calle: '',
                        colonia: '',
                        ciudad: 'Mazatlán',
                        estado: 'Sinaloa',
                        codigoPostal: '',
                        numeroExterior: '',
                        numeroInterior: '',
                        credito: 0,
                        rutaID: 0,
                        rutaDias: 0,
                        lat: 23.223437,
                        lng: -106.416192
                    },
                    url: 'functions/cliente/add',
                    method: 'POST',
                    waitMsg: new Wait('Creando cliente.'),
                    success: function(response) {
                        this.waitMsg.destroy();
                        document.querySelector('.form-submit').removeAttribute('disabled');
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Cliente creado con exito. Ya puede continuar.', 'success', function() {
                                document.querySelector('[name="clienteID"]').value = data['resultado']['id'];
                                submitForm();
                            });
                        }
                        else {
                            new Message('No se pudo crear el cliente. Intentelo de nuevo.<br>' + data['error'], 'error');
                        }
                    }
                });
            }, function() {
                document.querySelector('.form-submit').removeAttribute('disabled');
            });
        }
        else {
            new Message('El cliente llamado ' + clientName + ' no existe.', 'warning');
            document.querySelector('.form-submit').removeAttribute('disabled');
        }
        */
    }
    else {
        let pedidoProductos = getPedidoProductsData();
        let pagos = getPaymentsData();
        let fechaArr = document.querySelector('[name="fecha"]').value.split('/');
        let fechaTime = Math.floor((new Date(parseFloat(fechaArr[2]), parseFloat(fechaArr[1]) - 1, parseFloat(fechaArr[0]))).getTime() / 1000) + 3600;
        submitObj = {};
        switch (formMode) {
            case 'ADD':
                submitObj = {
                    url: 'functions/pedido/add',
                    msgWait: 'Creando pedido.',
                    msgSuccess: 'Pedido creado con exito.',
                    msgError: 'Ocurrio un error al tratar de crear el pedido.',
                }
                break;
            case 'EDIT':
                submitObj = {
                    url: 'functions/pedido/edit',
                    msgWait: 'Guardando pedido.',
                    msgSuccess: 'Pedido guardado con exito.',
                    msgError: 'Ocurrio un error al tratar de guardado el pedido.',
                }
                break;
        }
        Tools.ajaxRequest({
            data: {
                id: editID,
                clienteID: clientID,
                clienteNombre: nombreCliente,
                pedidoProductos: pedidoProductos,
                pedidoProductosEliminados: pedidoProductsDeleted,
                pagos: pagos,
                subtotal: document.querySelector('[name="subtotal"]').value,
                descuentoPorcentaje: document.querySelector('[name="descuentoPorcentaje"]').value,
                descuentoCantidad: document.querySelector('[name="descuentoCantidad"]').value,
                total: document.querySelector('[name="total"]').value,
                fecha: fechaTime
            },
            url: submitObj['url'],
            method: 'POST',
            waitMsg: new Wait(submitObj['msgWait']),
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message(submitObj['msgSuccess'], 'success', function() {
                        new Question('Imprimir pedido', '¿Desea imprimir este pedido?', function() {
                            let pedidoID = formMode == 'EDIT' ? editID : data['resultado']['id'];
                            Tools.ajaxRequest({
                                data: {
                                    ids: [pedidoID]
                                },
                                method: 'POST',
                                url: 'functions/pedido/printed',
                                waitMsg: new Wait('Imprimiendo'),
                                success: function(response) {
                                    this.waitMsg.destroy();
                                    window.location = '/admin/pedidos';
                                }
                            });
                            window.open('/admin/pedidos/imprimir/' + pedidoID);
                        }, function() {
                            window.location = '/admin/pedidos';
                        });
                    });
                }
                else {
                    new Message(submitObj['msgError'] + '<br>' + data['error'], 'error');
                }
            }
        });
    }
}

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
    document.querySelectorAll('#data-form .accounting-container input').forEach(function(elem, index) {
        elem.value = '0.00';
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#data-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('click', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
    this.removeEventListener('click', clearInvalid);
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/pedidos';
        });
    }
    else {        
        window.location = '/admin/pedidos';
    }
});

/* AÑADIR PRODUCTO */
if (formMode != 'VIEW') {
    productosAc.onChange = function() {
        if (this.value == '') {
            return;
        }
        let productID = this.value;
        let producto = productList.find(function(value) {
            return value['id'] == productID;
        });
        let productObj = {
            productoID: producto['id'],
            nombre: producto['nombre'],
            unidad: producto['unidad'],
            costo: producto['costo'],
            precio1: producto['precio1'],
            precio2: producto['precio2'],
            precio3: producto['precio3']
        };
        addProduct(productObj);
        this.clear();
    }
}

function addProduct(productObj) {
    let rowElem = document.createElement('div');
    let unitObj = Tools.unitConverter(productObj['unidad']);
    rowElem.className = 'pedido-table-row';
    rowElem.dataset.productoid = productObj.productoID;
    rowElem.dataset.id = productObj.id || 0;
    rowElem.dataset.contenido = 1;
    rowElem.dataset.nombre = productObj.nombre;
    rowElem.dataset.costo = productObj.costo;
    rowElem.innerHTML = (
        '<div class="pedido-table-column">'+
            '<input type="text" name="producto-nombre" disabled>'+
        '</div>'+
        '<div class="pedido-table-column">'+
            '<select name="producto-unidad">'+
                '<option value="' + productObj.unidad + '">' + productObj.unidad + '</option>'+
            '</select>'+
        '</div>'+
        '<div class="pedido-table-column">'+
            '<input class="inp-unit inp-' + productObj.unidad + ' text-align-right" type="text" name="producto-cantidad" autocomplete="off">'+
        '</div>'+
        '<div class="pedido-table-column prices-container">'+
        '</div>'+
        '<div class="pedido-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-total" value="0.00" disabled>'+
        '</div>'+
        '<div class="pedido-table-column">'+
            '<div class="actions-container">'+
                '<img tooltip-hint="Subir" class="product-up" src="/admin/imgs/icons/actions/up.png">'+
                '<img tooltip-hint="Bajar" class="product-down" src="/admin/imgs/icons/actions/down.png">'+
                '<img tooltip-hint="Eliminar" class="product-delete" src="/admin/imgs/icons/actions/delete.png">'+
            '</div>'+
        '</div>'
    );
    let productoRaw = productList.find(v => v.id == productObj.productoID);
    if (productoRaw) {
        let productoUnidadInp = rowElem.querySelector('[name="producto-unidad"]');
        for (let i=0; i<productoRaw.productoUnidades.length; i++) {
            let productoUnidad = productoRaw.productoUnidades[i];
            let option = document.createElement('option');
            option.value = productoUnidad.unidad;
            option.textContent = productoUnidad.unidad;
            productoUnidadInp.appendChild(option);
        }
        productoUnidadInp.addEventListener('change', ev => {
            updateRowPriceList(rowElem);
            updatePedidoRowTotal(rowElem);
        });
    }
    updateRowPriceList(rowElem);
    if (!productObj['id']) {
        rowElem.querySelector('[name="producto-cantidad"]').value = 1;
        rowElem.querySelector('[name="producto-total"]').value = rowElem.querySelector('[name="producto-precio"]').value;
        setTimeout(function(){
            rowElem.querySelector('[name="producto-cantidad"]').focus();
            updatePedidoSubtotal();
        }, 10);
    }
    else {
        rowElem.querySelector('[name="producto-unidad"]').value = productObj.unidad;
        updateRowPriceList(rowElem);
        rowElem.querySelector('[name="producto-cantidad"]').value = parseFloat(parseFloat(productObj['cantidad']).toFixed(3));
        rowElem.querySelector('[name="producto-precio"]').value = parseFloat(productObj.precio).toFixed(2);
        rowElem.querySelector('[name="producto-total"]').value = productObj['total'];
    }
    rowElem.querySelector('[name="producto-nombre"]').value = productObj['nombre'];

    document.querySelector('.pedido-table').appendChild(rowElem);

    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('keydown', productoCantidadKeydown);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('keydown', checkNumericEv);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('input', () => updatePedidoRowTotal(rowElem));
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('focus', selectInput);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('blur', blurDecimal);
    rowElem.querySelector('[name="producto-cantidad"]').addEventListener('blur', () => updatePedidoRowTotal(rowElem));

    if (formMode == 'VIEW') {
        rowElem.querySelector('.actions-container').remove();
    }
    else {
        rowElem.querySelector('.product-up').addEventListener('click', animateMoveUpPedidoRow);
        rowElem.querySelector('.product-down').addEventListener('click', animateMoveDownPedidoRow);
        rowElem.querySelector('.product-delete').addEventListener('click', deletePedidoRow);
    }

    ToolTip.update();
}

function updateRowPriceList(row) {
    let productoRaw = productList.find(v => v.id == row.dataset.productoid);
    if (!productoRaw) {
        return;
    }
    let productoCantidadInp = row.querySelector('[name="producto-cantidad"]');
    let pricesContainer = row.querySelector('.prices-container');
    while (pricesContainer.firstChild) {
        pricesContainer.firstChild.remove();
    }
    let unidad = row.querySelector('[name="producto-unidad"]').value;
    productoCantidadInp.className = 'text-align-right inp-unit inp-' + unidad;
    let preciosArray = [];
    if (unidad == productoRaw.unidad) {
        row.dataset.contenido = 1;
        preciosArray = [['Menudeo', productoRaw.precio1], ['Medio Mayoreo', productoRaw.precio2], ['Mayoreo', productoRaw.precio3]].filter(function(value) {
            return parseFloat(value) != 0;
        });
    }
    else {
        let productoUnidad = productoRaw.productoUnidades.find(v => v.unidad == unidad);
        if (productoUnidad) {
            row.dataset.contenido = productoUnidad.contenido;
            preciosArray = [['Menudeo', productoUnidad.precio1], ['Medio Mayoreo', productoUnidad.precio2], ['Mayoreo', productoUnidad.precio3]].filter(function(value) {
                return parseFloat(value[1]) != 0;
            });
        }
    }
    if (privilegesLevel > 1) {
        let priceSelector = document.createElement('input');
        priceSelector.className = 'inp-icon inp-price';
        priceSelector.name = 'producto-precio';
        priceSelector.autocomplete = 'off';
        priceSelector.value = preciosArray[0][1];
        pricesContainer.appendChild(priceSelector);
        if (preciosArray.length > 1) {
            let prices = [];
            for (let i=0; i<preciosArray.length; i++) {
                prices.push({
                    name: preciosArray[i][0] + '<br>' + preciosArray[i][1],
                    value: preciosArray[i][1],
                    keys: [preciosArray[i][1]]
                });
            }
            let priceSI = new SelectInp(priceSelector, {
                autocompleter: true,
                selectOnHover: false
            });
            priceSI.setOptions(prices);
        }
        row.querySelector('[name="producto-precio"]').addEventListener('keydown', productoPrecioKeydown);
        row.querySelector('[name="producto-precio"]').addEventListener('keydown', checkNumericEv);
        row.querySelector('[name="producto-precio"]').addEventListener('input', () => updatePedidoRowTotal(row));
        row.querySelector('[name="producto-precio"]').addEventListener('change', () => updatePedidoRowTotal(row));
        row.querySelector('[name="producto-precio"]').addEventListener('blur', blurInputNumeric);
        row.querySelector('[name="producto-precio"]').addEventListener('focus', function(){
            this.select();
        });
    }
    else if (preciosArray.length > 1) {
        let priceSelector = document.createElement('select');
        priceSelector.className = 'inp-icon inp-price';
        priceSelector.setAttribute('name', 'producto-precio');
        for (let i=0; i<preciosArray.length; i++) {
            priceSelector.innerHTML += '<option value="' + preciosArray[i][1] + '">' + preciosArray[i][1] + '</option>';
        }
        row.querySelector('.prices-container').appendChild(priceSelector);
        priceSelector.addEventListener('change', () => updatePedidoRowTotal(row));
    }
    else {
        let priceInput = document.createElement('input');
        priceInput.className = 'inp-icon inp-price';
        priceInput.setAttribute('name', 'producto-precio');
        priceInput.setAttribute('disabled', '');
        priceInput.value = preciosArray[0][1] || '0.00';
        row.querySelector('.prices-container').appendChild(priceInput);
    }
}

function productoPrecioKeydown(ev) {
    let value = this.value;
    switch (ev.key.toLowerCase()) {
        case 'enter':
            let rect = productosAc.searchInput.getBoundingClientRect();
            if (rect.top < 48) {
                window.scrollBy(0, rect.top - 80);
            }
            productosAc.searchInput.focus();
            break;
    }
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

function productoCantidadKeydown(ev) {
    let value = this.value;
    switch (ev.key.toLowerCase()) {
        case 'enter':
            let row = this.closest('.pedido-table-row');
            let priceInp = row.querySelector('[name="producto-precio"]');
            if (priceInp) {
                priceInp.focus();
            }
            else {
                productosAc.searchInput.focus();
            }
            break;
    }
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

/* FUNCIONES PRODUCTOS */

function animateMoveUpPedidoRow() {
    let productRow = this.closest('.pedido-table-row');
    previousProductRow = productRow.previousElementSibling;
    if (previousProductRow.classList.contains('pedido-table-row')) {
        productRow.style.animation = '';
        previousProductRow.style.animation = '';

        productRow.offsetHeight;

        productRow.style.animation = 'moveUpPedidoRow 0.2s ease-in-out';
        previousProductRow.style.animation = 'moveDownPedidoRow 0.2s ease-in-out';

        productRow.addEventListener('animationend', moveUpPedidoRow);
    }
}

function animateMoveDownPedidoRow() {
    let productRow = this.closest('.pedido-table-row');
    nextProductRow = productRow.nextElementSibling;
    if (nextProductRow) {
        productRow.style.animation = '';
        nextProductRow.style.animation = '';

        productRow.offsetHeight;

        productRow.style.animation = 'moveDownPedidoRow 0.2s ease-in-out';
        nextProductRow.style.animation = 'moveUpPedidoRow 0.2s ease-in-out';

        productRow.addEventListener('animationend', moveDownPedidoRow);
    }
}

function moveUpPedidoRow() {
    let productRow = this.closest('.pedido-table-row');
    previousProductRow = productRow.previousElementSibling;

    productRow.style.animation = '';
    previousProductRow.style.animation = '';

    document.querySelector('.pedido-table').insertBefore(productRow, previousProductRow);

    productRow.removeEventListener('animationend', moveUpPedidoRow);
}

function moveDownPedidoRow() {
    let productRow = this.closest('.pedido-table-row');
    nextProductRow = productRow.nextElementSibling;
    
    productRow.style.animation = '';
    nextProductRow.style.animation = '';
    
    document.querySelector('.pedido-table').insertBefore(productRow, nextProductRow.nextElementSibling);

    productRow.removeEventListener('animationend', moveDownPedidoRow);
}

function deletePedidoRow() {
    let row = this.closest('.pedido-table-row');
    let pedidoProductID = row.dataset.id;
    new Question('Eliminar producto', 'Se eliminara el producto del pedido', function(){
        if (pedidoProductID != 0) {                
            pedidoProductsDeleted.push(pedidoProductID);
        }
        row.remove();
        updatePedidoSubtotal();
    });
}

function selectInput() {
    this.select();
}

function blurInputNumeric() {
    if (this.value != '') {
        this.value = parseFloat(this.value).toFixed(2);
    }
    else {
        this.value = (0).toFixed(2);
    }
}

function blurDecimal() {
    let decimals = parseFloat(this.getAttribute('decimals') || 2);
    if (this.value != '') {
        let places = Tools.getDecimals(parseFloat(this.value));
        console.log(decimals, places);
        if (decimals && !places) {
            this.value = parseFloat(this.value).toFixed(0);
        }
        else {
            this.value = parseFloat(this.value).toFixed(decimals || places);
        }
    }
    else {
        this.value = (0).toFixed(decimals);
    }
}

function updatePedidoRowTotal(row) {
    let quantity = row.querySelector('[name="producto-cantidad"]').value || 0;
    let price = row.querySelector('[name="producto-precio"]').value || 0;
    let total = (quantity * price).toFixed(2);
    let totalElem = row.querySelector('[name="producto-total"]')
    row.querySelector('[name="producto-total"]').value = total;
    updatePedidoSubtotal();
}

/* FUNCIONES PEDIDO TOTALES */

function updatePedidoSubtotal() {
    let rows = document.querySelectorAll('.pedido-table-row');
    let subtotal = 0;
    rows.forEach(function(elem, index) {
        subtotal += parseFloat(elem.querySelector('[name="producto-total"]').value);
    });
    subtotal = subtotal.toFixed(2);
    document.querySelector('[name="subtotal"]').value = subtotal;
    updatePedidoTotal();
}

function updatePedidoTotal() {
    let subtotal = document.querySelector('[name="subtotal"]').value;
    let descuentoPorcentaje = document.querySelector('[name="descuentoPorcentaje"]').value || 0;
    let descuentoCantidad = subtotal * descuentoPorcentaje * 0.01;
    document.querySelector('[name="descuentoCantidad"]').value = descuentoCantidad.toFixed(2);
    let total = subtotal - descuentoCantidad;
    document.querySelector('[name="total"]').value = total.toFixed(2);
    updatePaymentsTotal();
}

/* EVENTOS PEDIDO TOTALES */

document.querySelector('[name="descuentoPorcentaje"]').addEventListener('focus', selectInput);
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('input', updatePedidoTotal);
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('blur', blurInputNumeric);
document.querySelector('[name="descuentoPorcentaje"]').addEventListener('blur', updatePedidoTotal);

/* PAGOS */

Payment.onAccept = function(method, quantity) {
    let date = new Date();
    addPayment(date, method, quantity);
}

function addPayment(date, method, quantity, cancelled = false, paymentID = 0) {
    let dateString = ('00' + date.getDate()).slice(-2) + '/' + ('00' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
    let dateUnix = Math.floor(date.getTime() / 1000)
    let paymentRow = document.createElement('div');
    paymentRow.setAttribute('data-id', paymentID);
    paymentRow.setAttribute('data-fecha', dateUnix);
    paymentRow.setAttribute('data-metodo', method);
    paymentRow.setAttribute('data-cantidad', quantity);
    if (cancelled) {
        paymentRow.setAttribute('cancelled', '');
    }
    paymentRow.className = 'payments-table-row';
    paymentRow.innerHTML =
        '<div class="payments-table-column">'+
            '<input type="text" value="' + dateString + '" disabled>'+
        '</div>'+
        '<div class="payments-table-column">'+
            '<div class="quantity-container">'+
                '<img tooltip-hint="' + (method[0].toUpperCase() + method.slice(1)) + '" class="method" src="/admin/imgs/icons/pago-' + method + '.png">'+
                '<p class="quantity">$' + quantity + '</p>'+
            '</div>'+
        '</div>'+
        '<div class="payments-table-column">'+
            '<div class="actions-container">'+
                '<img class="payment-delete" src="/admin/imgs/icons/actions/delete.png">'+
            '</div>'+
        '</div>';
    if (formMode == 'VIEW') {
        paymentRow.querySelector('.actions-container').remove();
    }
    else {
        if (paymentID == 0) {
            paymentRow.querySelector('.payment-delete').addEventListener('click', deletePaymentRow);
            paymentRow.querySelector('.payment-delete').setAttribute('tooltip-hint', 'Eliminar');
        }
        else {
            if (!cancelled) {
                paymentRow.querySelector('.payment-delete').setAttribute('src', '/admin/imgs/icons/actions/cancelar.png');
                paymentRow.querySelector('.payment-delete').addEventListener('click', cancelPaymentRow);
                paymentRow.querySelector('.payment-delete').setAttribute('tooltip-hint', 'Cancelar');
            }
            else {
                paymentRow.querySelector('.payment-delete').remove();
            }
        }
    }
    document.querySelector('.payments-table').appendChild(paymentRow);
    ToolTip.update();
    updatePaymentsTotal();
}

if (formMode != 'VIEW') {
    document.querySelector('.payment-add').addEventListener('click', function() {
        let resto = parseFloat(document.querySelector('[name="resto"]').value);
        Payment.open(resto);
    });
}

function updatePaymentsTotal() {
    let pagado = 0;
    let total = document.querySelector('[name="total"]').value;
    document.querySelectorAll('.payments-table-row').forEach(function(elem, index) {
        if (!elem.hasAttribute('cancelled')) {
            pagado += parseFloat(elem.getAttribute('data-cantidad'));
        }
    });
    document.querySelector('[name="pagado"]').value = pagado.toFixed(2);
    document.querySelector('[name="resto"]').value = (total - pagado).toFixed(2);
}

function deletePaymentRow() {
    let paymentRow = this.closest('.payments-table-row');
    new Question('Se eliminara este pago', '¿Esta de acuerdo?', function() {
        paymentRow.remove();
        updatePaymentsTotal();
    });
}

function cancelPaymentRow() {
    let self = this;
    let paymentRow = this.closest('.payments-table-row');
    new Question('Se cancelara este pago', '¿Esta de acuerdo?', function() {
        paymentRow.setAttribute('cancelled', '');
        updatePaymentsTotal();
        self.remove();
    });
}

/* GET ALL DATA */

function getPedidoProductsData() {
    let productsRows = document.querySelectorAll('.pedido-table-row');
    let productsData = [];
    productsRows.forEach(function(elem, index) {
        let cantidad = elem.querySelector('[name="producto-cantidad"]').value;
        productsData.push({
            id: elem.dataset.id, 
            productoID: elem.dataset.productoid,
            nombre: elem.querySelector('[name="producto-nombre"]').value,
            cantidad: cantidad,
            cantidadInventario: cantidad * elem.dataset.contenido,
            precio: elem.querySelector('[name="producto-precio"]').value,
            costo: elem.dataset.costo,
            total: elem.querySelector('[name="producto-total"]').value,
            unidad: elem.querySelector('[name="producto-unidad"]').value
        });
    });
    return productsData;
}

function getPaymentsData() {
    let paymentsRows = document.querySelectorAll('.payments-table-row');
    let paymentsData = [];
    paymentsRows.forEach(function(elem, index) {
        paymentsData.push({
            id: elem.getAttribute('data-id'),
            fecha: elem.getAttribute('data-fecha'),
            metodo: elem.getAttribute('data-metodo'),
            cantidad: elem.getAttribute('data-cantidad'),
            cancelado: elem.hasAttribute('cancelled') ? 1 : 0
        });
    });
    return paymentsData;
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

/* NUMERIC */

function checkNumericEv(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

document.querySelectorAll('[numeric]').forEach(function(elem, index) {
    elem.addEventListener('keydown', checkNumericEv);
});

// TINY PRODUCTO

if (document.querySelector('.new-producto-btn')) {
    document.querySelector('.new-producto-btn').addEventListener('click', function() {    
        TinyProducto.open('');
    });
}

TinyProducto.onSuccess = function(producto) {
    console.log(producto);

    producto['proveedorID'] = null;
    producto['proveedorNombre'] = 'S/D';
    producto['categoriaID'] = null;
    producto['categoriaNombre'] = null;
    producto['existencia'] = 0;
    producto['bloqueado'] = 0;
    producto['facturar'] = 1;
    producto['claveSAT'] = '';
    producto['impuesto'] = 'tasacero';

    productList.push(producto);

    let option = {
        'name': producto['nombre'],
        'value': producto['id'],
        'keys': [producto['nombre']]
    };

    productosAc.addOption(option);

    let productObj = {
        productoID: producto['id'],
        nombre: producto['nombre'],
        unidad: producto['unidad'],
        costo: producto['costo'],
        precio1: producto['precio1'],
        precio2: producto['precio2'],
        precio3: producto['precio3']
    };

    addProduct(productObj);
}
    
})();