
var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:10, bottom:10, right:10};

var logoData = '';
var logoSize = {width:0, height:0};

var pdf;

document.title = "Productos Vendidos";

var reporteFecha = decodeURIComponent(window.location.search).replace('?fecha=', '');

console.log(reporteFecha);

generatePDFs();

function generatePDFs() {
    let reporteData;

    Tools.ajaxRequest({
        data: {
            fecha: reporteFecha
        },
        method: 'POST',
        url: 'functions/reporte/get-products-sales',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                reporteData = data['resultado'];
                try {
                    createPDF();
                }
                catch (err) {
                    console.log(err);
                }
            }
            else {
                console.log(data['error']);
            }
        }
    });

    function createPDF() {
        if (!pdf) {
            pdf = new jsPDF({
                format: 'letter'
            });
        }

        let pageMax = 1;
        let productsMax = 60;

        pageMax = Math.ceil(reporteData.length / productsMax);

        for (let i=0; i<pageMax; i++) {
            //let pageProductsMax = Math.ceil(reporteData.length / pageMax);
            createPage(pdf, i, pageMax,  productsMax, i * productsMax);
        }
        
        savePDF('ventaspedidos');
    }

    function createPage(pdf, page, pageMax, productsMax, productsOffset) {

        let pageSize = {width:215.9, height:279.4};
        
        let lineY = 0;
        let lineHeight = 4;
        
        let titleTop = 12;

        let productsTop = 28;
        let productsHeight = 4;
        let productsLeft = margin.left;
        let productsSizes = [15, 25, 110, 20, 20];
        let productsNames = ['Codigo', 'Cantidad', 'Nombre', 'Precio', 'Total'];
        let productsKeys = ['folio', 'cantidad', 'nombre', 'precio', 'total'];

        /* TITULO */

        pdf.setFontSize(12);
        pdf.setFontStyle('bold');
        pdf.text('Reporte Ventas de Productos', pageSize.width/2, titleTop, { align: 'center' });
        pdf.setFontSize(8);
        pdf.setFontStyle('normal');
        lineY = titleTop + lineHeight;
        pdf.text('Fecha: ' + reporteFecha, pageSize.width/2, lineY, { align: 'center' });

        /* PRODUCTOS */
        
        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        let columnSep = productsLeft;
        let columnTextSep = 2;
        pdf.line(margin.left, productsTop - 6, pageSize.width - margin.right, productsTop - 6);
        //pdf.line(columnSep, productsTop - 4, columnSep, productsTop + 32);
        productsKeys.forEach(function(key, index) {
            //pdf.line(columnSep + productsSizes[index], productsTop - 4, columnSep + productsSizes[index], productsTop + 32);
            if (key == 'precio' || key == 'total' || key == 'cantidad') {
                pdf.text(productsNames[index], columnSep + productsSizes[index] - columnTextSep, productsTop, { align: 'right' });
            }
            else {
                pdf.text(productsNames[index], columnSep + columnTextSep, productsTop);
            }
            columnSep += productsSizes[index];
        });
        
        pdf.setFontSize(9);
        pdf.setFontStyle('normal');

        lineY = productsHeight;
        for (let i=0; i<Math.min(productsMax, reporteData.length - productsOffset); i++) {
            let producto = reporteData[i + productsOffset];
            //productosNum++;
            columnSep = productsLeft;
            productsKeys.forEach(function(key, index) {
                switch (key) {
                    case 'cantidad': {
                        let unitObj = Tools.unitConverter(producto['unidad']);
                        let decimalPlaces = Tools.getDecimals(parseFloat(producto['cantidad']));
                        if (unitObj['zeros'] && !decimalPlaces) {
                            pdf.text(parseFloat(producto['cantidad']).toFixed(0), columnSep + productsSizes[index] - columnTextSep - 14, productsTop + lineY, { align: 'right' });
                        }
                        else {
                            pdf.text(parseFloat(producto['cantidad']).toFixed(unitObj['zeros'] || decimalPlaces), columnSep + productsSizes[index] - columnTextSep - 14, productsTop + lineY, { align: 'right' });
                        }
                        pdf.text((parseFloat(producto['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + productsSizes[index] - columnTextSep - 6, productsTop + lineY);
                        break;
                    }
                    case 'folio':
                        pdf.text(('000' + producto['folio']).slice(-4), columnSep + columnTextSep, productsTop + lineY);
                        //pdf.text(String(i + productsOffset), columnSep + columnTextSep, productsTop + lineY);
                        break;
                    case 'nombre':
                        pdf.text(producto['nombre'], columnSep + columnTextSep, productsTop + lineY);
                        break;
                    case 'precio':
                        let price = (parseFloat(producto['total']) / parseFloat(producto['cantidad'])) || 0;
                        pdf.text('$ ' + parseFloat(price).toFixed(2), columnSep + productsSizes[index] - columnTextSep, productsTop + lineY, { align: 'right' });
                        break;
                    case 'total':
                        pdf.text('$ ' + parseFloat(producto['total']).toFixed(2), columnSep + productsSizes[index] - columnTextSep, productsTop + lineY, { align: 'right' });
                        break;
                }
                columnSep += productsSizes[index];
            });
            lineY += productsHeight;
        }
    
        if (page < pageMax - 1) {
            pdf.addPage();
        }
    }
}

function savePDF(name) {
    let file = new File([pdf.output('blob')], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI);
}