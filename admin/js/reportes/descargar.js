let waitMsg = new Wait('Cargando datos...');

let getObj = getGET();

function getGET() {
    let getArray = window.location.search.substr(1).split('&');
    let getObj = {};
    for (let i=0; i<getArray.length; i++) {
        let arr = getArray[i].split('=');
        getObj[arr[0]] = decodeURIComponent(arr[1]);
    }
    console.log(getObj);
    return getObj;
}

function getData() {
    switch (getObj['tipo']) {
        case 'productos-vendidos':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-products-sales',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        proccessProductsSales(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'pedidos-realizados':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-pedidos',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processPedidos(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'facturas':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/factura/get-global-facturas',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processFacturas(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'ventas-mostrador':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-mostrador',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processMostrador(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'ganancias':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-ganancias',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processGanancias(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
			break;
        case 'gastos':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-gastos',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processGastos(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
			break;
        case 'compras':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-compras',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processCompras(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
			break;
    }
}

getData();

//PRODUCTOS VENDIDOS

function proccessProductsSales(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', ''],
		['Codigo', 'Cantidad', 'Nombre', 'Total']
	];

	if (getObj['tipoVenta']) {
		csv[0][0] += ' | Tipo de venta: ' + (getObj['tipoVenta'] == 'mostrador' ? 'Mostrador' : 'Pedidos');
	}
	if (getObj['vendedor']) {
		csv[0][0] += ' | Vendedor: ' + getObj['vendedorNombre'];
	}

	for (let i=0; i<data.length; i++) {
		let producto = data[i];
		csv.push([
			('000' + producto['folio']).slice(-4),
			producto['cantidad'],
			producto['nombre'],
			parseFloat(producto['total']).toFixed(2)
		]);
	}
	
    saveCSV(csv, 'reporte-productos-vendidos');
}

//PEDIDOS

function processPedidos(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', '', ''],
		['Folio', 'Fecha', 'Cliente', 'Vendedor', 'Total']
	];

	if (getObj['metodoPago']) {
		csv[0][0] += ' | Metodo de Pago: ' + (getObj['metodoPago'] == 'credito' ? 'Crédito' : 'Contado');
	}
	if (getObj['vendedor']) {
		csv[0][0] += ' | Vendedor: ' + getObj['vendedorNombre'];
	}

	for (let i=0; i<data.length; i++) {
		let pedido = data[i];
		let dateArr = pedido['fecha'].split(' ');
		csv.push([
			('00000' + pedido['folio']).slice(-6),
			dateArr[0].split('-').reverse().join('/') + ' ' + dateArr[1],
			pedido['clienteNombre'],
			pedido['usuarioNombre'],
			parseFloat(pedido['total']).toFixed(2)
		]);
	}
	
    saveCSV(csv, 'reporte-pedidos');
}

//FACTURAS

function processFacturas(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', '', ''],
		['Folio', 'Fecha', 'Cliente', 'Emisor', 'Total']
	];

	if (getObj['clienteRFC']) {
		csv[0][0] += ' | RFC: ' + getObj['clienteRFC'] + ' | Razón Social: ' + getObj['clienteRazonSocial'];
	}
	if (getObj['emisor']) {
		csv[0][0] += ' | Emisor: ' + getObj['emisorNombre'];
	}
    
    if (getObj['clienteRFC']) {
        let dataSanatized = [];
        for (let i=0; i<data.length; i++) {
            if (data[i]['Receptor'] == getObj['clienteRFC']) {
                dataSanatized.push(data[i]);
            }
        }
        data = dataSanatized;
    }

	for (let i=0; i<data.length; i++) {
		let factura = data[i];
		csv.push([
			factura['Folio'],
			factura['FechaTimbrado'].split('-').reverse().join('/'),
			factura['RazonSocialReceptor'],
			factura['Emisor'],
			parseFloat(factura['Total']).toFixed(Math.max(2, (parseFloat(factura['Total']).toString().split('.')[1] || []).length))
		]);
	}
	
    saveCSV(csv, 'reporte-facturas');
}

//MOSTRADOR

function processMostrador(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', '', '', ''],
		['Folio', 'Fecha', 'Cliente', 'Vendedor', 'Tipo', 'Total']
	];

	if (getObj['metodoPago']) {
		csv[0][0] += ' | Metodo de Pago: ' + (getObj['metodoPago'] == 'credito' ? 'Crédito' : 'Contado');
	}
	if (getObj['tipoVenta']) {
		csv[0][0] += ' | Tipo de Venta: ' + (getObj['tipoVenta'] == 'domicilio' ? 'Domicilio' : 'Mostrador');
	}
	if (getObj['vendedor']) {
		csv[0][0] += ' | Vendedor: ' + getObj['vendedorNombre'];
	}

	for (let i=0; i<data.length; i++) {
		let venta = data[i];
		let dateArr = venta['fecha'].split(' ');
		csv.push([
			('00000' + venta['folio']).slice(-6),
			dateArr[0].split('-').reverse().join('/') + ' ' + dateArr[1],
			venta['clienteNombre'],
			venta['usuarioNombre'],
			venta['domicilio'] == 1 ? 'Domicilio' : 'Mostrador',
			parseFloat(venta['total']).toFixed(2)
		]);
	}
	
    saveCSV(csv, 'reporte-ventas-mostrador');
}

//GANASCIAS

function processGanancias(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', '', '', '', ''],
		['Folio', 'Tipo', 'Fecha', 'Vendedor', 'Total', 'Costo', 'Ganacia']
	];

	if (getObj['tipoVenta']) {
		csv[0][0] += ' | Tipo de Venta: ' + (getObj['tipoVenta'] == 'domicilio' ? 'Domicilio' : 'Mostrador');
	}
	if (getObj['vendedor']) {
		csv[0][0] += ' | Vendedor: ' + getObj['vendedorNombre'];
	}

	for (let i=0; i<data.length; i++) {
		let ganancia = data[i];
		let dateArr = ganancia['fecha'].split(' ');
		csv.push([
			('00000' + ganancia['folio']).slice(-6),
			ganancia['tipo'],
			dateArr[0].split('-').reverse().join('/') + ' ' + dateArr[1],
			ganancia['usuarioNombre'],
			ganancia['total'],
			ganancia['costo'],
			ganancia['ganancia']
		]);
	}
	
    saveCSV(csv, 'reporte-ganancias');
}

//GASTOS

function processGanancias(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', ''],
		['Folio', 'Fecha', 'Total']
	];

	for (let i=0; i<data.length; i++) {
		let gasto = data[i];
		let dateArr = gasto['fecha'].split(' ');
		csv.push([
			gasto['folio'],
			gasto['tipo'],
			gasto['fechaCreate'].split(' ')[0].split('-').reverse().join('/'),
			gasto['cantidad']
		]);
	}
	
    saveCSV(csv, 'reporte-gastos');
}

//GANASCIAS

function processCompras(data) {
	let csv = [
		['Fecha: ' + getObj['fecha'], '', '', ''],
		['Folio', 'Fecha', 'Proveedor', 'Total']
	];
	
	if (getObj['proveedor']) {
		csv[0][0] += ' | Proveedor: ' + getObj['proveedorNombre'];
	}

	for (let i=0; i<data.length; i++) {
		let compra = data[i];
		csv.push([
			compra['folio'],
			compra['fechaCreate'].split(' ')[0].split('-').reverse().join('/'),
			compra['proveedorNombre'],
			compra['total']
		]);
	}
	
    saveCSV(csv, 'reporte-compras');
}

//VALOR INVENTARIO

function proccessValorInventario(data) {

    let fechaObj = (new Date());
	let fechaStr = ('0' + fechaObj.getDay()).slice(-2) + '/' + ('0' + fechaObj.getMonth()).slice(-2) + '/' + fechaObj.getFullYear();
	
	let csv = [
		['Fecha: ' + fechaStr, '', '', '', ''],
		['Folio', 'Nombre', 'Existencia', 'Costo', 'Total']
	];

	for (let i=0; i<data.length; i++) {
		let producto = data[i];
		csv.push([
			('000' + producto['folio']).slice(-4),
			producto['nombre'],
			producto['existencia'],
			producto['costo'],
			producto['costo'] * producto['existencia']
		]);
	}

	csv.push([
		'',
		'',
		'',
		'Total',
		'=SUM(E3:E' + (data.length + 2) + ')'
	]);
	
    saveCSV(csv, 'reporte-valor-inventario');
}

function saveCSV(csv, name) {
	let content = [];

	for (let i=0; i<csv.length; i++) {
		let row = csv[i];
		let columns = [];
		for (let j=0; j<row.length; j++) {
			let column = row[j].toString().replaceAll('"', '""');
			if (column.indexOf(';') != -1 || column.indexOf(',') != -1 || column.indexOf('"') != -1) {
				column = '"' + column + '"';
			}
			columns.push(column);
		}
		content.push(columns.join(','));
	}
	content = content.join('\n');

	let csvData = new Blob([content], { type: 'text/csv' });
	var csvUrl = URL.createObjectURL(csvData);
	var linkElem = document.querySelector('a');
	linkElem.setAttribute('href', csvUrl);
	linkElem.setAttribute('download', name + '.csv');
	linkElem.click();
	
	setTimeout(function() {
		window.close();
	}, 100);
}