(function(){

let clienteRfcSI = new SelectInp(document.querySelector('[name="clienteRFC"]'), {
    placeholder: 'Todos',
    openEmpty: true,
    selectOnHover: false
});

let productsList = [];
let vendedoresList = [];
let clientesList = [];
let proveedoresList = [];

let productsPromise = loadProducts();
let vendedoresPromise = loadVendedores();
let clientesPromise = loadClientes();
let clientesFacturaPromise = loadClientesFactura();
let emisoresPromise = loadEmisores();
let proveedoresPromise = loadProveedores();

let desdeDP = new DatePicker(document.querySelector('[name="fechaDesde"]'), {
    dateMax: new Date()
});
let hastaDP = new DatePicker(document.querySelector('[name="fechaHasta"]'), {
    dateMin: new Date()
});

desdeDP.onChange = function() {
    let dateArr = desdeDP.input.value.split('/');
    let date = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
    hastaDP.setDateMin(date);
}

hastaDP.onChange = function() {
    let dateArr = hastaDP.input.value.split('/');
    let date = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
    desdeDP.setDateMax(date);
}

document.querySelector('[name="tipo"]').addEventListener('change', function() {
    let container = document.querySelector('.' + this.value + '-container');
    document.querySelectorAll('.reporte-container').forEach(function(elem) {
        elem.style.display = 'none';
    });
    if (container) {
        container.style.display = 'block';
    }
});
document.querySelector('[name="tipo"]').dispatchEvent(new Event('change'));

function loadProducts() {
    let promise = new Promise(function(resolve){
        Tools.ajaxRequest({
            url: 'functions/producto/get-all-min',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '') {
                    productsList = data['resultado'];
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadVendedores() {
    let promise = new Promise(function(resolve){
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/credencial/get-all',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '') {
                    vendedoresList = data['resultado'];
                    setVendedores(data['resultado']);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadClientes() {
    let promise = new Promise(function(resolve){
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/cliente/get-all',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '') {
                    setClientes(data['resultado']);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadClientesFactura() {
    let promise = new Promise(function(resolve){

        clienteRfcSI.searchInput.value = 'Cargando...'
        clienteRfcSI.searchInput.disabled = true;

        Tools.ajaxRequest({
            method: 'POST',
            url: 'functions/factura/get-global-clientes',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '' && data['resultado'].length > 0) {
                    setClientesFactura(data['resultado']);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadEmisores() {
    let promise = new Promise(function(resolve) {

        let selector = document.querySelector('[name="emisor"]');
        selector.innerHTML = '<option>Cargando...</option>';
        selector.disabled = true;

        Tools.ajaxRequest({
            method: 'POST',
            url: 'functions/factura/get-all-access',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '' && data['resultado'].length > 0) {
                    setEmisores(data['resultado']);
                    resolve();
                }
            }
        });
    });
    return promise;
}

function loadProveedores() {
    let promise = new Promise(function(resolve){
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/proveedor/get-all',
            success: function(response) {
                let data =JSON.parse(response);
                if (data['error'] == '') {
                    proveedoresList = data['resultado'];
                    setProveedores(data['resultado']);
                    resolve();
                }
            }
        });
    });
    return promise;
}

// SETTERS

function setVendedores(data) {
    let optionsFrag = document.createDocumentFragment();
    for (let i=0; i<data.length; i++) {
        let option = document.createElement('option');
        option.value = data[i]['id'];
        option.textContent = data[i]['nombre'];
        optionsFrag.appendChild(option);
    }
    document.querySelectorAll('[name="vendedor"]').forEach(function(selector) {
        selector.appendChild(optionsFrag.cloneNode(true));
    });
}

function setClientes(data) {
    let optionsFrag = document.createDocumentFragment();
    for (let i=0; i<data.length; i++) {
        let option = document.createElement('option');
        option.value = data[i]['id'];
        option.textContent = data[i]['nombre'];
        optionsFrag.appendChild(option);
    }
    document.querySelectorAll('[name="cliente"]').forEach(function(selector) {
        selector.appendChild(optionsFrag.cloneNode(true));
    });
}

function setClientesFactura(data) {
    clienteRfcSI.searchInput.value = '';
    clienteRfcSI.searchInput.disabled = false;

    let options = [];
    for (let i=0; i<data.length; i++) {
        let option = {
            name: data[i]['RazonSocial'],
            value: data[i]['RFC'],
            keys: [data[i]['RazonSocial']]
        };
        options.push(option);
    }
    clienteRfcSI.setOptions(options);
}

function setEmisores(data) {
    let selector = document.querySelector('[name="emisor"]');
    selector.children[0].remove();
    selector.innerHTML = '<option value="">Todos</option>';
    selector.disabled = false;

    let optionsFrag = document.createDocumentFragment();
    for (let i=0; i<data.length; i++) {
        let option = document.createElement('option');
        option.value = data[i]['id'];
        option.textContent = data[i]['nombre'];
        optionsFrag.appendChild(option);
    }
    selector.appendChild(optionsFrag.cloneNode(true));
}

function setProveedores(data) {
    let selector = document.querySelector('[name="proveedor"]');
    selector.children[0].remove();
    selector.innerHTML = '<option value="">Todos</option>';
    selector.disabled = false;

    let optionsFrag = document.createDocumentFragment();
    for (let i=0; i<data.length; i++) {
        let option = document.createElement('option');
        option.value = data[i]['id'];
        option.textContent = data[i]['nombre'];
        optionsFrag.appendChild(option);
    }
    selector.appendChild(optionsFrag.cloneNode(true));
}

let ProductosVendidos = new class {
    constructor() {
        let self = this;
        this.container = document.querySelector('.productos-vendidos-container');
        this.productosAC = new SelectInp(this.container.querySelector('[name="producto"]'), {
            placeholder: 'Producto',
            openEmpty: false,
            selectOnHover: false
        });
        this.container.querySelector('[name="tipoProducto"]').addEventListener('change', function() {
            if (this.value == 'solo') {
                document.querySelector('.just-products-container').style.display = '';
            }
            else {                
                document.querySelector('.just-products-container').style.display = 'none';
            }
        });
        this.container.querySelector('[name="tipoProducto"]').dispatchEvent(new Event('change'));
        productsPromise.then(function() {
            let options = [];
            for (let i=0; i<productsList.length; i++) {
                let product = productsList[i];
                let option = {
                    name: ('000' + product['folio']).slice(-4) + ' ' + product['nombre'],
                    value: product['id'],
                    keys: [('000' + product['folio']).slice(-4), product['nombre']]
                };
                options.push(option);
            }
            self.productosAC.setOptions(options);
        });
        this.productosAC.onChange = function() {
            if (this.value != '') {
                let productID = this.value;
                if (!document.querySelector('.products-list > .product[producto-id="' + productID + '"]')) {
                    let productData = productsList.find(function(value) {
                        return value['id'] == productID;
                    });
                    self.addProduct(productData);
                }
            }
            this.clear();
        }
    }
    addProduct(product) {
        let container = document.createElement('div');
        container.className = 'product';
        container.setAttribute('producto-id', product['id']);
        container.innerHTML = (
            '<p>' + product['nombre'] + '</p>' +
            '<img class="action-delete" src="/admin/imgs/icons/actions/delete.png">'
        );
        container.querySelector('.action-delete').addEventListener('click', function() {
            container.remove();
        });
        document.querySelector('.products-list').appendChild(container);
    }
}

function getQuery() {    
    let type = document.querySelector('[name="tipo"]').value;
    let query = '';
    switch (type) {
        case 'productos-vendidos':
            query = printProducts();
            break;
        case 'pedidos-realizados':
            query = printPedidos();
            break;
        case 'facturas':
            query = printFacturas();
            break;
        case 'ventas-mostrador':
            query = printMostrador();
            break;
        case 'ganancias':
            query = printGanancias();
            break;
        case 'pagos-generados':
            query = printPagos();
            break;
        case 'gastos':
            query = printGastos();
            break;
        case 'compras':
            query = printCompras();
            break;
    }
    return query;
}

document.querySelector('.act-print').addEventListener('click', function() {
    let query = getQuery();
    window.open('reportes/imprimir?' + query);
});

document.querySelector('.act-csv').addEventListener('click', function() {
    let query = getQuery();
    window.open('reportes/descargar?' + query);
});

function getReporteDate() {
    let fechaDesde = document.querySelector('[name="fechaDesde"]').value;
    let fechaHasta = document.querySelector('[name="fechaHasta"]').value;
    if (fechaDesde == fechaHasta) {
        return fechaDesde;
    }
    else {
        return fechaDesde + ' - ' + fechaHasta;
    }
}

function printProducts() {
    let container = document.querySelector('.productos-vendidos-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="tipoVenta"], [name="tipoProducto"], [name="vendedor"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['tipoProducto'] == 'solo') {
        let productsIDs = [];
        document.querySelectorAll('.products-list > .product').forEach(function(productElem) {
            productsIDs.push(productElem.getAttribute('producto-id'));
        });
        productsIDs = productsIDs.join('-');
        queryObj['ids'] = productsIDs;
    }
    if (queryObj['vendedor']) {
        queryObj['vendedorNombre'] = container.querySelector('[name="vendedor"]').querySelector('option[value="' + queryObj['vendedor'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printPedidos() {
    let container = document.querySelector('.pedidos-realizados-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="metodoPago"], [name="estadoPedido"], [name="vendedor"], [name="cliente"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['vendedor']) {
        queryObj['vendedorNombre'] = container.querySelector('[name="vendedor"]').querySelector('option[value="' + queryObj['vendedor'] + '"]').textContent;
    }
    if (queryObj['cliente']) {
        queryObj['clienteNombre'] = container.querySelector('[name="cliente"]').querySelector('option[value="' + queryObj['cliente'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printFacturas() {
    let container = document.querySelector('.facturas-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="clienteRFC"], [name="emisor"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['clienteRFC']) {
        queryObj['clienteRazonSocial'] = clienteRfcSI.searchInput.value;
    }
    if (queryObj['emisor']) {
        queryObj['emisorNombre'] = container.querySelector('[name="emisor"]').querySelector('option[value="' + queryObj['emisor'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printMostrador() {
    let container = document.querySelector('.ventas-mostrador-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="metodoPago"], [name="tipoVenta"], [name="vendedor"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['vendedor']) {
        queryObj['vendedorNombre'] = container.querySelector('[name="vendedor"]').querySelector('option[value="' + queryObj['vendedor'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printGanancias() {
    let container = document.querySelector('.ganancias-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="tipoVenta"], [name="metodoPago"], [name="vendedor"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['vendedor']) {
        queryObj['vendedorNombre'] = container.querySelector('[name="vendedor"]').querySelector('option[value="' + queryObj['vendedor'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printGastos() {
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

function printCompras() {
    let container = document.querySelector('.compras-container');
    let queryObj = {
        tipo: document.querySelector('[name="tipo"]').value,
        fecha: getReporteDate()
    };
    container.querySelectorAll('[name="proveedor"]').forEach(function(input) {
        if (input.value != '') {
            queryObj[input.getAttribute('name')] = input.value;
        }
    });
    if (queryObj['proveedor']) {
        queryObj['proveedorNombre'] = container.querySelector('[name="proveedor"]').querySelector('option[value="' + queryObj['proveedor'] + '"]').textContent;
    }
    let query = [];
    for (let key in queryObj) {
        query.push(key + '=' + encodeURIComponent(queryObj[key]));
    }
    return query.join('&');
}

})();