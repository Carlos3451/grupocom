
var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:10, bottom:10, right:10};

var logoData = '';
var logoSize = {width:0, height:0};

function getGET() {
    let getArray = window.location.search.substr(1).split('&');
    let getObj = {};
    for (let i=0; i<getArray.length; i++) {
        let arr = getArray[i].split('=');
        getObj[arr[0]] = decodeURIComponent(arr[1]);
    }
    console.log(getObj);
    return getObj;
}

let waitMsg = new Wait('Cargando datos.');

let getObj = getGET();

function getData() {
    switch (getObj['tipo']) {
        case 'productos-vendidos':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-products-sales',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        proccessProductsSales(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'pedidos-realizados':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-pedidos',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processPedidos(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'facturas':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/factura/get-global-facturas',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processFacturas(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'ventas-mostrador':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-mostrador',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processMostrador(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'ganancias':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-ganancias',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processGanancias(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'gastos':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-gastos',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processGastos(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
        case 'compras':
            Tools.ajaxRequest({
                data: getObj,
                method: 'POST',
                url: 'functions/reporte/get-compras',
                success: function(response) {
                    waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        processCompras(data['resultado']);
                    }
                    else {
                        new Message('Ocurrio un error al obtener los datos.<br>' + data['error'], 'error', function() {
                            window.close();
                        });
                    }
                }
            });
            break;
    }
}

getData();

//PRODUCTOS VENDIDOS

function proccessProductsSales(data) {
    document.title = "Reporte de Productos";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let productsMax = 60;
    let pageMax = Math.ceil(data.length / productsMax)
    try {
        for (let i=0; i<pageMax; i++) {
            createProductsSalesPage(pdf, data, i, pageMax, productsMax, i * productsMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-productos');
}

function createProductsSalesPage(pdf, data, page, pageMax, productsMax, productsOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let productsTop = 28;
    let productsHeight = 4;
    let productsLeft = margin.left;
    let productsSizes = [15, 25, 130, 20];
    let productsNames = ['Codigo', 'Cantidad', 'Nombre', 'Total'];
    let productsKeys = ['folio', 'cantidad', 'nombre', 'total'];

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    if (getObj['ids']) {
        pdf.text('Reporte de Ventas de Productos Seleccionados', pageSize.width/2, titleTop, { align: 'center' });
    }
    else {
        pdf.text('Reporte de Ventas de Productos', pageSize.width/2, titleTop, { align: 'center' });
    }
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['tipoVenta'] ? (' | Tipo de venta: ' + (getObj['tipoVenta'] == 'mostrador' ? 'Mostrador' : 'Pedidos')) : '') +
        (getObj['vendedor'] ? (' | Vendedor: ' + getObj['vendedorNombre']) : ''),
        pageSize.width/2, lineY, { align: 'center' });

    /* PRODUCTOS */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = productsLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, productsTop - 6, pageSize.width - margin.right, productsTop - 6);
    
    productsKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad') {
            pdf.text(productsNames[index], columnSep + productsSizes[index] - columnTextSep, productsTop, { align: 'right' });
        }
        else {
            pdf.text(productsNames[index], columnSep + columnTextSep, productsTop);
        }
        columnSep += productsSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = productsHeight;
    for (let i=0; i<Math.min(productsMax, data.length - productsOffset); i++) {
        let producto = data[i + productsOffset];
        columnSep = productsLeft;
        productsKeys.forEach(function(key, index) {
            switch (key) {
                case 'cantidad': {
                    let unitObj = Tools.unitConverter(producto['unidad']);
                    pdf.text(parseFloat(parseFloat(producto['cantidad']).toFixed(3)).toString(), columnSep + productsSizes[index] - columnTextSep - 8, productsTop + lineY, { align: 'right' });
                    pdf.text((parseFloat(producto['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + productsSizes[index] - columnTextSep - 6, productsTop + lineY);
                    break;
                }
                case 'folio':
                    pdf.text(('000' + producto['folio']).slice(-4), columnSep + columnTextSep, productsTop + lineY);
                    //pdf.text(String(i + productsOffset), columnSep + columnTextSep, productsTop + lineY);
                    break;
                case 'nombre':
                    pdf.text(producto['nombre'], columnSep + columnTextSep, productsTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(producto['total']).toFixed(2), columnSep + productsSizes[index] - columnTextSep, productsTop + lineY, { align: 'right' });
                    break;
            }
            columnSep += productsSizes[index];
        });
        lineY += productsHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
}

//PEDIDOS

function processPedidos(data) {
    document.title = "Reporte de Pedidos";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let pedidosMax = 28;

    let pageMax = Math.ceil(data.length / pedidosMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createPedidosPage(pdf, data, i, pageMax, pedidosMax, i * pedidosMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-pedidos');
}

function createPedidosPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 8;
    let tableLeft = margin.left;
    let tableSizes = [15, 20, 67, 67, 20];
    let tableNames = ['Folio', 'Fecha', 'Cliente', 'Vendedor', 'Total'];
    let tableKeys = ['folio', 'fecha', 'clienteNombre', 'usuarioNombre', 'total'];

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Pedidos', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['vendedor'] ? (' | Vendedor: ' + getObj['vendedorNombre']) : '') +
        (getObj['cliente'] ? (' | Cliente: ' + getObj['clienteNombre']) : '') +
        (getObj['metodoPago'] ? (' | Metodo de pago: ' + (getObj['metodoPago'] == 'credito' ? 'Crédito' : 'Contado')) : '') +
        (getObj['estadoPedido'] ? (' | Estado del pedido: ' + (getObj['estadoPedido'] == 'pagado' ? 'Pagado' : 'Sin pagar')) : ''),
        pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' }
    );

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        if ((i % 2) == 0) {
            pdf.setFillColor('#EEE');
            pdf.rect(tableLeft, tableTop + lineY - 3.5, 200, tableHeight, 'F');
        }
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'cantidad': {
                    let unitObj = Tools.unitConverter(rowData['unidad']);
                    pdf.text(parseFloat(parseFloat(rowData['cantidad']).toFixed(3)).toString(), columnSep + tableSizes[index] - columnTextSep - 14, tableTop + lineY, { align: 'right' });
                    pdf.text((parseFloat(rowData['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + tableSizes[index] - columnTextSep - 6, tableTop + lineY);
                    break;
                }
                case 'folio':
                    pdf.text(('00000' + rowData['folio']).slice(-6), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fecha'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(rowData['total']).toFixed(2), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY, { maxWidth: tableSizes[index] });
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
    else {
        let sumaTotal = 0;
        for (let i=0; i<data.length; i++) {
            sumaTotal += parseFloat(data[i]['total']);
        }
        pdf.setFontStyle('bold');
        pdf.text('Suma Total:', 160, tableTop + lineY, { align: 'right' });
        pdf.text('$' + sumaTotal.toFixed(2), 197, tableTop + lineY, { align: 'right' });
    }
}

//FACTURAS

function processFacturas(data) {
    document.title = "Reporte de Facturas";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let pedidosMax = 28;
    
    if (getObj['clienteRFC']) {
        let dataSanatized = [];
        for (let i=0; i<data.length; i++) {
            if (data[i]['Receptor'] == getObj['clienteRFC']) {
                dataSanatized.push(data[i]);
            }
        }
        data = dataSanatized;
    }

    let pageMax = Math.ceil(data.length / pedidosMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createFacturasPage(pdf, data, i, pageMax, pedidosMax, i * pedidosMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-facturas');
}

function createFacturasPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 8;
    let tableLeft = margin.left;
    let tableSizes = [15, 20, 67, 67, 20];
    let tableNames = ['Folio', 'Fecha', 'Cliente', 'Emisor', 'Total'];
    let tableKeys = ['folio', 'fecha', 'cliente', 'emisor', 'total'];

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Facturas', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['clienteRFC'] ? (' | RFC: ' + getObj['clienteRFC']) : '') +
        (getObj['clienteRFC'] ? (' | Razón Social: ' + getObj['clienteRazonSocial']) : '') +
        (getObj['emisor'] ? (' | Emisor: ' + getObj['emisorNombre']) : ''),
        pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' }
    );

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        if ((i % 2) == 0) {
            pdf.setFillColor('#EEE');
            pdf.rect(tableLeft, tableTop + lineY - 3.5, 200, tableHeight, 'F');
        }
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'cantidad': {
                    let unitObj = Tools.unitConverter(rowData['unidad']);
                    pdf.text(parseFloat(parseFloat(rowData['cantidad']).toFixed(3)).toString(), columnSep + tableSizes[index] - columnTextSep - 14, tableTop + lineY, { align: 'right' });
                    pdf.text((parseFloat(rowData['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + tableSizes[index] - columnTextSep - 6, tableTop + lineY);
                    break;
                }
                case 'emisor':
                    pdf.text(rowData['Emisor'], columnSep + columnTextSep, tableTop + lineY, { maxWidth: tableSizes[index] - (columnTextSep * 2) });
                    break;
                case 'cliente':
                    pdf.text(rowData['RazonSocialReceptor'], columnSep + columnTextSep, tableTop + lineY, { maxWidth: tableSizes[index] - (columnTextSep * 2) });
                    break;
                case 'folio':
                    pdf.text(rowData['Folio'], columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['FechaTimbrado'].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(rowData['Total']).toFixed(Math.max(2, (parseFloat(rowData['Total']).toString().split('.')[1] || []).length)), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY, { maxWidth: tableSizes[index] - (columnTextSep * 2) });
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
    else {
        let sumaTotal = 0;
        for (let i=0; i<data.length; i++) {
            sumaTotal += parseFloat(data[i]['Total']);
        }
        pdf.setFontStyle('bold');
        pdf.text('Suma Total:', 160, tableTop + lineY, { align: 'right' });
        pdf.text('$' + sumaTotal.toFixed(2), 197, tableTop + lineY, { align: 'right' });
    }
}

//MOSTRADOR

function processMostrador(data) {
    document.title = "Reporte de Ventas de Mostrador";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let mostradorMax = 28;

    if (getObj['metodoPago']) {
        let newData = [];
        for (let i=0; i<data.length; i++) {
            if (data[i]['metodo'] == getObj['metodoPago']) {
                newData.push(data[i]);
            }
        }
        data = newData;
    }

    let pageMax = Math.ceil(data.length / mostradorMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createMostradorPage(pdf, data, i, pageMax, mostradorMax, i * mostradorMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-ventas-mostrador');
}

function createMostradorPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 8;
    let tableLeft = margin.left;
    let tableSizes = [15, 20, 44, 44, 22, 22, 22];
    let tableNames = ['Folio', 'Fecha', 'Cliente', 'Vendedor', 'Tipo', 'Metodo', 'Total'];
    let tableKeys = ['folio', 'fecha', 'clienteNombre', 'usuarioNombre', 'tipo', 'metodo', 'total'];

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Ventas de Mostrador', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['vendedor'] ? (' | Vendedor: ' + getObj['vendedorNombre']) : '') +
        (getObj['metodoPago'] ? (' | Metodo de pago: ' + (getObj['metodoPago'] == 'credito' ? 'Crédito' : 'Contado')) : '') +
        (getObj['tipoVenta'] ? (' | Tipo de venta: ' + (getObj['tipoVenta'] == 'domicilio' ? 'Domicilio' : 'Mostrador')) : ''),
        pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' });

    /* TABLE */

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        if ((i % 2) == 0) {
            pdf.setFillColor('#EEE');
            pdf.rect(tableLeft, tableTop + lineY - 3.5, 200, tableHeight, 'F');
        }
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'folio':
                    pdf.text(('000000' + rowData['folio']).slice(-6), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fecha'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + rowData['total'], columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key].toString() : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY, { maxWidth: tableSizes[index] - (columnTextSep * 2) });
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
    else {
        let sumaTotal = 0;
        for (let i=0; i<data.length; i++) {
            sumaTotal += parseFloat(data[i]['total']);
        }
        pdf.setFontStyle('bold');
        pdf.text('Suma Total:', 160, tableTop + lineY, { align: 'right' });
        pdf.text('$' + sumaTotal.toFixed(2), 197, tableTop + lineY, { align: 'right' });
    }
    /*
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'cantidad': {
                    let unitObj = Tools.unitConverter(rowData['unidad']);
                    pdf.text(parseFloat(parseFloat(rowData['cantidad']).toFixed(3)).toString(), columnSep + tableSizes[index] - columnTextSep - 14, tableTop + lineY, { align: 'right' });
                    pdf.text((parseFloat(rowData['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + tableSizes[index] - columnTextSep - 6, tableTop + lineY);
                    break;
                }
                case 'folio':
                    pdf.text(('00000' + rowData['folio']).slice(-6), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fecha'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'domicilio':
                    pdf.text(rowData['domicilio'] == 1 ? 'Domicilio' : 'Mostrador', columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(rowData['total']).toFixed(2), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY);
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
    else {
        lineY += 2;
        pdf.setFontStyle('bold');
        let totalSum = 0;
        for (let i=0; i<data.length; i++) {
            totalSum += parseFloat(data[i]['total']) || 0;
        }
        pdf.text('Total', tableLeft + tableSizes.reduce(function(total, value) { return total + value}) - tableSizes[tableSizes.length - 1] - tableSizes[tableSizes.length - 2] + columnTextSep, tableTop + lineY);
        pdf.text('$' + totalSum.toFixed(2), tableLeft + tableSizes.reduce(function(total, value) { return total + value}) -  columnTextSep, tableTop + lineY, { align: 'right' });
    }
    */
}

//GANANCIAS

function processGanancias(data) {
    document.title = "Reporte de Ganancias";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let gananciasMax = 60;
    let pageMax = Math.ceil(data.length / gananciasMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createGananciasPage(pdf, data, i, pageMax, gananciasMax, i * gananciasMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-ganancias');
}

function createGananciasPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 4;
    let tableLeft = margin.left;
    let tableSizes = [15, 20, 30, 40, 25, 25, 40];
    let tableNames = ['Folio', 'Tipo', 'Fecha', 'Vendedor', 'Total', 'Costo', 'Ganancias'];
    let tableKeys = ['folio', 'tipo', 'fecha', 'usuarioNombre', 'total', 'costo', 'ganancia'];

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Ganancias', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['vendedor'] ? (' | Vendedor: ' + getObj['vendedorNombre']) : '') +
        (getObj['tipoVenta'] ? (' | Tipo de venta: ' + (getObj['tipoVenta'] == 'pedidos' ? 'Pedidos' : 'Mostrador')) : ''),
        pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' });

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total' || key == 'cantidad' || key == 'costo' || key == 'ganancia') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'cantidad': {
                    let unitObj = Tools.unitConverter(rowData['unidad']);
                    pdf.text(parseFloat(parseFloat(rowData['cantidad']).toFixed(3)).toString(), columnSep + tableSizes[index] - columnTextSep - 14, tableTop + lineY, { align: 'right' });
                    pdf.text((parseFloat(rowData['cantidad']) == 1 ? unitObj['singular'] : unitObj['plural']), columnSep + tableSizes[index] - columnTextSep - 6, tableTop + lineY);
                    break;
                }
                case 'folio':
                    pdf.text(('00000' + rowData['folio']).slice(-6), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fecha'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total': case 'costo':
                    pdf.text('$ ' + parseFloat(rowData[key]).toFixed(2), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                case 'ganancia':
                    let percent = (((parseFloat(rowData['ganancia']) || 0) / parseFloat(rowData['total'])) * 100).toFixed(2)
                    pdf.text('$ ' + parseFloat(rowData['ganancia']).toFixed(2) + ' (' + percent + '% )', columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY);
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
}

//GASTOS

function processGastos(data) {
    document.title = "Reporte de Gastos";
    let pdf = new jsPDF({
        format: 'letter'
    });
    let gananciasMax = 60;
    let pageMax = Math.ceil(data.length / gananciasMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createGastosPage(pdf, data, i, pageMax, gananciasMax, i * gananciasMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-gastos');
}

function createGastosPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 4;
    let tableLeft = margin.left;
    let tableSizes = [5, 20, 30, 35, 10];
    let tableNames = ['Folio', 'Fecha', 'Razon', 'Comentario', 'Total'];
    let tableKeys = ['folio', 'fecha', 'razon', 'comentario', 'total'];

    for (let i=0; i<tableSizes.length; i++) {
        tableSizes[i] = tableSizes[i] * 0.01 * 195;
    }

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Gastos', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'], pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' });

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'folio':
                    pdf.text(rowData['folio'].toString(), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fechaCreate'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(rowData['cantidad']).toFixed(2), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY);
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
}

//COMPRAS

function processCompras(data) {
    document.title = 'Reporte de Compras';
    let pdf = new jsPDF({
        format: 'letter'
    });
    let rowsMax = 60;
    let pageMax = Math.ceil(data.length / rowsMax);
    try {
        for (let i=0; i<pageMax; i++) {
            createComprasPage(pdf, data, i, pageMax, rowsMax, i * rowsMax);
        }
    }
    catch (err) {
        console.log(err);
    }
    savePDF(pdf, 'reporte-de-compras');
}

function createComprasPage(pdf, data, page, pageMax, dataMax, dataOffset) {
    let pageSize = {width:215.9, height:279.4};
        
    let lineY = 0;
    let lineHeight = 4;
        
    let titleTop = 12;

    let tableTop = 28;
    let tableHeight = 4;
    let tableLeft = margin.left;
    let tableSizes = [10, 15, 65, 10];
    let tableNames = ['Folio', 'Fecha', 'Proveedor', 'Total'];
    let tableKeys = ['folio', 'fecha', 'proveedorNombre', 'total'];

    for (let i=0; i<tableSizes.length; i++) {
        tableSizes[i] = tableSizes[i] * 0.01 * 195;
    }

    /* TITULO */

    pdf.setFontSize(12);
    pdf.setFontStyle('bold');
    pdf.text('Reporte de Compras', pageSize.width/2, titleTop, { align: 'center' });
    pdf.setFontSize(8);
    pdf.setFontStyle('normal');
    lineY = titleTop + lineHeight;
    pdf.text('Fecha: ' + getObj['fecha'] +
        (getObj['proveedor'] ? (' | Proveedor: ' + getObj['proveedorNombre']) : ''),
        pageSize.width/2, lineY, { align: 'center' }, pageSize.width/2, lineY, { align: 'center' }
    );

    /* TABLE */
    
    pdf.setFontSize(8);
    pdf.setFontStyle('bold');

    let columnSep = tableLeft;
    let columnTextSep = 2;
    pdf.line(margin.left, tableTop - 6, pageSize.width - margin.right, tableTop - 6);
    
    tableKeys.forEach(function(key, index) {
        if (key == 'total') {
            pdf.text(tableNames[index], columnSep + tableSizes[index] - columnTextSep, tableTop, { align: 'right' });
        }
        else {
            pdf.text(tableNames[index], columnSep + columnTextSep, tableTop);
        }
        columnSep += tableSizes[index];
    });
    
    pdf.setFontSize(9);
    pdf.setFontStyle('normal');

    lineY = tableHeight;
    for (let i=0; i<Math.min(dataMax, data.length - dataOffset); i++) {
        let rowData = data[i + dataOffset];
        columnSep = tableLeft;
        tableKeys.forEach(function(key, index) {
            switch (key) {
                case 'folio':
                    pdf.text(rowData['folio'].toString(), columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'fecha':
                    let date = rowData['fechaCreate'].split(' ')[0].split('-').reverse().join('/');
                    pdf.text(date, columnSep + columnTextSep, tableTop + lineY);
                    break;
                case 'total':
                    pdf.text('$ ' + parseFloat(rowData['total']).toFixed(2), columnSep + tableSizes[index] - columnTextSep, tableTop + lineY, { align: 'right' });
                    break;
                default:
                    pdf.text(rowData[key] != null ? rowData[key] : '[ELIMNADO]', columnSep + columnTextSep, tableTop + lineY);
                    break;
            }
            columnSep += tableSizes[index];
        });
        lineY += tableHeight;
    }

    if (page < pageMax - 1) {
        pdf.addPage();
    }
}

function savePDF(pdf, name) {
    let file = new File([pdf.output('blob')], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI);
}