(function() {

let loginForm = document.querySelector('#login-form');

loginForm.addEventListener('submit', function(ev){
    ev.preventDefault();
    let data = {
        usuario: document.querySelector('[name="usuario"]').value,
        contraseña: document.querySelector('[name="contraseña"]').value
    };
    Tools.ajaxRequest({
        data: data,
        url: 'functions/login/check',
        method: 'POST',
        waitMsg: new Wait('Iniciando sesión.'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error']=='') {
                new Message('Sesión iniciada con exito. Bienvendio ' + data['resultado']['nombre'] + '.', 'success', function(){
                    location.reload();
                });
            }
            else {
                new Message('Usuario o contraseña incorrectos. Intente de nuevo.', 'warning');
            }
        }
    });
});

})();