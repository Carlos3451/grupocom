const { degrees, PDFDocument, rgb, StandardFonts } = PDFLib;

(function(){

let clientsList = [];

function removeAccents(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, '');
}

Lista.actionPrint['active'] = true;
Lista.actionPrint['unique'] = true;
Lista.printFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value,
            UID: uid
        },
        method: 'POST',
        url: 'functions/factura-v4/pdf',
        responseType: 'blob',
        waitMsg: new Wait('Obteniendo PDF...'),
        success: function(response) {
            this.waitMsg.destroy();
            // window.open(window.URL.createObjectURL(response));
            // Create a copy of the PDF
            modidyPDF(response);
        }
    });
}

async function modidyPDF(data) {
    let dataBytes = await data.arrayBuffer();

    let pdf = await PDFDocument.load(dataBytes);
    let helveticaFont = await pdf.embedFont(StandardFonts.HelveticaBold);

    let pages = pdf.getPages();
    let pagesCopiesNumbers = [];
    for (let i=0; i<pages.length; i++) {
        pagesCopiesNumbers.push(i);
    }

    let pagesCopies = await pdf.copyPages(pdf, pagesCopiesNumbers);

    for (let i=0; i<pagesCopies.length; i++) {
        pdf.addPage(pagesCopies[i]);
    }

    pages = pdf.getPages();

    let xx = -70;
    let yy = 130;

    for (let i=pages.length / 2; i<pages.length; i++) {
        pages[i].drawRectangle({
            x: 0 + xx,
            y: 505 + yy,
            rotate: degrees(45),
            width: 400,
            height: 40,
            color: rgb(0.070, 0.043, 0.458),
        });
        pages[i].drawText('COPIA', {
            x: 100 + xx,
            y: 615 + yy,
            size: 32,
            rotate: degrees(45),
            font: helveticaFont,
            color: rgb(1.0, 1.0, 1.0)
        });
    }

    let pdfBytes = await pdf.save();
    window.open(window.URL.createObjectURL(new Blob([pdfBytes], {type: 'application/pdf'})));
}

Lista.actionDownload['active'] = true;
Lista.actionDownload['unique'] = true;
Lista.downloadFn = function(selecteds) {
    let waitMsg = new Wait('Obteniendo datos del complemento.');
    let uid = selecteds[0].dataset.uid;
    let promises = [];
    let folio = selecteds[0].dataset.folio.replace('F ', '');
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura-v4/xml',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    promises.push(new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                accesoID: document.querySelector('[name="accesoID"]').value,
                UID: uid
            },
            method: 'POST',
            url: 'functions/factura-v4/pdf',
            responseType: 'blob',
            success: function(response) {
                resolve(response);
            }
        });
    }));
    Promise.all(promises).then(function(result) {
        let zip = new JSZip();
        zip.file('complementopago-' + folio + '.pdf', result[1]);
        zip.file('complementopago-' + folio + '.xml', result[0]);
        zip.generateAsync({type: 'blob'}).then(function(content) {
            waitMsg.destroy();
            let downloadElem = document.createElement('a');
            downloadElem.setAttribute('href', window.URL.createObjectURL(content));
            downloadElem.setAttribute('download', 'factura-' + folio + '.zip');
            downloadElem.style.display = 'none';
            document.querySelector('body').appendChild(downloadElem);
            downloadElem.click();
            downloadElem.remove();
        });
    });
}

Lista.actionEmail['active'] = true;
Lista.actionEmail['unique'] = true;
Lista.emailFn = function(selecteds) {
    let uid = selecteds[0].dataset.uid;
    let folio = selecteds[0].dataset.folio;
    let fecha = selecteds[0].dataset.fecha;
    let total = selecteds[0].dataset.total;
    let email = '';
    let client = getClientByRFC(selecteds[0].dataset.rfc);
    if (client) {
        email = client['datosExtras'] ? client['datosExtras']['emails'] : client['Contacto']['Email'];
    }
    EmailFactura.open(folio, email, uid, document.querySelector('[name="accesoID"]').value);
    EmailFactura.ccsInp.value = 'contabilidad@shinyclean.mx';
    EmailFactura.mensajeInp.value = (`Para: <b>${client.RazonSocial}</b>
Estimado Cliente.

Se emité para Usted un(os) documento(s) de tipo Factura,consulte los datos adjuntos, por favor.

Serie y Folio: <b>${folio}</b>
Fecha de Expedición: <b>${fecha}</b>
Monto Total: <b>$${total}</b>


*No responder a este correo electronico, no podemos responder las consultas que se envíen a esta dirección.
Gracias.`);
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = true;
    Lista.cancelFn = function(selecteds) {
        let facturaUID = selecteds[0].dataset.uid;
        if (!selecteds[0].hasAttribute('cancelled')) {
            new FacturaCancel(facturaUID, function() {
                Tools.ajaxRequest({
                    data: {
                        accesoID: document.querySelector('[name="accesoID"]').value,
                        uid: facturaUID,
                        motivo: motivo,
                        folioSustituto: uuid
                    },
                    method: 'POST',
                    url: 'functions/factura-v4/complemento-cancel',
                    waitMsg: new Wait('Cancelando complemento de pago...', true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message('Complemento de pago cancelado con exito.', 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            if (data['errorLog'] && data['errorLog']['message']) {                                    
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago.' + '<br>' + data['errorLog']['message'], 'error');
                            }
                            else {
                                new Message('Ocurrio un error al tratar de cancelar el complemento de pago.', 'error');
                            }
                        }
                    }
                });
            });
        }
        else {
            new Message('No se puede cancelar un complemento de pago ya cancelado.', 'warning');
        }
    }
}

Lista.selectJustOne = true;

SortController.setOnMethodChange(function() {});

function reloadComplementos() {
    SortController.load();
    let filtros = SortController.filters;
    filtros['tipo'] = 'pago';
    Lista.load('factura-v4/get-all-complementos', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: filtros
    }, function(data) {
        let emisor = document.querySelector('[name="accesoID"]').children[document.querySelector('[name="accesoID"]').selectedIndex].textContent;
        let date = data['FechaTimbrado'].split('-').reverse().join('/');
        let cantidad = '-';
        if (data['datosExtra']) {
            cantidad = '$' + data['datosExtra'].reduce((a, c) => a + c['cantidad'], 0);
        }
        return [
            data['Folio'],
            date,
            data['RazonSocialReceptor'],
            emisor,
            (data['datosExtra'] ? data['datosExtra'][0]['usuarioNombre'] : '-'),
            data['Status'],
            cantidad
        ];
    }, function(data) {
        let date = data['FechaTimbrado'].split('-').reverse().join('/');
        let attrs = [
            ['data-uid', data['UID']],
            ['data-uuid', data['UUID']],
            ['data-folio', data['Folio']],
            ['data-fecha', date],
            ['data-rfc', data['Receptor']],
            ['data-total', data['Total']]
        ];
        if (data['Status'] == 'cancelada') {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    });
}

SortController.setMethods([
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['RFC', 'select', 'accesoID', []],
        ['Cliente', 'select', 'clienteRFC', [
            ['Todos', 0]
        ]]
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    selectUntil: 'month'
});

function loadClients() {
    clientsList = [];
    Lista.clear();
    Lista.createLoading();
    Tools.ajaxRequest({
        data: {
            accesoID: document.querySelector('[name="accesoID"]').value
        },
        method: 'POST',
        url: 'functions/factura-v4/get-all-clients',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                clientsList = data['resultado'];
                let optionsFragment = document.createDocumentFragment();
                for (let i=0; i<clientsList.length; i++) {
                    let cliente = clientsList[i];
                    let option = document.createElement('option');
                    option.value = cliente['RFC'];
                    option.textContent = cliente['RazonSocial'];
                    optionsFragment.appendChild(option);
                }
                document.querySelector('[name="clienteRFC"]').appendChild(optionsFragment);
            }
            reloadComplementos();
        }
    });
}

function getClientByRFC(rfc) {
    return clientsList.find(function(value) {
        return value['RFC'] == rfc;
    });
}

Tools.ajaxRequest({
    url: 'functions/factura-v4/get-all-access',
    method: 'GET',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let result = data['resultado'];
            let optionsFrag = document.createDocumentFragment();
            for (let i=0; i<result.length; i++) {
                let option = document.createElement('option');
                option.value = result[i]['id'];
                option.textContent = result[i]['nombre'];
                optionsFrag.appendChild(option);
            }
            document.querySelector('[name="accesoID"]').appendChild(optionsFrag);
            if (window.localStorage.getItem('factura-acceso-id')) {
                document.querySelector('[name="accesoID"]').value = window.localStorage.getItem('factura-acceso-id');
            }
            loadClients();
        }
    }
});

document.querySelector('[name="fecha"]').addEventListener('change', function() {
    reloadComplementos();
});

document.querySelector('[name="accesoID"]').addEventListener('change', function() {
    window.localStorage.setItem('factura-acceso-id', this.value);
    loadClients();
});

document.querySelector('[name="clienteRFC"]').addEventListener('change', function() {
    let rfc = this.value;
    document.querySelectorAll('.list-row').forEach(function(row) {
        row.style.display = 'none';
        if (row.dataset.rfc == rfc || rfc == 0) {
            row.style.display = '';
        }
    });
});

document.querySelector('.action-download').addEventListener('click', function() {
    Tools.ajaxRequest({
        data: {            
            busqueda: SortController.search,
            metodo: SortController.method,
            orden: SortController.order,
            filtros: SortController.filters
        },
        url: 'functions/factura-v4/get-all-complementos',
        method: 'POST',
        waitMsg: new Wait('Obteniendo complementos de pago...'),
        success: function(response) {
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let facturas = data['resultado'];
                let workbook = XLSX.utils.book_new();
                let worksheetData = [
                    ['Folio', 'Fecha', 'Tipo', 'Razón Social', 'RFC', 'Emisor', 'Total']
                ];
                let wscols = [
                    {wch:10},
                    {wch:12},
                    {wch:10},
                    {wch:36},
                    {wch:20},
                    {wch:36},
                    {wch:10}
                ];
                let total = 0;
                for (let i=0; i<facturas.length; i++) {
                    let factura = facturas[i];
                    if (factura['Status'] == 'enviada') {
                        let cantidad = '-';
                        if (factura['datosExtra']) {
                            cantidad = factura['datosExtra'].reduce((a, c) => a + c['cantidad'], 0);
                        }
                        total += parseFloat(cantidad) || 0;
                        worksheetData.push([
                            factura['Folio'],
                            factura['FechaTimbrado'],
                            'CPD',
                            factura['RazonSocialReceptor'],
                            factura['Receptor'],
                            factura['datosExtra'] ? factura['datosExtra'][0]['usuarioNombre'] : 'S/D',
                            parseFloat(cantidad)
                        ]);
                    }
                }
                worksheetData.push(['', '', '', '', '', '', '']);
                worksheetData.push(['', '', '', '', '', 'Total', total]);
                let worksheet = XLSX.utils.aoa_to_sheet(worksheetData);
                worksheet['!cols'] = wscols;
                XLSX.utils.book_append_sheet(workbook, worksheet, 'Facturas - ' + document.querySelector('[name="fecha"]').value.replace('/', '-'));
                XLSX.writeFile(workbook, 'complementos-pago-' + document.querySelector('[name="fecha"]').value.replace('/', '-') + '.xlsx');
            }
            else {
                new Message('Ocurrio un error al tratar de obtener los complementos de pago.<br>' + data['error'], 'error');
            }
        }
    })
});

})();

class FacturaCancel {
    constructor(facturaUID, onAccept) {
        let self = this;
        this.facturaUID = facturaUID;
        this.onAccept = onAccept;
        this.message = new OptionsQuestion('Cancelar factura', 'Elija el motivo de la cancelación.', [
            {
                'text': '01 - Comprobante emitido con errores con relación',
                'value': '01'
            },
            {
                'text': '02 - Comprobante emitido con errores sin relación',
                'value': '02'
            },
            {
                'text': '03 - No se llevó a cabo la operación',
                'value': '03'
            },
            {
                'text': '04 - Operación nominativa relacionada en la factura global',
                'value': '04'
            }
        ], function(motivo) {
            let uuid = '';
            if (motivo == '01') {
                uuid = self.searcherSelector.value;
            }
            self.onAccept(motivo, uuid);
        });

        this.searcherContainer = document.createElement('div');
        this.searcherContainer.style.margin = '32px 0';
        this.searcherContainer.innerHTML = (`
            <h3 style="font-size:16px;margin-bottom:8px;">Selecciona el CFDI que lo reemplazará</h3>
            <select class="reemplazo-select" disabled><option>Cargando...</option></option>
        `);
        this.searcherSelector = this.searcherContainer.querySelector('.reemplazo-select');

        this.message.element.querySelector('.message-content').insertBefore(this.searcherContainer, this.message.element.querySelector('.message-accept'));
        this.message.element.querySelector('.message-options').addEventListener('change', function() {
            if (this.value == '01') {
                self.searcherContainer.style.display = 'block';
            }
            else {
                self.searcherContainer.style.display = 'none';
            }
        });

        this.loadFacturas();
    }
    loadFacturas() {
        let self = this;
        Tools.ajaxRequest({
            data: {
                filtros: {
                    accesoID: document.querySelector('[name="accesoID"]').value
                }
            },
            url: 'functions/factura-v4/get-latest-complementos',
            method: 'POST',
            success: function(response) {
                let result = JSON.parse(response);
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<result['resultado'].length; i++) {
                    let factura = result['resultado'][i];
                    if (factura['UID'] != self.facturaUID) {
                        let option = document.createElement('option');
                        option.value = factura['UUID'];
                        option.textContent = 'Folio: ' + factura['Folio'] + ' | Cliente: ' + factura['RazonSocialReceptor'];
                        optionsFrag.appendChild(option);
                    }
                }
                self.searcherSelector.children[0].remove();
                if (optionsFrag.children.length > 0) {
                    self.searcherSelector.disabled = false;
                    self.searcherSelector.appendChild(optionsFrag);
                }
                else {
                    self.searcherSelector.innerHTML = '<option>Vacío</option>'
                }
            }
        });
    }
}