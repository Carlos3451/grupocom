(function() {

clearInputs();

let clientToggle = new class {
    constructor() {
        let self = this;
        this.toggle = document.querySelector('.client-toggle');
        this.container = document.querySelector('.client-data-container');
        this.toggle.addEventListener('click', function() {
            this.classList.toggle('open');
            self.toggleClientContainer();
        });
    }
    toggleClientContainer() {
        this.container.classList.toggle('open');
    }
}

let preloadClientUID = null;
let clientsLoaded = false;

let getData = {};
let productsList = null;
let clientsList = null;

let clienteSI = new SelectInp(document.querySelector('[name="clienteUID"]'), {
    selectOnHover: false
});

let dateNow = new Date();

let fechaFacturacionDP = new DatePicker(document.querySelector('[name="fechaFacturacion"]'), {
    dateMin: new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 3),
    dateMax: new Date()
});

let fechaPago = new DatePicker(document.querySelector('[name="fechaPago"]'));

clienteSI.searchInput.setAttribute('enterfocus', 1);

loadFacturaAccesos();

function clearFacturaInputs() {
    clienteSI.searchInput.value = 'Cargando clientes...';
    clienteSI.searchInput.setAttribute('disabled', '');

    Tools.removeChilds(document.querySelector('[name="formaPago"]'));
    document.querySelector('[name="formaPago"]').innerHTML = '<option>Cargando datos...</option>';
    document.querySelector('[name="formaPago"]').setAttribute('disabled', '');
}

function loadFacturaAccesos() {
    let emisorSelect = document.querySelector('[name="emisorID"]');
    emisorSelect.innerHTML = '<option>Cargando emisores...</option>';
    emisorSelect.setAttribute('disabled', '');
    clearFacturaInputs();
    Tools.ajaxRequest({
        url: 'functions/factura-v4/get-all-access',
        method: 'GET',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                Tools.removeChilds(emisorSelect);
                emisorSelect.removeAttribute('disabled');
                let result = data['resultado'];
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<result.length; i++) {
                    let option = document.createElement('option');
                    option.value = result[i]['id'];
                    option.textContent = result[i]['nombre'];
                    optionsFrag.appendChild(option);
                }
                emisorSelect.appendChild(optionsFrag);
                emisorSelect.dispatchEvent(new Event('change'));
            }
        }
    });
}

let loadSatDataPromise;

document.querySelector('[name="emisorID"]').addEventListener('change', function() {
    clearFacturaInputs();
    // loadFormaPago(this.value);
    loadClients(this.value);
    loadSatDataPromise = loadSATData(this.value);
    removeFacturas();
});

function loadClients(accessID) {
    let inp = clienteSI.searchInput;
    clientsList = [];
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura-v4/get-all-clients',
        success: function(response) {
            inp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                clientsList = data['resultado'];
                setClients();
            }
        }
    });
}

function loadSATData(accesoID) {
    return new Promise(function(resolve) {
        let clienteRegimenFiscalSelector = document.querySelector('[name="clienteRegimenFiscal"]');        
        let clienteUsoCFDISelector = document.querySelector('[name="clienteUsoCFDI"]');
        let clienteFormaPagoSelector = document.querySelector('[name="clienteFormaPago"]');

        let formaPagoSelector = document.querySelector('[name="formaPago"]');

        Tools.ajaxRequest({
            data: {
                accesoID: accesoID
            },
            method: 'POST',
            url: 'functions/factura-v4/get-sat-data',
            success: function(response) {
                Tools.removeChilds(clienteRegimenFiscalSelector);
                Tools.removeChilds(clienteUsoCFDISelector);
                Tools.removeChilds(clienteFormaPagoSelector);
                Tools.removeChilds(formaPagoSelector);

                formaPagoSelector.disabled = false;

                let data = JSON.parse(response);
                if (data['error'] == '') {
                    if (data['resultado']['regimenFiscal']['error'] == '') {
                        let result = data['resultado']['regimenFiscal']['resultado'];
                        let optionsFrag = document.createDocumentFragment();
                        for (let i=0; i<result.length; i++) {
                            let option = document.createElement('option');
                            option.textContent = result[i]['name'];
                            option.setAttribute('value', result[i]['key']);
                            optionsFrag.appendChild(option);
                        }
                        clienteRegimenFiscalSelector.appendChild(optionsFrag);
                    }
                    if (data['resultado']['usoCfdi']['error'] == '') {
                        let result = data['resultado']['usoCfdi']['resultado'];
                        let optionsFrag = document.createDocumentFragment();
                        for (let i=0; i<result.length; i++) {
                            let option = document.createElement('option');
                            option.textContent = result[i]['name'];
                            option.setAttribute('value', result[i]['key']);
                            optionsFrag.appendChild(option);
                        }
                        clienteUsoCFDISelector.appendChild(optionsFrag);
                    }
                    if (data['resultado']['formaPago']['error'] == '') {
                        let result = data['resultado']['formaPago']['resultado'];
                        let optionsFrag = document.createDocumentFragment();
                        for (let i=0; i<result.length; i++) {
                            let option = document.createElement('option');
                            option.textContent = result[i]['name'];
                            option.setAttribute('value', result[i]['key']);
                            optionsFrag.appendChild(option);
                        }
                        clienteFormaPagoSelector.appendChild(optionsFrag.cloneNode(true));
                        formaPagoSelector.appendChild(optionsFrag);
                    }
                }
                resolve();
            }
        });
    });
}

function setClients() {
    let options = [];
    for (let i=0; i<clientsList.length; i++) {
        let client = clientsList[i];
        let option = {
            name: '[' + client['RFC'] + '] ' + client['RazonSocial'],
            value: client['UID'],
            keys: [client['RFC'], client['RazonSocial'], '[' + client['RFC'] + '] ' + client['RazonSocial']]
        }
        options.push(option);
    }
    clienteSI.setOptions(options);
    clienteSI.searchInput.value = '';
    clienteSI.searchInput.removeAttribute('disabled');
    clientsLoaded = true;
    if (preloadClientUID != null) {
        if (preloadClientUID == 1) {
            let uid = clienteSI.container.querySelector('.selectinp-option').getAttribute('value');
            clienteSI.setValue(uid);
            clienteSI.ghostInput.dispatchEvent(new Event('change'));
        }
        else {
            clienteSI.setValue(preloadClientUID);
            clienteSI.ghostInput.dispatchEvent(new Event('change'));
        }
    }
}

clienteSI.onChange = function() {
    let value = clienteSI.value;
    let client = clientsList.find(function(obj) {
        return obj['UID'] == value;
    });
    if (client) {
        document.querySelectorAll('.client-actions-container > .actions-container > .client-edit').forEach(function(elem, index) {
            elem.classList.remove('disabled');
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'bounce 0.2s ease-in-out forwards';
        });
        document.querySelector('[name="clienteNombres"]').value = client['Contacto']['Nombre'];
        document.querySelector('[name="clienteApellidos"]').value = client['Contacto']['Apellidos'];
        document.querySelector('[name="clienteEmail"]').value = client['datosExtras'] ? client['datosExtras']['emails'] : client['Contacto']['Email'];
        document.querySelector('[name="clienteTelefono"]').value = client['Contacto']['Telefono'];
        document.querySelector('[name="clienteRazonSocial"]').value = client['RazonSocial'];
        document.querySelector('[name="clienteRFC"]').value = client['RFC'];
        document.querySelector('[name="clienteCalle"]').value = client['Calle'];
        document.querySelector('[name="clienteCodigoPostal"]').value = client['CodigoPostal'];
        document.querySelector('[name="clienteNumeroExterior"]').value = client['Numero'];
        document.querySelector('[name="clienteNumeroInterior"]').value = client['Interior'];
        document.querySelector('[name="clienteColonia"]').value = client['Colonia'];
        document.querySelector('[name="clienteCiudad"]').value = client['Ciudad'];
        document.querySelector('[name="clienteEstado"]').value = client['Estado'];

        loadSatDataPromise.then(function() {
            let clienteRegimenFiscalSelector = document.querySelector('[name="clienteRegimenFiscal"]');
            let options = clienteRegimenFiscalSelector.querySelectorAll('option');
            for (let i=0; i<options.length; i++) {
                let option = options[i];
                if (option.textContent == client['Regimen']) {
                    clienteRegimenFiscalSelector.value = option.value;
                    break;
                }
            }
            document.querySelector('[name="clienteUsoCFDI"]').value = client['UsoCFDI'];
            document.querySelector('[name="clienteFormaPago"]').value = client['datosExtras'] ? client['datosExtras']['formaPago'] : '';

            document.querySelector('[name="formaPago"]').value =client['datosExtras'] ? client['datosExtras']['formaPago'] : '';
        });

        FacturaAdder.loadFacturas(client['RFC']);
    }
    else {
        document.querySelectorAll('.client-actions-container > .actions-container > .client-edit').forEach(function(elem, index) {
            elem.style.animation = 'none';
            elem.offsetHeight;
            elem.style.animation = 'disappear 0.2s ease-in-out forwards';
        });
        document.querySelector('[name="clienteNombres"]').value = '';
        document.querySelector('[name="clienteApellidos"]').value = '';
        document.querySelector('[name="clienteEmail"]').value = '';
        document.querySelector('[name="clienteTelefono"]').value = '';
        document.querySelector('[name="clienteRazonSocial"]').value = '';
        document.querySelector('[name="clienteRFC"]').value = '';
        document.querySelector('[name="clienteCalle"]').value = '';
        document.querySelector('[name="clienteCodigoPostal"]').value = '';
        document.querySelector('[name="clienteNumeroExterior"]').value = '';
        document.querySelector('[name="clienteNumeroInterior"]').value = '';
        document.querySelector('[name="clienteColonia"]').value = '';
        document.querySelector('[name="clienteCiudad"]').value = '';
        document.querySelector('[name="clienteEstado"]').value = '';

        document.querySelector('[name="clienteRegimenFiscal"]').selectedIndex = 0;
        document.querySelector('[name="clienteUsoCFDI"]').selectedIndex = 0;
        document.querySelector('[name="clienteFormaPago"]').selectedIndex = 0;
    }
    removeFacturas();
}

function removeFacturas() {    
    document.querySelectorAll('.custom-table > .custom-table-row').forEach(function(row) {
        row.remove();
    });
    updateMontoTotal();
}

/* FACTURA DATA */

function loadFormaPago(accessID) {
    let selectInp = document.querySelector('[name="formaPago"]');
    Tools.ajaxRequest({
        data: {
            accesoID: accessID
        },
        method: 'POST',
        url: 'functions/factura-v4/get-formas-pago',
        success: function(response) {
            Tools.removeChilds(selectInp);
            selectInp.removeAttribute('disabled');
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let optionsFrag = document.createDocumentFragment();
                for (let i=0; i<data['resultado'].length; i++) {
                    let option = document.createElement('option');
                    option.textContent = data['resultado'][i]['name'];
                    option.setAttribute('value', data['resultado'][i]['key']);
                    optionsFrag.appendChild(option);
                }                
                selectInp.appendChild(optionsFrag);
            }
        }
    });
}

function addFactura(factura) {
    if (factura != null) {
        let row = document.createElement('div');
        row.className = 'custom-table-row';
        row.dataset.uid = factura['uid'];
        row.dataset.uuid = factura['uuid'];
        row.dataset.pagos = factura['pagos'];
        row.innerHTML = (
            '<div class="custom-table-column"><input name="facturaFecha" type="text" value="' + factura['fecha'] + '" disabled></div>' +
            '<div class="custom-table-column"><input name="facturaFolio" type="text" value="' + factura['folio'] + '" disabled></div>' +
            '<div class="custom-table-column"><input class="inp-icon inp-price" name="facturaTotal" type="text" value="' + factura['total'] + '" numbers disabled></div>' +
            '<div class="custom-table-column"><input class="inp-icon inp-price" name="facturaSaldoAnterior" type="text" value="' + factura['saldoAnterior'] + '" numbers disabled></div>' +
            '<div class="custom-table-column"><input class="inp-icon inp-price" name="facturaImporte" type="text" value="' + factura['saldoAnterior'] + '" numbers></div>' +
            '<div class="custom-table-column"><div class="actions-container"><img class="action-delete-row" src="imgs/icons/actions/delete.png"></div></div>'
        );
        
        row.querySelectorAll('[numbers]').forEach(function(elem) {
            elem.addEventListener('keydown', evKeydownIsNumber);
            elem.addEventListener('focus', function() { this.select() });
        });

        row.querySelectorAll('input, select').forEach(function(elem) {
            elem.addEventListener('keydown', function(ev) {
                let columnIdx = -1;
                let column = this.closest('.custom-table-column');
                let row = column.parentElement;
                switch (ev.key) {
                    case 'ArrowLeft':
                        ev.preventDefault();
                        while (column.previousElementSibling) {
                            let input = column.previousElementSibling.querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                setTimeout(function() {
                                    input.select();
                                });
                                break;
                            }
                            else {
                                column = column.previousElementSibling;
                            }
                        }
                        break;
                    case 'ArrowRight':
                        ev.preventDefault();
                        while (column.nextElementSibling) {
                            let input = column.nextElementSibling.querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                if (input.tagName != 'SELECT') {
                                    setTimeout(function() {
                                        input.select();
                                    });
                                }
                                break;
                            }
                            else {
                                column = column.nextElementSibling;
                            }
                        }
                        break;
                    case 'Enter': case 'ArrowDown':
                        if (this.tagName == 'SELECT') {
                            return
                        }
                        ev.preventDefault();
                        columnIdx = -1;
                        for (let i=0; i<row.children.length; i++) {
                            if (row.children[i].isEqualNode(column)) {
                                columnIdx = i;
                                break;
                            }
                        }
                        if (columnIdx == -1) {
                            return;
                        }
                        while (row.nextElementSibling) {
                            let input = row.nextElementSibling.children[columnIdx].querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                setTimeout(function() {
                                    input.select();
                                });
                                break;
                            }
                            else {
                                row = row.nextElementSibling;
                            }
                        }
                        break;
                    case 'ArrowUp':
                        if (this.tagName == 'SELECT') {
                            return
                        }
                        ev.preventDefault();
                        columnIdx = -1;
                        for (let i=0; i<row.children.length; i++) {
                            if (row.children[i].isEqualNode(column)) {
                                columnIdx = i;
                                break;
                            }
                        }
                        if (columnIdx == -1) {
                            return;
                        }
                        while (row.previousElementSibling) {
                            let input = row.previousElementSibling.children[columnIdx].querySelector('input, select');
                            if (input && !input.disabled) {
                                input.focus();
                                setTimeout(function() {
                                    input.select();
                                });
                                break;
                            }
                            else {
                                row = row.previousElementSibling;
                            }
                        }
                        break;
                }
            });
        });

        row.querySelector('[name="facturaImporte"]').addEventListener('blur', evBlurPrice);

        row.querySelector('[name="facturaImporte"]').addEventListener('input', function() {
            updateMontoTotal();
        });

        row.querySelector('.action-delete-row').addEventListener('click', function() {
            new Question('Se eliminara esta factura', '¿Esta de acuerdo?', function() {
                row.remove();
                updateMontoTotal();
            });
        });

        document.querySelector('.custom-table').appendChild(row);
        updateMontoTotal();

        setTimeout(function(){
            row.querySelector('[name="facturaImporte"]').focus();
        }, 1);
    }
}

function updateMontoTotal() {
    let total = 0;
    document.querySelectorAll('.custom-table-row').forEach(function(row) {
        total += parseFloat(row.querySelector('[name="facturaImporte"]').value);
    });
    document.querySelector('[name="montoTotal"]').value = parseFloat(total.toFixed(6));
}

function clearInputs() {
    document.querySelectorAll('input').forEach(function(inp) {
        inp.value = '';
    });
    document.querySelector('[name="montoTotal"]').value = '0.00';
}

/* NUMERIC */

function evKeydownIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

function evBlurPrice() {
    if (this.value != '') {
        this.value = parseFloat(parseFloat(this.value).toFixed(6));
    }
    else {
        this.value = (0).toFixed(2);
    }
}

function getFacturas() {
    let facturas = [];
    document.querySelectorAll('.custom-table-row').forEach(function(row) {
        facturas.push({
            uid: row.dataset.uid,
            uuid: row.dataset.uuid,
            numeroParcialidad: parseFloat(row.dataset.pagos) + 1,
            importe: row.querySelector('[name="facturaImporte"]').value,
            saldoAnterior: row.querySelector('[name="facturaSaldoAnterior"]').value
        });
    });
    return facturas;
}

function getFormData() {
    let data = {
        accesoID: document.querySelector('[name="emisorID"]').value,
        clienteUID: document.querySelector('[name="clienteUID"]').value,
        fecha: document.querySelector('[name="fechaFacturacion"]').value,
        fechaPago: document.querySelector('[name="fechaPago"]').value,
        formaPago: document.querySelector('[name="formaPago"]').value,
        montoTotal: document.querySelector('[name="montoTotal"]').value,
        facturas: getFacturas()
    }
    return data;
}

/* GETS */

getGetValues();

function getGetValues() {
    let getArr = window.location.search.replace('?', '').split('&');
    for (let i=0; i<getArr.length; i++) {
        let get = getArr[i].split('=');
        getData[get[0]] = decodeURIComponent(get[1]).replace(/\+/g, ' ');
    }
    if (getData['tipo']) {
        switch (getData['tipo']) {
            case 'mostrador':
                if (getData['id']) {
                    loadTicketFromID(getData['id']);
                }
                else if (getData['fecha']) {
                    loadTicketFromDate(getData['fecha']);
                }
                break;
            case 'pedido':
                if (getData['id']) {
                    loadPedidoFromID(getData['id']);
                }
                else if (getData['fecha']) {
                    loadPedidoFromDate(getData['fecha']);
                }
                break;
        }
    }
}

/* SUBMIT */
    
document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

document.querySelector('.form-submit').addEventListener('click', function(ev) {
    let filled = isFormFilled();
    if (filled) {
        submitForm();
    }
});

function submitForm() {
    document.querySelector('.form-submit').setAttribute('disabled', '');
    Tools.ajaxRequest({
        data: getFormData(),
        url: 'functions/factura-v4/add-complemento-multiples',
        method: 'POST',
        waitMsg: new Wait('Generando complemento de pago...'),
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                new Question('Complemento de pago generado con exito.', '¿Desea mandarla por correo?', function() {
                    let razonSocial = document.querySelector('[name="clienteRazonSocial"]').value;
                    let folio = result.INV.Serie + ' ' + result.INV.Folio;
                    let fecha = document.querySelector('[name="fechaFacturacion"]').value;
                    let total = document.querySelector('[name="montoTotal"]').value;
                    EmailFactura.open(folio, document.querySelector('[name="clienteEmail"]').value, result['uid'], document.querySelector('[name="emisorID"]').value, function() {
                        location = '/admin/complementos-v4';
                    });
                    EmailFactura.ccsInp.value = 'contabilidad@shinyclean.mx';
                    EmailFactura.mensajeInp.value = (`Para: <b>${razonSocial}</b>
Estimado Cliente.

Se emité para Usted un(os) documento(s) de tipo Factura,consulte los datos adjuntos, por favor.

Serie y Folio: <b>${folio}</b>
Fecha de Expedición: <b>${fecha}</b>
Monto Total: <b>$${total}</b>


*No responder a este correo electronico, no podemos responder las consultas que se envíen a esta dirección.
Gracias.`);
                }, function() {
                    location = '/admin/complementos-v4';
                });
            }
            else {
                console.log(data);
                switch (data['error']) {
                    case 'FACTURA_ERROR':
                        if (data['errorLog']['message']) {
                            if (data['errorLog']['message']['message']) {
                                new Message('Ocurrio un error al tratar de generar el complemento de pago<br>"' + data['errorLog']['message']['message'] + '"', 'error');
                            }
                            else {
                                new Message('Ocurrio un error al tratar de generar el complemento de pago<br>"' + data['errorLog']['message'] + '"', 'error');
                            }
                        }
                        else if (data['errorLog']) {
                            new Message('Ocurrio un error al tratar de generar el complemento de pago<br>' + JSON.stringify(data['errorLog']), 'error');
                        }
                        else {
                            new Message('Ocurrio un error al tratar de generar el complemento de pago<br>"' + data['error'] + '"', 'error');
                        }
                        break;
                    default:
                        new Message('Ocurrio un error al tratar de generar el complemento de pago<br>"' + data['error'] + '"', 'error');
                        break;
                }
            }
        },
        error: function() {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            new Message('Ocurrio un error al tratar de conectarse con el servidor, intentelo de nuevo.', 'error');
        }
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('.inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

/* AGREAGAR FACTURAS */

const FacturaAdder = new class {
    constructor() {
        let self = this;
        this.facturasAjax = null;
        this.overlay = document.querySelector('.facturadder-overlay');
        this.container = this.overlay.querySelector('.facturadder-container');
        this.table = this.container.querySelector('.facturadder-table');
        this.container.querySelector('.facturadder-table').addEventListener('click', function(ev) {
            if (ev.target.classList.contains('facturadder-selector')) {
                ev.target.parentElement.parentElement.classList.toggle('selected');
                let selectedsCount = self.table.querySelectorAll('.selected').length;
                if (selectedsCount != 0) {                    
                    self.container.querySelector('.facturadder-add-btn').disabled = false;
                    self.container.querySelector('.facturadder-add-btn').textContent = 'Agregar (' + selectedsCount + ')';
                }
                else {                    
                    self.container.querySelector('.facturadder-add-btn').disabled = true;
                    self.container.querySelector('.facturadder-add-btn').textContent = 'Agregar';
                }
            }
        });
        this.container.querySelector('.facturadder-add-btn').addEventListener('click', function() {
            let facturas = self.getSelecteds();
            for (let i=0; i<facturas.length; i++) {
                if (document.querySelector('.custom-table [data-uid="' + facturas[i]['uid'] + '"]')) {
                    continue;
                }
                addFactura(facturas[i]);
            }
            self.close();
        });
        this.container.querySelector('.facturadder-close-btn').addEventListener('click', function() {
            self.close();
        });
    }
    loadFacturas(rfc) {
        let self = this;
        if (rfc == '') {
            return;
        }
        if (this.facturasAjax) {
            this.facturasAjax.abort();
        }
        this.clearTable();
        let loadingRow = this.createLoading();
        this.facturasAjax = Tools.ajaxRequest({
            data: {
                filtros: {
                    accesoID: document.querySelector('[name="emisorID"]').value,
                    rfc: rfc,
                    limite: 1000
                }
            },
            url: 'functions/factura-v4/get-all',
            method: 'POST',
            success: function(response) {
                loadingRow.remove();
                let result = JSON.parse(response);
                if (result['error'] == '') {
                    self.addFacturas(result['resultado']);
                }
            }
        });
    }
    createLoading() {
        let row = document.createElement('div');
        row.className = 'facturadder-table-loading';
        row.innerHTML = (
            '<div class="loading"></div>'+
            '<p>Cargando...</p>'
        );
        this.table.appendChild(row);
        return row;
    }
    clearTable() {
        this.table.querySelectorAll('.facturadder-table-row').forEach(function(row) {
            row.remove();
        });
    }
    addFacturas(facturas) {
        let rowsFragment = document.createDocumentFragment();
        for (let i=0; i<facturas.length; i++) {
            let factura = facturas[i];
            if (factura['datosExtra']) {
                let row = this.generateRow(factura);
                rowsFragment.appendChild(row);
            }
        }
        this.container.querySelector('.facturadder-table').appendChild(rowsFragment);
    }
    generateRow(factura) {
        let row = document.createElement('div');
        let saldoAnterior = parseFloat(factura['Total']) - parseFloat(factura['datosExtra']['pagoAcumulado']);
        row.className = 'facturadder-table-row';
        row.dataset.uid = factura['UID'];
        row.dataset.uuid = factura['UUID'];
        row.dataset.pagos = factura['datosExtra']['pagos'];
        row.dataset.saldoanterior = saldoAnterior;
        row.dataset.total = factura['Total'];
        row.dataset.fecha = factura['FechaTimbrado'].split('-').reverse().join('/');
        row.dataset.folio = factura['Folio'];
        row.innerHTML = (
            '<div class="facturadder-table-column"><div class="facturadder-selector"></div></div>'+
            '<div class="facturadder-table-column">'+
                '<p>' + row.dataset.fecha + '</p>'+
            '</div>'+
            '<div class="facturadder-table-column">'+
                '<p>' + factura['Folio'] + '</p>'+
            '</div>'+
            '<div class="facturadder-table-column">'+
                '<p class="text-align-right saldo-restante">' + (saldoAnterior == 0 ? 'PAGADO' : ('$' + saldoAnterior)) + '</p>'+
            '</div>'+
            '<div class="facturadder-table-column">'+
                '<p class="text-align-right">$' + parseFloat(factura['Total']) + '</p>'+
            '</div>'
        );
        if (saldoAnterior == 0) {
            row.querySelector('.facturadder-selector').remove();
            row.setAttribute('pagado', '');
        }
        return row;
    }
    getSelecteds() {
        let facturas = [];
        this.table.querySelectorAll('.selected').forEach(function(row) {
            facturas.push({
                folio: row.dataset.folio,
                fecha: row.dataset.fecha,
                uid: row.dataset.uid,
                uuid: row.dataset.uuid,
                pagos: row.dataset.pagos,
                saldoAnterior: row.dataset.saldoanterior,
                total: row.dataset.total
            });
        });
        return facturas;
    }
    disableDuplicateds() {
        let uids = [];
        document.querySelectorAll('.custom-table > .custom-table-row').forEach(function(row) {
            uids.push(row.dataset.uid);
        });
        this.table.querySelectorAll('.facturadder-table-row').forEach(function(row) {
            row.style.display = '';
        });
        for (let i=0; i<uids.length; i++) {
            let row = this.table.querySelector('[data-uid="' + uids[i] + '"]');
            if (row) {
                row.style.display = 'none';
            }
        }
    }
    open() {
        this.table.querySelectorAll('.selected').forEach(function(row) {
            row.classList.remove('selected');
        });
        this.disableDuplicateds();
        this.container.querySelector('.facturadder-add-btn').textContent = 'Agregar';
        this.container.querySelector('.facturadder-add-btn').disabled = true;
        this.overlay.style.display = 'flex';
        this.overlay.offsetHeight;
        this.overlay.style.opacity = 1;
    }
    close() {
        let self = this;
        this.overlay.style.opacity = 0;
        setTimeout(function() {
            self.overlay.style.display = '';
        }, 200);
    }
}

document.querySelector('.add-factura-btn').addEventListener('click', function() {
    let clienteUID = document.querySelector('[name="clienteUID"]').value;
    if (clienteUID == '') {
        new Message('Seleccione un cliente primero', 'warning');
        return;
    }
    FacturaAdder.open();
});

// PASOS CON ENTER

document.querySelectorAll('[enterfocus]').forEach(function(elem) {
    elem.addEventListener('keydown', function(ev) {
        console.log(ev.key);
        if (ev.key == 'Enter') {
            ev.preventDefault();
            let enabledInputs = document.querySelectorAll('[enterfocus="' + this.getAttribute('enterfocus') +'"]');
            let idx = -1;
            for (let i=0; i<enabledInputs.length; i++) {
                if (enabledInputs[i].isEqualNode(this)) {
                    idx = i;
                    break;
                }
            }
            if (idx == -1) {
                return;
            }
            if (idx == enabledInputs.length) {
                return;
            }
            console.log(enabledInputs[idx + 1]);
            enabledInputs[idx + 1].focus();
            if (enabledInputs[idx + 1].tagName == 'INPUT') {
                setTimeout(function() {
                    enabledInputs[idx + 1].select();
                });
            }
        }
    });
});

})();