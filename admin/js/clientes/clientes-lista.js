(function(){

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/clientes/ver/" + dataID;
}

Lista.actionPedidos['active'] = true;
Lista.actionPedidos['unique'] = true;
Lista.pedidosFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    let date = new Date();
    let year = encodeURIComponent(date.getFullYear());
    window.location = '/admin/pedidos?fecha=' + year + '&cliente=' + dataID;
}

if (privilegiosNivel > 1) {
    Lista.actionLock['active'] = true;
    Lista.actionLock['unique'] = false;
    Lista.lockFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se bloqueara el cliente seleccionado.',
            msgWait: 'Bloqueando cliente.',
            msgSuccess: 'Cliente bloqueado con exito.',
            msgError: 'Ocurrio un error al tratar de bloquear el cliente.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se bloquearan los clientes seleccionados.',
                msgWait: 'Bloqueando clientes.',
                msgSuccess: 'Clientes bloqueados con exito.',
                msgError: 'Ocurrio un error al tratar de bloquear los clientes.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Question(requestObj.msgQuestion, '¿Esta de acuerdo?', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/cliente/lock',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.setAttribute('bloqueado', '');
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
    Lista.actionUnlock['active'] = true;
    Lista.actionUnlock['unique'] = false;
    Lista.unlockFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se desbloqueara el cliente seleccionado.',
            msgWait: 'Desbloqueando cliente.',
            msgSuccess: 'Cliente desbloqueado con exito.',
            msgError: 'Ocurrio un error al tratar de desbloquear el cliente.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se desbloquearan los clientes seleccionados.',
                msgWait: 'Desbloqueando cliente.',
                msgSuccess: 'Clientes desbloqueados con exito.',
                msgError: 'Ocurrio un error al tratar de desbloquear los cliente.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Question(requestObj.msgQuestion, '¿Esta de acuerdo?', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/cliente/unlock',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.removeAttribute('bloqueado');
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        if (dataID == 1) {
            new Message('No se puede editar al Publica en General', 'warning');
        }
        else {
            window.location = "/admin/clientes/editar/" + dataID;
        }
    }
}

if (privilegiosNivel > 2) {
    Lista.actionDelete['active'] = true;
    Lista.actionDelete['unique'] = false;
    Lista.deleteFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se eliminara al cliente seleccionado.',
            msgWait: 'Eliminando cliente.',
            msgSuccess: 'Cliente eliminado con exito.',
            msgError: 'Ocurrio un error al tratar de elimninar al cliente.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se eliminaran los clientes seleccionadas.',
                msgWait: 'Eliminando clientes.',
                msgSuccess: 'Clientes eliminados con exito.',
                msgError: 'Ocurrio un error al tratar de eliminar a los clientes.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Confirm(requestObj.msgQuestion, 'eliminar', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/cliente/delete',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.remove();
                        });
                        Lista.updateSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
}

SortController.setOnMethodChange(function() {
    Lista.loadByParts('cliente/get-all-list', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let columns = [];
        let direccion = data['calle'];
        let ruta = data['rutaID'] != null ? '<a href="/admin/rutas/ver/' + + data['rutaID'] + '">' + data['rutaNombre'] + '</a>' : 'S/D';
        if (data['numeroExterior'] != '') { direccion += ' #' + data['numeroExterior']; }
        if (data['numeroInterior'] != '') { direccion += ' Int. ' + data['numeroInterior']; }
        if (data['colonia'] != '') { direccion += ', ' + data['colonia']; }
        if (data['codigoPostal'] != '') { direccion += ', ' + data['codigoPostal']; }
        let routeDaysElem = document.createElement('div');
        routeDaysElem.innerHTML =
            '<div class="routedays-containers">' +
                '<div class="routeday-container"><p>L</p></div>' +
                '<div class="routeday-container"><p>M</p></div>' +
                '<div class="routeday-container"><p>M</p></div>' +
                '<div class="routeday-container"><p>J</p></div>' +
                '<div class="routeday-container"><p>V</p></div>' +
                '<div class="routeday-container"><p>S</p></div>' +
                '<div class="routeday-container"><p>D</p></div>' +
            '</div>';
        let routeDaysChilds = routeDaysElem.querySelectorAll('.routeday-container');
        let routeDays = data['rutaDias'].toString(2).split('').reverse();
        for (let i=0; i<routeDays.length; i++) {
            if (routeDays[i] == 1) {
                if (routeDaysChilds[i]) {
                    routeDaysChilds[i].classList.add('enabled');
                }
            }
        }
        columns = [
            '<a href="/admin/clientes/ver/' + data['id'] + '">' + data.id + '</a>',
            data['nombre'],
            ruta,
            routeDaysElem.innerHTML,
            data['telefono'],
            '$' + data['deuda'],
            data['direccionCompleta']
        ];
        return columns;
    }, function(data) {
        let attrs = [
            ['data-id', data['id']]
        ];
        if (data['bloqueado'] == 1) {
            attrs.push(['bloqueado', '']);
        }
        return attrs;
    });

});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Ruta', 'ruta'],
    ['Nombre', 'nombre'],
    ['Dirección', 'direccion'],
    ['Deuda', 'deuda']
]);

SortController.setFilterOptions([
    [
        ['Ruta', 'select', 'rutaID', [['Todas', -1], ['Sin Ruta', 0]]],
        ['Días', 'select', 'dias', [
            ['Todos', 0],
            ['Lunes', 1],
            ['Martes', 2],
            ['Miercoles', 3],
            ['Jueves', 4],
            ['Viernes', 5],
            ['Sabado', 6],
            ['Domingo', 7],
            ['Sin Definir', -1]
        ]],
    ], [
        ['Solo deudores', 'input', 'deudor', 'checkbox'],
        ['Solo bloqueados', 'input', 'bloqueado', 'checkbox']
    ]
]);

new CheckSwitch(document.querySelector('[name="deudor"]'), "Si", "No");
new CheckSwitch(document.querySelector('[name="bloqueado"]'), "Si", "No");

Tools.ajaxRequest({
    data: {
        metodo: 'nombre',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/ruta/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let frag = document.createDocumentFragment();
            let rutas = data['resultado'];
            for (let i=0; i<rutas.length; i++) {
                let option = document.createElement('option');
                option.setAttribute('value', rutas[i]['id']);
                option.textContent = rutas[i]['nombre'] + ' (' + rutas[i]['usuarioNombre'] + ')';
                frag.appendChild(option);
            }
            document.querySelector('[name="rutaID"]').appendChild(frag);
        }
    }
});

SortController.load();

})();