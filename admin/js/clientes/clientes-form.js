(function() {

clearForm();

if (formMode == 'EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos.');
}

document.querySelectorAll('[prettycheckbox]').forEach(function(elem, index) {
    let contextData = elem.getAttribute('data-context');
    new PrettyCheckbox(elem, {
        contextMode: 'text',
        contextData: contextData
    });
});

getRoutes().then(function(values) {
    if (formMode == 'EDIT' || formMode == 'VIEW') {
        if (editID == 1 && formMode != 'VIEW') {
            new Message('No se puede editar este cliente.', 'warning', function() {
                window.location = '/admin/clientes';
            });
        }
        else {
            getCliente();
        }
    }
});

function getRoutes() {
    let promise = new Promise(function(resolve, reject) {
        Tools.ajaxRequest({
            data: {
                metodo: 'nombre',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/ruta/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    loadRoutes(result);
                }
                resolve();
            }
        });
    });
    return promise;
}

function loadRoutes(routes) {
    let optionsFrag = document.createDocumentFragment();
    for (let i=0; i<routes.length; i++) {
        let option = document.createElement('option');
        option.value = routes[i]['id'];
        option.text = routes[i]['nombre'];
        optionsFrag.appendChild(option);
    }
    document.querySelector('[name="rutaID"]').appendChild(optionsFrag);
}

function getCliente() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/cliente/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadCliente(data['resultado']);
            }
            else {
                new Message('Ocurrio un error al obtener el cliente.<br>' + data['error'], 'error', function() {
                    window.location = 'clientes';
                });
            }
        }
    });
}

function loadCliente(cliente) {
    document.querySelector('[name="nombre"]').value = cliente['nombre'];
    document.querySelector('[name="telefono"]').value = cliente['telefono'];
    document.querySelector('[name="email"]').value = cliente['email'];
    document.querySelector('[name="credito"]').value = cliente['credito'];
    document.querySelector('[name="calle"]').value = cliente['calle'];
    document.querySelector('[name="numeroExterior"]').value = cliente['numeroExterior'];
    document.querySelector('[name="numeroInterior"]').value = cliente['numeroInterior'];
    document.querySelector('[name="colonia"]').value = cliente['colonia'];
    document.querySelector('[name="ciudad"]').value = cliente['ciudad'];
    document.querySelector('[name="estado"]').value = cliente['estado'];
    document.querySelector('[name="codigoPostal"]').value = cliente['codigoPostal'];
    document.querySelector('[name="rutaID"]').value = cliente['rutaID'] || 0;
    document.querySelector('[name="lat"]').value = cliente['lat'];
    document.querySelector('[name="lng"]').value = cliente['lng'];

    loadRouteDays(cliente['rutaDias']);
    ClientMap.updateMarker();
    if (formMode == 'VIEW') {
        disableInputs();
    }
}
    
document.querySelector('#cliente-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
    
    document.querySelector('.map-geocoding').addEventListener('click', function(ev) {
        let street = document.querySelector('[name="calle"]').value;
        let numExt = document.querySelector('[name="numeroExterior"]').value;
        let colony = document.querySelector('[name="colonia"]').value;
        let city = document.querySelector('[name="ciudad"]').value;
        let state = document.querySelector('[name="estado"]').value;
        let postalCode = document.querySelector('[name="codigoPostal"]').value;
        Tools.ajaxRequest({
            url: 'https://us1.locationiq.com/v1/search.php?key=973c76f149e41f&street=' + street + ' ' + numExt + ' ' + colony + '&city=' + city +'&country=México&postalcode=' + postalCode + '&format=json&limit=1&countrycodes=MX',
            success: function(response) {
                let data = JSON.parse(response);
                ClientMap.setView(data[0]['lat'], data[0]['lon'], 16);
                ClientMap.setMarker(data[0]['lat'], data[0]['lon']);
            }
        });
    });

    document.querySelector('.map-reverse-geocoding').addEventListener('click', function(ev) {
        let lat = document.querySelector('[name="lat"]').value;
        let lng = document.querySelector('[name="lng"]').value;
        Tools.ajaxRequest({
            url: 'https://us1.locationiq.com/v1/reverse.php?key=973c76f149e41f&lat=' + lat + '&lon=' + lng + '&format=json&countrycodes=MX&zoom=14',
            success: function(response) {
                let data = JSON.parse(response);
                console.log(data);
                let colony = data['address']['neighbourhood'] || data['address']['suburb'] || data['address']['village'] || data['address']['town'] || '';
                let postalCode = data['address']['postcode'] || '';
                let city = data['address']['city'] || data['address']['county'] || '';
                let state = data['address']['state'] || '';
                document.querySelector('[name="ciudad"]').value = city;
                document.querySelector('[name="estado"]').value = state;
                document.querySelector('[name="colonia"]').value = colony;
                document.querySelector('[name="codigoPostal"]').value = postalCode;
            }
        });
        Tools.ajaxRequest({
            url: 'https://us1.locationiq.com/v1/reverse.php?key=973c76f149e41f&lat=' + lat + '&lon=' + lng + '&format=json&countrycodes=MX&zoom=18',
            success: function(response) {
                let data = JSON.parse(response);
                console.log(data);
                let street = data['address']['road'] || '';
                document.querySelector('[name="calle"]').value = street;
            }
        });
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/clientes';
        });
    }
    else {
        window.location = '/admin/clientes';
    }
});

function loadRouteDays(days) {
    let binaryDays = days.toString(2);
    let routeDays = ('0000000' + binaryDays).slice(-7).split('').reverse();
    let routeDaysElems = document.querySelectorAll('[data-route-day]');
    for (let i=0; i<routeDays.length; i++) {
        if (routeDays[i] == '1') {
            routeDaysElems[i].checked = true;
            routeDaysElems[i].dispatchEvent(new Event('change'));
        }
    }
}

function getRouteDays() {
    let days = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo'];
    let routeDays = '';
    for (let i=0; i<days.length; i++) {
        let elem = document.querySelector('[data-route-day="' + days[i] + '"]');
        routeDays += elem.checked ? '1' : '0';
    }
    return parseInt(routeDays.split('').reverse().join(''), 2);
}

function submitForm() {
    let formData = new FormData(document.querySelector('#cliente-form'));
    document.querySelector('.form-submit').setAttribute('disabled', '');
    formData.append('rutaDias', getRouteDays());
    if (formMode == 'ADD') {
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/cliente/add',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Añadiendo cliente.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Cliente añadido con exito.', 'success', function() {
                        location = '/admin/clientes';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            new Message('Ocurrio un error al añadir al cliente: "' + data['error'] + '"', 'error');
                            break;
                    }
                }
            }
        });
    }
    else if (formMode == 'EDIT') {
        formData.append('id', editID);
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/cliente/edit',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Guardando cliente.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Cliente guardado con exito.', 'success', function() {
                        location = '/admin/clientes';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            new Message('Ocurrio un error al tratar de guardar al cliente: "' + data['error'] + '"', 'error');
                            break;
                    }
                }
            }
        });
    }
}

function clearForm() {
    document.querySelectorAll('input').forEach(function(elem, index) {
        elem.value = '';
        elem.checked = false;
    });
    document.querySelector('[name="ciudad"]').value = 'Mazatlán';
    document.querySelector('[name="estado"]').value = 'Sinaloa';
    document.querySelector('[name="credito"]').value = '0.00';
    document.querySelector('[name="lat"]').value = 23.236614;
    document.querySelector('[name="lng"]').value = -106.4162317;
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#cliente-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}

document.querySelector('[name="credito"]').addEventListener('keypress', function(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
});
document.querySelector('[name="credito"]').addEventListener('blur', function(ev) {
    this.value = parseFloat(this.value).toFixed(2);
});

/* MAP */

ClientMap = new class {
    constructor() {
        let self = this;
        this.map = L.map(document.querySelector('.map-container > .map'), {
            fullscreenControl: true,
            center: [23.236614, -106.4162317],
            zoom: 12,
            doubleClickZoom: false,
            scrollWheelZoom: false
        });

        this.mapIcon = L.icon({
            iconUrl: '/admin/imgs/markers/shop-marker.png',
            iconAnchor: L.point(16, 41)
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);

        this.marker = L.marker([23.236614, -106.416231], {
            icon: this.mapIcon,
            draggable: true
        }).addTo(this.map);

        this.marker.setLatLng(L.latLng(23.236614, -106.416231));
        if (formMode != 'VIEW') {
            this.map.on('dblclick', function(ev) {
                self.marker.setLatLng(ev.latlng);
            });
            this.marker.on('move', function(ev) {
                let latLng = this.getLatLng();
                document.querySelector('[name="lat"]').value = latLng['lat'];
                document.querySelector('[name="lng"]').value = latLng['lng'];
            });
        }
        else {
            this.marker.draggable = false;
        }
    }
    updateMarker() {
        let latLng = L.latLng(document.querySelector('[name="lat"]').value, document.querySelector('[name="lng"]').value);
        this.map.setView(latLng, 16)
        this.marker.setLatLng(latLng);
    }
    setView(lat, lng, zoom) {
        let latLng = L.latLng(lat, lng);
        this.map.setView(latLng, zoom)
    }
    setMarker(lat, lng) {
        let latLng = L.latLng(lat, lng);
        this.marker.setLatLng(latLng);
    }
}();
    
})();