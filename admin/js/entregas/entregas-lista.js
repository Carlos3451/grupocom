(function(){

let newsTimeout = null;

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/ventas/ver/" + dataID;
}

Lista.actionDelivery['active'] = true;
Lista.actionDelivery['unique'] = false;
Lista.deliveryFn = function(selecteds) {
    let ids = [];
    for (let i=0; i<selecteds.length; i++) {
        ids.push(selecteds[i].getAttribute('data-id'));
    }
    Repartidor.open(ids);
}

Repartidor.onAccept = function(ids, repartidorID) {
    if (ids.length > 0) {
        Tools.ajaxRequest({
            data: {
                ids: ids,
                repartidorID: repartidorID
            },
            method: 'POST',
            url: 'functions/ticket/set-delivery',
            waitMsg: new Wait('Asignando repartidor.'),
            success: function(response) {
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Repartidor asignado con exito.', 'success', function() {
                        location.reload();
                    });
                }
                else {
                    new Message('Ocurrio un error al tratar de asignar al repartidor. Por favor intente de nuevo.<br>' + data['error'], 'error');
                }
            }
        });
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('ticket/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let dateTimeArray = data['fechaCreate'].split(' ');
        let dateArray = dateTimeArray[0].split('-');
        let timeArray = dateTimeArray[1].split(':');
        let payed = data['total'] - data['pagado'];
        return [
            '<a href="/admin/pedidos/ver/' + data['id'] + '">' + ('000' + data['folio']).slice(-4) + '</a>',
            dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0] + ' ' + dateTimeArray[1],
            '<a href="/admin/clientes/ver/' + data['clienteID'] + '">' + data['clienteNombre'] + '</a>',
            data['usuarioNombre'],
            data['repartidorNombre'],
            data['tipo'][0].toUpperCase() + data['tipo'].slice(1),
            '<p class="text-align-right">$ ' + data['total'] + '</p>'
        ];
    }, function(data) {
        let payed = data['total'] - data['pagado'];
        let attrs = [
            ['data-id', data['id']],
            ['data-total', data['total']],
            ['data-pagado', data['pagado']]
        ];
        if (payed <= 0) {
            attrs.push(['payed', '']);
        }
        if (data['cancelado'] == 1) {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    }, function(response) {
        getNewsPedidos(0);
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['Cliente', 'input', 'cliente', 'text'],
        ['Repartidor', 'input', 'repartidorID', 'text']
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'));
let clienteSI = new SelectInp(document.querySelector('[name="cliente"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});
let repartidorSI = new SelectInp(document.querySelector('[name="repartidorID"]'), {
    placeholder: 'Sin repartidor',
    defaultValue: -1
});

Tools.ajaxRequest({
    data: {
        metodo: 'folio',
        orden: 'asc',
        busqueda: ''
    },
        method: 'POST',
        url: 'functions/cliente/get-all-min',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let clientes = data['resultado'];
            for (let i=0; i<clientes.length; i++) {
                options.push({
                    'name': ('000' + clientes[i]['folio']).slice(-4) + ' ' + clientes[i]['nombre'],
                    'value': clientes[i]['id'],
                    'keys': [('000' + clientes[i]['folio']).slice(-4), clientes[i]['nombre']]
                });
            }
            clienteSI.setOptions(options);
        }
    }
});

Tools.ajaxRequest({
    data: {
        metodo: 'id',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-dealers',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            options.push({
                'name': 'Todos',
                'value': 0,
                'keys': ['todos']
            });
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            repartidorSI.setOptions(options);
            console.log(repartidorSI);
        }
    }
});

SortController.load();

function getNewsPedidos(idMax) {
    Tools.ajaxRequest({
        data: {
            busqueda: SortController.search,
            metodo: SortController.method,
            orden: SortController.order,
            filtros: SortController.filters,
            ultimoID: idMax
        },
        method: 'POST',
        url: 'functions/pedido/get-news',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (idMax != 0) {
                    document.querySelector('.list-news').classList.add('open');
                    if (data['resultado']['nuevos'] == 1) {
                        document.querySelector('.list-news > a').textContent = '1 pedido nuevo. Haz click aquí para recargar.';
                    }
                    else {
                        document.querySelector('.list-news > a').textContent = data['resultado']['nuevos'] + ' pedidos nuevos. Haz click aquí para recargar.';
                    }
                }
                else {
                    idMax = data['resultado']['idMax'];
                }
            }
            if (newsTimeout) {
                clearTimeout(newsTimeout);
            }
            newsTimeout = setTimeout(function() {
                getNewsPedidos(idMax);
            }, 30000);
        }
    });
}

})();