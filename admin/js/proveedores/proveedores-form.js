(function() {

clearForm();

if (formMode == 'EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos.');
}

document.querySelectorAll('[prettycheckbox]').forEach(function(elem, index) {
    let contextData = elem.getAttribute('data-context');
    new PrettyCheckbox(elem, {
        contextMode: 'text',
        contextData: contextData
    });
});

if (formMode == 'EDIT' || formMode == 'VIEW') {
    getProveedor();
}

function getProveedor() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/proveedor/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadProveedor(data['resultado']);
            }
            else {
                new Message('Ocurrio un error al obtener el proveedor.<br>' + data['error'], 'error', function() {
                    window.location = 'proveedores';
                });
            }
        }
    });
}

function loadProveedor(proveedor) {
    document.querySelector('[name="nombre"]').value = proveedor['nombre'];
    document.querySelector('[name="telefono"]').value = proveedor['telefono'];
    document.querySelector('[name="email"]').value = proveedor['email'];
    document.querySelector('[name="calle"]').value = proveedor['calle'];
    document.querySelector('[name="numeroExterior"]').value = proveedor['numeroExterior'];
    document.querySelector('[name="numeroInterior"]').value = proveedor['numeroInterior'];
    document.querySelector('[name="colonia"]').value = proveedor['colonia'];
    document.querySelector('[name="ciudad"]').value = proveedor['ciudad'];
    document.querySelector('[name="estado"]').value = proveedor['estado'];
    document.querySelector('[name="codigoPostal"]').value = proveedor['codigoPostal'];
    if (formMode == 'VIEW') {
        disableInputs();
    }
}
    
document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        let filled = isFormFilled();
        if (filled) {
            submitForm();
        }
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/proveedores';
        });
    }
    else {
        window.location = '/admin/proveedores';
    }
});

function submitForm() {
    let formData = new FormData(document.querySelector('#data-form'));
    document.querySelector('.form-submit').setAttribute('disabled', '');
    if (formMode == 'ADD') {
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/proveedor/add',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Añadiendo proveedor.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Proveedor añadido con exito.', 'success', function() {
                        location = '/admin/proveedores';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            new Message('Ocurrio un error al añadir al proveedor<br>' + data['error'], 'error');
                            break;
                    }
                }
            }
        });
    }
    else if (formMode == 'EDIT') {
        formData.append('id', editID);
        Tools.ajaxRequest({
            data: formData,
            url: 'functions/proveedor/edit',
            method: 'POST',
            contentType: false,
            waitMsg: new Wait('Guardando proveedor.', true),
            progress: function(progress) {
                this.waitMsg.setProgress(progress * 100);
            },
            success: function(response) {
                document.querySelector('.form-submit').removeAttribute('disabled');
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Proveedores guardado con exito.', 'success', function() {
                        location = '/admin/proveedores';
                    });
                }
                else {
                    switch (data['error']) {
                        default:
                            new Message('Ocurrio un error al tratar de guardar al proveedor<br>' + data['error'], 'error');
                            break;
                    }
                }
            }
        });
    }
}

function clearForm() {
    document.querySelectorAll('input').forEach(function(elem, index) {
        elem.value = '';
        elem.checked = false;
    });
    document.querySelector('[name="ciudad"]').value = 'Mazatlán';
    document.querySelector('[name="estado"]').value = 'Sinaloa';
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#data-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}

})();