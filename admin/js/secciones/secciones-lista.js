(function(){

Lista.actionEdit['active'] = true;
Lista.actionEdit['unique'] = true;
Lista.editFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/secciones/editar/" + dataID;
}

Lista.actionDelete['active'] = true;
Lista.actionDelete['unique'] = false;
Lista.deleteFn = function(selecteds) {
    let requestObj = {
        msgQuestion: 'Se eliminara la sección seleccionada.',
        msgWait: 'Eliminando sección.',
        msgSuccess: 'Sección eliminada con exito.',
        msgError: 'Ocurrio un error al tratar de elimninar la sección.' 
    };
    if (selecteds.length > 1) {
        requestObj = {
            msgQuestion: 'Se eliminaran las secciones seleccionadas.',
            msgWait: 'Eliminando secciones.',
            msgSuccess: 'Secciones eliminadas con exito.',
            msgError: 'Ocurrio un error al tratar de eliminar las secciones.'
        };
    }
    let ids = [];
    for (let i=0; i<selecteds.length; i++) {
        ids.push(selecteds[i].getAttribute('data-id'));
    }
    new Confirm(requestObj.msgQuestion, 'eliminar', function() {
        Tools.ajaxRequest({
            data: {
                ids: ids
            },
            method: 'POST',
            url: 'functions/seccion/delete',
            waitMsg: new Wait(requestObj['msgWait'], true),
            success: function(response) {
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message(requestObj['msgSuccess'], 'success');
                    selecteds.forEach(function(elem, index) {
                        elem.remove();
                    });
                    Lista.updateSelections();
                }
                else {
                    new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                }
            }
        });
    });
}

SortController.setOnMethodChange(function() {
    Lista.load('seccion/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order
    }, function(data) {
        return [('000' + data['id']).slice(-4), data['nombre']];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']]
        ];
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'id'],
    ['Nombre', 'nombre']
]);

SortController.load();

})();