(function() {

clearForm();

if (formMode=='EDIT') {
    loadingDataMsg = new Wait('Cargando datos.');
}

if (formMode == 'EDIT') {
    getSection();
}

function getSection() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/seccion/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadSection(data['resultado']);         
            }
        }
    });
}

function loadSection(seccion) {
    document.querySelector('[name="nombre"]').value = seccion['nombre'];
    document.querySelector('[name="claveGerente"]').value = seccion['claveGerente'];
    document.querySelector('.image-container > img').setAttribute('src', seccion['imagen']);
    document.querySelector('[name="lat"]').value = seccion['lat'];
    document.querySelector('[name="lng"]').value = seccion['lng'];
    SectionsMap.updateMarker();
}

document.querySelector('.image-container').addEventListener('click', function(ev) {
    this.querySelector('[name="imagen"]').click();
});

document.querySelector('[name="imagen"]').addEventListener('change', function(ev) {
    Tools.fileToBase64(this.files[0], function(data) {
        document.querySelector('.image-container > img').setAttribute('src', data);
    });
});

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

document.querySelector('.form-submit').addEventListener('click', function(ev) {
    let filled = isFormFilled();
    if (filled) {
        submitForm();
    }
});

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
        window.location = '/admin/secciones';
    });
});

function submitForm() {
    let formData = new FormData(document.querySelector('#data-form'));
    let processForm = {
        url: '',
        waitMessage: '',
        successMsg: '',
        errorMessage: '',
    }
    document.querySelector('.form-submit').setAttribute('disabled', '');
    switch (formMode) {
        case 'ADD':
            processForm = {
                url: 'add',
                waitMessage: 'Creando sección.',
                successMsg: 'Sección creada con exito.',
                errorMessage: 'Ocurrio un error al tratar de crear la sección.',
            }
            break;
        case 'EDIT':
            formData.append('id', editID);
            processForm = {
                url: 'edit',
                waitMessage: 'Guardando sección.',
                successMsg: 'Sección guardada con exito.',
                errorMessage: 'Ocurrio un error al tratar de guardar la sección.',
            }
            break;
    }
    Tools.ajaxRequest({
        data: formData,
        url: 'functions/seccion/' + processForm['url'],
        method: 'POST',
        contentType: false,
        waitMsg: new Wait(processForm['waitMessage'], true),
        progress: function(progress) {
            this.waitMsg.setProgress(progress * 100);
        },
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                new Message(processForm['successMsg'], 'success', function() {
                    location = '/admin/secciones';
                });
            }
            else {
                new Message(processForm['errorMessage'] + '<br>' + data['error'], 'error');
            }
        }
    });
}

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
}

function isFormFilled() {
    let filled = true;
    let requiredInputs = document.querySelectorAll('#data-form .inp-required');
    requiredInputs.forEach(function(elem, index) {
        if (elem.value == '') {
            filled = false;
            if (!elem.hasAttribute('invalid')) {
                elem.setAttribute('invalid', '');
                elem.addEventListener('keydown', clearInvalid);
            }
        }
    });
    if (!filled) {
        new Message('Por favor rellene los campos marcados para continuar.', 'warning');
    }
    return filled;
}

function clearInvalid() {
    this.removeAttribute('invalid');
}

/* MAP */

SectionsMap = new class {
    constructor() {
        let self = this;
        this.map = L.map(document.querySelector('.map-container > .map'), {
            center: [23.236614, -106.4162317],
            zoom: 12,
            doubleClickZoom: false,
            scrollWheelZoom: false
        });

        this.mapIcon = L.icon({
            iconUrl: '/admin/imgs/markers/matriz-marker.png',
            iconAnchor: L.point(16, 41)
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);

        this.marker = L.marker([23.236614, -106.416231], {
            icon: this.mapIcon,
            draggable: true
        }).addTo(this.map);

        this.marker.on('move', function(ev) {
            let latLng = this.getLatLng();
            document.querySelector('[name="lat"]').value = latLng['lat'];
            document.querySelector('[name="lng"]').value = latLng['lng'];
        });

        this.marker.setLatLng(L.latLng(23.236614, -106.416231));

        this.map.on('dblclick', function(ev) {
            self.marker.setLatLng(ev.latlng);
        });
    }
    updateMarker() {
        let latLng = L.latLng(document.querySelector('[name="lat"]').value, document.querySelector('[name="lng"]').value);
        this.map.setView(latLng, 16)
        this.marker.setLatLng(latLng);
    }
    setView(lat, lng, zoom) {
        let latLng = L.latLng(lat, lng);
        this.map.setView(latLng, zoom)
    }
    setMarker(lat, lng) {
        let latLng = L.latLng(lat, lng);
        this.marker.setLatLng(latLng);
    }
}();

})();