
class CheckSwitch {

	constructor(element, labelTrue, labelFalse) {
		this.element = element;
		this.labelTrue = labelTrue;
		this.labelFalse = labelFalse;
		this.checkSwitchElem = document.createElement("div");
		this.checkSwitchElem.className = "checkswitch-container";
		this.checkSwitchElem.innerHTML = 
			'<div class="checkswitch-background"></div>' +
			'<div class="checkswitch-ball"></div>' +
			'<p class="checkswitch-label">' + (this.element.checked ? labelTrue : labelFalse) + '</p>';
		this.element.parentNode.insertBefore(this.checkSwitchElem, this.element);
		this.checkSwitchElem.insertBefore(this.element, this.checkSwitchElem.querySelector('.checkswitch-background')).checked = this.element.checked;

		let self = this;

		this.checkSwitchElem.addEventListener('click', function() {
			self.toggleCheckbox();
		});
		this.element.addEventListener('change', function() {
			let value = self.element.checked;
			self.checkSwitchElem.querySelector('.checkswitch-label').innerHTML = value == 0 ? self.labelFalse : self.labelTrue;
		});
	}

	toggleCheckbox() {
		this.element.checked = !this.element.checked;
		this.element.dispatchEvent(new Event("change"));
	}

}