
class Message {

	constructor(str, type, func = null) {

		this.callback = func;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<button class="message-accept button">Aceptar</button>' +
					'<button class="message-focus"></button>' +
				'</div>' +
			'</div>';

		switch (type) {
			case 'success':
				this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/success.png');
				this.element.querySelector('.message-title').innerHTML = 'Exito';
				this.element.querySelector('.message-container').classList.add('success');
				break;
			case 'error':
				this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/error.png');
				this.element.querySelector('.message-title').innerHTML = 'Error';
				this.element.querySelector('.message-container').classList.add('error');
				break;
			case 'warning':
				this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/warning.png');
				this.element.querySelector('.message-title').innerHTML = 'Precaución';
				this.element.querySelector('.message-container').classList.add('warning');
				break;
			case 'info': default:
				this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/info.png');
				this.element.querySelector('.message-title').innerHTML = 'Info';
				this.element.querySelector('.message-container').classList.add('info');
				break;
		}

		this.element.querySelector('.message-text').innerHTML = str;

		let thisClass = this;

		this.element.querySelector('.message-accept').addEventListener('click', function(){
			thisClass.destroy();
		});

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);

		this.element.querySelector('.message-focus').focus();
	}

	destroy() {

		this.element.querySelector('.message-accept').setAttribute('disabled', '');

		if (this.callback!==null) {
			this.callback();
		}

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
	}

}

class Question {

	constructor(title, str, callback1, callback2 = null) {

		let self = this;

		this.callback1 = callback1;
		this.callback2 = callback2;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<button class="message-accept button">Aceptar</button>' +
					'<button class="message-cancel button">Cancelar</button>' +
					'<button class="message-focus"></button>' +
				'</div>' +
			'</div>';

		this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/question.png');
		this.element.querySelector('.message-container').classList.add('question');

		this.element.querySelector('.message-title').innerHTML = title;
		this.element.querySelector('.message-text').innerHTML = str;

		let thisClass = this;

		this.element.querySelector('.message-accept').addEventListener('click', function(){
			if (thisClass.callback1!==null) {
				thisClass.callback1();
			}
			thisClass.destroy();
		});

		this.element.querySelector('.message-cancel').addEventListener('click', function(){
			if (thisClass.callback2!==null) {
				thisClass.callback2();
			}
			thisClass.destroy();
		});

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);

		this.element.querySelector('.message-focus').focus();

	}

	destroy() {

		this.element.querySelector('.message-accept').setAttribute('disabled', '');
		this.element.querySelector('.message-cancel').setAttribute('disabled', '');

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
		
	}

}

class OpenQuestion {

	constructor(title, str, def, callback1, callback2 = null) {

		this.callback1 = callback1;
		this.callback2 = callback2;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<input type="text" class="message-answer"><br>' +
					'<button class="message-accept button">Aceptar</button>' +
					'<button class="message-cancel button">Cancelar</button>' +
					'<button class="message-focus"></button>' +
				'</div>' +
			'</div>';

		this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/question.png');
		this.element.querySelector('.message-container').classList.add('question');

		this.element.querySelector('.message-title').innerHTML = title;
		this.element.querySelector('.message-text').innerHTML = str;
		this.element.querySelector('.message-answer').value = def;

		let thisClass = this;

		this.element.querySelector('.message-accept').addEventListener('click', function() {
			if (thisClass.callback1!==null) {
				let answer = thisClass.element.querySelector('.message-answer').value;
				thisClass.callback1(answer);
			}
			thisClass.destroy();
		});

		this.element.querySelector('.message-cancel').addEventListener('click', function() {
			if (thisClass.callback2 != null) {
				thisClass.callback2();
			}
			thisClass.destroy();
		});

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);

		this.element.querySelector('.message-focus').focus();

	}	

	destroy() {

		this.element.querySelector('.message-accept').setAttribute('disabled', '');
		this.element.querySelector('.message-cancel').setAttribute('disabled', '');

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
		
	}

}

class Confirm {

	constructor(title, word, func) {

		this.callback = func;

		this.deleteWord = word;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<input type="text" name="confirm-word"><p class="error-alert"></p><br>' +
					'<button class="message-accept button">Aceptar</button>' +
					'<button class="message-cancel button">Cancelar</button>' +
					'<button class="message-focus"></button>' +
				'</div>' +
			'</div>';

		this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/question.png');
		this.element.querySelector('.message-container').classList.add('question');

		this.element.querySelector('.message-title').innerHTML = title;
		this.element.querySelector('.message-text').innerHTML = 'Escriba <span class="highlight">' + word + '</span> para confirmar.';

		let thisClass = this;

		this.element.querySelector('.message-accept').addEventListener('click', function(){
			thisClass.accept();
		});

		this.element.querySelector('.message-cancel').addEventListener('click', function(){
			thisClass.destroy();
		});

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);	

		this.element.querySelector('.message-focus').focus();

	}

	accept() {

		if (this.element.querySelector('[name="confirm-word"]').value == this.deleteWord) {
			if (this.callback!==null) {
				this.callback();
			}
			this.destroy();
		}
		else {
			this.element.querySelector('[name="confirm-word"]').setAttribute('invalid', '');
			this.element.querySelector('.error-alert').innerHTML = 'La palabra escrita es incorrecta';
		}
	}

	destroy() {

		this.element.querySelector('.message-accept').setAttribute('disabled', '');
		this.element.querySelector('.message-cancel').setAttribute('disabled', '');

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
	}

}

class Wait {

	constructor(str, bar = false, func = null) {

		this.progress = 0;

		this.callback = func;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<button class="message-focus"></button>' +
					'<div class="message-progress">' +
						'<div class="message-progress-bar"></div>' +
					'</div>' +
				'</div>' +
			'</div>';

		if (!bar) {
			this.element.querySelector('.message-progress').style.display = 'none';
		}

		this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/wait.png');
		this.element.querySelector('.message-title').innerHTML = 'Por favor espere.';
		this.element.querySelector('.message-container').classList.add('wait');

		this.element.querySelector('.message-text').innerHTML = str;

		let thisClass = this;

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);

		this.element.querySelector('.message-focus').focus();

	}

	setProgress(percent) {
		this.progress = Math.max(Math.min(percent, 100), 0);
		this.element.querySelector('.message-progress > .message-progress-bar').style.width = this.progress + '%';
	}

	getProgress(percent) {
		return this.progress;
	}

	destroy() {

		if (this.callback!==null) {
			this.callback();
		}

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
	}

}

class OptionsQuestion {

	constructor(title, str, options, callback1, callback2 = null) {

		let self = this;

		this.options = options;
		this.callback1 = callback1;
		this.callback2 = callback2;

		this.element = document.createElement('div');
		this.element.className = 'message-overlay';
		this.element.innerHTML =
			'<div class="message-container">' +
				'<img class="message-image">' +
				'<div class="message-content">' +
					'<h3 class="message-title"></h3>' +
					'<p class="message-text"></p><br>' +
					'<select class="message-options"></select>' +
					'<button class="message-accept button">Aceptar</button>' +
					'<button class="message-cancel button">Cancelar</button>' +
					'<button class="message-focus"></button>' +
				'</div>' +
			'</div>';

		this.element.querySelector('.message-image').setAttribute('src','imgs/icons/message/question.png');
		this.element.querySelector('.message-container').classList.add('question');

		this.element.querySelector('.message-title').innerHTML = title;
		this.element.querySelector('.message-text').innerHTML = str;

		let thisClass = this;

		let optionsFragment = document.createDocumentFragment();
		for (let i=0; i<this.options.length; i++) {
			let option = document.createElement('option');
			option.textContent = this.options[i]['text'];
			option.value = this.options[i]['value'];
			optionsFragment.appendChild(option);
		}
		this.element.querySelector('.message-options').appendChild(optionsFragment);

		this.element.querySelector('.message-accept').addEventListener('click', function(){
			let option = thisClass.element.querySelector('.message-options').value;
			if (thisClass.callback1!==null) {
				thisClass.callback1(option);
			}
			thisClass.destroy();
		});

		this.element.querySelector('.message-cancel').addEventListener('click', function(){
			if (thisClass.callback2!==null) {
				thisClass.callback2();
			}
			thisClass.destroy();
		});

		this.element.style.animation = 'appear 0.2s ease-in-out forwards';

		document.querySelector('body').appendChild(this.element);

		this.element.querySelector('.message-focus').focus();

	}

	destroy() {

		this.element.querySelector('.message-accept').setAttribute('disabled', '');
		this.element.querySelector('.message-cancel').setAttribute('disabled', '');

		this.element.style.animation = 'disappear 0.2s ease-in-out forwards';

		this.element.addEventListener('animationend', function(){
			this.parentElement.removeChild(this);
		});
		
	}

}