
var ToolTip = new class {

	constructor() {

		let self = this;
		
		this.target = null;

		this.element = document.createElement('div');
		this.element.className = 'tooltip-container';
		this.element.innerHTML = 
			'<div class="tooltip-shape">' + 
				'<div class="rectangle"></div>' +
				'<div class="triangle"></div>' +
			'</div>' +
			'<p class="hint"></p>';

		document.body.appendChild(this.element);

		this.update();

		this.element.addEventListener('animationend', function(ev) {
			if (ev.animationName == 'disappear') {
				self.element.style.display = 'none';
				self.element.style.animaton = 'none';
			}
		});

		window.addEventListener('scroll', function(ev) {
			self.followTarget(false);
		});

	}

	update() {
		let self = this;
		let tooltips = document.querySelectorAll('[tooltip-hint]');
		for (let i=0; i<tooltips.length; i++) {
			tooltips[i].setAttribute('tooltip-ready-hint', tooltips[i].getAttribute('tooltip-hint'));
			tooltips[i].removeAttribute('tooltip-hint');
			tooltips[i].addEventListener('mouseenter', function(ev) {
				
				self.target = this;
				self.followTarget();
				
				this.addEventListener('mouseleave', function mouseLeave() {
					self.target = null;
					self.element.style.animation = 'disappear 0.2s ease-in-out forwards';
					this.removeEventListener('mouseleave', mouseLeave);
				});
			});
		}
	}

	followTarget(restartAnim = true) {
		if (this.target != null) {
			let hint = this.target.getAttribute('tooltip-ready-hint');
			let scrollTop = document.documentElement.scrollTop;
			let elemRect = this.target.getBoundingClientRect();
			let elemWidth = elemRect.right - elemRect.left;
			let elemHeight = elemRect.bottom - elemRect.top;

			let posVertical = 'top';
			let posHorizontal = 'center';

			this.element.className = 'tooltip-container';
			
			this.element.querySelector('.hint').innerHTML = hint;
			
			this.element.style.left = (elemRect.left + (elemWidth / 2)) + 'px';
			this.element.style.top = (elemRect.bottom + scrollTop) + 'px';

			if (restartAnim) {
				this.element.offsetHeight;
				this.element.style.display = 'block';
				this.element.style.animation = 'none';
				this.element.offsetHeight;
				this.element.style.animation = 'appear 0.2s ease-in-out forwards';
			}

			if (this.element.getBoundingClientRect().left<=0) {
				let posHorizontal = 'left';
			}

			if (this.element.getBoundingClientRect().right>=window.innerWidth) {
				let posHorizontal = 'right';
			}

			if (this.element.getBoundingClientRect().bottom>=window.innerHeight) {
				posVertical = 'bottom';
			}

			this.element.classList.add(posVertical + '-' + posHorizontal);

			if (posVertical == 'bottom') {
				this.element.style.top = (elemRect.top + scrollTop) + 'px';
			}
		}
	}
}