
var SortController = new class {
    constructor() {

        let self = this;

        this.onMethodChange = null;
        this.onSearchChange = null;
        this.onFilterChange = null;

        this.search = '';
        this.method = '';
        this.order = 'desc';
        this.url = window.location.pathname + '/';
        this.filters = [];

        this.element = document.querySelector('.sortcontroller-container');

        this.element.querySelector('.method-selected').addEventListener('click', function() {
            let options = self.element.querySelector('.method-options');
            let isOpen = options.classList.contains('open');
            if (isOpen) {
                options.classList.remove('open');
                document.body.removeEventListener('click', evCloseMethodOptions);
            }
            else {
                options.classList.add('open');
                document.body.addEventListener('click', evCloseMethodOptions);
            }
        });

        this.element.querySelectorAll('.order-container > *').forEach(function(elem, index) {
            elem.addEventListener('click', function(ev) {
                if (!this.classList.contains('selected')) {
                    let value = this.getAttribute('value');
                    self.setOrder(value);
                    if (SortController.onMethodChange !== null) {
                        SortController.onMethodChange();
                        self.save();
                    }
                }
            });
        });

        this.element.querySelector('.order-container > *').classList.add('selected');
        this.element.querySelector('.search').value = '';

        this.element.querySelector('.search').addEventListener('input', function(ev) {
            self.search = this.value;
            if (self.onMethodChange != null) {
                self.onMethodChange();
            }
        });

        this.element.querySelector('.filter-container > img').addEventListener('click', this.openFilters);

        document.querySelector('.sortcontroller-filter-container .filter-close').addEventListener('click', function() {
            self.closeFilters();
        });

        document.querySelector('.sortcontroller-filter-overlay').addEventListener('animationend', function(ev) {
            if (ev.animationName == 'disappear') {
                this.style.display = '';
            }
        });

        function evCloseMethodOptions(ev) {
            let target = ev.target;
            if (!self.element.querySelector('.method-selector').contains(target)) {
                self.element.querySelector('.method-options').classList.remove('open');
                document.body.removeEventListener('click', evCloseMethodOptions);
            }
        }
    }

    closeFilters() {
        let filterOverlay = document.querySelector('.sortcontroller-filter-overlay');
        filterOverlay.style.animation = '';
        filterOverlay.offsetHeight;
        filterOverlay.style.animation = 'disappear 0.2s ease-in-out forwards';
    }

    openFilters() {
        let filterOverlay = document.querySelector('.sortcontroller-filter-overlay');
        filterOverlay.style.display = 'flex';
        filterOverlay.style.animation = '';
        filterOverlay.offsetHeight;
        filterOverlay.style.animation = 'appear 0.2s ease-in-out forwards';
    }

    updateFiltersValues() {
        let self = this;
        this.filters = {};
        document.querySelectorAll('.sortcontroller-filter-container input, .sortcontroller-filter-container select').forEach(function(elem, index) {            
            if (elem.hasAttribute('name') && elem.getAttribute('name') != '') {
                if (elem.getAttribute('type') == 'checkbox') {
                    let name = elem.getAttribute('name');
                    self.filters[name] = elem.checked ? 1 : 0;
                }
                else {
                    let name = elem.getAttribute('name');
                    self.filters[name] = elem.value;
                }
            }
        });
    }

    loadFiltersValues() {
        let getQueriesStr = window.location.search;
        if (getQueriesStr != '') {
            let getQueriesArray = getQueriesStr.replace('?', '').split('&');
            for (let i=0; i<getQueriesArray.length; i++) {
                let getQuery = getQueriesArray[i].split('=');
                let inputElem = document.querySelector('.sortcontroller-filter-container [name="' + decodeURIComponent(getQuery[0]) + '"]');
                if (inputElem) {
                    inputElem.value = decodeURIComponent(getQuery[1]);
                }
            }
        }
    }

    save() {
        localStorage.setItem(this.url + 'sortMethod', this.method);
        localStorage.setItem(this.url + 'sortOrder', this.order);
    }

    load() {
        let method = localStorage.getItem(this.url + 'sortMethod') || this.method;
        let order = localStorage.getItem(this.url + 'sortOrder') || this.order;

        if (method == '') {
            let methodElem = this.element.querySelector('.method-options > .method-option');
            method = methodElem.getAttribute('value');
        }

        this.setMethod(method);
        this.setOrder(order);
        this.loadFiltersValues();
        this.updateFiltersValues();

        if (this.onMethodChange !== null) {
            this.onMethodChange();
        }
    }

    setOrder(value) {
        this.order = value;
        let elem = this.element.querySelector('.order-container > *[value="' + value + '"]');
        this.element.querySelectorAll('.order-container > *').forEach(function(elem, index) {
            elem.classList.remove('selected');
        });
        elem.classList.add('selected');
    }

    setMethod(value) {
        let option = this.element.querySelector('.method-options > .method-option[value="' + value + '"]');
        if (!option) {
            option = this.element.querySelector('.method-options > .method-option');
        }
        let realValue = option.getAttribute('value');
        this.method = realValue;
        let name = option.querySelector('p').innerHTML;
        let elemSelected = this.element.querySelector('.method-selected');
        elemSelected.setAttribute('value', realValue);
        elemSelected.querySelector('p').innerHTML = name;
    }

    setMethods(methods) {
        let self = this;
        for (let i=0; i<methods.length; i++) {
            let elem = document.createElement('div');
            elem.className = 'method-option';
            elem.setAttribute('value', methods[i][1]);
            elem.innerHTML = '<p value="' + methods[i][1] + '">' + methods[i][0] + '</p>';
            self.element.querySelector('.method-options').appendChild(elem);
            elem.addEventListener('click', evMethodOptionClick);
        }
        function evMethodOptionClick(ev) {
            let elem = this.querySelector('p');
            let value = elem.getAttribute('value');
            self.element.querySelector('.method-options').classList.remove('open');
            if (self.value != value) {
                self.setMethod(value);
                if (self.onMethodChange !== null) {
                    self.onMethodChange();
                    self.save();
                }
            }
        }
    }

    setOnMethodChange(func) {
        this.onMethodChange = func;
    }

    setOnSearchChange(func) {
        this.onSearchChange = func;
    }

    setFilterOptions(filterObj) {
        let self = this;
        let filterFrag = document.createDocumentFragment();
        filterObj.forEach(function(filterGroup, index) {
            let inputsContainers = document.createElement('div');
            inputsContainers.className = 'inputs-containers';
            filterGroup.forEach(function(filter) {
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input-container';
                let description = filter[0]
                let tag = filter[1];
                let name = filter[2];
                let paragraphElem = document.createElement('p');
                paragraphElem.textContent = description;
                inputContainer.appendChild(paragraphElem);
                switch (tag) {
                    case 'input':
                        let type = filter[3] || 'text';
                        let inpElem = document.createElement('input');
                        inpElem.setAttribute('name', name);
                        inpElem.setAttribute('type', type);
                        inputContainer.appendChild(inpElem);
                        break;
                    case 'select':
                        let options = filter[3];
                        let selectElem = document.createElement('select');
                        selectElem.setAttribute('name', name);
                        if (options) {
                            for (let i=0; i<options.length; i++) {
                                let option = document.createElement('option');
                                option.textContent = options[i][0];
                                option.setAttribute('value', options[i][1]);
                                selectElem.appendChild(option);
                            }
                        }
                        inputContainer.appendChild(selectElem);
                        break;
                }
                inputsContainers.appendChild(inputContainer);
            });
            filterFrag.appendChild(inputsContainers);
        });
        document.querySelector('.sortcontroller-filter-container .filters-containers').appendChild(filterFrag);
        document.querySelector('.sortcontroller-filter-container .filters-containers').querySelectorAll('input, select').forEach(function(elem, index) {
            if (elem.hasAttribute('name') && elem.getAttribute('name') != '') {
                elem.addEventListener('change', function() {
                    setTimeout(function(){
                        self.updateFiltersValues();
                        if (self.onMethodChange) {
                            self.onMethodChange();
                        }
                    }, 1);
                });
            }
        });

        this.updateFiltersValues();

        this.element.querySelector('.filter-container').style.display = 'flex';
    }
}