class SelectInp {

    constructor(input, options = {}) {
        let self = this;

        this.searchInput = input;
        this.value = '';
        this.defaultValue = '';

        this.selectOptions = [];

        this.selectFirst = true;
        this.maxResults = options['maxResults'] || 100;
        this.onChange = null;
        this.openEmpty = true;
        this.selectOnFocus = true;
        this.autocompleter = false;
        this.selectOnHover = true;
        this.clearSearcher = true;

        this.container = document.createElement('div');
        this.container.className = 'selectinp-container';

        this.ghostInput = document.createElement('input');
        this.ghostInput.className = 'selectinp-ghost';
        this.ghostInput.setAttribute('name', this.searchInput.getAttribute('name'));
        this.searchInput.setAttribute('name', '');

        this.selector = document.createElement('div');
        this.selector.className = 'selectinp-selector-container';

        this.searchInput.before(this.container);
        this.container.appendChild(this.searchInput);
        this.container.appendChild(this.selector);
        this.container.appendChild(this.ghostInput);

        this.searchInput.addEventListener('focus', function(ev) {
            if (self.openEmpty) {
                self.open();
            }
            if (self.selectOnFocus) {
                this.select();
            }
        });

        this.searchInput.addEventListener('blur', function(ev) {
            let key = this.value;
            setTimeout(function(){
                self.container.classList.remove('open');
            }, 1);
            if (!self.autocompleter) {
                if (key != '') {
                    let value = self.ghostInput.value;
                    self.setValue(value);
                }
                else {
                    self.ghostInput.value = self.defaultValue;
                    self.searchInput.value = '';
                    self.searchInput.dispatchEvent(new Event('change'));
                }
            }
            else {
                self.setValue(key);
            }
        });

        this.searchInput.addEventListener('input', function(ev) {
            let key = this.value;
            let selectedOption = self.container.querySelector('.selectinp-option.selected');
            if (selectedOption) {
                selectedOption.classList.remove('selected');
            }
            if (key != '' || self.openEmpty) {
                self.open();
            }
            else {
                self.container.classList.remove('open');
            }
            self.search(key);
            self.scrollToSelected();
        });

        this.searchInput.addEventListener('keydown', function(ev) {
            let value = this.value;
            if (self.openEmpty || value != '') {
                let key = ev.key.toLowerCase();
                switch (key) {
                    case 'arrowup': {
                        ev.preventDefault();
                        let selectedOption = self.container.querySelector('.selectinp-option.selected');
                        if (selectedOption) {
                            let previousSelectedOption = selectedOption.previousElementSibling;
                            while (previousSelectedOption && previousSelectedOption.classList.contains('hidden')) {
                                previousSelectedOption = previousSelectedOption.previousElementSibling;
                            }
                            if (previousSelectedOption) {
                                selectedOption.classList.remove('selected');
                                previousSelectedOption.classList.add('selected');
                            }
                        }
                        self.scrollToSelected();
                        break;
                    }
                    case 'arrowdown': {
                        ev.preventDefault();
                        let selectedOption = self.container.querySelector('.selectinp-option.selected');
                        if (selectedOption) {
                            let nextSelectedOption = selectedOption.nextElementSibling;
                            while (nextSelectedOption && nextSelectedOption.classList.contains('hidden')) {
                                nextSelectedOption = nextSelectedOption.nextElementSibling;
                            }
                            if (nextSelectedOption) {
                                selectedOption.classList.remove('selected');
                                nextSelectedOption.classList.add('selected');
                            }
                        }
                        else {
                            selectedOption = self.container.querySelector('.selectinp-option');
                            while (selectedOption && selectedOption.classList.contains('hidden')) {
                                selectedOption = selectedOption.nextElementSibling;
                            }
                            if (selectedOption) {
                                selectedOption.classList.add('selected');
                            }
                        }
                        self.scrollToSelected();
                        break;
                    }
                    case 'enter': {
                        let selectedOption = self.container.querySelector('.selectinp-option.selected');
                        if (selectedOption) {
                            selectedOption.dispatchEvent(new Event('mousedown'));
                            self.searchInput.blur();
                        }
                    }
                }
            }
        });
        
        this.ghostInput.addEventListener('change', function(ev) {
            let value = this.value;
            self.setValue(value);
            if (self.onChange) {
                self.onChange();
            }
        });
        
        if (options['defaultValue'] != undefined) {
            this.defaultValue = options['defaultValue'];
            this.value = this.defaultValue;
            this.ghostInput.value = this.defaultValue;
        }
        if (options['placeholder'] != undefined) {            
            this.searchInput.setAttribute('placeholder', options['placeholder']);
        }
        if (options['openEmpty'] != undefined) {            
            this.openEmpty = options['openEmpty'];
        }
        if (options['selectFirst'] != undefined) {            
            this.selectFirst = options['selectFirst'];
        }
        if (options['selectOnFocus'] != undefined) {            
            this.selectOnFocus = options['selectOnFocus'];
        }
        if (options['selectOnHover'] != undefined) {            
            this.selectOnHover = options['selectOnHover'];
        }
        if (options['autocompleter'] != undefined) {
            this.autocompleter = options['autocompleter'];
            if (this.autocompleter) {
                this.searchInput.setAttribute('name', this.ghostInput.getAttribute('name'));
                this.ghostInput.removeAttribute('name');
            }
        }
        if (options['clearSearcher'] != undefined) {            
            this.clearSearcher = options['clearSearcher'];
        }
    }

    open() {
        if (!this.container.classList.contains('open')) {
            this.container.classList.add('open');
            let selectedOption = this.container.querySelector('.selectinp-option.selected');
            if (selectedOption) {
                selectedOption.classList.remove('selected');
            }
            this.selector.scrollTop = 0;
            let key = this.searchInput.value;
            let pageY = window.pageYOffset;
            let windowHeight = window.innerHeight;
            let selectorRect = this.selector.getBoundingClientRect();
            let maxHeight = windowHeight - selectorRect.top - 32;
            this.selector.style.maxHeight = maxHeight + 'px';
            if (this.openEmpty) {
                this.search(key);
            }
        }
    }

    scrollToSelected() {
        let selectedOption = this.container.querySelector('.selectinp-option.selected');
        if (selectedOption) {
            let scrollTop = this.selector.scrollTop;
            let selectorHeight = this.selector.offsetHeight;
            let selectedOptionPosition = {top: selectedOption.offsetTop, bottom: selectedOption.offsetTop + selectedOption.offsetHeight};
            if (selectedOptionPosition.bottom > scrollTop + selectorHeight) {
                this.selector.scrollTop = selectedOptionPosition.bottom - selectorHeight;
            }
            else if (selectedOptionPosition.top < scrollTop) {
                this.selector.scrollTop = selectedOptionPosition.top;
            }
        }
    }

    setValue(value) {
        let option = this.selector.querySelector('.selectinp-option[value="' + value + '"]');
        let previousValue = this.ghostInput.value;
        this.searchInput.removeAttribute('notfound');
        if (!this.autocompleter) {
            if (option != null) {
                let elem = option;
                this.ghostInput.value = value;
                this.searchInput.value = elem.querySelector('.name').textContent;
                this.value = value;
            }
            else {
                this.ghostInput.value = this.defaultValue;
                if (this.clearSearcher) {
                    this.searchInput.value = '';
                }
                else {
                    this.searchInput.setAttribute('notfound', '');
                }
                this.value = this.defaultValue;
                this.searchInput.dispatchEvent(new Event('change'));
            }
        }
        else if (option) {
            let name = option.innerHTML;
            this.ghostInput.value = value;
            this.searchInput.value = value;
            this.value = value;
        }
        if (previousValue != this.ghostInput.value) {
            this.searchInput.dispatchEvent(new Event('change'));
        }
    }

    clear() {
        this.ghostInput.value = this.defaultValue;
        this.searchInput.value = '';
        this.value = this.defaultValue;
    }

    setOptions(options) {
        this.selectOptions = options;        
        this.loadOptions(options);
        this.search(this.searchInput.value); //NECESARIO PARA CONTINUAR CON LA BUSQUEDA AL ACTUALIZAR
    }

    addOption(option) {
        this.selectOptions.push(option);
        this.loadOptions(this.selectOptions);
        this.search(this.searchInput.value);
    }

    loadOptions(options) {
        let self = this;
        let optionsFrag = document.createDocumentFragment();
        for (let i=0; i<options.length; i++) {
            let name = options[i]['name'];
            let value = options[i]['value'];
            let suffix = options[i]['suffix'] || '';
            let suffixFn = options[i]['suffixFn'] || null;
            let option = document.createElement('div');
            if (suffixFn) {
                suffix = suffixFn(options[i], option);
            }
            option.className = 'selectinp-option';
            option.setAttribute('value', value);
            option.innerHTML = '<div class="name">' + name + '</div>' + (suffix != '' ? ('<div class="suffix-container">' + suffix + '</div>') : '');
            optionsFrag.appendChild(option);
            if (options[i]['data']) {
                let optionData = options[i]['data'];
                for (let key in optionData) {
                    option.dataset[key] = optionData[key];
                }
            }
            if (options[i]['color']) {
                option.style.backgroundColor = options[i]['color'];
            }
            option.addEventListener('mousedown', function(ev) {
                let value = this.getAttribute('value');
                self.setValue(value);
                if (self.onChange) {
                    self.onChange();
                }
            });
            if (this.selectOnHover) {
                option.addEventListener('mouseover', function(ev) {
                    let selected = self.container.querySelector('.selectinp-option.selected');
                    if (selected) {
                        selected.classList.remove('selected');
                    }
                    this.classList.add('selected');
                });
            }
        }
        this.selector.appendChild(optionsFrag);
        if (this.selectFirst) {
            let firstElem = this.selector.querySelector('.selectinp-option');
            if (firstElem) {
                firstElem.classList.add('selected');
            }
        }
    }

    search(skey) {
        if (!this.autocompleter) {
            skey = this._removeAccents(skey.trim().toLowerCase());
            this.selector.innerHTML = '';
            let results = [];

            for (let i=0; i<this.selectOptions.length; i++) {
                let keys = this.selectOptions[i]['keys'].slice();
                for (let k=0; k<keys.length; k++) {
                    let name = this._removeAccents(keys[k].trim().toLowerCase());
                    if (this.openEmpty && skey == '') {                        
                        results.push(this.selectOptions[i]);
                        break;
                    }
                    else if (!isNaN(skey) && name == skey) {
                        results.push(this.selectOptions[i]);
                        break;
                    }
                    else if (name.indexOf(skey) != -1) {
                        results.push(this.selectOptions[i]);
                        break;
                    }
                }
                if (results.length >= this.maxResults) {
                    break;
                }
            }

            this.loadOptions(results);
        }
    }

    _removeAccents(str) {
        let strArr = str.split('');
        let accents = 'áéíóú';
        let accentsOut = 'aeiou';
        for (let i=0; i<str.length; i++) {
            let accentsIdx = accents.indexOf(strArr[i]);
            if (accentsIdx != -1) {
                strArr[i] = accentsOut[accentsIdx];
            }
        }
        return strArr.join('');
    }

    _stringScore(str, search, fuzziness) {
        let start = 0;
        let scoreWord = 0;
        let scoreTotal = 0;

        let word = '';

        for (let i=0; i<search.length; i++) {
            let idx = str.indexOf(word + search[i]);
            word += search[i];
            if (idx == -1) {
                break;
            }
            else if (idx > 0 && str[idx - 1] == ' ') {
                start = idx;
            }
        }

        for (let i=0; i<search.length; i++) {
            let idx = str.indexOf(search[i], start);
            if (idx == -1) {
                return 0;
            }
            if (start == idx) {
                if (start != 0) {
                    scoreWord = 0.5;
                }
                else {
                    scoreWord = 0.7;
                }
            }
            else {
                scoreWord = 0.1 / (idx - start);
            }
            scoreTotal += scoreWord;
            start = idx + 1;
        }
        scoreTotal = 0.5 * ((scoreTotal / str.length) + (scoreTotal / search.length)) / fuzziness;
        return scoreTotal || 0;
    }

}