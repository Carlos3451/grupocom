
class Graph {
    constructor(container, options) {
        
        this.container = container;
        this.valuesNumber = options['valuesNumber'] || 5;
        this.valuePrefix = options['valuePrefix'] || '';
        this.conceptsNames = options['conceptsNames'] || [];
        this.conceptsValues = options['conceptsValues'] || [];
        this.barsWidth = options['barsWidth'] || '50%';
        this.barsColors = options['barsColors'] || 'blue';
        this.descriptions = options['descriptions'] || null;

        this.graphContainer = document.createElement('div');
        this.graphContainer.className = 'graph-container';
        this.graphContainer.innerHTML =
            '<div class="graph-values"></div>'+
            '<div class="graph-concepts"></div>'+
            '<div class="graph-lines"></div>'+
            '<div class="graph-bars"></div>'+
            '<div class="graph-descriptions"></div>';
        this.container.appendChild(this.graphContainer);
        
        this.setConceptsNames(this.conceptsNames);
        this.setConceptsValues(this.conceptsValues);
        
        if (this.descriptions) {
            if (this.descriptions instanceof Array) {
                for (let i=0; i<this.descriptions.length; i++) {                
                    let graphDescription = document.createElement('div');
                    graphDescription.className = 'graph-description';
                    graphDescription.innerHTML = '<div class="description-color"></div><p>' + this.descriptions[i] + '</p>';
                    graphDescription.querySelector('.description-color').style.backgroundColor = this.barsColors instanceof Array ? this.barsColors[i] : this.barsColors;
                    this.container.querySelector('.graph-descriptions').append(graphDescription);
                }
            }
            else {
                let graphDescription = document.createElement('div');
                graphDescription.className = 'graph-description';
                graphDescription.innerHTML = '<div class="description-color"></div><p>' + this.descriptions + '</p>';
                graphDescription.querySelector('.description-color').style.backgroundColor = this.barsColors.length ? this.barsColors[0] : this.barsColors;
                this.container.querySelector('.graph-descriptions').append(graphDescription);
            }
        }

    }

    setConceptsNames(conceptsNames) {
        this.conceptsNames = conceptsNames;
        this.container.querySelector('.graph-concepts').innerHTML = '';
        for (let i=0; i<this.conceptsNames.length; i++) {
            let graphConceptOutside = document.createElement('div');
            graphConceptOutside.className = 'graph-concept-outside';
            graphConceptOutside.innerHTML = '<p>' + this.conceptsNames[i] + '</p>';
            this.container.querySelector('.graph-concepts').append(graphConceptOutside);
        }
    }

    setConceptsValues(conceptsValues) {
        this.conceptsValues = conceptsValues;
        this.container.querySelector('.graph-bars').innerHTML = '';
        this.container.querySelector('.graph-values').innerHTML = '';
        this.container.querySelector('.graph-lines').innerHTML = '';
        let conceptsValueMax = 0;
        for (let i=0; i<this.conceptsValues.length; i++) {
            if (this.conceptsValues[i] instanceof Array) {
                for (let b=0; b<this.conceptsValues[i].length; b++) {
                    conceptsValueMax = Math.max(conceptsValueMax, this.conceptsValues[i][b]);
                }
            }
            else {                
                conceptsValueMax = Math.max(conceptsValueMax, this.conceptsValues[i] || 0);
            }
        }
        this.valueGrow = conceptsValueMax / this.valuesNumber;
        if (this.valueGrow == 0) {
            this.valueGrow = 100 / this.valuesNumber;
        }
        this.valueGrow = Math.ceil(this.valueGrow / Math.pow(5, this.valueGrow.toFixed(0).length)) * Math.pow(5, this.valueGrow.toFixed(0).length);
        this.valuesMax = this.valueGrow * this.valuesNumber;
        for (let i=0; i<this.conceptsNames.length; i++) {
            let graphBarOutside = document.createElement('div');
            graphBarOutside.className = 'graph-bar-outside';
            if (this.conceptsValues[i] instanceof Array) {
                for (let b=0; b<this.conceptsValues[i].length; b++) {
                    let value = this.conceptsValues[i][b];
                    let graphBar = document.createElement('div');
                    let valueText = document.createElement('p');
                    valueText.textContent = this.valuePrefix + value;
                    valueText.style.color = this.barsColors instanceof Array ? this.barsColors[b] : this.barsColors;
                    graphBar.className = 'graph-bar';
                    graphBar.style.height = ((value / this.valuesMax) * 100) + '%';
                    graphBar.style.width = this.barsWidth;
                    graphBar.style.backgroundColor = this.barsColors instanceof Array ? this.barsColors[b] : this.barsColors;
                    graphBar.appendChild(valueText);
                    graphBarOutside.appendChild(graphBar);
                }
            }
            else {
                let value = this.conceptsValues[i] || 0;
                let graphBar = document.createElement('div');
                let valueText = document.createElement('p');
                valueText.textContent = this.valuePrefix + value;
                valueText.style.color = this.barsColors instanceof Array ? this.barsColors[0] : this.barsColors;
                graphBar.className = 'graph-bar';
                graphBar.style.height = ((value / this.valuesMax) * 100) + '%';
                graphBar.style.width = this.barsWidth;
                graphBar.style.backgroundColor = this.barsColors.length ? this.barsColors[0] : this.barsColors;
                graphBar.appendChild(valueText);
                graphBarOutside.appendChild(graphBar);
            }
            this.container.querySelector('.graph-bars').append(graphBarOutside);
        }
        for (let i=0; i<this.valuesNumber; i++) {
            let value = this.valueGrow * (this.valuesNumber - i);
            let graphValueOutside = document.createElement('div');
            graphValueOutside.className = 'graph-value-outside';
            graphValueOutside.innerHTML = '<p>' + this.valuePrefix + value + '</p>';
            let graphLineOutside = document.createElement('div');
            graphLineOutside.className = 'graph-line-outside';
            graphLineOutside.innerHTML = '<div class="graph-lien"></div>';
            this.container.querySelector('.graph-values').append(graphValueOutside);
            this.container.querySelector('.graph-lines').append(graphLineOutside);
        }
    }

}