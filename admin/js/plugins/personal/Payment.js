var Payment = new class {
    constructor() {
        let self = this;
        this.overlayElem = document.querySelector('.payment-container-overlay');
        this.quantityElem = this.overlayElem.querySelector('.payment-quantity');
        this.acceptElem = this.overlayElem.querySelector('.payment-accept');
        this.methodsContainer = this.overlayElem.querySelector('.methods-container');
        this.onAccept = null;
        this.quantityElem.addEventListener('keypress', function(ev) {
            let value = this.value;
            if (!Tools.checkNumeric(ev, value)) {
                ev.preventDefault();
            }
        });
        this.quantityElem.addEventListener('keydown', function(ev) {
            let value = this.value;
            let key = ev.key.toLowerCase();
            switch (key) {
                case 'e':
                    self.methodsContainer.querySelector('[value="efectivo"]').click();
                    break;
                case 'c':
                    self.methodsContainer.querySelector('[value="cheque"]').click();
                    break;
                case 't':
                    self.methodsContainer.querySelector('[value="tarjeta"]').click();
                    break;
                case 'd':
                    self.methodsContainer.querySelector('[value="transferencia"]').click();
                    break;
                case 'enter':
                    self.acceptElem.click();
                    break;
            }
            if (!Tools.checkNumeric(ev, value)) {
                ev.preventDefault();
            }
        });
        this.quantityElem.addEventListener('focus', function(ev) {
            this.select();
        });
        this.quantityElem.addEventListener('blur', function(ev) {
            this.value = (parseFloat(this.value) || 0).toFixed(2);
        });
        this.methodsContainer.querySelectorAll('img').forEach(function(elem, index) {
            elem.addEventListener('click', function() {
                Payment.selectMethod(index);
            });
        });
        this.overlayElem.querySelector('.payment-accept').addEventListener('click', function() {
            this.setAttribute('disabled', '');
            let method = Payment.getMethod();
            let quantity = Payment.getQuantity();
            if (parseFloat(quantity) > 0) {
                if (Payment.onAccept) {
                    Payment.onAccept(method, quantity);
                }
                Payment.close();
            }
            else if (parseFloat(quantity) == 0) {
                new Message('La cantidad de pago esta vacia.', 'warning');
                this.removeAttribute('disabled');
            }
            else {
                new Message('La cantidad de pago es negativa.', 'warning');
                this.removeAttribute('disabled');
            }
        });
        this.overlayElem.querySelector('.payment-cancel').addEventListener('click', function() {
            Payment.close();
        });
        this.overlayElem.addEventListener('animationend', function(ev) {
            if (ev.animationName == 'disappear') {
                this.style.display = 'none';
            }
        });
    }
    open(value = 0) {
        this.overlayElem.querySelector('.payment-accept').removeAttribute('disabled');
        this.selectMethod(0);
        this.quantityElem.value = value.toFixed(2);
        this.overlayElem.style.display = 'flex';
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'appear 0.2s ease-in-out forwards';
        this.quantityElem.focus();
        this.quantityElem.select();
    }
    close() {
        this.overlayElem.style.animation = '';
        this.overlayElem.offsetHeight;
        this.overlayElem.style.animation = 'disappear 0.2s ease-in-out forwards';
    }
    selectMethod(index) {
        this.methodsContainer.querySelector('.selected').classList.remove('selected');
        this.methodsContainer.querySelectorAll('img')[index].classList.add('selected');
    }
    getMethod() {
        return this.methodsContainer.querySelector('.selected').getAttribute('value');
    }
    getQuantity() {
        return this.quantityElem.value;
    }
}