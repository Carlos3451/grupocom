
var TogglePassword = new class {
    constructor() {
        let passwordInputs = document.querySelectorAll('input[togglePassword]');
        passwordInputs.forEach(function(elem) {
            let container = document.createElement('div');
            let toggleIcon = document.createElement('img');

            container.className = 'togglePassword-container';
            toggleIcon.className = 'togglePassword-toggle';
            toggleIcon.setAttribute('src', '/admin/imgs/icons/password-hidden.png');
            toggleIcon.addEventListener('click', function() {
                let isShown = container.classList.contains('show');
                toggleIcon.style.animation = 'shrink 0.1s ease-in-out';
                if (isShown) {
                    container.classList.remove('show');
                    toggleIcon.setAttribute('src', '/admin/imgs/icons/password-hidden.png');
                    elem.setAttribute('type', 'password');
                }
                else {
                    container.classList.add('show');
                    toggleIcon.setAttribute('src', '/admin/imgs/icons/password-shown.png');
                    elem.setAttribute('type', 'text');
                }
            });
            toggleIcon.addEventListener('animationend', function() {
                toggleIcon.style.animation = '';
            });
            
            elem.parentElement.insertBefore(container, elem);

            container.appendChild(elem);
            container.appendChild(toggleIcon);
        });
    }
}