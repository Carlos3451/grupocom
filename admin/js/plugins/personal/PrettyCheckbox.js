
class PrettyCheckbox {
    constructor(input, options) {

        let self = this;

        this.onChange = null;

        this.input = input;
        this.container = document.createElement('div');
        this.container.className = 'prettycheckbox-container';

        this.contextMode = options['contextMode'] || 'text';
        this.contextData = options['contextData'] || 'DEF';

        this.container.innerHTML =
            '<div class="prettycheckbox-context"></div>'+
            '<div class="prettycheckbox-checkmark">'+
                '<img src="/admin/imgs/icons/checkmark.png">'+
            '</div>';

        let contextElem;
        
        switch (this.contextMode) {
            case 'text':
                contextElem = document.createElement('p');
                contextElem.textContent = this.contextData;
                break;
            case 'image':
                contextElem = document.createElement('img');
                contextElem.setAttribute('src', this.contextData);
        }
        this.container.querySelector('.prettycheckbox-context').appendChild(contextElem);

        this.container.addEventListener('click', function() {
            self.input.click();
        });

        this.input.addEventListener('change', function() {
            if (self.onChange) {
                self.onChange();
            }
            self.checkInputChecked();
        });

        self.checkInputChecked();

        this.input.before(this.container);
        this.container.appendChild(this.input);
    }
    checkInputChecked() {
        let checked = this.input.checked;
        if (checked) {
            this.container.classList.add('checked');
        }
        else {
            this.container.classList.remove('checked');
        }
    }
}