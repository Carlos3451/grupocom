var Tools = new class {    

ajaxRequest(ajaxObj) {
    ajaxObj['data'] = ajaxObj['data'] != undefined ? ajaxObj['data'] : {};
    ajaxObj['method'] = ajaxObj['method'] != undefined ? ajaxObj['method'] : 'GET';
    ajaxObj['url'] = ajaxObj['url'] != undefined ? ajaxObj['url'] : '';
    ajaxObj['success'] = ajaxObj['success'] != undefined ? ajaxObj['success'] : function(){};
    ajaxObj['error'] = ajaxObj['error'] != undefined ? ajaxObj['error'] : function(){};
    ajaxObj['progress'] = ajaxObj['progress'] != undefined ? ajaxObj['progress'] : function(){};
    ajaxObj['contentType'] = ajaxObj['contentType'] != undefined ? ajaxObj['contentType'] : 'application/x-www-form-urlencoded; charset=UTF-8';
    ajaxObj['responseType'] = ajaxObj['responseType'] != undefined ? ajaxObj['responseType'] : 'text';

    let uri = ajaxObj['data'] instanceof FormData ? ajaxObj['data'] : Tools.jsonToURI(ajaxObj['data']);

    let xhttp = new XMLHttpRequest();

    xhttp.responseType = ajaxObj['responseType'];

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                if (ajaxObj['responseType'] == 'text') {
                    ajaxObj['success'](xhttp.responseText);
                }
                else {
                    ajaxObj['success'](xhttp.response);
                }
            }
            else {
                if (ajaxObj['responseType'] == 'text') {
                    ajaxObj['error'](this.readyState, this.status, xhttp.responseText);
                }
                else {
                    ajaxObj['error'](this.readyState, this.status, xhttp.response);
                }
            }
        }
    };

    xhttp.addEventListener('progress', function(ev) {
        if (ev.lengthComputable) {
            let percentComplete = ev.loaded / ev.total;
            ajaxObj['progress'](percentComplete);
        }
    });

    xhttp.open(ajaxObj['method'], ajaxObj['url'], true);

    if (ajaxObj['method'] == 'POST') {
        if (ajaxObj['contentType'] !== false) {
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        }
        xhttp.send(uri);
    }
    else {
        xhttp.send();
    }
    return xhttp;
}

jsonToURI(obj) {
    let uri = Object.keys(obj).map(function(k) {
        if (typeof obj[k] == 'object') {
            if (Object.entries(obj[k]).length == 0) {
                return '';
            }
            let nestObj = {};
            Object.keys(obj[k]).map(function(kk) {
                nestObj[k + '[' + kk + ']'] = obj[k][kk];
            });
            return Tools.jsonToURI(nestObj);
        }
        else {
            return encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]);
        }
    }).filter(function(value, index) {
        return value != '';
    }).join('&');
    return uri;
}

fileToBase64(file, callback) {
    let reader = new FileReader();
    reader.onload = function() {
        let data = reader.result;
        callback(data);
    }
    reader.readAsDataURL(file);
}

checkNumeric(ev, value) {
    let key = ev.key.toLowerCase();
    if (key == ' ') {
        return false;
    }
    if ((!isNaN(key)) || (key == 'tab') || (key == 'arrowleft') || (key == 'enter') || (key == 'arrowright') || (key == 'backspace') || (key == 'delete') || (ev.ctrlKey && key == 'r') || (ev.ctrlKey && key == 'f5') || (ev.ctrlKey && key == 'z') || (ev.ctrlKey && key == 'a')) {
        return true;
    }
    else if (key =='.') {
        if (value.indexOf('.') == -1 || (ev.target.selectionStart == 0 && ev.target.selectionEnd == value.length)) {
            return true;
        }
    }
    return false;
}

getDecimals(number) {
    let numberArray = number.toString().split('.');
    if (numberArray.length == 1) {
        return 0;
    }
    return Math.min(numberArray[1].toString().split('').length, 3);
}

removeChilds(elem) {
    while (elem.firstChild) {
        elem.firstChild.remove();
    }
}

unitConverter(unitStr) {
    let unitObj = {};
    switch (unitStr) {
        case 'gramo':
            unitObj = new Unit('gr', 'gr', 0);
            break;
        case 'kilogramo':
            unitObj = new Unit('kgm', 'kgm', 3);
            break;
        case 'libra':
            unitObj = new Unit('lb', 'lb', 3);
            break;
        case 'paquete':
            unitObj = new Unit('paq', 'paq', 0);
            break;
        case 'caja':
            unitObj = new Unit('caja', 'caja', 0);
            break;
        case 'burbuja':
            unitObj = new Unit('burb', 'burb', 0);
            break;
        case 'costal':
            unitObj = new Unit('cost', 'cost', 0);
            break;
        case 'maso':
            unitObj = new Unit('maso', 'maso', 0);
            break;
        case 'pieza':
            unitObj = new Unit('pz', 'pz', 0);
            break;
        case 'mililitro':
            unitObj = new Unit('mL', 'mL', 0);
            break;
        case 'litro':
            unitObj = new Unit('L', 'L', 3);
            break;
        case 'unidad':
            unitObj = new Unit('ud', 'ud', 3);
            break;
        default:
            unitObj = new Unit('s/d', 's/d', 0);
            break;
    }
    function Unit(singular, plural, zeros) {
        return {singular: singular, plural: plural, zeros: zeros};
    }
    return unitObj;
}

getMonthName(month, abbr = false) {
    if (abbr) {
        switch(month) {
            case 1: return 'Ene';
            case 2: return 'Feb';
            case 3: return 'Mar';
            case 4: return 'Abr';
            case 5: return 'May';
            case 6: return 'Jun';
            case 7: return 'Jul';
            case 8: return 'Ago';
            case 9: return 'Sep';
            case 10: return 'Oct';
            case 11: return 'Nov';
            case 12: return 'Dic';
        }
    }
    else {
        switch(month) {
            case 1: return 'Enero';
            case 2: return 'Febrero';
            case 3: return 'Marzo';
            case 4: return 'Abril';
            case 5: return 'Mayo';
            case 6: return 'Junio';
            case 7: return 'Julio';
            case 8: return 'Agosto';
            case 9: return 'Septiembre';
            case 10: return 'Octubre';
            case 11: return 'Noviembre';
            case 12: return 'Diciembre';
        }
    }
    return -1;
}

numeroALetras(num, currency) {

    // Código basado en https://gist.github.com/alfchee/e563340276f89b22042a
        function Unidades(num){
    
            switch(num)
            {
                case 1: return 'UN';
                case 2: return 'DOS';
                case 3: return 'TRES';
                case 4: return 'CUATRO';
                case 5: return 'CINCO';
                case 6: return 'SEIS';
                case 7: return 'SIETE';
                case 8: return 'OCHO';
                case 9: return 'NUEVE';
            }
    
            return '';
        }//Unidades()
    
        function Decenas(num){
    
            let decena = Math.floor(num/10);
            let unidad = num - (decena * 10);
    
            switch(decena)
            {
                case 1:
                    switch(unidad)
                    {
                        case 0: return 'DIEZ';
                        case 1: return 'ONCE';
                        case 2: return 'DOCE';
                        case 3: return 'TRECE';
                        case 4: return 'CATORCE';
                        case 5: return 'QUINCE';
                        default: return 'DIECI' + Unidades(unidad);
                    }
                case 2:
                    switch(unidad)
                    {
                        case 0: return 'VEINTE';
                        default: return 'VEINTI' + Unidades(unidad);
                    }
                case 3: return DecenasY('TREINTA', unidad);
                case 4: return DecenasY('CUARENTA', unidad);
                case 5: return DecenasY('CINCUENTA', unidad);
                case 6: return DecenasY('SESENTA', unidad);
                case 7: return DecenasY('SETENTA', unidad);
                case 8: return DecenasY('OCHENTA', unidad);
                case 9: return DecenasY('NOVENTA', unidad);
                case 0: return Unidades(unidad);
            }
        }//Unidades()
    
        function DecenasY(strSin, numUnidades) {
            if (numUnidades > 0)
                return strSin + ' Y ' + Unidades(numUnidades)
    
            return strSin;
        }//DecenasY()
    
        function Centenas(num) {
            let centenas = Math.floor(num / 100);
            let decenas = num - (centenas * 100);
    
            switch(centenas)
            {
                case 1:
                    if (decenas > 0)
                        return 'CIENTO ' + Decenas(decenas);
                    return 'CIEN';
                case 2: return 'DOSCIENTOS ' + Decenas(decenas);
                case 3: return 'TRESCIENTOS ' + Decenas(decenas);
                case 4: return 'CUATROCIENTOS ' + Decenas(decenas);
                case 5: return 'QUINIENTOS ' + Decenas(decenas);
                case 6: return 'SEISCIENTOS ' + Decenas(decenas);
                case 7: return 'SETECIENTOS ' + Decenas(decenas);
                case 8: return 'OCHOCIENTOS ' + Decenas(decenas);
                case 9: return 'NOVECIENTOS ' + Decenas(decenas);
            }
    
            return Decenas(decenas);
        }//Centenas()
    
        function Seccion(num, divisor, strSingular, strPlural) {
            let cientos = Math.floor(num / divisor)
            let resto = num - (cientos * divisor)
    
            let letras = '';
    
            if (cientos > 0)
                if (cientos > 1)
                    letras = Centenas(cientos) + ' ' + strPlural;
                else
                    letras = strSingular;
    
            if (resto > 0)
                letras += '';
    
            return letras;
        }//Seccion()
    
        function Miles(num) {
            let divisor = 1000;
            let cientos = Math.floor(num / divisor)
            let resto = num - (cientos * divisor)
    
            let strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
            let strCentenas = Centenas(resto);
    
            if(strMiles == '')
                return strCentenas;
    
            return strMiles + ' ' + strCentenas;
        }//Miles()
    
        function Millones(num) {
            let divisor = 1000000;
            let cientos = Math.floor(num / divisor)
            let resto = num - (cientos * divisor)
    
            let strMillones = Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
            let strMiles = Miles(resto);
    
            if(strMillones == '')
                return strMiles;
    
            return strMillones + ' ' + strMiles;
        }//Millones()
        
        currency = currency || {};
        let data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: '',
            letrasMonedaPlural: currency.plural || 'PESOS',//'PESOS', 'Dólares', 'Bolívares', 'etcs'
            letrasMonedaSingular: currency.singular || 'PESOS', //'PESO', 'Dólar', 'Bolivar', 'etc'
            letrasMonedaCentavoPlural: currency.centPlural || 'CHIQUI PESOS CHILENOS',
            letrasMonedaCentavoSingular: currency.centSingular || 'CHIQUI PESO CHILENO'
        };

        data.letrasCentavos = data.centavos + '/100 M.N.';

        if(data.enteros == 0)
            return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        if (data.enteros == 1)
            return Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
        else
            return Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    
    }

    checkFormFilled(form) {
        let filled = true;
        let requiredInputs = form.querySelectorAll('.inp-required');
        requiredInputs.forEach(function(elem, index) {
            if (elem.value == '') {
                filled = false;
                if (!elem.hasAttribute('invalid')) {
                    elem.setAttribute('invalid', '');
                    elem.addEventListener('click', clearInvalid);
                }
            }
        });

        function clearInvalid() {
            this.removeAttribute('invalid');
            this.removeEventListener('click', clearInvalid);            
        }

        return filled;
    }

}