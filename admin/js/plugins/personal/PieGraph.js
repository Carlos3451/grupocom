class PieGraph {
    constructor(container, options) {
        this.container = container;

        this.values = options['values'] || [0];
        this.tags = options['tags'] || [''];
        this.colors = options['colors'] || ['blue'];
        this.defaultTag = options['defaultTag'] || '';
        this.defaultColor = options['defaultColor'] || '';
        this.valueMax = options['valueMax'] || 0;

        this.piegraphContainer = document.createElement('div');
        this.piegraphContainer.className = 'piegraph-container';
        this.piegraphContainer.innerHTML =
            '<div class="piegraph-svg-container"><svg class="piegraph-svg" viewBox="-1 -1 2 2"></svg></div>'+
            '<div class="piegraph-tags"></div>';
        
        let namespaceURI = 'http://www.w3.org/2000/svg';

        let piePath = document.createElementNS(namespaceURI, 'path');
        piePath.setAttributeNS(null, 'class', 'pie-path');
        piePath.setAttributeNS(null, 'piechart-tag-name', this.defaultTag);
        piePath.setAttributeNS(null, 'fill', this.defaultColor);

        this.piegraphContainer.querySelector('.piegraph-svg').appendChild(piePath);

        this.setTagValue(this.defaultTag, this.valueMax);
        
        let angleTotal = 0;
        let valuesTotal = 0;

        let sortedValues = [];
        for (let i=0; i<this.values.length; i++) {
            sortedValues.push([this.values[i], i]);
        }
        sortedValues.sort(function(left, right) {
            return right[0] - left[0];
        });
        console.log(sortedValues);

        for (let i=0; i<sortedValues.length; i++) {
            let index = sortedValues[i][1];
            console.log(index);
            let piePath = document.createElementNS(namespaceURI, 'path');
            piePath.setAttributeNS(null, 'class', 'pie-path');
            piePath.setAttributeNS(null, 'piechart-tag-name', this.tags[index]);
            piePath.setAttributeNS(null, 'fill', this.colors[index]);
            this.piegraphContainer.querySelector('.piegraph-svg').appendChild(piePath);
            this.setTagValue(this.tags[index], this.values[index], angleTotal);
            let angle = (this.values[index] / this.valueMax) * 360;
            if (angle > 10) {
                let dirX = Math.sin((angleTotal + angle / 2) * Math.PI / 180);
                let dirY = -Math.cos((angleTotal + angle / 2) * Math.PI / 180);
                let textElem = document.createElement('p');
                textElem.textContent = this.values[index];
                textElem.style.position = 'absolute';
                textElem.style.left = 'calc(50% + ' + (dirX * 80) + 'px)';
                textElem.style.top = 'calc(50% + ' + (dirY * 80) + 'px)';
                this.piegraphContainer.querySelector('.piegraph-svg-container').appendChild(textElem);
            }
            angleTotal += angle;
            valuesTotal += this.values[index];
        }
        
        let dirX = Math.sin((angleTotal + (360 - angleTotal) / 2) * Math.PI / 180);
        let dirY = -Math.cos((angleTotal + (360 - angleTotal) / 2) * Math.PI / 180);
        let textElem = document.createElement('p');
        textElem.textContent = this.valueMax - valuesTotal;
        textElem.style.position = 'absolute';
        textElem.style.left = 'calc(50% + ' + (dirX * 80) + 'px)';
        textElem.style.top = 'calc(50% + ' + (dirY * 80) + 'px)';
        this.piegraphContainer.querySelector('.piegraph-svg-container').appendChild(textElem);

        for (let i=0; i<this.tags.length; i++) {
            let tagElem = document.createElement('div');
            tagElem.className = 'tag';
            tagElem.innerHTML = 
                '<div class="square" data-tag-name="' + this.tags[i] + '"></div>'+
                '<p>' + this.tags[i] + ' (' + (this.values[i])  + ')</p>';
            tagElem.querySelector('.square').style.backgroundColor = this.colors[i];
            this.piegraphContainer.querySelector('.piegraph-tags').appendChild(tagElem);            
        }

        let tagElem = document.createElement('div');
        tagElem.className = 'tag';
        tagElem.innerHTML = 
            '<div class="square" data-tag-name="' + this.defaultTag + '"></div>'+
            '<p>' + this.defaultTag + ' (' + (this.valueMax - valuesTotal)  + ')</p>';
        tagElem.querySelector('.square').style.backgroundColor = this.defaultColor;
        this.piegraphContainer.querySelector('.piegraph-tags').appendChild(tagElem);    
        
        this.container.appendChild(this.piegraphContainer);
    }
    
    setTagValue(tag, value, angle = 0   ) {
        let perc = value / this.valueMax;
        let angledPos = {x: Math.sin(angle * Math.PI / 180), y: -Math.cos(angle * Math.PI / 180)};
        let coords = {x: Math.sin((angle + perc * 360) * Math.PI / 180), y: -Math.cos((angle + perc * 360) * Math.PI / 180)};
        //let coords = this.getCoordinatesForPercent(perc);
        let largeArcFlag = perc > 0.5 ? 1 : 0;
        //let d = 'M 1 0 A 1 1 0 ' + largeArcFlag + ' 1 ' + coords['x'] + ' ' + coords['y'] + ' L 0 0';
        let d = 'M ' + angledPos.x + ' ' + angledPos.y + ' A 1 1 0 ' + largeArcFlag + ' 1 ' + coords.x + ' ' + coords.y + ' L 0 0';
        this.piegraphContainer.querySelector('[piechart-tag-name="' + tag + '"]').setAttribute('d', d);
    }

    getCoordinatesForPercent(percent) {
        const x = Math.cos(2 * Math.PI * percent);
        const y = Math.sin(2 * Math.PI * percent);
        return {x: x, y: y};
    }
}