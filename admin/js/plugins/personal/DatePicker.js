class DatePicker {
    constructor(input, options = {}) {
        let self = this;

        this.selectUntil = options['selectUntil'] || 'days';

        this.selectedDate = options['selectedDate'] || new Date();

        this.rangeActived = options['rangeActived'] || false;

        this.dateMin = options['dateMin'] || null;
        this.dateMax = options['dateMax'] || null;

        this.monthSelect = options['monthSelect'] || false;
        this.yearSelect = options['yearSelect'] || false;

        this.currentDate = options['date'] || new Date();
        this.currentDateTime = Math.floor(this.currentDate.getTime() / 1000);

        this.onChange = null;

        this.currentYear = this.currentDate.getFullYear();
        this.currentMonth = this.currentDate.getMonth() + 1;
        this.currentDay = this.currentDate.getDate();

        this.selectedYear = this.selectedDate.getFullYear();
        this.selectedMonth = this.selectedDate.getMonth() + 1;
        this.selectedDay = this.selectedDate.getDate();

        this.viewingYear = this.selectedYear;
        this.viewingMonth = this.selectedMonth;
        this.viewingDay = this.selectedDay;

        this.rangeStartDate;
        this.rangeEndDate;
        this.rangeViewDate;

        this.input = input;
        this.datePickerContainer = document.createElement('div');
        this.datePickerContainer.className = 'datepicker-container';
        this.datePickerContainer.innerHTML = 
            '<div class="datepicker-calendar">'+
                '<div class="datepicker-triangle"></div>'+
                '<div class="datepicker-years-container">'+
                    '<div class="datepicker-nav">'+
                        '<div class="datepicker-nav-arrow-container-left"><div class="datepicker-nav-arrow-left"><div></div></div></div>'+
                        '<p class="datepicker-nav-title"></p>'+
                        '<div class="datepicker-nav-arrow-container-right"><div class="datepicker-nav-arrow-right"><div></div></div></div>'+
                    '</div>'+
                    '<div class="datepicker-pickers-containers">'+
                    '</div>'+
                '</div>'+
                '<div class="datepicker-months-container">'+
                    '<div class="datepicker-nav">'+
                        '<div class="datepicker-nav-arrow-container-left"><div class="datepicker-nav-arrow-left"><div></div></div></div>'+
                        '<p class="datepicker-nav-title"></p>'+
                        '<div class="datepicker-nav-arrow-container-right"><div class="datepicker-nav-arrow-right"><div></div></div></div>'+
                    '</div>'+
                    '<div class="datepicker-pickers-containers">'+
                    '</div>'+
                '</div>'+
                '<div class="datepicker-days-container">'+
                    '<div class="datepicker-nav">'+
                        '<div class="datepicker-nav-arrow-container-left"><div class="datepicker-nav-arrow-left"><div></div></div></div>'+
                        '<p class="datepicker-nav-title"></p>'+
                        '<div class="datepicker-nav-arrow-container-right"><div class="datepicker-nav-arrow-right"><div></div></div></div>'+
                    '</div>'+
                    '<div class="datepicker-weekdays">' +
                        '<p>LU</p>'+
                        '<p>MA</p>'+
                        '<p>MI</p>'+
                        '<p>JU</p>'+
                        '<p>VI</p>'+
                        '<p>SA</p>'+
                        '<p>DO</p>'+
                    '</div>'+
                    '<div class="datepicker-pickers-containers"></div>'+
                '</div>'+
            '</div>';
        this.input.after(this.datePickerContainer);
        this.datePickerContainer.appendChild(this.input);

        this.input.setAttribute('autocomplete', 'off');

        this.initYearsEvents();
        this.initMonthsEvents();
        this.initDaysEvents();

        this.datePickerContainer.addEventListener('click', function(ev) {
            //ev.stopPropagation();
        });

        this.input.addEventListener('focus', function() {
            self.openCalendar();
        });
        this.input.addEventListener('blur', function() {
            self.updateInputValue();
        });
        this.input.addEventListener('input', function() {
            self.inputChange();
        });
        this.input.addEventListener('keydown', function(ev) {
            if (ev.key == 'Enter') {
                this.blur();
                self.closeCalendar();
            }
        });

        this.clickedOutsideFn = function(ev) {
            let container = ev.target.closest('.datepicker-container');
            console.log(ev.target);
            if (!container || container != self.datePickerContainer) {
                self.closeCalendar();
            }
        }

        this.transitionendCalendarClose = function() {
            this.style.display = '';
            this.removeEventListener('transitionend', self.transitionendCalendarClose);
        }

        switch (this.selectUntil) {
            case 'days':
                this.updateDaysPicker(this.selectedYear, this.selectedMonth);
                this.closeContainer('years', '');
                this.closeContainer('months', '');
                this.openContainer('days', '');
                break;
            case 'month':
                this.selectedDay = 0;
                this.viewingDay = 0;
                this.updateMonthsPicker(this.selectedYear);
                this.closeContainer('years', '');
                this.closeContainer('days', '');
                this.openContainer('months', '');
                break;
            case 'year':
                this.selectedDay = 0;
                this.selectedMonth = 0;
                this.viewingMonth = 0;
                this.viewingDay = 0;
                this.updateYearPicker(this.selectedYear);
                this.closeContainer('days', '');
                this.closeContainer('months', '');
                this.openContainer('years', '');
                break;
        }

        this.updateInputValue();

    }

    setDateMax(date) {
        this.dateMax = date;
    }

    setDateMin(date) {
        this.dateMin = date;
    }

    changeDate(date) {
        this.selectedDate = date;
        this.selectedYear = this.selectedDate.getFullYear();
        this.selectedMonth = this.selectedDate.getMonth() + 1;
        this.selectedDay = this.selectedDate.getDate();
        this.updateInputValue();
        this.setDate(this.selectedYear, this.selectedMonth, this.selectedDay);
    }

    inputChange() {
        let value = this.input.value;
        if (value.indexOf(' - ') >= 0) {
            let dateArray = value.split(' - ');
            let valids = 0;
            if (dateArray.length == 2 && this.monthSelect) {
                for (let a=0; a<2; a++) {
                    dateArray[a] = dateArray[a].split('/');
                    for (let i=0; i<3; i++) {
                        let value = parseInt(dateArray[a][i]);
                        if (isNaN(value)) {
                            return;
                        }
                        dateArray[a][i] = value;
                    }
                    if (dateArray[a].length == 3) {
                        let valid = this.validateDateArray(dateArray[a]);
                        if (valid) {
                            valids++;
                        }
                    }
                }
                if (valids == 2) {
                    let firstDate = new Date(dateArray[0][2], dateArray[0][1] - 1, dateArray[0][0]);
                    let secondDate = new Date(dateArray[1][2], dateArray[1][1] - 1, dateArray[1][0]);
                    this.setDateStart(firstDate.getFullYear(), firstDate.getMonth() + 1, firstDate.getDate());
                    this.setDateEnd(secondDate.getFullYear(), secondDate.getMonth() + 1, secondDate.getDate());
                }
            }
        }
        else {
            let dateArray = value.split('/');
            for (let i=0; i<dateArray.length; i++) {
                let value = parseInt(dateArray[i]);
                if (isNaN(value)) {
                    return;
                }
                dateArray[i] = value;
            }
            if (dateArray.length == 1 && this.yearSelect) {
                let valid = this.validateDateArray(dateArray);
                if (valid) {
                    this.setDate(dateArray[0]);
                }
            }
            else if (dateArray.length == 2 && this.monthSelect) {
                let valid = this.validateDateArray(dateArray);
                if (valid) {
                    this.setDate(dateArray[1], dateArray[0]);
                }
            }
            else if (dateArray.length == 3) {
                let valid = this.validateDateArray(dateArray);
                if (valid) {
                    this.setDate(dateArray[2], dateArray[1], dateArray[0]);
                }
            }
        }
    }

    validateDateArray(dateArray) {
        let valid = true;
        let date;        
        if (dateArray.length == 1) {
            date = new Date(dateArray[0], 0, 1);
            if (date.getTime()) {
                if (dateArray[0] != date.getFullYear()) {
                    valid = false;
                }
            }
        }
        else if (dateArray.length == 2) {
            date = new Date(dateArray[1], dateArray[0] - 1, 1);
            if (date.getTime()) {
                if (dateArray[1] != date.getFullYear() || dateArray[0] != date.getMonth() + 1) {
                    valid = false;
                }
            }
        }
        else if (dateArray.length == 3) {
            date = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
            if (date.getTime()) {
                if (dateArray[2] != date.getFullYear() || dateArray[1] != date.getMonth() + 1 || dateArray[0] != date.getDate()) {
                    valid = false;
                }
            }
        }
        else {
            valid = false;
        }
        return valid;
    }

    openCalendar() {
        let self = this;
        let calendarElem = this.datePickerContainer.querySelector('.datepicker-calendar');
        calendarElem.style.display = 'block';
        calendarElem.offsetHeight;
        calendarElem.style.opacity = 1;
        calendarElem.removeEventListener('transitionend', this.transitionendCalendarClose);
        window.removeEventListener('click', this.clickedOutsideFn);
        setTimeout(function() {
            window.addEventListener('click', self.clickedOutsideFn);
        }, 100);

        this.viewingYear = this.selectedYear;
        this.viewingMonth = this.selectedMonth;
        this.viewingDay = this.selectedDay;

        if (this.input.value.indexOf('-') >= 0) {
            this.updateDaysPicker(this.selectedYear, this.selectedMonth);
            this.openContainer('days', '');
        }
        else {
            let dateArray = this.input.value.split('/');
            if (dateArray.length == 3) {
                this.updateDaysPicker(this.selectedYear, this.selectedMonth);
                this.closeContainer('years', '');
                this.closeContainer('months', '');
                this.openContainer('days', '');
            }
            else if (dateArray.length == 2) {
                this.updateMonthsPicker(this.selectedYear);
                this.closeContainer('years', '');
                this.closeContainer('days', '');
                this.openContainer('months', '');
            }
            else if (dateArray.length == 1) {
                this.updateYearPicker(this.selectedYear);
                this.closeContainer('days', '');
                this.closeContainer('months', '');
                this.openContainer('years', '');
            }
        }
    }

    closeCalendar() {
        window.removeEventListener('click', this.clickedOutsideFn);
        let calendarElem = this.datePickerContainer.querySelector('.datepicker-calendar');
        calendarElem.style.opacity = 0;
        calendarElem.addEventListener('transitionend', this.transitionendCalendarClose);
    }

    canMove() {
        if (document.querySelector('.datepicker-ghost-picker')) {
            return false;
        }
        return true;
    }

    movePickers(pickersName, direction) {
        let pickers = this.datePickerContainer.querySelector('.datepicker-' + pickersName + '-container .datepicker-pickers-containers');
        let clonedPickers = pickers.cloneNode(true);
        clonedPickers.classList.add('datepicker-ghost-picker');
        this.datePickerContainer.querySelector('.datepicker-' + pickersName + '-container').appendChild(clonedPickers);
        if (direction == 'right') {
            pickers.style.left = '-100%';
            clonedPickers.style.left = '0%';
            clonedPickers.offsetHeight;
            pickers.style.left = '0%';
            clonedPickers.style.left = '100%';
        }
        else {
            pickers.style.left = '100%';
            clonedPickers.style.left = '0%';
            clonedPickers.offsetHeight;
            pickers.style.left = '0%';
            clonedPickers.style.left = '-100%';
        }
        clonedPickers.addEventListener('transitionend', function() {
            pickers.style.left = '';
            this.remove();
        });
    }

    openContainer(containerName, type) {
        let container = this.datePickerContainer.querySelector('.datepicker-' + containerName + '-container');

        container.removeEventListener('transitionend', transitionendOpen);
        container.removeEventListener('transitionend', transitionendClose);
        container.classList.remove('datepicker-disappearing');

        container.style.display = 'block';

        switch (type) {
            case 'grow':
                container.style.transform = 'scale(0.8)';
                container.style.opacity = '0';
                container.offsetHeight;
                container.style.transform = 'scale(1)';
                container.style.opacity = '1';
                break;
            case 'shrink':
                container.style.transform = 'scale(1.2)';
                container.style.opacity = '0';
                container.offsetHeight;
                container.style.transform = 'scale(1)';
                container.style.opacity = '1';
                break;
        }

        container.addEventListener('transitionend', transitionendOpen);

    }

    closeContainer(containerName, type = '') {
        let container = this.datePickerContainer.querySelector('.datepicker-' + containerName + '-container');

        container.removeEventListener('transitionend', transitionendOpen);
        container.removeEventListener('transitionend', transitionendClose);
        container.classList.remove('datepicker-disappearing');

        container.classList.add('datepicker-disappearing');

        switch (type) {
            case 'grow':
                container.style.transform = 'scale(1)';
                container.style.opacity = '1';        
                container.offsetHeight;        
                container.style.transform = 'scale(1.2)';
                container.style.opacity = '0';
                container.addEventListener('transitionend', transitionendClose);
                break;
            case 'shrink':
                container.style.transform = 'scale(1)';
                container.style.opacity = '1';        
                container.offsetHeight;
                container.style.transform = 'scale(0.8)';
                container.style.opacity = '0';        
                container.addEventListener('transitionend', transitionendClose);
                break;
            default:
                container.style.transform = '';
                container.style.opacity = '';
                container.style.display = '';
                container.classList.remove('datepicker-disappearing');
                container.removeEventListener('transitionend', transitionendClose);
        }
    }

    initYearsEvents() {
        let self = this;
        this.datePickerContainer.querySelector('.datepicker-years-container .datepicker-nav-arrow-left').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('years', 'right');
                self.viewingYear -= 10;
                self.updateYearPicker(self.viewingYear);
            }
        });
        this.datePickerContainer.querySelector('.datepicker-years-container .datepicker-nav-arrow-right').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('years', 'left');
                self.viewingYear += 10;
                self.updateYearPicker(self.viewingYear);
            }
        });
    }
    updateYearPicker(year) {
        let self = this;
        let pickersContainer = this.datePickerContainer.querySelector('.datepicker-years-container .datepicker-pickers-containers');
        let yearsStartRange = Math.floor(year / 10) * 10;
        let yearsEndRange = yearsStartRange + 9;
        let yearsRangeFrag = document.createDocumentFragment();
        this.datePickerContainer.querySelector('.datepicker-years-container .datepicker-nav-title').textContent = yearsStartRange + ' - ' + yearsEndRange;
        for (let i=-1; i<11; i++) {
            let pickerElem = document.createElement('div');
            pickerElem.className = 'datepicker-picker' + ((i == -1 || i == 10) ? ' datepicker-outrange' : '') +
                (i + yearsStartRange == this.currentYear ? ' datepicker-current' : '') +
                (i + yearsStartRange == this.selectedYear ? ' datepicker-selected' : '');
            if (this.dateMin != null) {
                pickerElem.className += i + yearsStartRange < this.dateMin.getFullYear() ? ' datepicker-disabled' : '';
            }
            if (this.dateMax != null) {
                pickerElem.className += i + yearsStartRange > this.dateMax.getFullYear() ? ' datepicker-disabled' : '';
            }
            pickerElem.setAttribute('value', yearsStartRange + i);
            pickerElem.innerHTML = '<p>' + (yearsStartRange + i) + '</p>';
            if (!pickerElem.classList.contains('datepicker-disabled')) {
                if (this.yearSelect) {
                    pickerElem.addEventListener('dblclick', function(ev) {
                        self.resetRange()
                        this.dataset.doubleclick = 1;
                        let year = this.getAttribute('value');
                        self.viewingYear = parseInt(year);
                        self.viewingMonth = 0;
                        self.viewingDay = 0;
                        self.setDate(self.viewingYear);
                        self.updateInputValue();
                        self.closeCalendar();
                    });
                }
                pickerElem.addEventListener('click', function(ev) {
                    let pickerSelf = this;
                    this.dataset.doubleclick = 0;
                    setTimeout(function() {
                        if (pickerSelf.dataset.doubleclick != 1) {
                            if ((ev.ctrlKey && self.yearSelect) || self.selectUntil == 'year') {
                                self.resetRange()
                                let year = pickerSelf.getAttribute('value');
                                self.viewingYear = parseInt(year);
                                self.viewingMonth = 0;
                                self.viewingDay = 0;
                                self.setDate(self.viewingYear);
                                self.updateInputValue();
                                self.closeCalendar();
                            }
                            else {
                                let year = pickerSelf.getAttribute('value');
                                self.viewingYear = parseInt(year);
                                self.updateMonthsPicker(year);
                                self.closeContainer('years', 'grow');
                                self.openContainer('months', 'grow');
                            }
                        }
                    }, 150);
                });
            }
            yearsRangeFrag.appendChild(pickerElem);
        }
        pickersContainer.innerHTML = '';
        pickersContainer.appendChild(yearsRangeFrag);
    }

    initMonthsEvents() {
        let self = this;
        this.datePickerContainer.querySelector('.datepicker-months-container .datepicker-nav-arrow-left').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('months', 'right');
                self.viewingYear -= 1;
                self.updateMonthsPicker(self.viewingYear);
            }
        });
        this.datePickerContainer.querySelector('.datepicker-months-container .datepicker-nav-arrow-right').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('months', 'left');
                self.viewingYear += 1;
                self.updateMonthsPicker(self.viewingYear);
            }
        });
        this.datePickerContainer.querySelector('.datepicker-months-container .datepicker-nav-title').addEventListener('click', function() {
            self.updateYearPicker(self.viewingYear);
            self.openContainer('years', 'shrink');
            self.closeContainer('months', 'shrink');
        });
    }
    updateMonthsPicker(year) {
        let self = this;
        let pickersContainer = this.datePickerContainer.querySelector('.datepicker-months-container .datepicker-pickers-containers');
        let monthsFrag = document.createDocumentFragment();
        this.datePickerContainer.querySelector('.datepicker-months-container .datepicker-nav-title').textContent = year;
        for (let i=1; i<=12; i++) {
            let monthObj = this.createMonthObject(i);
            let pickerElem = document.createElement('div');
            pickerElem.className = 'datepicker-picker' +
                ((i == this.currentMonth && year == this.currentYear) ? ' datepicker-current' : '') +
                ((i == this.selectedMonth && year == this.selectedYear) ? ' datepicker-selected' : '');
            if (this.dateMin != null) {
                pickerElem.className += ((i <= this.dateMin.getMonth() && year == this.dateMin.getFullYear()) || (year < this.dateMin.getFullYear())) ? ' datepicker-disabled' : '';                
            }
            if (this.dateMax != null) {
                pickerElem.className += ((i > this.dateMax.getMonth() + 1 && year == this.dateMax.getFullYear()) || (year > this.dateMax.getFullYear())) ? ' datepicker-disabled' : '';                
            }
            pickerElem.setAttribute('value', i);
            pickerElem.innerHTML = '<p>' + (monthObj['abreviatura']) + '</p>';
            if (!pickerElem.classList.contains('datepicker-disabled')) {
                if (this.monthSelect) {
                    pickerElem.addEventListener('dblclick', function(ev) {
                        self.resetRange()
                        this.dataset.doubleclick = 1;
                        self.viewingMonth = parseInt(this.getAttribute('value'));
                        self.viewingDay = 0;
                        self.setDate(self.viewingYear, self.viewingMonth);
                        self.updateInputValue();
                        self.closeCalendar();
                    });
                }
                pickerElem.addEventListener('click', function(ev) {
                    let pickerSelf = this;
                    this.dataset.doubleclick = 0;
                    setTimeout(function() {
                        if (pickerSelf.dataset.doubleclick != 1) {
                            if ((ev.ctrlKey && self.monthSelect) || self.selectUntil == 'month') {
                                self.resetRange()
                                self.viewingMonth = parseInt(pickerSelf.getAttribute('value'));
                                self.viewingDay = 0;
                                self.setDate(self.viewingYear, self.viewingMonth);
                                self.updateInputValue();
                                self.closeCalendar();
                            }
                            else {
                                let month = pickerSelf.getAttribute('value');
                                self.viewingMonth = parseInt(month);
                                self.updateDaysPicker(self.viewingYear, self.viewingMonth);
                                self.openContainer('days', 'grow');
                                self.closeContainer('months', 'grow');
                            }
                        }
                    }, 150);
                });
            }
            monthsFrag.appendChild(pickerElem);
        }
        pickersContainer.innerHTML = '';
        pickersContainer.appendChild(monthsFrag);
    }

    initDaysEvents() {
        let self = this;
        this.datePickerContainer.querySelector('.datepicker-days-container .datepicker-nav-arrow-left').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('days', 'right');
                self.viewingMonth -= 1;
                if (self.viewingMonth<=0) {
                    self.viewingYear -= 1;
                }
                self.viewingMonth = ((self.viewingMonth + 11) % 12) + 1;
                self.updateDaysPicker(self.viewingYear, self.viewingMonth);
            }
        });
        this.datePickerContainer.querySelector('.datepicker-days-container .datepicker-nav-arrow-right').addEventListener('click', function() {
            if (self.canMove()) {
                self.movePickers('days', 'left');
                self.viewingMonth += 1;
                if (self.viewingMonth>12) {
                    self.viewingYear += 1;
                }
                self.viewingMonth = ((self.viewingMonth + 11) % 12) + 1;
                self.updateDaysPicker(self.viewingYear, self.viewingMonth);
            }
        });
        this.datePickerContainer.querySelector('.datepicker-days-container .datepicker-nav-title').addEventListener('click', function() {
            self.updateMonthsPicker(self.viewingYear);
            self.openContainer('months', 'shrink');
            self.closeContainer('days', 'shrink');
        });
    }
    updateDaysPicker(year, month) {
        let self = this;
        let monthObj = this.createMonthObject(month || 1);
        let pickersContainer = this.datePickerContainer.querySelector('.datepicker-days-container .datepicker-pickers-containers');
        let frag = document.createDocumentFragment();
        let weekStart = this.getFirstWeekDay(year, month);
        let daysInMonth = this.getDaysInMonth(year, month);
        let daysInPreviousMonth = this.getDaysInMonth(year, month - 1);
        this.datePickerContainer.querySelector('.datepicker-days-container .datepicker-nav-title').textContent = monthObj['nombre'] + ' ' + year;
        for (let i=1; i<=42; i++) {
            let day = i - weekStart + 1;
            let dateObj = new Date(this.viewingYear, this.viewingMonth - 1, day);
            let pickerElem = document.createElement('div');
            pickerElem.className = 'datepicker-picker' +
                ((dateObj.getDate() == this.currentDay && (dateObj.getMonth() + 1) == this.currentMonth && dateObj.getFullYear() == this.currentYear) ? ' datepicker-current' : '') +
                ((dateObj.getDate() == this.selectedDay && (dateObj.getMonth() + 1) == this.selectedMonth && dateObj.getFullYear() == this.selectedYear) ? ' datepicker-selected' : '');
            if (this.dateMin != null) {
                pickerElem.className += ((dateObj.getDate() < this.dateMin.getDate() && dateObj.getMonth() == this.dateMin.getMonth()) || (dateObj.getMonth() < this.dateMin.getMonth() && dateObj.getFullYear() == this.dateMin.getFullYear()) || (dateObj.getFullYear() < this.dateMin.getFullYear())) ? ' datepicker-disabled' : '';
            }
            if (this.dateMax != null) {
                pickerElem.className += ((dateObj.getDate() > this.dateMax.getDate() && dateObj.getMonth() == this.dateMax.getMonth()) || (dateObj.getMonth() > this.dateMax.getMonth() && dateObj.getFullYear() == this.dateMax.getFullYear()) || (dateObj.getFullYear() > this.dateMax.getFullYear())) ? ' datepicker-disabled' : '';
            }
            if (self.rangeStartDate && self.rangeEndDate) {
                if (dateObj.getTime() == self.rangeEndDate.getTime()) {
                    pickerElem.classList.add('endrange');
                }
                else if (dateObj.getTime() == self.rangeStartDate.getTime()) {
                    pickerElem.classList.add('startrange');
                }
                else if (dateObj > self.rangeStartDate && dateObj < self.rangeEndDate) {
                    pickerElem.classList.add('inrange');
                }
            }
            if (day <= 0) {
                pickerElem.classList.add('datepicker-outrange');
                day = daysInPreviousMonth + day;
            }
            else if (day > daysInMonth) {
                pickerElem.classList.add('datepicker-outrange');
                day = day - daysInMonth;
            }
            let week = ((i - 1) % 7) + 1;
            let weekObj = this.createWeekObject(week);
            pickerElem.setAttribute('data-day', day);
            pickerElem.setAttribute('data-month', dateObj.getMonth() + 1);
            pickerElem.setAttribute('data-year', dateObj.getFullYear());
            pickerElem.innerHTML = '<p>' + day + '</p>';
            pickerElem.addEventListener('mousemove', function(ev) {
                if (self.rangeStartDate && !self.rangeEndDate) {
                    let rangeViewingDay = parseInt(this.getAttribute('data-day'));
                    let rangeViewingMonth = parseInt(this.getAttribute('data-month'));
                    let rangeViewingYear = parseInt(this.getAttribute('data-year'));
                    let selectedDayElem = self.datePickerContainer.querySelector('.datepicker-days-container .datepicker-picker.datepicker-selected');
                    self.setDateRangeView(rangeViewingYear, rangeViewingMonth, rangeViewingDay);
                    if (selectedDayElem) {
                        selectedDayElem.classList.remove('startrange');
                        selectedDayElem.classList.remove('endrange');
                        if (self.rangeStartDate < self.rangeViewDate) {
                            selectedDayElem.classList.add('startrange');
                        }
                        else if (self.rangeStartDate > self.rangeViewDate) {
                            selectedDayElem.classList.add('endrange');
                        }
                    }
                    self.datePickerContainer.querySelectorAll('.datepicker-days-container .datepicker-picker').forEach(function(elem) {
                        let rangeDateObj = new Date(elem.getAttribute('data-year'), elem.getAttribute('data-month') - 1, elem.getAttribute('data-day'));
                        elem.classList.remove('inrange');
                        if (rangeDateObj > Math.min(self.rangeStartDate, self.rangeViewDate) && rangeDateObj < Math.max(self.rangeStartDate, self.rangeViewDate)) {
                            elem.classList.add('inrange');
                        }
                    });
                }
            });
            if (!pickerElem.classList.contains('datepicker-disabled')) {
                pickerElem.addEventListener('click', function(ev) {
                    self.viewingDay = parseInt(this.getAttribute('data-day'));
                    self.viewingMonth = parseInt(this.getAttribute('data-month'));
                    self.viewingYear = parseInt(this.getAttribute('data-year'));
                    if (!ev.shiftKey) {
                        if (!self.rangeStartDate || self.rangeEndDate) {
                            self.resetRange();
                            self.setDate(self.viewingYear, self.viewingMonth, self.viewingDay);
                            self.updateInputValue();
                            self.closeCalendar();
                        }
                        else {
                            self.setDateEnd(self.viewingYear, self.viewingMonth, self.viewingDay);
                            self.updateInputValue();
                            self.closeCalendar();
                        }
                    }
                    else if (self.rangeActived) {
                        self.resetRange();
                        self.setDateStart(self.viewingYear, self.viewingMonth, self.viewingDay);
                        self.updateInputValue();
                        this.classList.add('range-start');
                    }
                });
            }
            frag.appendChild(pickerElem);
        }
        pickersContainer.innerHTML = '';
        pickersContainer.appendChild(frag);
    }

    resetRange() {
        this.rangeViewDate = null;
        this.rangeStartDate = null;
        this.rangeEndDate = null;
    }

    setDate(year, month = 0, day = 0) {
        let self = this;
        this.selectedYear = year;
        this.selectedMonth = month;
        this.selectedDay = day;
        this.viewingYear = year;
        this.viewingMonth = month;
        this.viewingDay = day;
        setTimeout(function() {
            self.updateDaysPicker(year, month);
            self.updateMonthsPicker(year);
            self.updateYearPicker(year);
            if (self.onChange) {
                self.onChange();
            }
        }, 2);
    }

    setDateRangeView(year, month = 0, day = 0) {
        let date = new Date(year, month - 1, day);
        this.rangeViewDate = date;
    }

    setDateStart(year, month = 0, day = 0) {
        let date = new Date(year, month - 1, day);
        this.rangeStartDate = date;
        this.setDate(year, month, day);
    }

    setDateEnd(year, month = 0, day = 0) {
        let date = new Date(year, month - 1, day);
        this.rangeEndDate = date;
        if (this.rangeEndDate < this.rangeStartDate) {
            this.rangeEndDate = this.rangeStartDate;
            this.rangeStartDate = date;
            this.setDate(year, month, day);
        }
        else {
            this.setDate(this.rangeStartDate.getFullYear(), this.rangeStartDate.getMonth() + 1, this.rangeStartDate.getDate());
        }
    }

    updateInputValue() {
        this.input.removeAttribute('data-date-time');
        this.input.removeAttribute('data-date-time-start');
        this.input.removeAttribute('data-date-time-end');
        let previousValue = this.input.value;
        if (this.rangeStartDate && this.rangeEndDate) {
            let dateStartArray = [('0' + this.rangeStartDate.getDate()).slice(-2), ('0' + (this.rangeStartDate.getMonth() + 1)).slice(-2), this.rangeStartDate.getFullYear()];
            let dateEndArray = [('0' + this.rangeEndDate.getDate()).slice(-2), ('0' + (this.rangeEndDate.getMonth() + 1)).slice(-2), this.rangeEndDate.getFullYear()];            
            this.input.value = dateStartArray.join('/') + ' - ' + dateEndArray.join('/');
            this.input.setAttribute('data-date-time-start', Math.floor(this.rangeStartDate.getTime() / 1000));
            this.input.setAttribute('data-date-time-end', Math.floor(this.rangeEndDate.getTime() / 1000));
        }
        else {
            let date = new Date(this.selectedYear, this.selectedMonth - 1, this.selectedYear);
            let dateTime = Math.floor(date.getTime() / 1000);
            let dateArray = [('0' + this.selectedDay).slice(-2), ('0' + this.selectedMonth).slice(-2), this.selectedYear];
            dateArray = dateArray.filter(function(value, index) {
                return parseInt(value) != 0;
            });         
            this.input.value = dateArray.join('/');
            this.input.setAttribute('data-date-time', dateTime);
        }
        if (previousValue != this.input.value) {
            this.input.dispatchEvent(new Event('change'));
        }
    }

    getDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }
    getFirstWeekDay(year, month) {
        month -= 1;
        return ((new Date(year, month, 1).getDay() + 6) % 7) + 1;
    }
    createMonthObject(month) {
        let monthObj = {};
        switch (month) {
            case 1:
                monthObj['nombre'] = 'Enero';
                monthObj['abreviatura'] = 'Ene';
                break;
            case 2:
                monthObj['nombre'] = 'Febero';
                monthObj['abreviatura'] = 'Feb';
                break;
            case 3:
                monthObj['nombre'] = 'Marzo';
                monthObj['abreviatura'] = 'Mar';
                break;
            case 4:
                monthObj['nombre'] = 'Abril';
                monthObj['abreviatura'] = 'Abr';
                break;
            case 5:
                monthObj['nombre'] = 'Mayo';
                monthObj['abreviatura'] = 'May';
                break;
            case 6:
                monthObj['nombre'] = 'Junio';
                monthObj['abreviatura'] = 'Jun';
                break;
            case 7:
                monthObj['nombre'] = 'Julio';
                monthObj['abreviatura'] = 'Jul';
                break;
            case 8:
                monthObj['nombre'] = 'Agosto';
                monthObj['abreviatura'] = 'Ago';
                break;
            case 9:
                monthObj['nombre'] = 'Septiembre';
                monthObj['abreviatura'] = 'Sep';
                break;
            case 10:
                monthObj['nombre'] = 'Octubre';
                monthObj['abreviatura'] = 'Oct';
                break;
            case 11:
                monthObj['nombre'] = 'Noviembre';
                monthObj['abreviatura'] = 'Nov';
                break;
            case 12:
                monthObj['nombre'] = 'Diciembre';
                monthObj['abreviatura'] = 'Dic';
                break;
        }
        return monthObj;
    }
    createWeekObject(weekDay) {
        let weekObj = {};
        switch (weekDay) {
            case 1:
                weekObj['nombre'] = 'Lunes';
                weekObj['abreviatura'] = 'Lun';
                break;
            case 2:
                weekObj['nombre'] = 'Martes';
                weekObj['abreviatura'] = 'Mar';
                break;
            case 3:
                weekObj['nombre'] = 'Miercoles';
                weekObj['abreviatura'] = 'Mie';
                break;
            case 4:
                weekObj['nombre'] = 'Jueves';
                weekObj['abreviatura'] = 'Jue';
                break;
            case 5:
                weekObj['nombre'] = 'Viernes';
                weekObj['abreviatura'] = 'Vie';
                break;
            case 6:
                weekObj['nombre'] = 'Sabado';
                weekObj['abreviatura'] = 'Sab';
                break;
            case 7:
                weekObj['nombre'] = 'Domingo';
                weekObj['abreviatura'] = 'Dom';
                break;
        }
        return weekObj;
    }    
}

function transitionendOpen() {
    this.style.transform = '';
    this.style.opacity = '';
    this.removeEventListener('transitionend', transitionendOpen);
}

function transitionendClose() {
    this.style.transform = '';
    this.style.opacity = '';
    this.style.display = '';
    this.classList.remove('datepicker-disappearing');
    this.removeEventListener('transitionend', transitionendClose);
}