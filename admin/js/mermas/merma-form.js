(function() {

let productosList = [];
let mermaProductosInitial = [];

clearForm();

if (formMode == 'EDIT' || formMode == 'VIEW') {
    loadingDataMsg = new Wait('Cargando datos...');
    loadMerma();
}

let productosLoadPromise = loadProductos();
let categoriasLoadPromise = loadCategorias();

function loadCategorias() {
    return new Promise(function(resolve) {
        let selector = document.querySelector('[name="categoriaID"]');
        selector.disabled = true;
        selector.innerHTML = '<option>Cargando...</option>';
        Tools.ajaxRequest({
            url: 'functions/categoria/get-all',
            success: function(response) {
                let result = JSON.parse(response);
                selector.disabled = false;
                selector.children[0].remove();
                if (result['error'] == '') {
                    let data = result['resultado'];
                    let optionsFragment = document.createDocumentFragment();

                    let optionTodos = document.createElement('option');
                    optionTodos.value = '-1';
                    optionTodos.textContent = 'Todos';
                    optionsFragment.appendChild(optionTodos);

                    for (let i=0; i<data.length; i++) {
                        let option = document.createElement('option');
                        option.value = data[i]['id'];
                        option.textContent = data[i]['nombre'];
                        optionsFragment.appendChild(option);
                    }

                    let optionNinguno = document.createElement('option');
                    optionNinguno.value = '0';
                    optionNinguno.textContent = 'Ninguna';
                    optionsFragment.appendChild(optionNinguno);

                    selector.appendChild(optionsFragment);
                }
                resolve();
            }
        });
    });
}

function loadMerma() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/merma/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                setMerma(data['resultado']);
            }
        }
    });
}

function loadProductos() {
    return new Promise(function(resolve) {
        Tools.ajaxRequest({
            data: {
                metodo: 'folio',
                orden: 'asc',
                busqueda: ''
            },
            method: 'POST',
            url: 'functions/producto/get-all',
            success: function(response) {
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    productosList = data['resultado'];
                    for (let i=0; i<productosList.length; i++) {
                        addProduct(productosList[i]);
                    }
                    resolve();
                }
            }
        });
    });
}

function setMerma(data) {
    let productosResult = data['productos'];
    productosLoadPromise.then(function() {
        if (productosResult['error'] == '') {
            let productos = productosResult['resultado'];
            for (let i=0; i<productos.length; i++) {
                let producto = productos[i];
                let row = document.querySelector('.custom-table-row[data-productoid="' + producto['productoID'] + '"]');
                if (row) {
                    row.dataset.id = producto['id'];
                    let cantidadInput = row.querySelector('[name="cantidad"]');
                    cantidadInput.disabled = true;
                    cantidadInput.value = parseFloat(producto['cantidad']);
                }
            }
        }
        if (formMode == 'VIEW') {
            document.querySelectorAll('.custom-table-row > .custom-table-column [name="cantidad"]').forEach(function(input) {
                if (input.value == '') {
                    input.parentElement.parentElement.style.display = 'none';
                }
            });
            disableInputs();
        }
    });
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

if (formMode != 'VIEW') {
    document.querySelector('.form-submit').addEventListener('click', function(ev) {
        submitForm();
    });
    window.onbeforeunload = function() {
        let data = getMermaProductos();
        if (JSON.stringify(data) != JSON.stringify(mermaProductosInitial)) {
            return 'Se perderan los datos no guardados. ¿Seguro que deseas abandonar?'
        }
    }
}

function submitForm() {
    let mermaProductos = getMermaProductos();
    document.querySelector('.form-submit').setAttribute('disabled', '');
    submitObj = {};
    switch (formMode) {
        case 'ADD':
            submitObj = {
                url: 'functions/merma/add',
                msgWait: 'Creando merma...',
                msgSuccess: 'Merma registrada con exito.',
                msgError: 'Ocurrio un error al tratar de registrar la merma.',
            }
            break;
        case 'EDIT':
            submitObj = {
                url: 'functions/merma/edit',
                msgWait: 'Guardando merma...',
                msgSuccess: 'Merma guardada con exito.',
                msgError: 'Ocurrio un error al tratar de guardar la merma.',
            }
            break;
    }
    Tools.ajaxRequest({
        data: {
            id: editID,
            productos: mermaProductos
        },
        url: submitObj['url'],
        method: 'POST',
        waitMsg: new Wait(submitObj['msgWait']),
        success: function(response) {
            document.querySelector('.form-submit').removeAttribute('disabled');
            this.waitMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                mermaProductosInitial = mermaProductos;
                new Message(submitObj['msgSuccess'], 'success', function() {
                    location = '/admin/mermas';
                });
            }
            else {
                new Message(submitObj['msgError'] + '<br>' + data['error'], 'error');
            }
        }
    });
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    if (formMode != 'VIEW') {
        new Question('Cancelar', 'Se perderan los datos no guardados.', function() {
            window.location = '/admin/mermas';
        });
    }
    else {
        window.location = '/admin/mermas';
    }
});

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
}

document.querySelector('[name="categoriaID"]').addEventListener('change', updateSearchProductos);
document.querySelector('[name="busqueda"]').addEventListener('input', updateSearchProductos);

function updateSearchProductos() {
    let searchKey = document.querySelector('[name="busqueda"]').value.toLowerCase();
    let categoriaID = document.querySelector('[name="categoriaID"]').value;
    document.querySelectorAll('.custom-table-row').forEach(function(row) {
        let productoNombre = row.dataset.nombre.toLowerCase();
        row.style.display = 'none';
        if ((categoriaID == -1 || row.dataset.categoriaid == categoriaID) && (searchKey == '' || productoNombre.indexOf(searchKey) != -1)) {
            row.style.display = '';
        }
    });
}

/* AÑADIR PRODUCTO */
function addProduct(producto) {
    let row = document.createElement('div');
    row.className = 'custom-table-row';
    row.dataset.productoid = producto['id'];
    row.dataset.id = 0;
    row.dataset.categoriaid = producto['categoriaID'] || 0;
    row.dataset.nombre = producto['nombre'];
    row.innerHTML = (
        '<div class="custom-table-column">'+
            '<input type="text" name="nombre" value="' + producto['nombre'] + '" disabled>'+
        '</div>'+
        '<div class="custom-table-column">'+
            '<input class="inp-unit inp-' + producto['unidad'] + ' text-align-right" type="text" name="cantidad" autocomplete="off">'+
        '</div>'
    );

    row.querySelector('[name="cantidad"]').addEventListener('keydown', keydownProductoRow);
    row.querySelector('[name="cantidad"]').addEventListener('focus', selectInput);
    row.querySelector('[name="cantidad"]').addEventListener('blur', blurDecimal);

    ToolTip.update();    
    document.querySelector('.custom-table').appendChild(row);
}

function keydownProductoRow(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
    let row = this.parentElement.parentElement;
    let iRow = row;
    switch (ev.key) {
        case 'Enter': case 'ArrowDown':
            while (iRow.nextElementSibling) {
                iRow = iRow.nextElementSibling;
                let column = iRow.children[1];
                if (!column) {
                    return;
                }
                let input = column.children[0];
                if (!input) {
                    return;
                }
                if (iRow.style.display != 'none') {
                    input.focus();
                    return;
                }
            }
            break;
        case 'ArrowUp':
            while (iRow.previousElementSibling) {
                iRow = iRow.previousElementSibling;
                let column = iRow.children[1];
                if (!column) {
                    return;
                }
                let input = column.children[0];
                if (!input) {
                    return;
                }
                if (iRow.style.display != 'none') {
                    input.focus();
                    return;
                }
            }
            break;
    }
}

function selectInput() {
    this.select();
}

function blurDecimal() {
    if (this.value != '') {
        this.value = parseFloat(parseFloat(this.value).toFixed(3));
    }
}

/* GET ALL DATA */

function getMermaProductos() {
    let productsData = [];
    document.querySelectorAll('.custom-table-row').forEach(function(row) {
        let cantidad = row.querySelector('[name="cantidad"]').value;
        if (row.dataset.id == 0 && cantidad != '') {
            productsData.push({
                id: row.dataset.id, 
                productoID: row.dataset.productoid,
                cantidad: row.querySelector('[name="cantidad"]').value
            });
        }
    });
    return productsData;
}

function disableInputs() {
    document.querySelectorAll('input').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

/* NUMERIC */

function keypressIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

document.querySelectorAll('[numeric]').forEach(function(elem, index) {
    elem.addEventListener('keypress', keypressIsNumber);
});
    
})();