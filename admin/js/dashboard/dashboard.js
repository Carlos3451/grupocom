(function(){

document.querySelector('[name="usuarioID"]').value = '';

let ventasGraph = new Graph(document.querySelector('.latest-sales-graph'), {
    barsWidth: '20%',
    valuePrefix: '$',
    valuesNumber: 5,
    barsColors: ['#0575E6', '#f06060', '#62d25f'],
    descriptions: ['Ventas', 'Costos', 'Ganancias']
});
/*
let visitasGraph = new PieGraph(document.querySelector('.visitas-graph'), {
    values: [10, 56, 10],
    valueMax: 100,
    defaultTag: 'Sin visitar',
    defaultColor: '#DDD',
    colors: ['#0575E6', '#f06060', '#62d25f'],
    tags: ['Pidio', 'No pidio', 'Cerrado']
});
*/
let paymentsGraph = new Graph(document.querySelector('.payments-graph'), {
    barsWidth: '20%',
    valuePrefix: '$',
    conceptsNames: ['Efectivo', 'Tarjeta', 'Transferencia', 'Cheque'],
    valuesNumber: 5,
    barsColors: '#0575E6'
});

let datePicker = new DatePicker(document.querySelector('[name="fecha"]'), {
    rangeActived: true,
});

let usuarioSI = new SelectInp(document.querySelector('[name="usuarioID"]'), {
    placeholder: 'Todos',    
    defaultValue: 0
});

document.querySelector('[name="fecha"]').addEventListener('change', function(){
    getProductsSalesTop();
    getProductsSalesTopMostrador();
    getPayments();
    getSales();
    getSalesmanSales();
    getDeliveriesSales();
});

usuarioSI.onChange = function() {
    getProductsSalesTop();
    getProductsSalesTopMostrador();
    getPayments();
    getSales();
};

Tools.ajaxRequest({
    data: {
        metodo: 'nombre',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            usuarioSI.setOptions(options);
        }
    }
});

getProductsSalesTop(new Date());
getProductsSalesTopMostrador(new Date());
getSalesmanSales(new Date());
getDeliveriesSales(new Date());
getLatestSales();
getPayments(new Date());
getSales(new Date());

function getProductsSalesTop() {
    let date = document.querySelector('[name="fecha"]').value;
    let userID = usuarioSI.value;
    document.querySelectorAll('.top-table > .top-row').forEach(function(elem, index) {
        elem.remove();
    });
    let waitElem = document.createElement('div');
    waitElem.className = 'top-row wait';
    waitElem.innerHTML = '<div class="top-column"><p>Cargando</p><div class="loading"></div></div>';
    document.querySelector('.top-table').append(waitElem);
    Tools.ajaxRequest({
        data: {
            fecha: date,
            usuarioID: userID
        },
        method: 'POST',
        url: 'functions/dashboard/get-products-sales-top',
        success: function(response) {
            document.querySelector('.top-table > .top-row.wait').remove();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                for (let i=0; i<result.length; i++) {
                    let elem = document.createElement('div');
                    elem.className = 'top-row';
                    elem.innerHTML = 
                        '<div class="top-column"><p>' + (i + 1) + '</p></div>'+
                        '<div class="top-column"><a href="/admin/productos/ver/' + result[i]['productoID'] + '">' + result[i]['nombre'] + '</a></div>'+
                        '<div class="top-column"><p class="text-align-right">$' + parseFloat(result[i]['total']).toFixed(2) + '</p></div>';
                    document.querySelector('.top-table').append(elem);
                }
            }
        }
    });
}

function getProductsSalesTopMostrador() {
    let date = document.querySelector('[name="fecha"]').value;
    let userID = usuarioSI.value;
    document.querySelectorAll('.top-mostrador-table > .top-row').forEach(function(elem, index) {
        elem.remove();
    });
    let waitElem = document.createElement('div');
    waitElem.className = 'top-row wait';
    waitElem.innerHTML = '<div class="top-column"><p>Cargando</p><div class="loading"></div></div>';
    document.querySelector('.top-mostrador-table').append(waitElem);
    Tools.ajaxRequest({
        data: {
            fecha: date,
            usuarioID: userID
        },
        method: 'POST',
        url: 'functions/dashboard/get-products-sales-top-mostrador',
        success: function(response) {
            document.querySelector('.top-mostrador-table > .top-row.wait').remove();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                for (let i=0; i<result.length; i++) {
                    let elem = document.createElement('div');
                    elem.className = 'top-row';
                    elem.innerHTML = 
                        '<div class="top-column"><p>' + (i + 1) + '</p></div>'+
                        '<div class="top-column"><a href="/admin/productos/ver/' + result[i]['productoID'] + '">' + result[i]['nombre'] + '</a></div>'+
                        '<div class="top-column"><p class="text-align-right">$' + parseFloat(result[i]['total']).toFixed(2) + '</p></div>';
                    document.querySelector('.top-mostrador-table').append(elem);
                }
            }
        }
    });
}

function getSalesmanSales() {
    if (privilegesLevel == 4) {
        let date = document.querySelector('[name="fecha"]').value;
        let userID = usuarioSI.value;
        document.querySelectorAll('.salesmans-table > .salesmans-row').forEach(function(elem, index) {
            elem.remove();
        });
        let waitElem = document.createElement('div');
        waitElem.className = 'salesmans-row wait';
        waitElem.innerHTML = '<div class="salesmans-column"><p>Cargando</p><div class="loading"></div></div>';
        document.querySelector('.salesmans-table').append(waitElem);
        Tools.ajaxRequest({
            data: {
                fecha: date,
                usuarioID: userID
            },
            method: 'POST',
            url: 'functions/dashboard/get-salesmans-sales',
            success: function(response) {
                document.querySelector('.salesmans-table > .salesmans-row').remove();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    for (let i=0; i<result.length; i++) {
                        let elem = document.createElement('div');
                        elem.className = 'salesmans-row';
                        elem.innerHTML = 
                            '<div class="salesmans-column"><p>' + (i + 1) + '</p></div>'+
                            '<div class="salesmans-column"><p>' + result[i]['usuarioNombre'] + '</p></div>'+
                            '<div class="salesmans-column"><p class="text-align-right">$' + result[i]['total'] + '</p></div>';
                        document.querySelector('.salesmans-table').append(elem);
                    }
                }
            }
        });
    }
}

function getDeliveriesSales() {
    if (privilegesLevel == 4) {
        let date = document.querySelector('[name="fecha"]').value;
        document.querySelectorAll('.deliveries-table > .deliveries-row').forEach(function(elem, index) {
            elem.remove();
        });
        let waitElem = document.createElement('div');
        waitElem.className = 'deliveries-row wait';
        waitElem.innerHTML = '<div class="deliveries-column"><p>Cargando</p><div class="loading"></div></div>';
        document.querySelector('.deliveries-table').append(waitElem);
        Tools.ajaxRequest({
            data: {
                fecha: date
            },
            method: 'POST',
            url: 'functions/dashboard/get-deliveries-sales',
            success: function(response) {
                document.querySelector('.deliveries-table > .deliveries-row').remove();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    let result = data['resultado'];
                    for (let i=0; i<result.length; i++) {
                        let elem = document.createElement('div');
                        elem.className = 'deliveries-row';
                        elem.innerHTML = 
                            '<div class="deliveries-column"><p>' + (i + 1) + '</p></div>'+
                            '<div class="deliveries-column"><p>' + result[i]['usuarioNombre'] + '</p></div>'+
                            '<div class="deliveries-column"><p class="text-align-right">$' + result[i]['total'] + '</p></div>';
                        document.querySelector('.deliveries-table').append(elem);
                    }
                }
            }
        });
    }
}

function getLatestSales() {
    let promises = [];

    let conceptsValues = [];
    let conceptsNames = [];

    for (let i=-4; i<=0; i++) {
        promises.push(new Promise(function(resolve, reject) {
            let dateCur = new Date();
            let date = new Date(dateCur.getFullYear(), dateCur.getMonth(), dateCur.getDate() + i);
            conceptsNames.push(Tools.getMonthName(date.getMonth() + 1, true) + ' ' + date.getDate());
            Tools.ajaxRequest({
                data: {
                    fecha:  ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear(),
                    usuarioID: 0
                },
                method: 'POST',
                url: 'functions/dashboard/get-sales',
                success: function(response) {
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        resolve(data['resultado']);
                    }
                    else {
                        reject(data['error']);
                    }
                }
            });
        }));
    }

    Promise.all(promises).then(function(values) {
        for (let i=0; i<values.length; i++) {
            let sales = [(parseFloat(values[i]['total']) || 0).toFixed(2), (parseFloat(values[i]['costo']) || 0).toFixed(2), (parseFloat(values[i]['ganancia']) || 0).toFixed(2)];
            conceptsValues.push(sales);
        }
        ventasGraph.setConceptsNames(conceptsNames);
        ventasGraph.setConceptsValues(conceptsValues);
    });

}

function getPayments() {
    let date = document.querySelector('[name="fecha"]').value;
    let userID = usuarioSI.value;
    let efectivo = 0;
    let tarjeta = 0;
    let transferencia = 0;
    let cheque = 0;
    Tools.ajaxRequest({
        data: {
            fecha: date,
            usuarioID: userID
        },
        method: 'POST',
        url: 'functions/dashboard/get-payments',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                paymentsGraph.setConceptsValues([
                    parseFloat(result['efectivo']),
                    parseFloat(result['tarjeta']),
                    parseFloat(result['transferencia']),
                    parseFloat(result['cheque'])
                ]);
            }
        }
    });
}

function getSales() {
    let date = document.querySelector('[name="fecha"]').value;
    let userID = usuarioSI.value;
    document.querySelector('.total-sales').textContent = 'Calculando...';
    document.querySelector('.total-costs').textContent = 'Calculando...';
    document.querySelector('.total-earnings').textContent = 'Calculando...';
    document.querySelector('.total-margin').textContent = 'Calculando...';
    document.querySelector('.total-pagos').textContent = 'Calculando...';
    document.querySelector('.total-gastos').textContent = 'Calculando...';
    Tools.ajaxRequest({
        data: {
            fecha: date,
            usuarioID: userID
        },
        method: 'POST',
        url: 'functions/dashboard/get-sales',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                let result = data['resultado'];
                let margin = Math.round((parseFloat(result['total']) - parseFloat(result['costo'])) / parseFloat(result['total'] || 0) * 100) || 0;
                document.querySelector('.total-sales').textContent = '$' + parseFloat(result['total'] || 0).toFixed(2);
                document.querySelector('.total-costs').textContent = '$' + parseFloat(result['costo'] || 0).toFixed(2);
                document.querySelector('.total-earnings').textContent = '$' + parseFloat(result['ganancia'] || 0).toFixed(2);
                document.querySelector('.total-margin').textContent = margin + '%';
                document.querySelector('.total-pagos').textContent = '$' + parseFloat(result['pagos'] || 0).toFixed(2);
                document.querySelector('.total-gastos').textContent = '$' + parseFloat(result['gastos'] || 0).toFixed(2);
            }
        }
    });
}

}());