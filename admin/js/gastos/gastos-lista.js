(function() {

document.querySelector('.open-gastosprogramados').addEventListener('click', function() {
    GastosProgramados.open();
});

document.querySelector('.add-gasto').addEventListener('click', function() {
    Gasto.open();
    Gasto.containerElem.querySelector('h3').textContent = 'Nuevo gasto';
    Gasto.containerElem.querySelector('.accept-gasto').textContent = 'Agregar';
    Gasto.onAccept = function(data) {
        Tools.ajaxRequest({
            data: data,
            url: 'functions/gasto/add',
            method: 'POST',
            waitMsg: new Wait('Agregando gasto.'),
            success: function(response) {
                this.waitMsg.destroy();
                let data = JSON.parse(response);
                if (data['error'] == '') {
                    new Message('Gasto agregado con exito.', 'success', function() {
                        window.location.reload();
                    });
                }
                else {
                    new Message('Ocurrio un error al tratar de registrar el gasto.<br>' + data['error'], 'error');
                }
            }
        });
    }
});

if (privilegiosNivel > 1) {
    Lista.actionEdit['active'] = true;
    Lista.actionEdit['unique'] = true;
    Lista.editFn = function(selecteds) {
        let selected = selecteds[0];
        if (selected.hasAttribute('cancelled')) {
            new Message('No se puede editar un gasto cancelado.', 'warning');
            return;
        }
        let dataID = selected.getAttribute('data-id');
        Gasto.containerElem.querySelector('h3').textContent = 'Editando gasto';
        Gasto.containerElem.querySelector('.accept-gasto').textContent = 'Guardar';
        Gasto.open({
            fecha: selected.dataset.fecha,
            razon: selected.dataset.razon,
            cantidad: selected.dataset.cantidad,
            comentario: selected.dataset.comentario,
        });
        Gasto.onAccept = function(data) {
            data['id'] = dataID;
            Tools.ajaxRequest({
                data: data,
                url: 'functions/gasto/edit',
                method: 'POST',
                waitMsg: new Wait('Editando gasto...'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        selected.dataset.fecha = this.data['fecha'];
                        selected.dataset.razon = this.data['razon'];
                        selected.dataset.cantidad = this.data['cantidad'];
                        selected.dataset.comentario = this.data['comentario'];
                        selected.querySelector('.fecha').textContent = this.data['fecha'];
                        selected.querySelector('.razon').textContent = this.data['razon'];
                        selected.querySelector('.comentario').textContent = this.data['comentario'];
                        selected.querySelector('.cantidad').textContent = '$' + this.data['cantidad'];
                        new Message('Gasto editado con exito.', 'success');
                    }
                    else {
                        new Message('Ocurrio un error al tratar de editar el gasto.<br>' + data['error'], 'error');
                    }
                }
            });
        }
    }
}
if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = false;
    Lista.cancelFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se cancelara el gasto seleccionada.',
            msgWait: 'Cancelando gasto.',
            msgSuccess: 'Gasto cancelado con exito.',
            msgError: 'Ocurrio un error al tratar de cancelar el gasto.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se cancelaran los gastos seleccionadas.',
                msgWait: 'Cancelando gastos.',
                msgSuccess: 'Gastos canceladas con exito.',
                msgError: 'Ocurrio un error al tratar de cancelar los gastos.'
            };
        }
        let ids = [];
        let cancelleds = 0;
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
            if (selecteds[i].hasAttribute('cancelled')) {
                cancelleds += 1;
            }
        }
        if (cancelleds == 0) {
            new Confirm(requestObj.msgQuestion, 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        ids: ids
                    },
                    method: 'POST',
                    url: 'functions/gasto/cancel',
                    waitMsg: new Wait(requestObj['msgWait'], true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message(requestObj['msgSuccess'], 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                                elem.querySelector('.edit-cantidad').remove();
                            });
                            Lista.removeSelections();
                        }
                        else {
                            new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        }
        else {
            if (selecteds.length == 1) {
                new Message('No se puede cancelar un gasto ya cancelado.', 'warning');
            }
            else if (selecteds.length == cancelleds) {
                new Message('No se pueden cancelar gastos ya cancelados.', 'warning');
            }
            else {
                new Message('Existen algunos gastos seleccionadas que ya estan cancelados.', 'warning');                
            }
        }
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('gasto/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        return [
            data['folio'],
            '<p class="fecha">' + data['fechaCreate'].split(' ')[0].split('-').reverse().join('/') + '</p>',
            '<p class="razon">' + data['razon'] + '</p>',
            '<p class="comentario">' + data['comentario'] + '</p>',
            '<div class="gasto-cantidad-container"><p class="cantidad">$' + data['cantidad'] + '</p>' + (data['cancelado'] == 0 ? '<img class="edit-cantidad" src="imgs/icons/actions/edit.png">' : '') + '</div>'
        ];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']],
            ['data-fecha', data['fechaCreate'].split(' ')[0].split('-').reverse().join('/')],
            ['data-razon', data['razon']],
            ['data-cantidad', data['cantidad']],
            ['data-comentario', data['comentario']]
        ];
        if (data['cancelado'] == 1) {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text']
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    selectUntil: 'month'
});

SortController.load();

document.querySelector('.list-container').addEventListener('click', function(ev) {
    if (ev.target.closest('.edit-cantidad')) {
        let row = ev.target.closest('.list-row');
        let oq = new OpenQuestion('Cambiar cantidad', '¿Que valor nuevo desea asignarle?', parseFloat(row.dataset.cantidad).toFixed(2), function(value) {
            if (value == '') {
                new Message('Por favor ingrese una cantidad valida.', 'warning');
                return;
            }
            Tools.ajaxRequest({
                data: {
                    id: row.dataset.id,
                    cantidad: value
                },
                url: 'functions/gasto/edit-cantidad',
                method: 'POST',
                waitMsg: new Wait('Cambiando cantidad...'),
                success: function(response) {
                    this.waitMsg.destroy();
                    let result = JSON.parse(response);
                    if (result['error'] == '') {
                        new Message('Cantidad del gasto cambiada con exito.', 'success');
                        row.dataset.cantidad = value;
                        row.querySelector('.gasto-cantidad-container > .cantidad').textContent = '$' + parseFloat(value).toFixed(2);
                    }
                    else {
                        new Message('Ocurrio un error al tratar de cambiar la cantidad del gasto. Por favor intentelo de nuevo.<br>' + result['error'], 'error');
                    }
                }
            })
        });
        let answerInput = oq.element.querySelector('.message-answer');
        answerInput.classList.add('inp-icon');
        answerInput.classList.add('inp-price');
        answerInput.style.paddingLeft = '28px';
        answerInput.addEventListener('focus', function() {
            this.select();
        });
        answerInput.addEventListener('blur', function() {
            if (this.value != '') {
                this.value = (parseFloat(this.value) || 0).toFixed(2);
            }
        });
        answerInput.addEventListener('keydown', function(ev) {
            if (!Tools.checkNumeric(ev, this.value)) {
                ev.preventDefault();
            }
        });
    }
});

})();