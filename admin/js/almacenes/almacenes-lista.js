(function(){

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/almacenes/ver/" + dataID;
}

if (privilegiosNivel > 2) {
    Lista.actionDelete['active'] = true;
    Lista.actionDelete['unique'] = false;
    Lista.deleteFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se eliminara el almacen seleccionada.',
            msgWait: 'Eliminando almacen.',
            msgSuccess: 'Almacen eliminado con exito.',
            msgError: 'Ocurrio un error al tratar de eliminar el almacen.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se eliminaran los almacenes seleccionadas.',
                msgWait: 'Eliminando almacenes.',
                msgSuccess: 'Almacenes eliminados con exito.',
                msgError: 'Ocurrio un error al tratar de eliminar los almacenes.'
            };
        }
        let ids = [];
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
        }
        new Confirm(requestObj.msgQuestion, 'eliminar', function() {
            Tools.ajaxRequest({
                data: {
                    ids: ids
                },
                method: 'POST',
                url: 'functions/almacen/delete',
                waitMsg: new Wait(requestObj['msgWait'], true),
                success: function(response) {
                    this.waitMsg.destroy();
                    let data = JSON.parse(response);
                    if (data['error'] == '') {
                        new Message(requestObj['msgSuccess'], 'success');
                        selecteds.forEach(function(elem, index) {
                            elem.remove();
                        });
                        Lista.removeSelections();
                    }
                    else {
                        new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                    }
                }
            });
        });
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('almacen/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let dateTimeArray = data['fechaCreate'].split(' ');
        let dateArray = dateTimeArray[0].split('-');
        let timeArray = dateTimeArray[1].split(':');
        return ['<a href="/admin/almacenes/ver/' + data['id'] + '">' + ('000' + data['folio']).slice(-4) + '</a>', dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0] + ' ' + dateTimeArray[1], data['usuarioNombre']];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']]
        ];
        return attrs;
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['Usuario', 'input', 'usuario', 'text']
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'));
let usuarioSI = new SelectInp(document.querySelector('[name="usuario"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});

Tools.ajaxRequest({
    data: {
        metodo: 'id',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            usuarioSI.setOptions(options);
        }
    }
});

SortController.load();

})();