(function() {

clearForm();

if (formMode=='EDIT' || formMode=='VIEW') {
    loadingDataMsg = new Wait('Cargando datos.');
    getAlmacen();
}

function getAlmacen() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/almacen/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadAlmacen(data['resultado']);
            }
            if (formMode == 'EDIT') {
                loadTemporal();
            }
        }
    });
}

function loadAlmacen(almacen) {
    let almacenProducts = almacen['productos'];
    document.querySelector('[name="usuario"]').value = ('000' + almacen['usuarioID']).slice(-4) + ' ' + almacen['usuarioNombre'];
    for (let i=0; i<almacenProducts.length; i++) {
        let product = almacenProducts[i];
        addProduct(product);
    }
    if (formMode == 'VIEW') {
        disableInputs();
    }
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

document.querySelector('.form-cancel').addEventListener('click', function(ev) {
    window.location = '/admin/almacenes';
});

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
}

function addProduct(productObj) {
    let decimalPlaces;
    let rowElem = document.createElement('div');
    let unitObj = Tools.unitConverter(productObj['unidad']);
    rowElem.className = 'inventario-table-row';
    rowElem.innerHTML = 
        '<div class="inventario-table-column">'+
            '<input type="text" name="nombre" disabled>'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + productObj['unidad'] + ' text-align-right" type="text" name="cantidad-inicial" decimals="' + unitObj['zeros'] + '" autocomplete="off" disabled>'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + productObj['unidad'] + ' text-align-right" type="text" name="cantidad-computada" decimals="' + unitObj['zeros'] + '" autocomplete="off" disabled>'+
        '</div>'+
        '<div class="inventario-table-column">'+
            '<input class="inp-unit inp-' + productObj['unidad'] + ' text-align-right" type="text" name="cantidad-final" decimals="' + unitObj['zeros'] + '" autocomplete="off" disabled>'+
        '</div>';
    rowElem.querySelector('[name="nombre"]').value = productObj['nombre'];
    decimalPlaces = Tools.getDecimals(parseFloat(productObj['cantidadInicial']));
    if (unitObj['zeros'] && !decimalPlaces) {
        rowElem.querySelector('[name="cantidad-inicial"]').value = parseFloat(productObj['cantidadInicial']).toFixed(0);
    }
    else {
        rowElem.querySelector('[name="cantidad-inicial"]').value = parseFloat(productObj['cantidadInicial']).toFixed(unitObj['zeros'] || decimalPlaces);
    }
    
    decimalPlaces = Tools.getDecimals(parseFloat(productObj['cantidadComputada']));
    if (unitObj['zeros'] && !decimalPlaces) {
        rowElem.querySelector('[name="cantidad-computada"]').value = parseFloat(productObj['cantidadComputada']).toFixed(0);
    }
    else {
        rowElem.querySelector('[name="cantidad-computada"]').value = parseFloat(productObj['cantidadComputada']).toFixed(unitObj['zeros'] || decimalPlaces);
    }
    
    decimalPlaces = Tools.getDecimals(parseFloat(productObj['cantidadFinal']));
    if (unitObj['zeros'] && !decimalPlaces) {
        rowElem.querySelector('[name="cantidad-final"]').value = parseFloat(productObj['cantidadFinal']).toFixed(0);
    }
    else {
        rowElem.querySelector('[name="cantidad-final"]').value = parseFloat(productObj['cantidadFinal']).toFixed(unitObj['zeros'] || decimalPlaces);
    }
    
    document.querySelector('.inventario-table').appendChild(rowElem);
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}
    
})();