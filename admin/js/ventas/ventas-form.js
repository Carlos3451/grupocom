(function() {

let productList = [];

let clienteAc;
let productosAc;

clearForm();

document.querySelector('[name="clienteID"]').setAttribute('disabled', '');
loadingDataMsg = new Wait('Cargando datos.');

getTicket();

function getTicket() {
    Tools.ajaxRequest({
        data: {
            id: editID
        },
        method: 'POST',
        url: 'functions/ticket/get',
        success: function(response) {
            loadingDataMsg.destroy();
            let data = JSON.parse(response);
            if (data['error'] == '') {
                loadTicket(data['resultado']);
            }
        }
    });
}

function loadTicket(ticket) {    
    document.querySelector('[name="clienteID"]').value = ('000' + ticket['clienteFolio']).slice(-4) + ' ' + ticket['clienteNombre'];
    document.querySelector('[name="clienteID"]').dispatchEvent(new Event('change'));
    document.querySelector('[name="subtotal"]').value = ticket['subtotal'];
    document.querySelector('[name="descuento"]').value = ticket['descuento'];
    document.querySelector('[name="total"]').value = ticket['total'];
    document.querySelector('[name="metodo"]').value = ticket['metodo'];
    document.querySelector('[name="pago"]').value = ticket['pago'];
    document.querySelector('[name="cambio"]').value = ticket['cambio'];
    let productosTickets = ticket['productos'];
    if (productosTickets['error'] == '') {
        let productos = productosTickets['resultado'];
        for (let i=0; i<productos.length; i++) {
            addProduct(productos[i]);
        }
    }
    disableInputs();
}

document.querySelector('#data-form').addEventListener('submit', function(ev) {
    ev.preventDefault();
});

function clearForm() {
    document.querySelectorAll('#data-form input').forEach(function(elem, index) {
        elem.value = '';
    });
    document.querySelectorAll('#data-form .accounting-container input').forEach(function(elem, index) {
        elem.value = '0.00';
    });
    document.querySelector('[name="metodo"]').value = '';
}

document.querySelector('.form-cancel').addEventListener('click', function(ev) {  
    window.location = '/admin/ventas';
});

function addProduct(productObj) {
    let rowElem = document.createElement('div');
    let unitObj = Tools.unitConverter(productObj['unidad']);
    rowElem.className = 'ticket-table-row';
    rowElem.setAttribute('data-producto-id', productObj['productoID']);
    rowElem.setAttribute('data-ticket-producto-id', productObj['id'] || 0);
    rowElem.setAttribute('data-producto-nombre', productObj['nombre']);
    rowElem.setAttribute('data-producto-unidad', productObj['unidad']);
    rowElem.setAttribute('data-producto-costo', productObj['costo']);
    rowElem.innerHTML = 
        '<div class="ticket-table-column">'+
            '<input type="text" name="producto-nombre" disabled>'+
        '</div>'+
        '<div class="ticket-table-column">'+
            '<input class="inp-unit inp-' + productObj['unidad'] + ' text-align-right" type="text" name="producto-cantidad" decimals="' + unitObj['zeros'] + '" autocomplete="off">'+
        '</div>'+
        '<div class="ticket-table-column prices-container">'+
        '</div>'+
        '<div class="ticket-table-column">'+
            '<input class="inp-icon inp-price" type="text" name="producto-total" value="0.00" disabled>'+
        '</div>';
    let priceInput = document.createElement('input');
    let decimalPlaces =  Tools.getDecimals(parseFloat(productObj['cantidad']));
    priceInput.className = 'inp-icon inp-price';
    priceInput.setAttribute('name', 'producto-precio');
    priceInput.setAttribute('disabled', '');
    priceInput.value = productObj['precio'] || '0.00';
    rowElem.querySelector('.prices-container').appendChild(priceInput);
    if (unitObj['zeros'] && !decimalPlaces) {
        rowElem.querySelector('[name="producto-cantidad"]').value = parseFloat(productObj['cantidad']).toFixed(0);
    }
    else {
        rowElem.querySelector('[name="producto-cantidad"]').value = parseFloat(productObj['cantidad']).toFixed(unitObj['zeros'] || decimalPlaces);
    }
    rowElem.querySelector('[name="producto-total"]').value = productObj['total'];
    rowElem.querySelector('[name="producto-nombre"]').value = productObj['nombre'];

    document.querySelector('.ticket-table').appendChild(rowElem);
}

function orderTicket(order) {
    let tempFrag = document.createDocumentFragment();
    for (let i=0; i<order.length; i++) {
        let row = document.querySelector('.ticket-table-row[data-ticket-producto-id="' + order[i] +'"]');
        tempFrag.appendChild(row);
    }
    document.querySelector('.ticket-table').appendChild(tempFrag);
}

function disableInputs() {
    document.querySelectorAll('input, select').forEach(function(elem, index) {
        elem.setAttribute('disabled', '');
        elem.setAttribute('transparent', '');
    });    
}

/* NUMERIC */

function keypressIsNumber(ev) {
    let value = this.value;
    if (!Tools.checkNumeric(ev, value)) {
        ev.preventDefault();
    }
}

document.querySelectorAll('[numeric]').forEach(function(elem, index) {
    elem.addEventListener('keypress', keypressIsNumber);
    elem.addEventListener('keydown', keypressIsNumber);
});
    
})();