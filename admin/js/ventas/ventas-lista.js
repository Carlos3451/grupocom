(function(){

let newsTimeout = null;

Lista.actionView['active'] = true;
Lista.actionView['unique'] = true;
Lista.viewFn = function(selecteds) {
    let dataID = selecteds[0].getAttribute('data-id');
    window.location = "/admin/ventas/ver/" + dataID;
}

if (privilegios.indexOf('facturacion') != -1) {

    Lista.actionFacturar['active'] = true;
    Lista.actionFacturar['unique'] = true;
    Lista.facturarFn = function(selecteds) {
        let dataID = selecteds[0].getAttribute('data-id');
        window.location = '/admin/facturas-v4/agregar/?tipo=mostrador&id=' + dataID;
    }

}

Lista.actionPrint['active'] = true;
Lista.actionPrint['unique'] = false;
Lista.printFn = function(selecteds) {
    let ids = [];
    cancelleds = 0;
    for (let i=0; i<selecteds.length; i++) {
        ids.push(selecteds[i].getAttribute('data-id'));
    }
    window.open('/admin/ventas/imprimir/' + ids.join('+'));
}

if (privilegiosNivel > 2) {
    Lista.actionCancel['active'] = true;
    Lista.actionCancel['unique'] = false;
    Lista.cancelFn = function(selecteds) {
        let requestObj = {
            msgQuestion: 'Se cancelara la venta seleccionada.',
            msgWait: 'Cancelando venta.',
            msgSuccess: 'Venta cancelada con exito.',
            msgError: 'Ocurrio un error al tratar de cancelar la venta.' 
        };
        if (selecteds.length > 1) {
            requestObj = {
                msgQuestion: 'Se cancelaran las ventas seleccionadas.',
                msgWait: 'Cancelando ventas.',
                msgSuccess: 'Ventas canceladas con exito.',
                msgError: 'Ocurrio un error al tratar de cancelar las ventas.'
            };
        }
        let ids = [];
        let cancelleds = 0;
        for (let i=0; i<selecteds.length; i++) {
            ids.push(selecteds[i].getAttribute('data-id'));
            if (selecteds[i].hasAttribute('cancelled')) {
                cancelleds += 1;
            }
        }
        if (cancelleds == 0) {
            new Confirm(requestObj.msgQuestion, 'cancelar', function() {
                Tools.ajaxRequest({
                    data: {
                        ids: ids
                    },
                    method: 'POST',
                    url: 'functions/ticket/cancel',
                    waitMsg: new Wait(requestObj['msgWait'], true),
                    success: function(response) {
                        this.waitMsg.destroy();
                        let data = JSON.parse(response);
                        if (data['error'] == '') {
                            new Message(requestObj['msgSuccess'], 'success');
                            selecteds.forEach(function(elem, index) {
                                elem.setAttribute('cancelled', '');
                            });
                            Lista.removeSelections();
                        }
                        else {
                            new Message(requestObj['msgError'] + '<br>' + data['error'], 'error');
                        }
                    }
                });
            });
        }
        else {
            if (selecteds.length == 1) {
                new Message('No se puede cancelar una venta ya cancelado.', 'warning');
            }
            else if (selecteds.length == cancelleds) {
                new Message('No se pueden cancelar ventas ya cancelados.', 'warning');
            }
            else {
                new Message('Existen algunas ventas seleccionados que ya estan cancelados.', 'warning');                
            }
        }
    }
}

SortController.setOnMethodChange(function() {
    Lista.load('ticket/get-all', {
        busqueda: SortController.search,
        metodo: SortController.method,
        orden: SortController.order,
        filtros: SortController.filters
    }, function(data) {
        let dateTimeArray = data['fechaCreate'].split(' ');
        let dateArray = dateTimeArray[0].split('-');
        let timeArray = dateTimeArray[1].split(':');
        let entrega = data['domicilio'] == 1 ? 'Domicilio' : 'Mostrador';
        return [
            '<a href="/admin/ventas/ver/' + data['id'] + '">' + ('00000' + data['folio']).slice(-6) + '</a>',
            dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0] + ' ' + dateTimeArray[1],
            '<a href="/admin/clientes/ver/' + data['clienteID'] + '">' +  data['clienteNombre'] + '</a>',
            data['usuarioNombre'],
            entrega,
            data['tipo'][0].toUpperCase() + data['tipo'].slice(1),
            '<p class="text-align-right">$ ' + data['total'] + '</p>'
        ];
    }, function(data) {
        let attrs = [
            ['data-id', data['id']],
            ['data-total', data['total']]
        ];
        if (data['cancelado'] == 1) {
            attrs.push(['cancelled', '']);
        }
        return attrs;
    }, function(response) {
        getNewsVentas(0);
    });
});

SortController.setMethods([
    ['Folio', 'folio'],
    ['Fecha', 'fecha'],
]);

SortController.setFilterOptions([
    [
        ['Fecha', 'input', 'fecha', 'text'],
        ['Cliente', 'input', 'cliente', 'text'],
        ['Usuario', 'input', 'usuario', 'text']
    ],
    [
        ['Tipo', 'select', 'tipo', [
                ['Todos', ''],
                ['Contado', 'contado'],
                ['Credito', 'credito']
            ]
        ],
        ['Entrega', 'select', 'entrega', [
                ['Todos', -1],
                ['Mostrador', 0],
                ['Domicilio', 1]
            ]
        ]
    ]
]);

new DatePicker(document.querySelector('[name="fecha"]'), {
    monthSelect: true,
    yearSelect: true
});
let clienteSI = new SelectInp(document.querySelector('[name="cliente"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});
let usuarioSI = new SelectInp(document.querySelector('[name="usuario"]'), {
    placeholder: 'Todos',
    defaultValue: 0
});

Tools.ajaxRequest({
    data: {
        metodo: 'folio',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/cliente/get-all-min',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let clientes = data['resultado'];
            for (let i=0; i<clientes.length; i++) {
                options.push({
                    'name': ('000' + clientes[i]['folio']).slice(-4) + ' ' + clientes[i]['nombre'],
                    'value': clientes[i]['id'],
                    'keys': [('000' + clientes[i]['folio']).slice(-4), clientes[i]['nombre']]
                });
            }
            clienteSI.setOptions(options);
        }
    }
});
Tools.ajaxRequest({
    data: {
        metodo: 'id',
        orden: 'asc',
        busqueda: ''
    },
    method: 'POST',
    url: 'functions/credencial/get-all',
    success: function(response) {
        let data = JSON.parse(response);
        if (data['error'] == '') {
            let options = [];
            let usuarios = data['resultado'];
            for (let i=0; i<usuarios.length; i++) {
                options.push({
                    'name': ('000' + usuarios[i]['id']).slice(-4) + ' ' + usuarios[i]['nombre'],
                    'value': usuarios[i]['id'],
                    'keys': [('000' + usuarios[i]['id']).slice(-4), usuarios[i]['nombre']]
                });
            }
            usuarioSI.setOptions(options);
        }
    }
});

SortController.load();

function getNewsVentas(idMax) {
    Tools.ajaxRequest({
        data: {
            busqueda: SortController.search,
            metodo: SortController.method,
            orden: SortController.order,
            filtros: SortController.filters,
            ultimoID: idMax
        },
        method: 'POST',
        url: 'functions/ticket/get-news',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                if (idMax != 0) {
                    document.querySelector('.list-news').classList.add('open');
                    if (data['resultado']['nuevos'] == 1) {
                        document.querySelector('.list-news > a').textContent = '1 venta nueva. Haz click aquí para recargar.';
                    }
                    else {
                        document.querySelector('.list-news > a').textContent = data['resultado']['nuevos'] + ' ventas nuevas. Haz click aquí para recargar.';
                    }
                }
                else {
                    idMax = data['resultado']['idMax'];
                }
            }
            if (newsTimeout) {
                clearTimeout(newsTimeout);
            }
            newsTimeout = setTimeout(function() {
                getNewsVentas(idMax);
            }, 30000);
        }
    });
}

})();