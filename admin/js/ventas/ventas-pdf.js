
var objectElem = document.querySelector('object');
var embedElem = document.querySelector('embed');

var margin = {top:10, left:10, bottom:10, right:10};

var logoData = '';
var logoSize = {width:0, height:0};

var pdf;

var originalIDs = idsStr.split('+');
var pedidosIDs = idsStr.split('+');

if (originalIDs.length > 1) {
    document.title = "Imprimir Pedidos";
}

console.log(originalIDs);

toDataURL('/config/logotipo.png', function(dataUrl) {
    let image = new Image();
    image.src = dataUrl;
    logoData = dataUrl;
    image.onload = function() {
        logoSize.width = this.width;
        logoSize.height = this.height;
        generatePDFs(pedidosIDs.shift());
    }
});

function toDataURL(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function() {
        let reader = new FileReader();
        reader.onloadend = function() {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

function generatePDFs(pedidoID) {
    let pedidoData;

    let productosNum = 0;

    let pagado = 0;

    Tools.ajaxRequest({
        data: {
            id: pedidoID
        },
        method: 'POST',
        url: 'functions/ticket/get-print',
        success: function(response) {
            let data = JSON.parse(response);
            if (data['error'] == '') {
                pedidoData = data['resultado'];
                if (originalIDs.length == 1) {
                    document.title = 'F-' + ('0000' + pedidoData['folio']).slice(-5);
                }
                try {
                    createPDF();
                }
                catch (err) {
                    console.log(err);
                }
            }
            else {
                console.log(data['error']);
            }
        }
    });

    function createPDF() {
        if (!pdf) {
            pdf = new jsPDF({
                format: 'letter'
            });
        }

        let pageMax = 1;
        let productsMax = 38;
        let emptyPageProductsMax = 57;

        if (!pedidoData['productos']['resultado']) {
            pedidoData['productos']['resultado'] = [];
        }

        /*

        console.log(pedidoData['pedidos']['resultado']);

        pedidoData['pedidos']['resultado'] = [];

        for (let i=0; i<38; i++) {
            pedidoData['pedidos']['resultado'][i] = {
                cantidad: '6.000',
                costo: '56.78',
                folio: i + 1,
                id: 479,
                nombre: 'CERILLOS',
                precio: '67.00',
                productoID: 1,
                total: '402.00',
                unidad: 'pieza'
            }
        }

        */

        /*
        if (pedidoData['pagos']['error'] == '') {
            pedidoData['pagos']['resultado'].forEach(function(pago, index) {
                if (!pago['cancelado']) {
                    pagado += parseFloat(pago['cantidad']);
                }
            });
        }
        */

        if (pedidoData['productos']['error'] == '') {
            pageMax = Math.ceil(pedidoData['productos']['resultado'].length / productsMax);
        }

        let productOffset = 0;

        for (let i=0; i<pageMax; i++) {
            createPage(pdf, i, pageMax, productsMax, productsMax * i);
        }

        /*

        let pageMode = 0;

        if (pedidoData['pedidos']['error'] == '') {
            pageMax = Math.ceil(pedidoData['pedidos']['resultado'].length / productsMax);
            let productsClampeds = ((pedidoData['pedidos']['resultado'].length / (productsMax + emptyPageProductsMax)) % 1) * (productsMax + emptyPageProductsMax);
            if (pageMax > 2) {
                if (productsClampeds > productsMax) {
                    pageMax = Math.ceil((pedidoData['pedidos']['resultado'].length - productsMax) / emptyPageProductsMax) + 1;
                    pageMode = 1;
                }
            }
        }

        for (let i=0; i<pageMax; i++) {
            if (pageMode == 0) {
                let pageProductsMax = Math.ceil(pedidoData['pedidos']['resultado'].length / pageMax);
                createPage(pdf, i, pageMax, pageProductsMax, pageProductsMax * i);
            }
            else {
                let pageProductsMax = Math.ceil((pedidoData['pedidos']['resultado'].length - productsMax) / (pageMax - 1));
                console.log(pedidoData['pedidos']['resultado'].length);
                if (i != pageMax - 1) {
                    createPage(pdf, i, pageMax, pageProductsMax, pageProductsMax * i);
                }
                else {
                    createPage(pdf, i, pageMax, productsMax, pageProductsMax * (pageMax - 1));
                }
            }
        }
        */

        //console.log(productosNum, pedidoData['pedidos']['resultado'].length);
        if (pedidosIDs.length != 0) {            
            pdf.addPage();
            generatePDFs(pedidosIDs.shift());
        }
        else {
            let name;
            if (originalIDs.length == 1) {
                name = 'F-' + ('0000' + pedidoData['folio']).slice(-5);
            }
            else {
                name = "Pedidos";
            }
            savePDF(name);
        }
    }

    function createPage(pdf, page, pageMax, productsMax, productsOffset) {

        let pageSize = {width:215.9, height:279.4};
        
        let lineY = 0;
        let lineHeight = 4;

        let logoY = 8;
        let logoHeight = 16;
        let logoTextLeft = 48;

        let pedidoDataLeft = 200;
        let pedidoDataFecha = new Date(pedidoData['fechaCreate']);
        let pedidoFecha = ('00' + pedidoDataFecha.getDate()).slice(-2) + '/' + ('00' + (pedidoDataFecha.getMonth() + 1)).slice(-2) + '/' + pedidoDataFecha.getFullYear();
        let pedidoHora = ('0' + pedidoDataFecha.getHours()).slice(-2) + ':' + ('0' + pedidoDataFecha.getMinutes()).slice(-2) + ':' + ('0' + pedidoDataFecha.getSeconds()).slice(-2);
        
        let clientDataY = 28;
        let clientDataSep = 18;
        let clientName = pedidoData['clienteNombre'];

        let pedidoProductsTop = 44;
        let pedidoProductsHeight = 4;
        let pedidoProductsLeft = margin.left;
        let pedidoProductsSizes = [15, 25, 110, 20, 20];
        let pedidoProductsNames = ['Folio', 'Cantidad', 'Nombre', 'Precio', 'Total'];
        let pedidoProductsKeys = ['productoFolio', 'cantidad', 'nombre', 'precio', 'total'];

        let totalsTop = 200; 
        let totalsRight = 18;
        let totalsLeft = 175;
        
        let stepsTop = 214;

        /* LOGOTIPO */

        pdf.addImage(logoData, 'PNG', margin.left, logoY, logoHeight * (logoSize.width / logoSize.height), logoHeight);

        lineY = logoY + 4;

        pdf.setFontSize(8);
        pdf.setFontStyle('bold');
        pdf.text('Grupo Frontera', logoTextLeft, lineY);
        lineY += lineHeight;
        pdf.text('Dirección', logoTextLeft, lineY);
        lineY += lineHeight;
        pdf.text('Mazatlán, Sinaloa, Teléfono', logoTextLeft, lineY);

        lineY = logoY + 4;

        pdf.text('F-' + ('0000' + pedidoData['folio']).slice(-5), pedidoDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        pdf.text(pedidoFecha + ' ' + pedidoHora, pedidoDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        pdf.text('Venta de Mostrador', pedidoDataLeft, lineY, { align: 'right' });
        lineY += lineHeight;
        pdf.text(pedidoData['usuarioNombre'], pedidoDataLeft, lineY, { align: 'right' });

        if (pageMax > 1) {
            lineY += lineHeight;
            pdf.text('Página ' + (page + 1) + ' / ' + pageMax, pedidoDataLeft, lineY, { align: 'right' });
        }


        /* CLIENTE */

        lineY = clientDataY;

        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        pdf.text('CLIENTE:', margin.left, lineY);
        lineY += lineHeight;
        pdf.text('DOMICILIO:', margin.left, lineY);
        lineY += lineHeight;
        pdf.text('COLONIA:', margin.left, lineY);

        lineY = clientDataY;

        pdf.text('CIUDAD:', margin.left + (pageSize.width / 2), lineY);
        lineY += lineHeight;
        pdf.text('TELEFONO:', margin.left + (pageSize.width / 2), lineY);

        lineY = clientDataY;

        pdf.setFontSize(9);
        pdf.setFontStyle('normal');

        let clienteDomicilio = [];
        if (pedidoData['clienteCalle'] != '') { clienteDomicilio.push(pedidoData['clienteCalle']); }
        if (pedidoData['clienteNumeroExterior'] != '') { clienteDomicilio.push('#' + pedidoData['clienteNumeroExterior']); }
        if (pedidoData['clienteNumeroInterior'] != '') { clienteDomicilio.push('Int. ' + pedidoData['clienteNumeroInterior']); }
        clienteDomicilio = clienteDomicilio.join(' ');
        
        pdf.text(('000' + (pedidoData['clienteFolio'] || '')).slice(-4) + ' ' + (clientName || '').toUpperCase(), margin.left + clientDataSep, lineY);
        lineY += lineHeight;
        pdf.text(clienteDomicilio.toUpperCase(), margin.left + clientDataSep, lineY);
        lineY += lineHeight;
        pdf.text((pedidoData['clienteColonia'] || '').toUpperCase(), margin.left + clientDataSep, lineY);

        lineY = clientDataY;

        pdf.text(((pedidoData['clienteCiudad'] || '') + ', ' + pedidoData['clienteEstado']).toUpperCase(), margin.left + clientDataSep + (pageSize.width / 2), lineY);
        lineY += lineHeight;
        pdf.text((pedidoData['clienteTelefono'] || '').toUpperCase(), margin.left + clientDataSep + (pageSize.width / 2), lineY);
        lineY += lineHeight;

        pdf.line(margin.left, lineY + 3, 200, lineY + 3);

        /* PRODUCTOS */
        
        pdf.setFontSize(8);
        pdf.setFontStyle('bold');

        let columnSep = pedidoProductsLeft;
        let columnTextSep = 2;
        //pdf.line(columnSep, pedidoProductsTop - 4, columnSep, pedidoProductsTop + 32);
        pedidoProductsKeys.forEach(function(key, index) {
            //pdf.line(columnSep + pedidoProductsSizes[index], pedidoProductsTop - 4, columnSep + pedidoProductsSizes[index], pedidoProductsTop + 32);
            if (key == 'precio' || key == 'total' || key == 'cantidad') {
                pdf.text(pedidoProductsNames[index], columnSep + pedidoProductsSizes[index] - columnTextSep, pedidoProductsTop, { align: 'right' });
            }
            else {
                pdf.text(pedidoProductsNames[index], columnSep + columnTextSep, pedidoProductsTop);
            }
            columnSep += pedidoProductsSizes[index];
        });

        pdf.setFontSize(9);
        pdf.setFontStyle('normal');

        if (pedidoData['productos']['error'] == '') {
            let productos = pedidoData['productos']['resultado'];
            lineY = pedidoProductsHeight;
            for (let i=0; i<Math.min(productsMax, productos.length - productsOffset); i++) {
                let producto = productos[i + productsOffset];
                //productosNum++;
                if (producto['pieza'] == 1) {
                    producto['unidad'] = 'caja';
                }
                columnSep = pedidoProductsLeft;
                pedidoProductsKeys.forEach(function(key, index) {
                    switch (key) {
                        case 'cantidad': {
                            let unitObj = Tools.unitConverter(producto['unidad']);
                            let decimalPlaces =  Tools.getDecimals(parseFloat(producto['cantidad']));
                            if (unitObj['zeros'] && !decimalPlaces) {
                                pdf.text(parseFloat(producto['cantidad']).toFixed(0), columnSep + pedidoProductsSizes[index] - columnTextSep - 14, pedidoProductsTop + lineY, { align: 'right' });
                            }
                            else {
                                pdf.text(parseFloat(producto['cantidad']).toFixed(unitObj['zeros'] || decimalPlaces), columnSep + pedidoProductsSizes[index] - columnTextSep - 14, pedidoProductsTop + lineY, { align: 'right' });
                            }
                            pdf.text(producto['cantidad'] == 1 ? unitObj['singular'] : unitObj['plural'], columnSep + pedidoProductsSizes[index] - columnTextSep - 6, pedidoProductsTop + lineY);
                            break;
                        }
                        case 'productoFolio':
                            pdf.text(producto['productoFolio'] == null ? '[ELIM]' : ('000' + producto['productoFolio']).slice(-4), columnSep + columnTextSep, pedidoProductsTop + lineY);
                            //pdf.text(String(i + productsOffset), columnSep + columnTextSep, pedidoProductsTop + lineY);
                            break;
                        case 'nombre':
                            pdf.text(producto['nombre'].length > 63 ? (producto['nombre'].slice(0, 60) + '...') : producto['nombre'], columnSep + columnTextSep, pedidoProductsTop + lineY);
                            break;
                        case 'precio':
                        case 'total':
                            pdf.text('$ ' + parseFloat(producto[key]).toFixed(2), columnSep + pedidoProductsSizes[index] - columnTextSep, pedidoProductsTop + lineY, { align: 'right' });
                            break;
                    }
                    columnSep += pedidoProductsSizes[index];
                });
                lineY += pedidoProductsHeight;
            }
        }

        if (page == pageMax - 1) {
            /* TOTAL */

            let pedidoTotal = 0;
            
            let productos = pedidoData['productos']['resultado'];

            for (let i=0; i<productos.length; i++) {
                pedidoTotal += parseFloat(productos[i]['total']);
            }

            pdf.setFontSize(8);
            pdf.setFontStyle('normal');

            lineY = totalsTop;
            pdf.text('$ ' + parseFloat(pedidoTotal).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('$ ' + parseFloat(pedidoData['pago']).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('$ ' + parseFloat(pedidoData['cambio']).toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            //pdf.text('$ ' + pagado.toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            //lineY += lineHeight;
            //pdf.text('$ ' + pedidoResto.toFixed(2), pageSize.width - totalsRight, lineY, { align: 'right' });
            
            pdf.setFontStyle('bold');

            lineY = totalsTop;
            pdf.text('Total', totalsLeft, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('Pago', totalsLeft, lineY, { align: 'right' });
            lineY += lineHeight;
            pdf.text('Cambio', totalsLeft, lineY, { align: 'right' });
            //pdf.text('Pagado', totalsLeft, lineY, { align: 'right' });
            //lineY += lineHeight;
            //pdf.text('Resto', totalsLeft, lineY, { align: 'right' });

            lineY = stepsTop;

            pdf.setFontStyle('normal');

            pdf.text('1 - CHECAR BIEN SU MERCANCIA AL MOMENTO DE RECIBIRLA', margin.left, lineY);
            lineY += lineHeight;
            pdf.text('2 - NO REALIZAR PAGOS NI ABONOS SIN FACTURA ORIGINAL', margin.left, lineY);
            lineY += lineHeight;
            pdf.text('3 - VERIFIQUE QUE SU ABONO SEA ANOTADO EN LA FACTURA', margin.left, lineY);
            lineY += lineHeight;
            
            //PAGARE

            pdf.setFontStyle('bold');
            
            pdf.line(margin.left, lineY - 1, pageSize.width - margin.right, lineY - 1);
            lineY += lineHeight;
            pdf.text('PAGARE', margin.left, lineY);
            //pdf.text('BUENO POR: $ ' + parseFloat(pedidoData['total']).toFixed(2), margin.left + 50, lineY);
            pdf.text('BUENO POR: $ ' + parseFloat(pedidoTotal).toFixed(2), margin.left + 50, lineY);
            pdf.text('F-' + ('0000' + pedidoData['folio']).slice(-5), margin.left + 100, lineY);
            lineY += lineHeight;
            lineY += lineHeight;
            pdf.setFontStyle('normal');
            pdf.text('Debe(mos) y pagare(mos) incondicionalmente por este pagare a la orden de GRUPO FRONTERA en la ciudad de Mazatlán, Sin. el ' + pedidoFecha + ' o en cualquier otra que se me requiera la cantidad de ' + Tools.numeroALetras(pedidoTotal) + ' Valor recibido a mi (nuestra) entera satisfacción. Se solventaran las obligaciones adquiridas en este pagare entregando su equivalente en moneda nacional a la fecha de pago. De no verificarse el pago de la cantidad que este pagare expresa el día de su vencimiento causara interés moratorios al tipo de 5% mensuales (cinco por ciento) pagadero en esta ciudad. Este pagare es mercantil y esta regido por la Ley General de Títulos y Operaciones de Crédito en su articulo 173 y artículos correlativos, por no ser pagare domiciliado.', margin.left, lineY, { maxWidth: (pageSize.width - margin.left - margin.right) });
            pdf.line(80, lineY + 30, pageSize.width - 80, lineY + 30);
            pdf.setFontStyle('bold');
            pdf.text(clientName || '', pageSize.width / 2, lineY + 34, { align: 'center' });
        }

        if (page < pageMax - 1) {
            pdf.addPage();
        }

    }
}

function savePDF(name) {
    let file = new File([pdf.output('blob')], name + '.pdf', {type: 'application/pdf'});
    let fileURI = URL.createObjectURL(file);

    objectElem.setAttribute('data', fileURI);
    embedElem.setAttribute('src', fileURI);
}