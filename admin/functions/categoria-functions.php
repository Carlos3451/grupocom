<?php
	include_once('classes/Categoria.php');
	$categoriaObj = new Categoria();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
    switch ($function) {
        case 'get-all':
            echo json_encode($categoriaObj->get_all($_POST));
            break;
    }
	if ((in_array('productos', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($categoriaObj->add($_POST));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($categoriaObj->edit($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'delete':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($categoriaObj->delete($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
		}
	}
?>