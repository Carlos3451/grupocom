<?php
	include_once('classes/Login.php');
	$loginObj = new Login();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	switch ($function) {
		case 'check':
			echo json_encode($loginObj->check($_POST));
			break;
		case 'logout':
			echo json_encode($loginObj->logout());
			break;
		case 'change-section':
			echo json_encode($loginObj->change_section($_POST));
			break;
		case 'keep-online':
			if (isset($GLOBALS['usuario'])) {
				echo json_encode($loginObj->keep_online());
			}
			else {
				echo '{"error":"SESSION_CLOSED"}';
			}
			break;
	}
?>