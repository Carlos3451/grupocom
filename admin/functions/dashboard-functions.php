<?php
	include_once('classes/Dashboard.php');
	$obj = new Dashboard();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if (in_array('pedidos', $GLOBALS['usuario']['privilegios']) || in_array('ventas', $GLOBALS['usuario']['privilegios'])) {
		switch ($function) {
            case 'get-sales':
                echo json_encode($obj->get_sales($_POST));
                break;
            case 'get-payments':
                echo json_encode($obj->get_payments($_POST));
                break;
            case 'get-products-sales-top':
                echo json_encode($obj->get_products_sales_top($_POST));
                break;
            case 'get-products-sales-top-mostrador':
                echo json_encode($obj->get_products_sales_top_mostrador($_POST));
                break;
            case 'get-salesmans-sales':
                if ($GLOBALS['usuario']['privilegiosNivel'] == 4) {
                    echo json_encode($obj->get_salesmans_sales($_POST));
                }
                else {
					echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'get-deliveries-sales':
                if ($GLOBALS['usuario']['privilegiosNivel'] == 4) {
                    echo json_encode($obj->get_deliveries_sales($_POST));
                }
                else {
					echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'get-products-sales':
                echo json_encode($obj->get_products_sales($_POST));
                break;
		}
    }
?>