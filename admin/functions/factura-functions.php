<?php
	include_once('classes/Factura.php');
	$obj = new Factura();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
    }
	if (in_array('facturacion', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['seccionActual']['facturacion'] == 1) {
		switch ($function) {
            case 'get-sat-data':
                echo json_encode($obj->get_sat_data($_POST));
                break;
            case 'get-formas-pago':
                echo json_encode($obj->get_formas_pago($_POST));
                break;
            case 'claves-sat-get-all':
                echo json_encode($obj->claves_sat_get_all($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
            case 'get-all-complementos':
                echo json_encode($obj->get_all_complementos($_POST));
                break;
            case 'get-all-clients':
                echo json_encode($obj->get_all_clients($_POST));
                break;
            case 'get-all-access':
                echo json_encode($obj->get_all_access());
                break;
            case 'facturar':
                echo json_encode($obj->facturar($_POST));
                break;
            case 'add-complemento':
                echo json_encode($obj->add_complemento($_POST));
                break;
            case 'add-complemento-multiples':
                echo json_encode($obj->add_complemento_multiples($_POST));
                break;
            case 'get-complementos':
                echo json_encode($obj->get_complementos($_POST));
                break;
            case 'complemento-cancel':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->complemento_cancel($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'cancel':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->cancel($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'pdf':
                echo $obj->pdf($_POST);
                break;
            case 'xml':
                echo $obj->xml($_POST);
                break;
            case 'email':
                echo json_encode($obj->email($_POST));
                break;
            case 'change-pay-status':
                echo json_encode($obj->change_pay_status($_POST));
                break;
            case 'add-client':
                echo json_encode($obj->add_client($_POST));
                break;
            case 'edit-client':
                echo json_encode($obj->edit_client($_POST));
                break;
            case 'get-global-clientes':
                echo json_encode($obj->get_global_clientes($_POST));
                break;
            case 'get-global-facturas':
                echo json_encode($obj->get_global_facturas($_POST));
                break;
        }
    }
    else if (in_array('reportes', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['seccionActual']['facturacion'] == 1) {
		switch ($function) {
            case 'claves-sat-get-all':
                echo json_encode($obj->claves_sat_get_all($_POST));
                break;
            case 'get-all-access':
                var_dump('here');
                echo json_encode($obj->get_all_access());
                break;
            case 'get-global-clientes':
                echo json_encode($obj->get_global_clientes($_POST));
                break;
            case 'get-global-facturas':
                echo json_encode($obj->get_global_facturas($_POST));
                break;
        }
    }
    else if (in_array('productos', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['seccionActual']['facturacion'] == 1) {
		switch ($function) {
            case 'claves-sat-get-all':
                echo json_encode($obj->claves_sat_get_all($_POST));
                break;
        }
    }
?>