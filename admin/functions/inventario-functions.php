<?php
	include_once('classes/Inventario.php');
	$obj = new Inventario();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ((in_array('productos', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                break;
            case 'get':
                echo json_encode($obj->get($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'delete':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->delete($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
		}
	}
?>