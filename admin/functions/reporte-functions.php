<?php
	include_once('classes/Reporte.php');
	$obj = new Reporte();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if (in_array('reportes', $GLOBALS['usuario']['privilegios'])) {
		switch ($function) {
            case 'get-products-sales':
                echo json_encode($obj->get_products_sales($_POST));
                break;
            case 'get-pedidos':
                echo json_encode($obj->get_pedidos($_POST));
                break;
            case 'get-mostrador':
                echo json_encode($obj->get_mostrador($_POST));
                break;
            case 'get-ganancias':
                echo json_encode($obj->get_ganancias($_POST));
                break;
            case 'get-gastos':
                echo json_encode($obj->get_gastos($_POST));
                break;
            case 'get-compras':
                echo json_encode($obj->get_compras($_POST));
                break;
		}
    }
?>