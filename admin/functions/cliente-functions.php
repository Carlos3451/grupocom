<?php
	include_once('classes/Cliente.php');
	$clienteObj = new Cliente();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	switch ($function) {
		case 'get-all':
			echo json_encode($clienteObj->get_all($_POST));
			break;
		case 'get-all-list':
			echo json_encode($clienteObj->get_all_list($_POST));
			break;
		case 'get-all-min':
			echo json_encode($clienteObj->get_all_min($_POST));
			break;
		case 'get-all-facturar':
			echo json_encode($clienteObj->get_all_facturar($_POST));
			break;
		case 'get-all-routeless':
			echo json_encode($clienteObj->get_all_routeless($_POST));
			break;
	}
	if ((in_array('clientes', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
			case 'add':
				echo json_encode($clienteObj->add($_POST));
				break;
			case 'get':
				echo json_encode($clienteObj->get($_POST));
				break;
			case 'edit':
				if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
					echo json_encode($clienteObj->edit($_POST));
				}
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
				break;
			case 'delete':
				if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
					echo json_encode($clienteObj->delete($_POST));
				}
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
				break;
			case 'lock':
				if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
					echo json_encode($clienteObj->lock($_POST));
				}
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
				break;
			case 'unlock':
				if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
					echo json_encode($clienteObj->unlock($_POST));
				}
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
				break;
		}
	}
?>