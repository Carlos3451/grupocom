<?php
	include_once('classes/Almacen.php');
	$obj = new Almacen();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ((in_array('pedidos', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                break;
            case 'get':
                echo json_encode($obj->get($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
		}
	}
?>