<?php
	include_once('classes/Caja.php');
	$obj = new Caja();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ((in_array('ventas', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'open':
                echo json_encode($obj->open($_POST));
                break;
            case 'close':
                echo json_encode($obj->close($_POST));
                break;
		}
	}
?>