<?php
	include_once('classes/Ruta.php');
	$obj = new Ruta();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
    switch ($function) {
        case 'get-all':
            echo json_encode($obj->get_all($_POST));
            break;
    }
	if ((in_array('rutas', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                break;
            case 'get':
                echo json_encode($obj->get($_POST));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'delete':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->delete($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'set-clients-route':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->set_clients_route($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'unset-clients-route':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->unset_clients_route($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'change-client-route-days':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->change_client_route_days($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
		}
	}
?>