<?php
	include_once('classes/Seccion.php');
	$obj = new Seccion();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ($GLOBALS['usuario']['id'] == 1) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST, $_FILES));
                break;
            case 'get':
                echo json_encode($obj->get($_POST));
                break;
            case 'delete':
                echo json_encode($obj->delete($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
            case 'edit':
                echo json_encode($obj->edit($_POST, $_FILES));
                break;
        }
    }
    else {
        echo '{"error":"NO_PRIVILEGES"}';
    }
?>