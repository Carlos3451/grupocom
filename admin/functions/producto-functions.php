<?php
    include_once('classes/Producto.php');
    $productoObj = new Producto();
    $function = isset($_GET['function']) ? $_GET['function'] : '';
    if ($function=='') {
        exit;
    }
    switch ($function) {
        case 'get-all':
            echo json_encode($productoObj->get_all($_POST));
            break;
        case 'get-all-list':
            echo json_encode($productoObj->get_all_list($_POST));
            break;
        case 'get-all-min':
            echo json_encode($productoObj->get_all_min($_POST));
            break;
        case 'get-all-facturar':
            echo json_encode($productoObj->get_all_facturar($_POST));
            break;
    }
    if ((in_array('productos', $GLOBALS['usuario']['privilegios']))) {
        switch ($function) {
            case 'add':
                echo json_encode($productoObj->add($_POST, $_FILES));
                break;
            case 'add-tiny':
                echo json_encode($productoObj->add_tiny($_POST, $_FILES));
                break;
            case 'get':
                echo json_encode($productoObj->get($_POST));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($productoObj->edit($_POST, $_FILES));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'change-prices':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($productoObj->change_prices($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'add-existence':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($productoObj->add_existence($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'lock':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($productoObj->lock($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'unlock':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($productoObj->unlock($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'delete':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($productoObj->delete($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
        }
    }
?>