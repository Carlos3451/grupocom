<?php
	include_once('classes/Ticket.php');
	$obj = new Ticket();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ((in_array('facturacion', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'get-all-facturar':
                echo json_encode($obj->get_all_facturar($_POST));
                return;
        }
    }
	if ((in_array('entregas', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'set-delivery':
                echo json_encode($obj->set_delivery($_POST));
                return;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                return;
        }
    }
	if ((in_array('ventas', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                return;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                return;
            case 'get':
                echo json_encode($obj->get($_POST));
                return;
            case 'get-print':
                echo json_encode($obj->get_print($_POST));
                break;
            case 'get-facturar':
                echo json_encode($obj->get_facturar($_POST));
                return;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                return;
            case 'get-all-desktop':
                echo json_encode($obj->get_all_desktop($_POST));
                return;
            case 'get-news':
                echo json_encode($obj->get_news($_POST));
                return;
            case 'pay':
                echo json_encode($obj->pay($_POST));
                return;
            case 'pay-cancel':
                echo json_encode($obj->pay_cancel($_POST));
                return;
            case 'cancel':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->cancel($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                return;
		}
	}
?>