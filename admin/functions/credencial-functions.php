<?php
	include_once('classes/Credencial.php');
	$credencialObj = new Credencial();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	switch ($function) {
		case 'get-all':
			echo json_encode($credencialObj->get_all($_POST));
			break;
		case 'get-dealers':
			echo json_encode($credencialObj->get_dealers($_POST));
			break;
	}
	if ((in_array('credenciales', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
					echo json_encode($credencialObj->add($_POST, $_FILES));
					break;
            case 'get':
					echo json_encode($credencialObj->get($_POST));
					break;
            case 'edit':
					if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
						echo json_encode($credencialObj->edit($_POST, $_FILES));
					}
					else {
						echo '{error:"NO_PRIVILEGES"}';
					}
				break;
            case 'change-user-data':
					if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
						echo json_encode($credencialObj->change_user_data($_POST));
					}
					else {
						echo '{error:"NO_PRIVILEGES"}';
					}
				break;
            case 'delete':
					if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
						echo json_encode($credencialObj->delete($_POST));
					}
					else {
						echo '{error:"NO_PRIVILEGES"}';
					}
				break;
		}
	}
?>