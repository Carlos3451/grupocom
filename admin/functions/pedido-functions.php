<?php
	include_once('classes/Pedido.php');
	$obj = new Pedido();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ((in_array('facturacion', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'get-all-facturar':
                echo json_encode($obj->get_all_facturar($_POST));
                break;
        }
    }
	if ((in_array('entregas', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'set-delivery':
                echo json_encode($obj->set_delivery($_POST));
                break;
        }
    }
	if ((in_array('pedidos', $GLOBALS['usuario']['privilegios']))) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                break;
            case 'add-desktop':
                echo json_encode($obj->add_desktop($_POST));
                break;
            case 'get':
                echo json_encode($obj->get($_POST));
                break;
            case 'get-facturar':
                echo json_encode($obj->get_facturar($_POST));
                return;
            case 'get-print':
                echo json_encode($obj->get_print($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
            case 'get-all-desktop':
                echo json_encode($obj->get_all_desktop($_POST));
                break;
            case 'get-pendings':
                echo json_encode($obj->get_pendings($_POST));
                break;
            case 'get-news':
                echo json_encode($obj->get_news($_POST));
                break;
            case 'printed':
                echo json_encode($obj->printed($_POST));
                break;
            case 'add-payment':
                echo json_encode($obj->add_payment($_POST['pedidoID'], $_POST, false));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'edit-desktop':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit_desktop($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
            case 'cancel':
                if ($GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->cancel($_POST));
                }
				else {
					echo '{error:"NO_PRIVILEGES"}';
				}
                break;
		}
	}
?>