<?php
	include_once('classes/Gasto.php');
	$obj = new Gasto();
	$function = isset($_GET['function']) ? $_GET['function'] : '';
	if ($function=='') {
		exit;
	}
	if ($GLOBALS['usuario']['id'] == 1 || in_array('gastos', $GLOBALS['usuario']['privilegios'])) {
		switch ($function) {
            case 'add':
                echo json_encode($obj->add($_POST));
                break;
            case 'edit':
                if ($GLOBALS['usuario']['id'] == 1 || $GLOBALS['usuario']['privilegiosNivel'] > 1) {
                    echo json_encode($obj->edit($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'edit-cantidad':
                echo json_encode($obj->edit_cantidad($_POST));
                break;
            case 'get-all':
                echo json_encode($obj->get_all($_POST));
                break;
            case 'cancel':
                if ($GLOBALS['usuario']['id'] == 1 || $GLOBALS['usuario']['privilegiosNivel'] > 2) {
                    echo json_encode($obj->cancel($_POST));
                }
                else {
                    echo '{error:"NO_PRIVILEGES"}';
                }
                break;
            case 'add-gastoprogramado':
                echo json_encode($obj->add_gastoprogramado($_POST));
                break;
            case 'edit-gastoprogramado':
                echo json_encode($obj->edit_gastoprogramado($_POST));
                break;
            case 'delete-gastoprogramado':
                echo json_encode($obj->delete_gastoprogramado($_POST));
                break;
            case 'get-all-gastosprogramados':
                echo json_encode($obj->get_all_gastosprogramados($_POST));
                break;
        }
	}
?>