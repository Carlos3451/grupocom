<?php


class Reporte {

    function get_products_sales($data) {
        $result = ['error' => ''];
        $getPedidos = true;
        $getTickets = true;
        $wheresPedidos = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        $wheresTickets = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheresPedidos['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheresPedidos['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            $wheresTickets['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheresTickets['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheresPedidos['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
                $wheresTickets['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheresPedidos['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheresPedidos['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                $wheresTickets['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheresTickets['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheresPedidos['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                $wheresTickets['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if (isset($data['tipoVenta'])) {
            switch ($data['tipoVenta']) {
                case 'pedidos':
                    $getTickets = false;
                    break;
                case 'mostrador':
                    $getPedidos = false;
                    break;
            }
        }
        if (isset($data['vendedor'])) {
            $wheresPedidos['pedidos'][] = ['i', 'usuarioID', '=', $data['vendedor']];
            $wheresTickets['tickets'][] = ['i', 'usuarioID', '=', $data['vendedor']];
        }
        if (isset($data['ids'])) {
            $wheresPedidos['pedidos_productos'][] = ['i', 'productoID', 'IN', explode('-', $data['ids'])];
            $wheresTickets['tickets_productos'][] = ['i', 'productoID', 'IN', explode('-', $data['ids'])];
        }
        try {
            $pedidosResult = [];
            $ticketsResult = [];
            if ($getPedidos) {
                $pedidosResult = $GLOBALS['DB']->prepareSelectFrom([
                    'table' => 'pedidos_productos',
                    'columns' => [
                        'pedidos_productos' => ['productoID', 'nombre', 'SUM(*.cantidad) AS cantidad', 'SUM(*.total * (100 - *.descuento) * 0.01) AS total'],
                        'productos' => ['folio', 'unidad']
                    ],
                    'leftJoins' => [
                        'pedidos' => ['id', 'pedidoID'],
                        'productos' => ['id', 'pedidos_productos.productoID']
                    ],
                    'wheres' => $wheresPedidos,
                    'groups' => [
                        'pedidos_productos' => ['productoID', 'nombre']
                    ],
                    'orders' => [
                        ['total', 'DESC']
                    ]
                ]);
            }
            if ($getTickets) {
                $ticketsResult = $GLOBALS['DB']->prepareSelectFrom([
                    'table' => 'tickets_productos',
                    'columns' => [
                        'tickets_productos' => ['productoID', 'nombre', 'SUM(*.cantidad) AS cantidad', 'SUM(*.total) AS total'],
                        'productos' => ['folio', 'unidad']
                    ],
                    'leftJoins' => [
                        'tickets' => ['id', 'ticketID'],
                        'productos' => ['id', 'tickets_productos.productoID']
                    ],
                    'wheres' => $wheresTickets,
                    'groups' => [
                        'tickets_productos' => ['productoID', 'nombre']
                    ],
                    'orders' => [
                        ['total', 'DESC']
                    ]
                ]);
            }
            $mergedArray = [];
            for ($i=0; $i<count($pedidosResult); $i++) {
                $mergedArray[] = $pedidosResult[$i];
            }
            for ($i=0; $i<count($ticketsResult); $i++) {
                $mergedArray[] = $ticketsResult[$i];
            }
            $assocArray = [];
            for ($i=0; $i<count($mergedArray); $i++) {
                $key = $mergedArray[$i]['productoID'] . '_' . $mergedArray[$i]['nombre'];
                if (array_key_exists($key, $assocArray)) {
                    $assocArray[$key]['cantidad'] += $mergedArray[$i]['cantidad'];
                    $assocArray[$key]['total'] += $mergedArray[$i]['total'];
                }
                else {
                    $assocArray[$key] = $mergedArray[$i];
                }
            }
            $finalResult = [];
            foreach ($assocArray AS $data) {
                $finalResult[] = $data;
            }
            if (count($finalResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $finalResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedidos($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if (isset($data['metodoPago'])) {
            $wheres['pedidos'][] = ['s', 'tipo', '=', $data['metodoPago']];
        }
        if (isset($data['estadoPedido'])) {
            $wheres['pedidos'][] = ['i', 'finalizado', '=', $data['estadoPedido'] == 'pagado' ? 1 : '0'];
        }
        if (isset($data['vendedor'])) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['vendedor']];
        }
        if (isset($data['cliente'])) {
            $wheres['pedidos'][] = ['i', 'clienteID', '=', $data['cliente']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['folio', 'total', 'fechaCreate AS fecha'],
                    'clientes' => ['nombre AS clienteNombre'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => [
                    ['folio', 'DESC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_mostrador($data) {
        $result = ['error' => ''];
        $wheres = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if (isset($data['tipoVenta'])) {
            $wheres['tickets'][] = ['i', 'domicilio', '=', $data['tipoVenta'] == 'domicilio' ? 1 : '0'];
        }
        if (isset($data['vendedor'])) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $data['vendedor']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['folio', 'tipo', 'total', 'domicilio', 'fechaCreate AS fecha'],
                    'clientes' => ['nombre AS clienteNombre'],
                    'usuarios' => ['nombre AS usuarioNombre'],
                    'tickets_pagos' => ['metodo']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID'],
                    'tickets_pagos' => ['ticketID', 'id']
                ],
                'wheres' => $wheres,
                'orders' => [
                    ['folio', 'DESC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_ganancias($data) {
        $result = ['error' => ''];
        $pedidosSales = $this->get_pedidos_sales($data);
        $ticketsSales = $this->get_ticket_sales($data);
        $resultData = [];
        if (!isset($data['tipoVenta']) || $data['tipoVenta'] == 'pedidos') {
            if ($pedidosSales['error'] != '') {
                $result['error'] = $pedidosSales['error'];
                return $result;
            }
            $resultData = array_merge($resultData, $pedidosSales['resultado']);
        }
        if (!isset($data['tipoVenta']) || $data['tipoVenta'] == 'mostrador') {
            if ($ticketsSales['error'] != '') {
                $result['error'] = $ticketsSales['error'];
                return $result;
            }
            $resultData = array_merge($resultData, $ticketsSales['resultado']);
        }
        if (count($resultData) <= 0) {
            $result['error'] = 'EMPTY';
            return $result;
        }
        $fechaUnix = [];
        foreach ($resultData as $key => $row) {
            $fechaUnix[$key] = $row['fechaUnix'];
        }
        array_multisort($fechaUnix, SORT_DESC, $resultData);
        $result['resultado'] = $resultData;
        return $result;
    }

    function get_ticket_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if (isset($data['vendedor'])) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $data['vendedor']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets_productos',
                'columns' => [
                    'tickets_productos' => ['ticketID AS id', 'SUM(*.total) AS total', 'SUM(*.costo * *.cantidad) AS costo', 'SUM(*.total - (*.costo * *.cantidad)) AS ganancia'],
                    'tickets' => ['folio', 'UNIX_TIMESTAMP(*.fechaCreate) AS fechaUnix', 'fechaCreate AS fecha'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'tickets' => ['id', 'ticketID'],
                    'usuarios' => ['id', 'tickets.usuarioID']
                ],
                'groups' => [
                    'tickets_productos' => ['ticketID'],
                    'tickets' => ['fechaCreate', 'folio'],
                    'usuarios' => ['nombre']
                ],
                'wheres' => $wheres
            ]);
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['tipo'] = 'mostrador';
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedidos_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if (isset($data['vendedor'])) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['vendedor']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => [
                    'pedidos_productos' => ['pedidoID AS id', 'COALESCE(SUM(*.total * (100 - *.descuento) * 0.01), 0) AS total', 'COALESCE(SUM(*.costo * *.cantidad), 0) AS costo', 'COALESCE(SUM((*.total * (100 - *.descuento) * 0.01) - (*.costo * *.cantidad)), 0) AS ganancia'],
                    'pedidos' => ['folio', 'UNIX_TIMESTAMP(*.fechaCreate) AS fechaUnix', 'fechaCreate AS fecha'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'pedidos' => ['id', 'pedidoID'],
                    'usuarios' => ['id', 'pedidos.usuarioID']
                ],
                'groups' => [
                    'pedidos_productos' => ['pedidoID'],
                    'pedidos' => ['folio', 'fechaCreate'],
                    'usuarios' => ['nombre']
                ],
                'wheres' => $wheres
            ]);
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['tipo'] = 'pedido';
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_access() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->preparedSelectFrom([
                'table' => 'factura_accesos',
                'columns' => ['id']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_facturas() {
        include_once('Facturas.php');
        $facturaObj = new Factura();

        $result = ['error' => ''];

        $accesosResult = $this->get_all_access();
        if ($accesosResult['error'] != '') {
            $result['error'] = 'ACCESOS_GET:' . $accesosResult;
            return $result;
        }
        $accesos = $accesosResult['resultado'];

        for ($i=0; $i<count($accesos); $i++) {
            $facturaResult = $facturaObj->get_all([
                'filtros' => [
                    'accesoID' => $accesos[$i]
                ],
                'fecha' => '12/2020'
            ]);
        }

    }

    function get_gastos($data) {
        $result = ['error' => ''];

        $wheres = [
            ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ['i', 'cancelado', '=', '0']
        ];

        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres[] = ['s', 'DATE(fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres[] = ['s', 'DATE(fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres[] = ['s', 'DATE(fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[0]];
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[0]];
            }
        }

        $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
            'table' => 'gastos',
            'columns' => [
                'id',
                'folio',
                'razon',
                'comentario',
                'cantidad',
                'fechaCreate'
            ],
            'wheres' => $wheres,
            'orders' => [
                ['fechaCreate', 'DESC']
            ]
        ]);

        if (count($preparedResult) == 0) {
            $result['error'] = 'EMPTY';
            return $result;
        }
        $result['resultado'] = $preparedResult;

        return $result;
    }

    function get_compras($data) {
        $result = ['error' => ''];

        $wheres = [
            'compras' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];

        if (isset($data['proveedor'])) {
            $wheres['compras'][] = ['i', 'proveedorID', '=', $data['proveedor']];
        }

        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['compras'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['compras'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['compras'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['compras'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['compras'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['compras'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }

        $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
            'table' => 'compras',
            'columns' => [
                'compras' => [
                    'id',
                    'folio',
                    'total',
                    'fechaCreate'
                ],
                'proveedores' => [
                    'nombre AS proveedorNombre'
                ]
            ],
            'leftJoins' => [
                'proveedores' => ['id', 'proveedorID']
            ],
            'wheres' => $wheres,
            'orders' => [
                ['fechaCreate', 'DESC']
            ]
        ]);

        if (count($preparedResult) == 0) {
            $result['error'] = 'EMPTY';
            return $result;
        }
        $result['resultado'] = $preparedResult;

        return $result;
    }

}

?>