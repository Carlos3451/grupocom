<?php

class Dashboard {

    function get_ticket_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets_productos' => ['COALESCE(SUM(*.total)) AS total', 'COALESCE(SUM(*.costo * *.cantidad)) AS costo', 'COALESCE(SUM((*.total) - (*.costo * *.cantidad))) AS ganancia']
                ],
                'leftJoins' => [
                    'tickets_productos' => ['ticketID', 'id']
                ],
                'wheres' => $wheres
            ]);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedidos_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos_productos' => ['COALESCE(SUM(*.total)) AS total', 'COALESCE(SUM(*.costo * *.cantidad)) AS costo', 'COALESCE(SUM((*.total) - (*.costo * *.cantidad))) AS ganancia']
                ],
                'leftJoins' => [
                    'pedidos_productos' => ['pedidoID', 'id']
                ],
                'wheres' => $wheres
            ]);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_gastos($data) {
        $result = ['error' => ''];
        $wheres = [
            ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ['i', 'cancelado', '=', '0']
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres[] = ['s', 'DATE(fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres[] = ['s', 'DATE(fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres[] = ['s', 'DATE(fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[0]];
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[0]];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos',
                'columns' => ['COALESCE(SUM(cantidad)) AS gastos'],
                'wheres' => $wheres
            ]);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }

        $result['resultado']['total'] = 0;
        $result['resultado']['costo'] = 0;
        $result['resultado']['ganancia'] = 0;
        $result['resultado']['pagos'] = 0;
        $result['resultado']['gastos'] = 0;

        $ticketsSalesResult = $this->get_ticket_sales($data);
        $pedidoSalesResult = $this->get_ticket_sales($data);

        $ticketsPagosResult = $this->get_tickets_pagos($data);
        $pedidosPagosResult = $this->get_pedidos_pagos($data);

        $gastosResult = $this->get_gastos($data);
        
        if ($ticketsSalesResult['error'] == '') {
            $result['resultado']['total'] += $ticketsSalesResult['resultado']['total'];
            $result['resultado']['costo'] += $ticketsSalesResult['resultado']['costo'];
            $result['resultado']['ganancia'] += $ticketsSalesResult['resultado']['ganancia'];
        }
        
        if ($pedidoSalesResult['error'] == '') {
            $result['resultado']['total'] += $pedidoSalesResult['resultado']['total'];
            $result['resultado']['costo'] += $pedidoSalesResult['resultado']['costo'];
            $result['resultado']['ganancia'] += $pedidoSalesResult['resultado']['ganancia'];
        }

        if ($ticketsPagosResult['error'] == '') {
            foreach ($ticketsPagosResult['resultado'] AS $pago) {
                $result['resultado']['pagos'] += $pago['total'];
            }
        }

        if ($pedidosPagosResult['error'] == '') {
            foreach ($pedidosPagosResult['resultado'] AS $pago) {
                $result['resultado']['pagos'] += $pago['total'];
            }
        }
        
        if ($gastosResult['error'] == '') {
            $result['resultado']['gastos'] += $gastosResult['resultado']['gastos'];
        }

        return $result;
    }

    //********************************************//
    //********************************************//
    //**                                        **//
    //**              P A G O S                 **//
    //**                                        **//
    //********************************************//
    //********************************************//
    

    function get_tickets_pagos($data) {
        $result = ['error' => ''];
        $wheres = [
            ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ['i', 'cancelado', '=', '0']
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres[] = ['s', 'DATE(fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres[] = ['s', 'DATE(fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres[] = ['s', 'DATE(fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[0]];
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres[] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres[] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets_pagos',
                'columns' => ['metodo', 'SUM(cantidad) AS total'],
                'wheres' => $wheres,
                'groups' => ['metodo']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedidos_pagos($data) {
        $result = ['error' => ''];
        $wheres = [
            ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ['i', 'cancelado', '=', '0']
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres[] = ['s', 'DATE(fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres[] = ['s', 'DATE(fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres[] = ['s', 'DATE(fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[0]];
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres[] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres[] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_pagos',
                'columns' => ['metodo', 'SUM(cantidad) AS total'],
                'wheres' => $wheres,
                'groups' => ['metodo']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payments($data) {
        $result = ['error' => ''];
        
        $result['resultado']['efectivo'] = 0;
        $result['resultado']['tarjeta'] = 0;
        $result['resultado']['transferencia'] = 0;
        $result['resultado']['cheque'] = 0;

        $ticketsPagosResult = $this->get_tickets_pagos($data);
        $pedidosPagosResult = $this->get_pedidos_pagos($data);
        
        if ($ticketsPagosResult['error'] == '') {
            for ($i=0; $i<count($ticketsPagosResult['resultado']); $i++) {
                $pago = $ticketsPagosResult['resultado'][$i];
                if (isset($result['resultado'][$pago['metodo']])) {
                    $result['resultado'][$pago['metodo']] += $pago['total'];
                }
            }
        }
        
        if ($pedidosPagosResult['error'] == '') {
            for ($i=0; $i<count($pedidosPagosResult['resultado']); $i++) {
                $pago = $pedidosPagosResult['resultado'][$i];
                if (isset($result['resultado'][$pago['metodo']])) {
                    $result['resultado'][$pago['metodo']] += $pago['total'];
                }
            }
        }

        return $result;
    }

    function get_products_sales_top_mostrador($data) {
        $result = ['error' => ''];
        $wheres = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets_productos' => ['productoID', 'nombre', 'SUM(*.cantidad) AS cantidad', 'SUM(*.total) AS total']
                ],
                'leftJoins' => [
                    'tickets_productos' => ['ticketID', 'id']
                ],
                'wheres' => $wheres,
                'groups' => [
                    'tickets_productos' => ['productoID', 'nombre']
                ],
                'orders' => [
                    ['total', 'DESC']
                ],
                'limit' => 10
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_products_sales_top($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if ($data['usuarioID'] != 0) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos_productos' => ['productoID', 'nombre', 'SUM(*.cantidad) AS cantidad', 'SUM(*.total * (100 - *.descuento) * 0.01) AS total']
                ],
                'leftJoins' => [
                    'pedidos_productos' => ['pedidoID', 'id']
                ],
                'wheres' => $wheres,
                'groups' => [
                    'pedidos_productos' => ['productoID', 'nombre']
                ],
                'orders' => [
                    ['total', 'DESC']
                ],
                'limit' => 10
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_salesmans_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['usuarioID', 'SUM(*.total) AS total'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'groups' => [
                    'pedidos' => ['usuarioID']
                ],
                'orders' => [
                    ['total', 'DESC']
                ]
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_deliveries_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'repartidorID', 'IS NOT', NULL],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['tickets'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['repartidorID', 'SUM(*.total) AS total'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'repartidorID']
                ],
                'wheres' => $wheres,
                'groups' => [
                    'tickets' => ['repartidorID']
                ],
                'orders' => [
                    ['total', 'DESC']
                ]
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_products_sales($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            if (count($date) == 3) {
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '=', $date[2] . '-' . $date[1] . '-' . $date[0]];
            }
            else if (count($date) == 2) {
                $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
            }
            else {
                $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
            }
        }
        if ($GLOBALS['usuario']['privilegiosNivel'] != 4) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else if (isset($data['usuarioID']) && $data['usuarioID'] != 0) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['usuarioID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => [
                    'pedidos_productos' => ['productoID', 'nombre', 'SUM(*.cantidad) AS cantidad', 'SUM(*.total * (100 - *.descuento) * 0.01) AS total']
                ],
                'leftJoins' => [
                    'pedidos' => ['id', 'pedidoID']
                ],
                'wheres' => $wheres,
                'groups' => [
                    'pedidos_productos' => ['productoID', 'nombre']
                ],
                'orders' => [
                    ['total', 'DESC']
                ]
            ]);
            for ($i=0; $i<count($preparedResult); $i++) {
                $productData = $this->get_product($preparedResult[$i]['productoID']);
                if ($productData['error'] == '') {
                    $preparedResult[$i]['folio'] = $productData['resultado']['folio'];
                    $preparedResult[$i]['unidad'] = $productData['resultado']['unidad'];
                }
                else {
                    $preparedResult[$i]['folio'] = 0;
                    $preparedResult[$i]['unidad'] = 'pieza';
                }
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_product($id) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['folio', 'unidad'],
                'wheres' => [
                    ['i', 'id', '=', $id]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>