<?php

class Notificacion {

    function create($type, $json) {
        $result = ['error' => ''];
        $users = $this->get_users(explode('.', $type)[0]);
        if ($users['error'] == '') {
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'notificaciones',
                    'values' => [
                        ['s', 'tipo', $type],
                        ['s', 'parametros', json_encode($json)],
                        ['i', 'remitenteID', $GLOBALS['usuario']['id']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                for ($i=0; $i<count($users['resultado']); $i++) {
                    $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                        'table' => 'notificaciones_usuarios',
                        'values' => [
                            ['i', 'usuarioID', $users['resultado'][$i]['id']],
                            ['i', 'notificacionID', $result['resultado']['id']],
                            ['i', 'visto', 0],
                            ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                        ]
                    ]);
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $users['error'];
        }
        return $result;
    }

    function get_users($type) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_wheres($type);
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id'],
                'wheres' => $wheres
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_wheres($type) {
        $wheres = [
            'GROUP_1' => [
                ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%', 'OR'],
                ['i', 'id', '=', '1']
            ]
        ];
        switch ($type) {
            case 'producto':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%productos%'];
                break;
            case 'existencia':
                $wheres['GROUP_2'] = [
                    ['s', 'privilegios', 'LIKE', '%productos%', 'OR'],
                    ['s', 'privilegios', 'LIKE', '%pedidos%', 'OR'],
                    ['s', 'privilegios', 'LIKE', '%ventas%']
                ];
                break;
            case 'comprapago': case 'compra': case 'proveedor':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%proveedores%'];
                break;
            case 'pedidopago': case 'pedido':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%pedidos%'];
                break;
            case 'ruta':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%rutas%'];
                break;
            case 'cliente':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%clientes%'];
                break;
            case 'credencial':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%credenciales%'];
                break;
            case 'producto': case 'inventario':
                $wheres[] = ['s', 'privilegios', 'LIKE', '%productos%'];
                break;
        }
        return $wheres;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = [
            'notificaciones' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
            ],
            'notificaciones_usuarios' => [
                ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']]
            ]
        ];
        if (isset($data['id'])) {
            $wheres['notificaciones'][] = ['i', 'id', '<', $data['id']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'notificaciones',
                'columns' => [
                    'notificaciones' => ['tipo', 'parametros', 'remitenteID', 'fechaCreate'],
                    'notificaciones_usuarios' => ['id', 'visto'],
                    'usuarios' => ['nombre AS remitenteNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'remitenteID'],
                    'notificaciones_usuarios' => ['notificacionID', 'id']
                ],
                'wheres' => $wheres,
                'orders' => [
                    'notificaciones' => [
                        ['fechaCreate', 'DESC'],
                        ['id', 'DESC']
                    ]
                ],
                'limit' => [100, 0]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
            $this->set_view_all();
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function set_view_all() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'notificaciones_usuarios',
                'values' => [
                    ['i', 'visto', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']],
                    ['i', 'visto', '=', '0']
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>