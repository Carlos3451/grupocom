<?php

include_once('classes/Notificacion.php');

class Compra {

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $providerData = $this->get_provider($data['proveedorID'])['resultado'];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            $pagado = 0;
            $finalizado = 0;
            if (isset($data['pagos'])) {
                foreach ($data['pagos'] AS $payment) {
                    $pagado += $payment['cantidad'];
                }
            }
            if ($pagado >= $data['total']) {
                $finalizado = 1;
            }
            $preparedObj = [
                'table' => 'compras',
                'values' => [
                    ['i', 'folio', $folio],
                    ['i', 'proveedorID', $data['proveedorID']],
                    ['d', 'total', $data['total']],
                    ['i', 'cancelado', 0],
                    ['i', 'finalizado', $finalizado],
                    ['s', 'fechaCreate', implode('-', array_reverse(explode('/', $data['fecha'])))],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ];
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto($preparedObj);
                $result['resultado'] = $preparedResult;
                $this->notificacion->create('compra.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'proveedorID' => $data['proveedorID'],
                    'proveedorFolio' => $providerData['folio'],
                    'proveedorNombre' => $providerData['nombre']
                ]);
                if (isset($data['compraProductos'])) {
                    foreach ($data['compraProductos'] AS $compraProducto) {
                        $this->add_compra_product($result['resultado']['id'], $compraProducto);
                        $this->change_product($data['proveedorID'], $compraProducto);
                        $this->add_existence($compraProducto['productoID'], $compraProducto['cantidad']);
                    }
                }
                if (isset($data['pagos'])) {
                    foreach ($data['pagos'] AS $payment) {
                        $payment = $this->add_payment($result['resultado']['id'], $payment);
                    }
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $compraData = $this->get(['id' => $data['id']])['resultado'];
        $finalizado = 0;
        $pagado = 0;
        if (isset($data['pagos'])) {
            foreach ($data['pagos'] AS $payment) {
                if ($payment['cancelado'] == 0) {
                    $pagado += $payment['cantidad'];
                }
            }
        }
        if ($pagado >= $compraData['total']) {
            $finalizado = 1;
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'compras',
                'values' => [
                    ['d', 'total', $data['total']],
                    ['i', 'finalizado', $finalizado],
                    ['s', 'fechaCreate', implode('-', array_reverse(explode('/', $data['fecha'])))]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'cancelado', '=', "0"],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('compra.edit', [
                'id' => $data['id'],
                'folio' => $compraData['folio'],
                'proveedorID' => $compraData['proveedorID'],
                'proveedorFolio' => $compraData['proveedorFolio'],
                'proveedorNombre' => $compraData['proveedorNombre']
            ]);
            if (isset($data['compraProductosEliminados'])) {
                foreach ($data['compraProductosEliminados'] AS $compraProductoEliminado) {
                    $this->delete_compra_product($data['id'], $compraProductoEliminado);
                }
            }
            if (isset($data['compraProductos'])) {
                foreach ($data['compraProductos'] AS $compraProducto) {
                    if ($compraProducto['id'] == 0) {
                        $this->add_compra_product($data['id'], $compraProducto);
                        $this->change_product($data['proveedorID'], $compraProducto);
                        $this->add_existence($compraProducto['productoID'], $compraProducto['cantidad']);
                    }
                    else {
                        $previousQuantity = $this->get_compra_product_quantity($compraProducto['id']);
                        $newQuantity = $compraProducto['cantidad'] - $previousQuantity;
                        $this->edit_compra_product($data['id'], $compraProducto);
                        $this->add_existence($compraProducto['productoID'], $newQuantity);
                    }
                }
            }
            if (isset($data['pagos'])) {
                foreach ($data['pagos'] AS $payment) {
                    if ($payment['id'] == 0) {
                        $this->add_payment($data['id'], $payment);
                    }
                    else {
                        if ($payment['cancelado'] == 1) {
                            $this->cancel_payment($data['id'], $payment);
                        }
                    }
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel($data) {
        $result = ['error' => ''];
        $comprasData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $comprasData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'compras',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $comprasProducts = $this->get_products_compra($data['ids'][$i]);
                $payments = $this->get_payments($data['ids'][$i]);
                $this->notificacion->create('compra.cancel', [
                    'id' => $data['ids'][$i],
                    'folio' => $comprasData[$i]['folio'],
                    'proveedorID' => $comprasData[$i]['proveedorID'],
                    'proveedorFolio' => $comprasData[$i]['proveedorFolio'],
                    'proveedorNombre' => $comprasData[$i]['proveedorNombre']
                ]);
                if ($comprasProducts['error'] == '') {
                    foreach ($comprasProducts['resultado'] AS $compraProducto) {
                        $this->subtract_existence($compraProducto['productoID'], $compraProducto['cantidad']);
                    }
                }
                if ($payments['error'] == '') {
                    foreach ($payments['resultado'] AS $payment) {
                        $this->cancel_payment($data['ids'][$i], $payment);
                    }
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_compra_product($compraID, $data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'compras_productos',
                'values' => [
                    ['i', 'compraID', $compraID],
                    ['i', 'productoID', $data['productoID']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['d', 'costo', $data['costo']],
                    ['d', 'precio1', $data['precio1']],
                    ['d', 'precio2', $data['precio2']],
                    ['d', 'precio3', $data['precio3']],
                    ['d', 'total', $data['total']],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_provider($providerID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'proveedores',
                'columns' => ['folio', 'nombre'],
                'wheres' => [
                    ['i', 'id', '=', $providerID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit_compra_product($compraID, $data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'compras_productos',
                'values' => [
                    ['d', 'cantidad', $data['cantidad']],
                    ['d', 'total', $data['total']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_compra_product($compraProductID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_productos',
                'columns' => ['productoID', 'cantidad'],
                'wheres' => [
                    ['i', 'id', '=', $compraProductID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_compra_product($compraID, $compraProductID) {
        $result = ['error' => ''];
        $compraProduct = $this->get_compra_product($compraProductID)['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'compras_productos',
                'wheres' => [
                    ['i', 'id', '=', $compraProductID],
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->subtract_existence($compraProduct['productoID'], $compraProduct['cantidad']);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_payment($compraID, $data) {
        $result = ['error' => ''];
        $compraData = $this->get(['id' => $compraID])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'compras_pagos',
                'values' => [
                    ['i', 'compraID', $compraID],
                    ['s', 'metodo', $data['metodo']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['i', 'cancelado', 0],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                    ['s', 'fechaCreate', date('Y-m-d G:i:s', $data['fecha'])]
                ]
            ]);
            $result['resultado'] = $preparedResult;
            $this->notificacion->create('comprapago.add', [
                'id' => $result['resultado']['id'],
                'compraID' => $compraID,
                'compraFolio' => $compraData['folio'],
                'proveedorID' => $compraData['proveedorID'],
                'proveedorFolio' => $compraData['proveedorFolio'],
                'proveedorNombre' => $compraData['proveedorNombre'],
                'cantidad' => $data['cantidad'],
                'metodo' => $data['metodo']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel_payment($compraID, $data) {
        $result = ['error' => ''];
        $paymentData = $this->get_payment($compraID, $data['id'])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'compras_pagos',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $compraData = $this->get(['id' => $compraID])['resultado'];
            $this->notificacion->create('comprapago.cancel', [
                'id' => $data['id'],
                'compraID' => $compraID,
                'compraFolio' => $compraData['folio'],
                'cantidad' => $paymentData['cantidad'],
                'proveedorID' => $compraData['proveedorID'],
                'proveedorFolio' => $compraData['proveedorFolio'],
                'proveedorNombre' => $compraData['proveedorNombre'],
                'metodo' => $paymentData['metodo']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function subtract_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $productResult = $this->get_inventory($productID);
            if ($productResult['error'] == '') {
                $productData = $productResult['resultado'];
                $newExistence = $productData['existencia'] - $quantity;
                $preparedResult = $GLOBALS['DB']->prepareUpdate([
                    'table' => 'productos',
                    'values' => [
                        ['d', 'existencia', $newExistence]
                    ],
                    'wheres' => [
                        ['i', 'id', '=', $productID]
                    ]
                ]);
                if ($productData['existencia'] >= $productData['inventarioMinimo'] && $newExistence < $productData['inventarioMinimo']) {
                    $this->notificacion->create('producto.low', [
                        'id' => $productID,
                        'folio' => $productData['folio'],
                        'nombre' => $productData['nombre']
                    ]);
                }
                if ($preparedResult['affected_rows'] == -1) {
                    $result['error'] = 'NOT_FOUND';
                    return $result;
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function change_product($providerID, $productoCompra) {
        $result = ['error' => ''];

        $cost = $productoCompra['costo'];

        $latestCompraResult = $this->get_latest_compra_producto($productoCompra['productoID']);
        if ($latestCompraResult['error'] == '') {
            $latestCompra = $latestCompraResult['resultado'];

            $totalQty = $latestCompra['cantidad'] + $productoCompra['cantidad'];
            $latestPerc = $latestCompra['cantidad'] / $totalQty;
            $newPerc = 1 - $latestPerc;

            $cost = ($latestCompra['costo'] * $latestPerc) + ($productoCompra['costo'] * $newPerc);
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'costo', $cost],
                    ['d', 'precio1', $productoCompra['precio1']],
                    ['d', 'precio2', $productoCompra['precio2']],
                    ['d', 'precio3', $productoCompra['precio3']],
                    ['s', 'impuesto', $productoCompra['impuesto']],
                    ['s', 'claveSAT', $productoCompra['claveSAT']],
                    ['i', 'proveedorID', $providerID]
                ],
                'wheres' => [
                    ['i', 'id', '=', $productoCompra['productoID']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $productData = $this->get_inventory($productID)['resultado'];
            $newExistence = $productData['existencia'] + $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $newExistence]
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_compra_product_quantity($compraProductID) {
        $quantity = 0;
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_productos',
                'columns' => ['cantidad'],
                'wheres' => [
                    ['i', 'id', '=', $compraProductID]
                ]
            ]);
            $quantity = $preparedResult[0]['cantidad'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $quantity;
    }

    function get_inventory($productID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['folio', 'existencia', 'inventarioMinimo', 'nombre'],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras',
                'columns' => [
                    'compras' => ['id', 'folio', 'proveedorID', 'total', 'fechaCreate'],
                    'proveedores' => ['folio AS proveedorFolio', 'nombre AS proveedorNombre']
                ],
                'leftJoins' => [
                    'proveedores' => ['id', 'proveedorID']
                ],
                'wheres' => [
                    'compras' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['pagos'] = $this->get_payments($preparedResult[0]['id']);
            $preparedResult[0]['compras'] = $this->get_products_compra($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['compras'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['compras'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['compras'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['compras'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['compras'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['compras'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['compras'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['compras'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        if (!empty($data['filtros']['usuario'])) {
            $wheres['compras'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
        }
        if (!empty($data['filtros']['proveedor'])) {
            $wheres['compras'][] = ['i', 'proveedorID', '=', $data['filtros']['proveedor']];
        }        
        if (!empty($data['filtros']['usuario'])) {
            $wheres['compras'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras',
                'columns' => [
                    'compras' => ['id', 'folio', 'proveedorID', 'total', 'cancelado', 'fechaCreate'],
                    'proveedores' => ['folio AS proveedorFolio', 'nombre AS proveedorNombre'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'proveedores' => ['id', 'proveedorID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['pagado'] = $this->get_payed($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_products_compra($compraID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_productos',
                'columns' => [
                    'compras_productos' => ['id', 'productoID', 'cantidad', 'costo', 'precio1', 'precio2', 'precio3', 'total'],
                    'productos' => ['folio', 'nombre', 'unidad', 'impuesto', 'claveSAT']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'compras_productos' => [
                        ['i', 'compraID', '=', $compraID],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payments($compraID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_pagos',
                'columns' => ['id', 'metodo', 'cantidad', 'cancelado', 'fechaCreate'],
                'wheres' => [
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payment($compraID, $paymentID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_pagos',
                'columns' => ['id', 'metodo', 'cantidad', 'cancelado', 'fechaCreate'],
                'wheres' => [
                    ['i', 'id', '=', $paymentID],
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payed($compraID) {
        $payed = 0;
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_pagos',
                'columns' => ['cantidad'],
                'wheres' => [
                    ['i', 'compraID', '=', $compraID],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                return $payed;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $payed += $preparedResult[$i]['cantidad'];
            }
        }
        catch (Exception $err) {
        }
        return $payed;
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'compras' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'compras' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'proveedores' => [
                    ['s', 'nombre', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'compras' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'compras' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function get_latest_compra_producto($productoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'compras_productos',
                'columns' => [
                    'compras_productos' => ['cantidad', 'costo']
                ],
                'leftJoins' => [
                    'compras' => ['id', 'compraID']
                ],
                'wheres' => [
                    'compras_productos' => [
                        ['i', 'productoID', '=', $productoID]
                    ],
                    'compras' => [
                        ['i', 'cancelado', '=', '0']
                    ]
                ],
                'orders' => [
                    'compras_productos' => [
                        ['id', 'DESC']
                    ]
                ],
                'limit' => [2]
            ]);

            if (count($preparedResult) < 2) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            $result['resultado'] = $preparedResult[1];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>