<?php

include_once('classes/Notificacion.php');

class Pedido {

    public $notificacion;

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $clientData = $this->get_client($data['clienteID'])['resultado'];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            $pagado = 0;
            $type = 'credito';
            $finalizado = 0;
            if (isset($data['pagos'])) {
                if (is_string($data['pagos'])) { $data['pagos'] = json_decode($data['pagos'], true); }
                foreach ($data['pagos'] AS $payment) {
                    $pagado += $payment['cantidad'];
                }
            }
            if ($pagado >= $data['total']) {
                $type = 'contado';
                $finalizado = 1;
            }
            $this->add_debt($data['clienteID'], $data['total']);
            if (isset($data['tipo'])) {
                if ($data['tipo'] == 'contado') {
                    $type = 'contado';
                }
            }
            $preparedObj = [
                'table' => 'pedidos',
                'values' => [
                    ['i', 'folio', $folio],
                    ['i', 'clienteID', $data['clienteID']],
                    ['d', 'subtotal', $data['subtotal']],
                    ['d', 'descuentoPorcentaje', $data['descuentoPorcentaje']],
                    ['d', 'descuentoCantidad', $data['descuentoCantidad']],
                    ['d', 'total', $data['total']],
                    ['i', 'impreso', 0],
                    ['i', 'cancelado', 0],
                    ['i', 'finalizado', $finalizado],
                    ['s', 'tipo', $type],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ];
            if (isset($data['fecha'])) { $preparedObj['values'][] = ['s', 'fechaCreate', date('Y-m-d G:i:s', $data['fecha'])]; }
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto($preparedObj);
                $result['resultado'] = $preparedResult;
                $result['resultado']['folio'] = $folio;
                $this->notificacion->create('pedido.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'tipo' => $type,
                    'clienteID' => $data['clienteID'],
                    'clienteFolio' => $clientData['folio'],
                    'clienteNombre' => $clientData['nombre']
                ]);
                if (isset($data['pedidoProductos'])) {
                    if (is_string($data['pedidoProductos'])) { $data['pedidoProductos'] = json_decode($data['pedidoProductos'], true); }
                    foreach ($data['pedidoProductos'] AS $pedidoProducto) {
                        $this->add_pedido_product($result['resultado']['id'], $pedidoProducto, $data['descuentoPorcentaje']);
                        $this->subtract_existence($pedidoProducto['productoID'], $pedidoProducto['cantidadInventario']);
                    }
                }
                if (isset($data['pagos'])) {
                    foreach ($data['pagos'] AS $payment) {
                        $payment = $this->add_payment($result['resultado']['id'], $payment, true);
                    }
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $pedidoResult = $this->get(['id' => $data['id']]);
        if ($pedidoResult['error'] != '') {
            $result = $pedidoResult;
            return $result;
        }
        $pedidoData = $pedidoResult['resultado'];

        $anteriorPagado = 0;
        if ($pedidoData['pagos']['error'] == '') {
            foreach ($pedidoData['pagos']['resultado'] AS $pago) {
                if ($pago['cancelado'] == 0) {
                    $anteriorPagado += $pago['cantidad'];
                }
            }
        }

        $finalizado = 0;
        $pagado = 0;

        if (isset($data['pagos'])) {
            foreach ($data['pagos'] AS $payment) {
                if ($payment['cancelado'] == 0) {
                    $pagado += $payment['cantidad'];
                }
            }
        }
        if ($pagado >= $data['total']) {
            $finalizado = 1;
        }

        $values = [
            ['i', 'clienteID', $data['clienteID']],
            ['d', 'subtotal', $data['subtotal']],
            ['d', 'descuentoPorcentaje', $data['descuentoPorcentaje']],
            ['d', 'descuentoCantidad', $data['descuentoCantidad']],
            ['d', 'total', $data['total']],
            ['i', 'finalizado', $finalizado]
        ];

        if (isset($data['fecha'])) { $values[] = ['s', 'fechaCreate', date('Y-m-d G:i:s', $data['fecha'])]; }

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos',
                'values' => $values,
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'cancelado', '=', "0"],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);

            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            $this->notificacion->create('pedido.edit', [
                'id' => $data['id'],
                'folio' => $pedidoData['folio'],
                'clienteID' => $pedidoData['clienteID'],
                'clienteFolio' => $pedidoData['clienteFolio'],
                'clienteNombre' => $pedidoData['clienteNombre']
            ]);
        
            $this->subtract_debt($pedidoData['clienteID'], $pedidoData['total'] - $anteriorPagado);

            if (isset($data['pedidoProductosEliminados'])) {
                foreach ($data['pedidoProductosEliminados'] AS $pedidoProductoEliminado) {
                    $this->delete_pedido_product($data['id'], $pedidoProductoEliminado);
                }
            }
            if (isset($data['pedidoProductos'])) {
                foreach ($data['pedidoProductos'] AS $pedidoProducto) {
                    if ($pedidoProducto['id'] == 0) {
                        $this->add_pedido_product($data['id'], $pedidoProducto, $data['descuentoPorcentaje']);
                        $this->subtract_existence($pedidoProducto['productoID'], $pedidoProducto['cantidadInventario']);
                    }
                    else {
                        $previousQuantity = $this->get_pedido_product_quantity($pedidoProducto['id']);
                        $newQuantity = floatval($pedidoProducto['cantidadInventario']) - floatval($previousQuantity);
                        $this->edit_pedido_product($data['id'], $pedidoProducto, $data['descuentoPorcentaje']);
                        $this->subtract_existence($pedidoProducto['productoID'], $newQuantity);
                    }
                }
            }

            $pagadoNuevo = 0;
            if (isset($data['pagos'])) {
                foreach ($data['pagos'] AS $payment) {
                    $pagadoNuevo += $payment['cantidad'];
                    if ($payment['id'] == 0) {
                        $this->add_payment($data['id'], $payment, true);
                    }
                    else {
                        if ($payment['cancelado'] == 1) {
                            $this->cancel_payment($data['id'], $payment, true);
                            $pagadoNuevo -= $payment['cantidad'];
                        }
                    }
                }
            }
            if ($pagadoNuevo < $data['total']) {
                $this->add_debt($data['clienteID'], $data['total'] - $pagadoNuevo);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel($data) {
        $result = ['error' => ''];
        $pedidosData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $pedidosData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $pedidosProducts = $this->get_products_pedido($data['ids'][$i]);
                $payments = $this->get_payments($data['ids'][$i]);
                $this->notificacion->create('pedido.cancel', [
                    'id' => $data['ids'][$i],
                    'folio' => $pedidosData[$i]['folio'],
                    'clienteID' => $pedidosData[$i]['clienteID'],
                    'clienteFolio' => $pedidosData[$i]['clienteFolio'],
                    'clienteNombre' => $pedidosData[$i]['clienteNombre']
                ]);
                if ($pedidosProducts['error'] == '') {
                    foreach ($pedidosProducts['resultado'] AS $pedidoProducto) {
                        $this->add_existence($pedidoProducto['productoID'], $pedidoProducto['cantidadInventario']);
                    }
                }
                if ($payments['error'] == '') {
                    foreach ($payments['resultado'] AS $payment) {
                        if ($payment['cancelado'] == 0) {
                            $this->cancel_payment($data['ids'][$i], $payment, true);
                        }
                    }
                }
                $this->subtract_debt($pedidosData[$i]['clienteID'], $pedidosData[$i]['total']);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function printed($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos',
                'values' => [
                    ['i', 'impreso', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_pedido_product($pedidoID, $data, $discount) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'pedidos_productos',
                'values' => [
                    ['i', 'pedidoID', $pedidoID],
                    ['i', 'productoID', $data['productoID']],
                    ['s', 'nombre', $data['nombre']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['d', 'cantidadInventario', $data['cantidadInventario']],
                    ['d', 'descuento', $discount],
                    ['d', 'precio', $data['precio']],
                    ['d', 'costo', $data['costo']],
                    ['d', 'total', $data['total']],
                    ['s', 'unidad', $data['unidad']],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_client($clientID) {
        $result = ['error' => ''];
        try {
            $wheres = [['i', 'id', '=', $clientID]];
            if ($clientID != 1) {
                $wheres[] = ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']];
            }
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => ['folio', 'nombre', 'deuda'],
                'wheres' => $wheres
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit_pedido_product($pedidoID, $data, $discount) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos_productos',
                'values' => [
                    ['s', 'unidad', $data['unidad']],
                    ['d', 'precio', $data['precio']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['d', 'cantidadInventario', $data['cantidadInventario']],
                    ['d', 'descuento', $discount],
                    ['d', 'total', $data['total']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedido_product($pedidoProductID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => [
                    'productoID',
                    'cantidad',
                    'cantidadInventario'
                ],
                'wheres' => [
                    ['i', 'id', '=', $pedidoProductID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_pedido_product($pedidoID, $pedidoProductID) {
        $result = ['error' => ''];
        $pedidoProduct = $this->get_pedido_product($pedidoProductID)['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'pedidos_productos',
                'wheres' => [
                    ['i', 'id', '=', $pedidoProductID],
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->add_existence($pedidoProduct['productoID'], $pedidoProduct['cantidadInventario']);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function set_finished($pedidoID, $finished) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos',
                'values' => [
                    ['i', 'finalizado', $finished]
                ],
                'wheres' => [
                    ['i', 'id', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_payment($pedidoID, $data, $subractDebt) {
        $result = ['error' => ''];
        $pedidoData = $this->get(['id' => $pedidoID])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'pedidos_pagos',
                'values' => [
                    ['i', 'pedidoID', $pedidoID],
                    ['s', 'metodo', $data['metodo']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['i', 'cancelado', 0],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                    ['s', 'fechaCreate', date('Y-m-d G:i:s', $data['fecha'])]
                ]
            ]);
            if ($pedidoData['pagado'] + $data['cantidad'] >= $pedidoData['total']) {
                $this->set_finished($pedidoID, 1);
            }
            $result['resultado'] = $preparedResult;
            $this->notificacion->create('pedidopago.add', [
                'id' => $result['resultado']['id'],
                'pedidoID' => $pedidoID,
                'pedidoFolio' => $pedidoData['folio'],
                'clienteID' => $pedidoData['clienteID'],
                'clienteFolio' => $pedidoData['clienteFolio'],
                'clienteNombre' => $pedidoData['clienteNombre'],
                'cantidad' => $data['cantidad'],
                'metodo' => $data['metodo']
            ]);
            if ($subractDebt) {
                $this->subtract_debt($pedidoData['clienteID'], $data['cantidad']);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel_payment($pedidoID, $data, $addDebt) {
        $result = ['error' => ''];
        $paymentData = $this->get_payment($pedidoID, $data['id'])['resultado'];
        $pedidoData = $this->get(['id' => $pedidoID])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'pedidos_pagos',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            if ($pedidoData['pagado'] - $paymentData['cantidad'] < $pedidoData['total']) {
                $this->set_finished($pedidoID, 0);
            }
            $this->notificacion->create('pedidopago.cancel', [
                'id' => $data['id'],
                'pedidoID' => $pedidoID,
                'pedidoFolio' => $pedidoData['folio'],
                'cantidad' => $paymentData['cantidad'],
                'clienteID' => $pedidoData['clienteID'],
                'clienteFolio' => $pedidoData['clienteFolio'],
                'clienteNombre' => $pedidoData['clienteNombre'],
                'metodo' => $paymentData['metodo']
            ]);
            if ($addDebt) {
                $this->add_debt($pedidoData['clienteID'], $data['cantidad']);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function subtract_existence($productID, $quantity) {
        $result = ['error' => ''];
        $productResult = $this->get_inventory($productID);
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $quantity, 'existencia - ?']
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($productResult['error'] == '') {
                $productData = $productResult['resultado'];
                if ($productData['existencia'] > $productData['inventarioMinimo'] && $productData['existencia'] - $quantity <= $productData['inventarioMinimo']) {
                    $this->notificacion->create('producto.low', [
                        'id' => $productID,
                        'folio' => $productData['folio'],
                        'nombre' => $productData['nombre']
                    ]);
                }
            }
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $quantity, 'existencia + ?']
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pedido_product_quantity($pedidoProductID) {
        $quantity = 0;
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => ['cantidadInventario'],
                'wheres' => [
                    ['i', 'id', '=', $pedidoProductID]
                ]
            ]);
            $quantity = $preparedResult[0]['cantidadInventario'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $quantity;
    }

    function get_inventory($productID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['folio', 'existencia', 'inventarioMinimo', 'nombre'],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'subtotal', 'descuentoPorcentaje', 'descuentoCantidad', 'total', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID']
                ],
                'wheres' => [
                    'pedidos' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['pagos'] = $this->get_payments($preparedResult[0]['id']);
            $preparedResult[0]['pedidos'] = $this->get_products_pedido($preparedResult[0]['id']);
            $preparedResult[0]['pagado'] = $this->get_payed($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_facturar($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'subtotal', 'descuentoPorcentaje', 'descuentoCantidad', 'total'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID']
                ],
                'wheres' => [
                    'pedidos' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['pedidos'] = $this->get_products_pedido_facturar($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_print($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'tipo', 'subtotal', 'descuentoPorcentaje', 'descuentoCantidad', 'total', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre', 'telefono AS clienteTelefono', 'calle', 'numeroExterior', 'numeroInterior', 'colonia', 'codigoPostal', 'ciudad', 'estado'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'pedidos' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['pagos'] = $this->get_payments($preparedResult[0]['id']);
            $preparedResult[0]['pedidos'] = $this->get_products_pedido($preparedResult[0]['id']);
            $preparedResult[0]['clienteDireccionCompleta'] = $this->get_complete_address($preparedResult[0]);
            $preparedResult[0]['seccionNombre'] = $GLOBALS['usuario']['seccionActual']['nombre'];
            $preparedResult[0]['seccionImagen'] = $GLOBALS['usuario']['seccionActual']['imagen'];
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['pedidos'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        if (!empty($data['filtros']['cliente'])) {
            $wheres['pedidos'][] = ['i', 'clienteID', '=', $data['filtros']['cliente']];
        }
        if (!empty($data['filtros']['tipo'])) {
            $wheres['pedidos'][] = ['s', 'tipo', '=', $data['filtros']['tipo']];
        }
        if (!empty($data['filtros']['estado'])) {
            switch ($data['filtros']['estado']) {
                case 'pendientes':
                    $wheres['pedidos'][] = ['i', 'finalizado', '=', '0'];
                    $wheres['pedidos'][] = ['i', 'cancelado', '=', '0'];
                    break;
                case 'pagados':
                    $wheres['pedidos'][] = ['i', 'finalizado', '=', '1'];
                    $wheres['pedidos'][] = ['i', 'cancelado', '=', '0'];
                    break;
                case 'cancelados':
                    $wheres['pedidos'][] = ['i', 'cancelado', '=', '1'];
                    break;
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && !in_array('reportes', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else {
            if (!empty($data['filtros']['usuario'])) {
                $wheres['pedidos'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'tipo', 'total', 'impreso', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['pagado'] = $this->get_payed($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_facturar($data) {
        $result = ['error' => ''];
        $wheres = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ['i', 'cancelado', '=', '0']
            ]
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            switch (count($date)) {
                case '1':
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                    break;
                case '2':
                    $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                    break;
                case '3':
                    $wheres['pedidos'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                    $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                    break;
            }
        }
        if (!empty($data['clienteID'])) {
            $wheres[] = ['i', 'clienteID', '=', $data['clienteID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => [
                        'id',
                        'folio',
                        'total',
                        'fechaCreate'
                    ],
                    'clientes' => [
                        'nombre AS clienteNombre'
                    ]
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID']
                ],
                'wheres' => $wheres,
                'orders' => [
                    ['folio', 'ASC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['pedidos'] = $this->get_products_pedido_facturar($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_desktop($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['pedidos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['pedidos'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'tipo', 'total', 'impreso', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre', 'calle', 'colonia', 'ciudad', 'estado', 'codigoPostal', 'numeroExterior', 'numeroInterior'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['pagado'] = $this->get_payed($preparedResult[$i]['id']);
                $preparedResult[$i]['pagos'] = $this->get_payments($preparedResult[$i]['id']);
                $preparedResult[$i]['pedidos'] = $this->get_products_pedido($preparedResult[$i]['id']);
                $preparedResult[$i]['clienteDireccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_pendings($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['id', 'folio', 'clienteID', 'folio', 'tipo', 'total', 'descuentoPorcentaje', 'descuentoCantidad', 'impreso', 'cancelado', 'fechaCreate', 'UNIX_TIMESTAMP(*.fechaCreate) AS fecha'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'pedidos' => [
                        ['i', 'finalizado', '=', '0'],
                        ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ],
                'orders' => [
                    'pedidos' => [
                        ['fechaCreate', 'DESC']
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['pagos'] = [];
                $preparedResult[$i]['pedidoProductos'] = [];
                $pedidoProductos = $this->get_products_pedido($preparedResult[$i]['id']);
                $payments = $this->get_payments($preparedResult[$i]['id']);
                if ($pedidoProductos['error'] == '') {
                    $preparedResult[$i]['pedidoProductos'] = $pedidoProductos['resultado'];
                }
                if ($payments['error'] == '') {
                    $preparedResult[$i]['pagos'] = $payments['resultado'];
                }
                $preparedResult[$i]['pagado'] = $this->get_payed($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_products_pedido_facturar($pedidoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => [
                    'pedidos_productos' => ['id', 'productoID', 'nombre', 'cantidad', 'precio', 'total', 'unidad'],
                    'productos' => ['folio', 'claveSAT', 'impuesto']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'pedidos_productos' => [
                        ['i', 'pedidoID', '=', $pedidoID],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_products_pedido($pedidoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_productos',
                'columns' => [
                    'pedidos_productos' => ['id', 'productoID', 'nombre', 'cantidad', 'cantidadInventario', 'precio', 'costo', 'total', 'unidad'],
                    'productos' => ['folio']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'pedidos_productos' => [
                        ['i', 'pedidoID', '=', $pedidoID],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payments($pedidoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_pagos',
                'columns' => ['id', 'metodo', 'cantidad', 'cancelado', 'fechaCreate', 'UNIX_TIMESTAMP(fechaCreate) AS fecha'],
                'wheres' => [
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payment($pedidoID, $paymentID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_pagos',
                'columns' => ['id', 'metodo', 'cantidad', 'cancelado', 'fechaCreate'],
                'wheres' => [
                    ['i', 'id', '=', $paymentID],
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_payed($pedidoID) {
        $payed = 0;
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos_pagos',
                'columns' => ['cantidad'],
                'wheres' => [
                    ['i', 'pedidoID', '=', $pedidoID],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                return $payed;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $payed += $preparedResult[$i]['cantidad'];
            }
        }
        catch (Exception $err) {
        }
        return $payed;
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'pedidos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'pedidos' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'clientes' => [
                    ['s', 'nombre', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'pedidos' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'pedidos' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function get_news($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        if (isset($data['filtros']['fecha'])) {
            $date = explode('/', $data['filtros']['fecha']);
            switch (count($date)) {
                case '1':
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                    break;
                case '2':
                    $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                    break;
                case '3':
                    $wheres['pedidos'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                    $wheres['pedidos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                    $wheres['pedidos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                    break;
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['pedidos'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        if ($data['ultimoID'] > 0) {
            $wheres['pedidos'][] = ['i', 'id', '>', $data['ultimoID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'pedidos',
                'columns' => [
                    'pedidos' => ['MAX(*.id) AS idMax', 'COUNT(id) AS nuevos'],
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres
            ]);
            if ($preparedResult[0]['nuevos'] == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    
    function add_debt($clientID, $quantity) {
        $result = ['error' => ''];
        $clientData = $this->get_client($clientID)['resultado'];
        try {
            $newDebt = $clientData['deuda'] + $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['d', 'deuda', $newDebt]
                ],
                'wheres' => [
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function subtract_debt($clientID, $quantity) {
        $result = ['error' => ''];
        $clientData = $this->get_client($clientID)['resultado'];
        try {
            $newDebt = $clientData['deuda'] - $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['d', 'deuda', max($newDebt, 0)]
                ],
                'wheres' => [
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complete_address($data) {
        $street = [];
        if ($data['calle'] != '') { $street[] = $data['calle']; }
        if ($data['numeroExterior'] != '') { $street[] = '#' . $data['numeroExterior']; }
        if ($data['numeroInterior'] != '') { $street[] = 'Int. ' . $data['numeroInterior']; }
        $street = implode(' ', $street);
        $address = [];
        if ($street != '') { $address[] = $street; }
        if ($data['colonia'] != '') { $address[] = $data['colonia']; }
        if ($data['codigoPostal'] != '') {$address[] = $data['codigoPostal']; }
        if ($data['ciudad'] != '') { $address[] = $data['ciudad']; }
        if ($data['estado'] != '') { $address[] = $data['estado']; }
        $address = implode(', ', $address);
        return $address;
    }

}

?>