<?php

include_once('classes/Notificacion.php');

class Ruta {

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function check_user_assigned($userID) {
        try {
            $prepareResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'rutas',
                'columns' => ['id'],
                'wheres' => [
                    ['i', 'usuarioID', '=', $userID]
                ]
            ]);
            if (count($prepareResult) == 0) {
                return false;
            }
        }
        catch (Exception $err) {
            return true;
        }
        return true;
    }

    function add($data) {
        $result = ['error' => ''];
        $userID = $data['usuarioID'] == 0 ? NULL : $data['usuarioID'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'rutas',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['i', 'usuarioID', $userID],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
            $this->notificacion->create('ruta.add', [
                'id' => $result['resultado']['id'],
                'nombre' => $data['nombre']
            ]);
            if (isset($data['clientes'])) {
                $data['clientes'] = json_decode($data['clientes'], true);
                for ($i=0; $i<count($data['clientes']); $i++) {
                    $clientData = $this->get_client($data['clientes'][$i]['id'])['resultado'];
                    try {
                        $clientsResult = $GLOBALS['DB']->prepareUpdate([
                            'table' => 'clientes',
                            'values' => [
                                ['i', 'rutaID', $result['resultado']['id']],
                                ['i', 'rutaDias', $data['clientes'][$i]['rutaDias']],
                            ],
                            'wheres' => [
                                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                                ['i', 'id', '=', $data['clientes'][$i]['id']]
                            ]
                        ]);
                        $this->notificacion->create('cliente.routed', [
                            'id' => $data['clientes'][$i]['id'],
                            'folio' => $clientData['folio'],
                            'nombre' => $clientData['nombre'],
                            'rutaID' => $result['resultado']['id'],
                            'rutaNombre' => $data['nombre']
                        ]);
                    }
                    catch (Exception $err) {
                        $result['error'] = $err->getMessage();
                    }
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $routeData = $this->get(['id' => $data['id']])['resultado'];
        try {
            $userID = $data['usuarioID'] == 0 ? NULL : $data['usuarioID'];
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'rutas',
                'values' => [
                    ['i', 'usuarioID', $userID],
                    ['s', 'nombre', $data['nombre']]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('ruta.edit', [
                'id' => $data['id'],
                'nombre' => $routeData['nombre']
            ]);
            if ($userID != $routeData['usuarioID']) {
                if ($userID == NULL) {
                    $this->notificacion->create('ruta.unassign', [
                        'id' => $data['id'],
                        'folio' => $routeData['folio']
                    ]);
                }
                else {
                    $userData = $this->get_user($userID)['resultado'];
                    $this->notificacion->create('ruta.assign', [
                        'id' => $data['id'],
                        'nombre' => $routeData['nombre'],
                        'usuarioID' => $userData['id'],
                        'usuarioNombre' => $userData['nombre']
                    ]);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $ids = implode(',', $data['ids']);
        $rutasData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $rutasData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'rutas',
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('ruta.delete', [
                    'id' => $rutasData[$i]['id'],
                    'nombre' => $rutasData[$i]['nombre'],
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_user($userID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id', 'nombre'],
                'wheres' => [
                    ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%'],
                    ['i', 'id', '=', $userID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_client($clientID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => ['id', 'folio', 'nombre'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'rutas',
                'columns' => ['id', 'nombre', 'usuarioID'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $preparedResult[0]['clientes'] = $this->get_clients($data['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all() {
        $result = ['error' => ''];
        $orders = $this->get_order($_POST['metodo'], $_POST['orden']);
        $where = $this->get_where($_POST['busqueda']);
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'rutas',
                'columns' => [
                    'rutas' => ['id', 'usuarioID', 'nombre'],
                    'usuarios' => ['COALESCE(*.nombre, "S/D") AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' =>  ['id', 'usuarioID']
                ],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_clients($id) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => ['id', 'folio', 'nombre', 'telefono', 'email', 'calle', 'colonia', 'ciudad', 'estado', 'codigoPostal', 'numeroExterior', 'numeroInterior', 'credito', 'rutaID', 'rutaDias', 'lat', 'lng'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'rutaID', '=', $id]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complete_address($data) {
        $street = [];
        if ($data['calle'] != '') { $street[] = $data['calle']; }
        if ($data['numeroExterior'] != '') { $street[] = '#' . $data['numeroExterior']; }
        if ($data['numeroInterior'] != '') { $street[] = 'Int. ' . $data['numeroInterior']; }
        $street = implode(' ', $street);
        $address = [];
        if ($street != '') { $address[] = $street; }
        if ($data['colonia'] != '') { $address[] = $data['colonia']; }
        if ($data['codigoPostal'] != '') {$address[] = $data['codigoPostal']; }
        if ($data['ciudad'] != '') { $address[] = $data['ciudad']; }
        if ($data['estado'] != '') { $address[] = $data['estado']; }
        $address = implode(', ', $address);
        return $address;
    }

    function set_clients_route($data) {
        $result = ['error' => ''];
        $clientsData = [];
        $rutaData = $this->get(['id' => $data['rutaID']])['resultado'];
        for ($i=0; $i<count($data['clientesIDs']); $i++) {
            $clientsData[] = $this->get_client($data['clientesIDs'][$i])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['i', 'rutaID', $data['rutaID']],
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['clientesIDs']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['clientesIDs']); $i++) {
                $clientsData[] = $this->get_client($data['clientesIDs'][$i])['resultado'];
                $this->notificacion->create('cliente.routed', [
                    'id' => $clientsData[$i]['id'],
                    'folio' => $clientsData[$i]['folio'],
                    'nombre' => $clientsData[$i]['nombre'],
                    'rutaID' => $data['rutaID'],
                    'rutaNombre' => $rutaData['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function unset_clients_route($data) {
        $result = ['error' => ''];
        $clientsData = [];
        for ($i=0; $i<count($data['clientesIDs']); $i++) {
            $clientsData[] = $this->get_client($data['clientesIDs'][$i])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['i', 'rutaID', NULL],
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['clientesIDs']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['clientesIDs']); $i++) {
                $clientsData[] = $this->get_client($data['clientesIDs'][$i])['resultado'];
                $this->notificacion->create('cliente.unrouted', [
                    'id' => $clientsData[$i]['id'],
                    'folio' => $clientsData[$i]['folio'],
                    'nombre' => $clientsData[$i]['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function change_client_route_days($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['i', 'rutaDias', $data['rutaDias']],
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'rutaID', '=', $data['rutaID']],
                    ['i', 'id', '=', $data['clienteID']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_where($search) {
        $search = '%' . $search . '%';
        $where = [
            'rutas' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'rutas' => [
                    ['s', 'nombre', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ],
            ]
        ];
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'nombre':
                $orderArray = [
                    'rutas' => [
                        ['nombre', $orderValue]
                    ]
                ];
                break;
            case 'vendedor':
                $orderArray = [
                    'usuario' => [
                        ['nombre', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

}

?>