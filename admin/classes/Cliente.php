<?php

include_once('classes/Notificacion.php');

class Cliente {

    public $notificacion;

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function add($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'clientes',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'telefono', $data['telefono']],
                    ['s', 'email', $data['email']],
                    ['s', 'calle', $data['calle']],
                    ['s', 'colonia', $data['colonia']],
                    ['s', 'ciudad', $data['ciudad']],
                    ['s', 'estado', $data['estado']],
                    ['s', 'codigoPostal', $data['codigoPostal']],
                    ['s', 'numeroExterior', $data['numeroExterior']],
                    ['s', 'numeroInterior', $data['numeroInterior']],
                    ['d', 'credito', $data['credito']],
                    ['d', 'deuda', 0],
                    ['d', 'rutaID', $data['rutaID'] == 0 ? NULL : $data['rutaID']],
                    ['i', 'rutaDias', $data['rutaDias']],
                    ['d', 'lat', $data['lat']],
                    ['d', 'lng', $data['lng']],
                    ['i', 'bloqueado', 0],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
            $this->notificacion->create('cliente.add', [
                'id' => $result['resultado']['id'],
                'nombre' => $data['nombre']
            ]);
            if ($data['rutaID'] != 0) {
                $routeData = $this->get_route($data['rutaID'])['resultado'];
                $this->notificacion->create('cliente.routed', [
                    'id' => $result['resultado']['id'],
                    'nombre' => $data['nombre'],
                    'rutaID' => $routeData['id'],
                    'rutaNombre' => $routeData['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_route($routeID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'rutas',
                'columns' => ['id', 'nombre'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $routeID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $clientData = $this->get(['id' => $data['id']])['resultado'];
        if ($data['id'] != 1) {
            try {
                $preparedResult = $GLOBALS['DB']->prepareUpdate([
                    'table' => 'clientes',
                    'values' => [
                        ['s', 'nombre', $data['nombre']],
                        ['s', 'telefono', $data['telefono']],
                        ['s', 'email', $data['email']],
                        ['s', 'calle', $data['calle']],
                        ['s', 'colonia', $data['colonia']],
                        ['s', 'ciudad', $data['ciudad']],
                        ['s', 'estado', $data['estado']],
                        ['s', 'codigoPostal', $data['codigoPostal']],
                        ['s', 'numeroExterior', $data['numeroExterior']],
                        ['s', 'numeroInterior', $data['numeroInterior']],
                        ['d', 'credito', $data['credito']],
                        ['d', 'rutaID', $data['rutaID'] == 0 ? NULL : $data['rutaID']],
                        ['i', 'rutaDias', $data['rutaDias']],
                        ['d', 'lat', $data['lat']],
                        ['d', 'lng', $data['lng']]
                    ],
                    'wheres' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]);
                if ($preparedResult['affected_rows'] == -1) {
                    $result['error'] = 'NOT_FOUND';
                    return $result;
                }
                $this->notificacion->create('cliente.edit', [
                    'id' => $data['id'],
                    'nombre' => $data['nombre']
                ]);
                $clientData['rutaID'] = $clientData['rutaID'] == NULL ? 0 : $clientData['rutaID'];
                if ($clientData['rutaID'] != $data['rutaID']) {
                    if ($data['rutaID'] == 0) {
                        $this->notificacion->create('cliente.unrouted', [
                            'id' => $data['id'],
                            'nombre' => $data['nombre']
                        ]);
                    }
                    else {
                        $routeData = $this->get_route($data['rutaID'])['resultado'];
                        $this->notificacion->create('cliente.routed', [
                            'id' => $data['id'],
                            'nombre' => $data['nombre'],
                            'rutaID' => $routeData['id'],
                            'rutaNombre' => $routeData['nombre']
                        ]);
                    }
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'CANT_EDIT';
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $clientsData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $clientsData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'clientes',
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('cliente.delete', [
                    'id' => $clientsData[$i]['id'],
                    'nombre' => $clientsData[$i]['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        $wheres = [
            ['i', 'id', '=', $data['id']]
        ];
        if ($data['id'] != 1) {
            $wheres[] = ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => [
                    'id',
                    'nombre',
                    'telefono',
                    'email',
                    'calle',
                    'colonia',
                    'ciudad',
                    'estado',
                    'codigoPostal',
                    'numeroExterior',
                    'numeroInterior',
                    'credito',
                    'deuda',
                    'rutaID',
                    'rutaDias',
                    'lat',
                    'lng',
                    'bloqueado'
                ],
                'wheres' => $wheres
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all() {
        $result = ['error' => ''];
        $orders = $this->get_order($_POST['metodo'], $_POST['orden']);
        $where = $this->get_where($_POST['busqueda']);

        if (isset($_POST['filtros'])) {
            if (isset($_POST['filtros']['rutaID'])) {
                if ($_POST['filtros']['rutaID'] != -1) {
                    if ($_POST['filtros']['rutaID'] == 0) {
                        $where['clientes'][] = ['i', 'rutaID', 'IS', NULL];
                    }
                    else {
                        $where['clientes'][] = ['i', 'rutaID', '=', $_POST['filtros']['rutaID']];
                    }
                }
            }
            if (isset($_POST['filtros']['deudor'])) {
                if ($_POST['filtros']['deudor'] == 1) {
                    $where['clientes'][] = ['i', 'deuda', '>', '0'];
                }
            }
            if (isset($_POST['filtros']['bloqueado'])) {
                if ($_POST['filtros']['bloqueado'] == 1) {
                    $where['clientes'][] = ['i', 'bloqueado', '=', '1'];
                }
            }
            if (isset($_POST['filtros']['dias'])) {
                switch ($_POST['filtros']['dias']) {
                    case -1:
                        $where['clientes'][] = ['i', 'rutaDias', '=', '0'];
                        break;
                    case 1:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '1'];
                        break;
                    case 2:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '2'];
                        break;
                    case 3:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '4'];
                        break;
                    case 4:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '8'];
                        break;
                    case 5:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '16'];
                        break;
                    case 6:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '32'];
                        break;
                    case 7:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '64'];
                        break;
                }
            }
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => [
                    'clientes' => [
                        'id',
                        'nombre',
                        'telefono',
                        'email',
                        'calle',
                        'colonia',
                        'ciudad',
                        'estado',
                        'codigoPostal',
                        'numeroExterior',
                        'numeroInterior',
                        'credito',
                        'deuda',
                        'rutaID',
                        'rutaDias',
                        'lat',
                        'lng',
                        'bloqueado'
                    ],
                    'rutas' => ['nombre AS rutaNombre']
                ],
                'leftJoins' => [
                    'rutas' => ['id', 'rutaID']
                ],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_list() {
        $result = ['error' => ''];
        $orders = $this->get_order($_POST['metodo'], $_POST['orden']);
        $where = $this->get_where($_POST['busqueda']);

        if (isset($_POST['filtros'])) {
            if (isset($_POST['filtros']['rutaID'])) {
                if ($_POST['filtros']['rutaID'] != -1) {
                    if ($_POST['filtros']['rutaID'] == 0) {
                        $where['clientes'][] = ['i', 'rutaID', 'IS', NULL];
                    }
                    else {
                        $where['clientes'][] = ['i', 'rutaID', '=', $_POST['filtros']['rutaID']];
                    }
                }
            }
            if (isset($_POST['filtros']['deudor'])) {
                if ($_POST['filtros']['deudor'] == 1) {
                    $where['clientes'][] = ['i', 'deuda', '>', '0'];
                }
            }
            if (isset($_POST['filtros']['bloqueado'])) {
                if ($_POST['filtros']['bloqueado'] == 1) {
                    $where['clientes'][] = ['i', 'bloqueado', '=', '1'];
                }
            }
            if (isset($_POST['filtros']['dias'])) {
                switch ($_POST['filtros']['dias']) {
                    case -1:
                        $where['clientes'][] = ['i', 'rutaDias', '=', '0'];
                        break;
                    case 1:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '1'];
                        break;
                    case 2:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '2'];
                        break;
                    case 3:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '4'];
                        break;
                    case 4:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '8'];
                        break;
                    case 5:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '16'];
                        break;
                    case 6:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '32'];
                        break;
                    case 7:
                        $where['clientes'][] = ['i', 'rutaDias', '&', '64'];
                        break;
                }
            }
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => [
                    'clientes' => [
                        'id',
                        'nombre',
                        'telefono',
                        'email',
                        'calle',
                        'colonia',
                        'ciudad',
                        'estado',
                        'codigoPostal',
                        'numeroExterior',
                        'numeroInterior',
                        'credito',
                        'deuda',
                        'rutaID',
                        'rutaDias',
                        'lat',
                        'lng',
                        'bloqueado'
                    ],
                    'rutas' => ['nombre AS rutaNombre']
                ],
                'leftJoins' => [
                    'rutas' => ['id', 'rutaID']
                ],
                'orders' => $orders,
                'wheres' => $where,
                'limit' => [100, $_POST['offset']]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_min() {
        $result = ['error' => ''];

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => [
                    'id',
                    'nombre'
                ],
                'orders' => [
                    ['id', 'ASC']
                ],
                'wheres' => [
                    ['i', 'id', '=', 1, 'OR'],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_routeless() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => [
                    'id',
                    'nombre',
                    'telefono',
                    'email',
                    'calle',
                    'colonia',
                    'ciudad',
                    'estado',
                    'codigoPostal',
                    'numeroExterior',
                    'numeroInterior',
                    'rutaDias',
                    'lat',
                    'lng'
                ],
                'orders' => [
                    ['id', 'ASC']
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '!=', 1],
                    ['i', 'rutaID', 'IS', NULL]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complete_address($data) {
        $street = [];
        if (isset($data['calle']) && $data['calle'] != '') { $street[] = $data['calle']; }
        if (isset($data['numeroExterior']) && $data['numeroExterior'] != '') { $street[] = '#' . $data['numeroExterior']; }
        if (isset($data['numeroInterior']) && $data['numeroInterior'] != '') { $street[] = 'Int. ' . $data['numeroInterior']; }
        $street = implode(' ', $street);
        $address = [];
        if ($street != '') { $address[] = $street; }
        if (isset($data['colonia']) && $data['colonia'] != '') { $address[] = $data['colonia']; }
        if (isset($data['codigoPostal']) && $data['codigoPostal'] != '') {$address[] = $data['codigoPostal']; }
        if (isset($data['ciudad']) && $data['ciudad'] != '') { $address[] = $data['ciudad']; }
        if (isset($data['estado']) && $data['estado'] != '') { $address[] = $data['estado']; }
        $address = implode(', ', $address);
        return $address;
    }

    function get_where($search) {
        $search = '%' . $search . '%';
        $where = [
            'GROUP_1' => [
                'clientes' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id'], 'OR'],
                    ['i', 'id', '=', 1],
                ],
            ],
            'GROUP_2' => [
                'clientes' => [
                    ['s', 'nombre', 'LIKE', $search, 'OR'],
                    ['s', 'telefono', 'LIKE', $search, 'OR'],
                    ['s', 'email', 'LIKE', $search, 'OR'],
                    ['s', 'calle', 'LIKE', $search, 'OR'],
                    ['s', 'CONCAT("#", *.numeroExterior)', 'LIKE', $search, 'OR'],
                    ['s', 'CONCAT("Int. ", *.numeroInterior)', 'LIKE', $search, 'OR'],
                    ['s', 'colonia', 'LIKE', $search, 'OR'],
                    ['s', 'ciudad', 'LIKE', $search, 'OR'],
                    ['s', 'estado', 'LIKE', $search, 'OR']
                ],
                'rutas' => [
                    ['s', 'COALESCE(CONCAT("Ruta ", *.nombre), "S/D")', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'clientes' => [
                        ['id', $orderValue]
                    ]
                ];
                break;
            case 'ruta':
                $orderArray = [
                    'clientes' => [
                        ['rutaID IS NULL', $orderValue]
                    ]
                ];
                break;
            case 'nombre':
                $orderArray = [
                    'clientes' => [
                        ['nombre', $orderValue]
                    ]
                ];
                break;
            case 'direccion':
                $orderArray = [
                    'clientes' => [
                        ['calle', $orderValue],
                        ['numeroInterior', $orderValue],
                        ['numeroExterior', $orderValue],
                        ['colonia', $orderValue],
                        ['ciudad', $orderValue],
                        ['estado', $orderValue],
                        ['codigoPostal', $orderValue]
                    ]
                ];
                break;
            case 'deuda':
                $orderArray = [
                    'clientes' => [
                        ['deuda', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function lock($data) {
        $result = ['error' => ''];
        $clientsData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $clientsData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['i', 'bloqueado', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                if ($clientsData[$i]['bloqueado'] == 0) {
                    $this->notificacion->create('cliente.lock', [
                        'id' => $clientsData[$i]['id'],
                        'nombre' => $clientsData[$i]['nombre']
                    ]);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function unlock($data) {
        $result = ['error' => ''];
        $clientsData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $clientsData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['i', 'bloqueado', 0]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                if ($clientsData[$i]['bloqueado'] == 1) {
                    $this->notificacion->create('cliente.unlock', [
                        'id' => $clientsData[$i]['id'],
                        'nombre' => $clientsData[$i]['nombre']
                    ]);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>