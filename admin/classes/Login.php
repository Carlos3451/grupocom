<?php

class Login {

    function keep_online() {
        $result = ['error' => ''];
        return $result;
    }

    function check_cookie() {
        $result = ['error' => ''];
        if (isset($_COOKIE['PBGUj8WtVXZVGfn2'])) {
            try {
                $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                    'table' => 'usuarios',
                    'columns' => ['id', 'nombre', 'privilegios', 'privilegiosNivel', 'secciones', 'seccionActual'],
                    'wheres' => [
                        ['s', 'cookie', '=', $_COOKIE['PBGUj8WtVXZVGfn2']]
                    ]
                ]);
                if (count($preparedResult)==0) {
                    $result['error'] = 'NOT_FOUND';
                    return $result;
                }
                /* PRIVILEGIOS */
                $preparedResult[0]['privilegios'] = explode(',', $preparedResult[0]['privilegios']);
                /* SECCIONES */
                $sections = explode(',', $preparedResult[0]['secciones']);
                $section = $this->get_section($preparedResult[0]['seccionActual']);
                $preparedResult[0]['secciones'] = [];
                if ($section['error'] == '') {
                    $preparedResult[0]['seccionActual'] = $section['resultado'];
                }
                if ($preparedResult[0]['id'] == 1) {
                    $sectionsResult = $this->get_all_sections();
                    if ($sectionsResult['error'] == '') {
                        $sections = $sectionsResult['resultado'];
                        for ($i=0; $i<count($sections); $i++) {
                            $preparedResult[0]['secciones'][] = $sections[$i];
                        }
                    }         
                }
                else {
                    for ($i=0; $i<count($sections); $i++) {
                        $section = $this->get_section($sections[$i]);
                        if ($section['error'] == '') {
                            $preparedResult[0]['secciones'][] = $section['resultado'];
                        }
                    }
                }
                $preparedResult[0]['notificaciones'] = 0;
                $notificacionesResult =  $this->get_notifications($preparedResult[0]['id'], $preparedResult[0]['seccionActual']['id']);
                if ($notificacionesResult['error'] == '') {
                    $preparedResult[0]['notificaciones'] = $notificacionesResult['resultado'];
                }
                $preparedResult[0]['imagen'] = $this->get_user_image($preparedResult[0]['id']);
                $result['resultado'] = $preparedResult[0];
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'NOT_LOGIN';
        }
        return $result;
    }

    function get_notifications($userID, $sectionID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareGetRows([
                'table' => 'notificaciones_usuarios',
                'wheres' => [
                    ['i', 'usuarioID', '=', $userID],
                    ['i', 'visto', '=', 0],
                    ['i', 'seccionID', '=', $sectionID]
                ]
            ]);
            $result['resultado'] = $preparedResult['filas'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_section($sectionID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id', 'nombre', 'claveGerente', 'lat', 'lng', 'facturacion'],
                'wheres' => [
                    ['i', 'id', '=', $sectionID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $preparedResult[0]['imagen'] = $this->get_section_image($sectionID);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_sections() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id', 'nombre', 'claveGerente', 'lat', 'lng']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['imagen'] = $this->get_section_image($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function change_section($data) {
        $result = ['error' => ''];
        if (!isset($GLOBALS['usuario'])) {
            $result['error'] = 'NOT_LOGIN';
            return $result;
        }
        if ($GLOBALS['usuario']['id'] == 1 || $this->check_section($data['id'])) {
            try {
                $preparedResult = $GLOBALS['DB']->prepareUpdate([
                    'table' => 'usuarios',
                    'values' => [
                        ['i', 'seccionActual', $data['id']]
                    ],
                    'wheres' => [                        
                        ['i', 'id', '=', $GLOBALS['usuario']['id']]
                    ]
                ]);
                $result['resultado'] = $preparedResult;
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'NO_SECTION_ASSIGNED';
        }
        return $result;
    }

    function check_section($sectionID) {
        $sections = $GLOBALS['usuario']['secciones'];
        for ($i=0; $i<count($sections); $i++) {
            if ($sections[$i]['id'] == $sectionID) {
                return true;
            }
        }
        return false;
    }

    function get_section_image($sectionID) {
        $imgsArray = [];
        $imgPath = '';
        $dirPath = '/secciones/' . $sectionID;
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirPath)) {
            foreach (array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $dirPath), array('..', '.')) as $img) {
                $imgsArray[] = $img;
            }
            if (count($imgsArray)!=0) {
                $imgPath = $dirPath . '/' . $imgsArray[0];
            }
        }
        return $imgPath;
    }

    function get_user_image($userID) {
        $imgsArray = [];
        $dirPath = '/usuarios/' . $userID;
        $imgPath = '/usuarios/0/default.jpg';
        if (file_exists($dirPath)) {
            foreach (array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $dirPath), array('..', '.')) as $img) {
                $imgsArray[] = $img;
            }
            if (count($imgsArray)!=0) {
                $imgPath = $dirPath . '/' . $imgsArray[0];
            }
        }
        return $imgPath;
    }

    function check($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => [
                    'usuarios' => ['id', 'nombre', 'privilegios', 'privilegiosNivel', 'contraseña'],
                    'secciones' => ['id AS seccionID', 'nombre AS seccionNombre', 'claveGerente'],
                    'rutas' => ['id AS rutaID']
                ],
                'leftJoins' => [
                    'secciones' => ['id', 'seccionActual'],
                    'rutas' => ['usuarioID', 'id']
                ],
                'wheres' => [
                    'usuarios' => [
                        ['s', 'usuario', '=', $data['usuario']]
                    ]
                ]
            ]);
            if (count($preparedResult)==0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            if (password_verify($data['contraseña'], $preparedResult[0]['contraseña'])) {
                try {
                    $cookieValue = password_hash(rand(), PASSWORD_DEFAULT);
                    $GLOBALS['DB']->prepareUpdate([
                        'table' => 'usuarios',
                        'values' => [
                            ['s', 'cookie', $cookieValue]
                        ],
                        'wheres' => [
                            ['i', 'id', '=', $preparedResult[0]['id']]
                        ]
                    ]);
                    setcookie('PBGUj8WtVXZVGfn2', $cookieValue, time() + (60 * 60 * 24 * 7), '/');
                    $result['resultado']['id'] = $preparedResult[0]['id'];
                    $result['resultado']['nombre'] = $preparedResult[0]['nombre'];
                    $result['resultado']['seccionID'] = $preparedResult[0]['seccionID'];
                    $result['resultado']['seccionNombre'] = $preparedResult[0]['seccionNombre'];
                    $result['resultado']['claveGerente'] = $preparedResult[0]['claveGerente'];
                    $result['resultado']['privilegios'] = $preparedResult[0]['privilegios'];
                    $result['resultado']['privilegiosNivel'] = $preparedResult[0]['privilegiosNivel'];
                    $result['resultado']['rutaID'] = $preparedResult[0]['rutaID'];
                    $result['resultado']['cookie'] = $cookieValue;
                }
                catch (Exception $err) {
                    $result['error'] = $err->getMessage();
                }
            }
            else {              
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

        }
        catch (Exception $err) {
			$result['error'] = $err->getMessage();
        }
        return $result;
    }

    function logout() {
        $result = ['error' => ''];
        setcookie('PBGUj8WtVXZVGfn2', null, -1, '/');
        return $result;     
    }
}

?>