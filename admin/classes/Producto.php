<?php

include_once('classes/Notificacion.php');

class Producto {

    public $notificacion;

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function add($data) {
        $result = ['error' => ''];
        try {
            $data['categoriaID'] = $data['categoriaID'] == 0 ? NULL : $data['categoriaID'];
            $data['proveedorID'] = $data['proveedorID'] == 0 ? NULL : $data['proveedorID'];
            $claveSAT = isset($data['claveSAT']) ? $data['claveSAT'] : '';
            $impuesto = isset($data['impuesto']) ? $data['impuesto'] : 'tasacero';
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'productos',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'codigo', $data['codigo']],
                    ['s', 'unidad', $data['unidad']],
                    ['d', 'costo', $data['costo']],
                    ['d', 'precio1', $data['precio1']],
                    ['d', 'precio2', $data['precio2']],
                    ['d', 'precio3', $data['precio3']],
                    ['d', 'inventarioMinimo', $data['inventarioMinimo']],
                    ['i', 'bloqueado', 0],
                    ['i', 'categoriaID', $data['categoriaID']],
                    ['i', 'proveedorID', $data['proveedorID']],
                    ['s', 'claveSAT', $claveSAT],
                    ['s', 'impuesto', $impuesto]
                ]
            ]);
            $result['resultado'] = $preparedResult;

            if (isset($data['productoUnidades'])) {
                $data['productoUnidades'] = json_decode($data['productoUnidades'], true);
                if (!empty($data['productosUnidades'])) {
                    $this->add_producto_unidades($result['resultado']['id'], $data['productoUnidades']);
                }
            }

            $this->add_productos_existencias($preparedResult['id']);

            $this->notificacion->create('producto.add', [
                'id' => $result['resultado']['id'],
                'nombre' => $data['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $product = $this->get(['id' => $data['id']])['resultado'];
        try {
            $data['categoriaID'] = $data['categoriaID'] == 0 ? NULL : $data['categoriaID'];
            $data['proveedorID'] = $data['proveedorID'] == 0 ? NULL : $data['proveedorID'];
            $claveSAT = isset($data['claveSAT']) ? $data['claveSAT'] : '';
            $impuesto = isset($data['impuesto']) ? $data['impuesto'] : 'tasacero';
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'codigo', $data['codigo']],
                    ['s', 'unidad', $data['unidad']],
                    ['d', 'costo', $data['costo']],
                    ['d', 'precio1', $data['precio1']],
                    ['d', 'precio2', $data['precio2']],
                    ['d', 'precio3', $data['precio3']],
                    ['d', 'inventarioMinimo', $data['inventarioMinimo']],
                    ['i', 'categoriaID', $data['categoriaID']],
                    ['s', 'claveSAT', $claveSAT],
                    ['s', 'impuesto', $impuesto],
                    ['i', 'proveedorID', $data['proveedorID']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            $this->delete_producto_unidades($data['id']);
            if (isset($data['productoUnidades'])) {
                $data['productoUnidades'] = json_decode($data['productoUnidades'], true);
                if (!empty($data['productosUnidades'])) {
                    $this->add_producto_unidades($data['id'], $data['productoUnidades']);
                }
            }

            $this->notificacion->create('producto.edit', [
                'id' => $data['id'],
                'nombre' => $data['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_existence($data) {
        $result = ['error' => ''];
        $productoResult = $this->get(['id' => $data['id']]);
        if ($productoResult['error'] != '') {
            $result['error'] = 'GET_PRODUCT:' . $productoResult['error'];
            return $result;
        }
        $productoData = $productoResult['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos_existencias',
                'values' => [
                    ['d', 'existencia', $data['cantidad'], 'existencia + ?']
                ],
                'wheres' => [
                    ['i', 'productoID', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('existencia.add', [
                'id' => $data['id'],
                'cantidad' => $data['cantidad'],
                'unidad' => $productoData['unidad'],
                'nombre' => $productoData['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function lock($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['i', 'bloqueado', 1]
                ],
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function unlock($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['i', 'bloqueado', 0]
                ],
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $productos = [];   
        for ($i=0; $i<count($data['ids']); $i++) {
            $productos[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'productos',
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('producto.remove', [
                    'id' => $data['ids'][$i],
                    'nombre' => $productos[$i]['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos_existencias',
                'columns' => [
                    'productos' => [
                        'id',
                        'nombre',
                        'codigo',
                        'unidad',
                        'costo',
                        'precio1',
                        'precio2',
                        'precio3',
                        'inventarioMinimo',
                        'categoriaID',
                        'proveedorID',
                        'claveSAT',
                        'impuesto'
                    ],
                    'productos_existencias' => [
                        'existencia'
                    ]
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID'],
                    'categorias' => ['id', 'productos.categoriaID']
                ],
                'wheres' => [
                    'productos_existencias' => [
                        ['i', 'productoID', '=', $data['id']],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['productoUnidades'] = [];
            $productoUnidadesResult = $this->get_producto_unidades($data['id']);
            if ($productoUnidadesResult['error'] == '') {
                $preparedResult[0]['productoUnidades'] = $productoUnidadesResult['resultado'];
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $orders = $this->get_order($data['metodo'], $data['orden']);
        $where = $this->get_where($data['busqueda']);
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['proveedorID'])) {
                if ($data['filtros']['proveedorID'] != -1) {
                    if ($data['filtros']['proveedorID'] == 0) {
                        $where['GROUP_3']['productos'][] = ['i', 'proveedorID', 'IS', NULL];
                    }
                    else {
                        $where['GROUP_3']['productos'][] = ['i', 'proveedorID', '=', $data['filtros']['proveedorID']];
                    }
                }
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos_existencias',
                'columns' => [
                    'productos' => [
                        'id',
                        'nombre',
                        'codigo',
                        'unidad',
                        'costo',
                        'precio1',
                        'precio2',
                        'precio3',
                        'inventarioMinimo',
                        'bloqueado',
                        'proveedorID',
                        'claveSAT',
                        'impuesto'
                    ],
                    'productos_existencias' => [
                        'existencia'
                    ],
                    'categorias' => ['id AS categoriaID', 'nombre AS categoriaNombre'],
                    'proveedores' => ['COALESCE(*.nombre, "S/D") AS proveedorNombre']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID'],
                    'categorias' => ['id', 'productos.categoriaID'],
                    'proveedores' => ['id', 'productos.proveedorID']
                ],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['productoUnidades'] = $this->get_producto_unidades($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_list($data) {
        $result = ['error' => ''];
        $orders = $this->get_order($data['metodo'], $data['orden']);
        $where = $this->get_where($data['busqueda']);
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['proveedorID'])) {
                if ($data['filtros']['proveedorID'] != -1) {
                    if ($data['filtros']['proveedorID'] == 0) {
                        $where['GROUP_3']['productos'][] = ['i', 'proveedorID', 'IS', NULL];
                    }
                    else {
                        $where['GROUP_3']['productos'][] = ['i', 'proveedorID', '=', $data['filtros']['proveedorID']];
                    }
                }
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos_existencias',
                'columns' => [
                    'productos' => [
                        'id',
                        'nombre',
                        'codigo',
                        'unidad',
                        'costo',
                        'precio1',
                        'precio2',
                        'precio3',
                        'inventarioMinimo',
                        'bloqueado',
                        'proveedorID',
                        'claveSAT',
                        'impuesto'
                    ],
                    'productos_existencias' => [
                        'existencia'
                    ],
                    'categorias' => ['id AS categoriaID', 'nombre AS categoriaNombre'],
                    'proveedores' => ['COALESCE(*.nombre, "S/D") AS proveedorNombre']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID'],
                    'categorias' => ['id', 'productos.categoriaID'],
                    'proveedores' => ['id', 'productos.proveedorID']
                ],
                'orders' => $orders,
                'wheres' => $where,
                'limit' => [100, $_POST['offset']]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['productoUnidades'] = $this->get_producto_unidades($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_min($data) {
        $result = ['error' => ''];
        try {
            $productosUnidades = [];
            $productosUnidadesJson = [];
            $productosUnidadesResult = $this->get_productos_unidades();
            if ($productosUnidadesResult['error'] == '') {
                $productosUnidades = $productosUnidadesResult['resultado'];
            }
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => [
                    'id',
                    'nombre',
                    'unidad',
                    'costo',
                    'precio1',
                    'precio2',
                    'precio3',
                    'bloqueado'
                ],
                'orders' => [
                    ['id', 'ASC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($productosUnidades); $i++) {
                $productoUnidad = $productosUnidades[$i];
                $productosUnidadesJson[strval($productoUnidad['productoID'])][] = $productoUnidad;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                // $preparedResult[$i]['productoUnidades'] = $this->get_producto_unidades($preparedResult[$i]['id']);
                $preparedResult[$i]['productoUnidades'] = [];
                if (array_key_exists(strval($preparedResult[$i]['id']), $productosUnidadesJson)) {
                    $preparedResult[$i]['productoUnidades'] = $productosUnidadesJson[strval($preparedResult[$i]['id'])];
                }
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_facturar($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['id', 'nombre', 'unidad', 'precio1', 'claveSAT', 'impuesto'],
                'orders' => [
                    ['id', 'ASC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['productoUnidades'] = $this->get_producto_unidades($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_where($search) {
        $where = [
            'GROUP_1' => [
                'productos_existencias' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]
        ];
        if ($search != '') {
            $search = '%' . $search . '%';
            $where['GROUP_SEARCH'] = [
                'productos' => [
                    ['s', 'id', 'LIKE', $search, 'OR'],
                    ['s', 'codigo', 'LIKE', $search, 'OR'],
                    ['s', 'nombre', 'LIKE', $search, 'OR']
                ],
                'categorias' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ];
        }
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'productos' => [
                        ['id', $orderValue]
                    ]
                ];
                break;
            case 'codigo':
                $orderArray = [
                    'productos' => [
                        ['codigo', $orderValue]
                    ]
                ];
                break;
            case 'nombre':
                $orderArray = [
                    'productos' => [
                        ['nombre', $orderValue]
                    ]
                ];
                break;
            case 'precio':
                $orderArray = [
                    'productos' => [
                        ['precio1', $orderValue]
                    ]
                ];
                break;
            case 'existencia':
                $orderArray = [
                    'productos_existencias' => [
                        ['existencia', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function add_tiny($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'productos',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'codigo', $data['codigo']],
                    ['s', 'unidad', $data['unidad']],
                    ['d', 'costo', $data['costo']],
                    ['d', 'precio1', $data['precio1']],
                    ['d', 'precio2', $data['precio2']],
                    ['d', 'precio3', $data['precio3']],
                    ['d', 'inventarioMinimo', $data['inventarioMinimo']],
                    ['i', 'bloqueado', 0],
                    ['i', 'categoriaID', NULL],
                    ['i', 'proveedorID', NULL],
                    ['s', 'claveSAT', ''],
                    ['s', 'impuesto', 'tasacero'],
                ]
            ]);
            
            $result['resultado']['id'] = $preparedResult['id'];

            $this->notificacion->create('producto.add', [
                'id' => $result['resultado']['id'],
                'nombre' => $data['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

	function change_prices($data) {
		$result = ['error' => ''];
		try {
			$preparedResult = $GLOBALS['DB']->prepareUpdate([
				'table' => 'productos',
				'values' => [
					['d', 'precio1', $data['precio1']],
					['d', 'precio2', $data['precio2']],
					['d', 'precio3', $data['precio3']]
				],
				'wheres' => [
					['i', 'id', '=', $data['id']]
				]
			]);

			if ($preparedResult['affected_rows'] == -1) {
				$result['error'] = 'NOT_FOUND';
				return $result;
			}
		}
		catch (Exception $err) {
			$result['error'] = $err->getMessage();
		}
		return $result;
	}

    /* PRODUCTOS UNIDADES */

    function add_producto_unidades($productoID, $productoUnidades) {
        $result = ['error' => ''];

        $values = [];
        for ($i=0; $i<count($productoUnidades); $i++) {
            $productoUnidad = $productoUnidades[$i];
            $values[] = [
                ['i', 'productoID', $productoID],
                ['s', 'unidad', $productoUnidad['unidad']],
                ['d', 'costo', $productoUnidad['costo']],
                ['d', 'contenido', $productoUnidad['contenido']],
                ['d', 'precio1', $productoUnidad['precio1']],
                ['d', 'precio2', $productoUnidad['precio2']],
                ['d', 'precio3', $productoUnidad['precio3']]
            ];
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'productos_unidades',
                'values' => $values
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_producto_unidades($productoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'productos_unidades',
                'wheres' => [
                    ['i', 'productoID', '=', $productoID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'DELETE_ERROR';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_producto_unidades($productoID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos_unidades',
                'columns' => [
                    'id',
                    'unidad',
                    'costo',
                    'contenido',
                    'precio1',
                    'precio2',
                    'precio3'
                ],
                'wheres' => [
                    ['i', 'productoID', '=', $productoID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_productos_unidades() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos_unidades',
                'columns' => [
                    'productos_unidades' => [
                        'id',
                        'productoID',
                        'unidad',
                        'costo',
                        'contenido',
                        'precio1',
                        'precio2',
                        'precio3'
                    ]
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    // ADD EXISTENCIAS

    function get_all_secciones() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_productos_existencias($productoID) {
        $result = ['error' => ''];

        $seccionesResult = $this->get_all_secciones();
        if ($seccionesResult['error'] != '') {
            $result['error'] = 'GET_SECCIONES:' . $seccionesResult['error'];
        }
        $seccionesData = $seccionesResult['resultado'];

        $values = [];
        foreach ($seccionesData AS $seccion) {
            $values[] = [
                ['d', 'existencia', 0],
                ['i', 'productoID', $productoID],
                ['i', 'seccionID', $seccion['id']]
            ];
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'productos_existencias',
                'values' => $values
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>