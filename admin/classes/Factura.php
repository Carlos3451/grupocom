<?php

include_once('Notificacion.php');

class Factura {

    function __construct() {
        $this->notificacion = new Notificacion();

		//TEST KEYS
		$this->devApiKey = 'JDJ5JDEwJFgzRjVKbVUxZXJEQ0pSdy5OWXlDWS55aEp4bzlYbUpHdjdiYlA1eTJYOGZlVU0xcXdjRzEu';
        $this->devSecretKey = 'JDJ5JDEwJFJCeFFLa3hTcTl2QmJPdDZ3T1VTRy50UXdqUEliZzNsZk0wUE4yODQ4eWJObGN1LkppSmtH';

        $this->verifySSL = $GLOBALS['config']->facturaVerifySSL;
    }

    function get_access($id) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'factura_accesos',
                'columns' => ['nombre', 'apiPublicKey', 'apiSecretKey', 'serieF', 'serieCP', 'emailHost', 'emailPort', 'emailDireccion', 'emailContraseña', 'emailNombre'],
                'wheres' => [
                    ['i', 'id', '=', $id]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function change_pay_status($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'facturas',
                'values' => [
                    ['i', 'pagado', $data['pagado']]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_sat_data($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $result['resultado']['usoCfdi'] = $this->get_catalogo($access, 'UsoCfdi');
        $result['resultado']['formaPago'] = $this->get_catalogo($access, 'FormaPago');
        $result['resultado']['metodoPago'] = $this->get_catalogo($access, 'MetodoPago');
        $result['resultado']['tipoRelacion'] = $this->get_catalogo($access, 'Relacion');

        return $result;
    }

	function get_usos_cfdi($data) {
        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];
		return $this->get_catalogo($access, 'UsoCfdi');
	}

	function get_formas_pago($data) {
        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];
		return $this->get_catalogo($access, 'FormaPago');
	}

	function get_metodos_pago($data) {
        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];
		return $this->get_catalogo($access, 'MetodoPago');
	}

	function get_catalogo($access, $catalogo) {
        $result = ['error' => ''];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/catalogo/' . $catalogo);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            return $result;
        }

        $result['resultado'] = $responseJSON['data'];

		return $result;
    }
    
	function claves_sat_get_all() {
		$json = json_decode(file_get_contents('json/claves-productos.json'), true);

		return $json;
    }
    
    function get_all_clients($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v1/clients');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-Api-Key: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-Secret-Key: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

        $response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            return $result;
        }

        $result['resultado'] = $responseJSON['data'];

        $uids = [];
        foreach ($result['resultado'] AS $cliente) {
            $uids[] = $cliente['UID'];
        }
        $clientesLocalesResult = $this->get_clientes_locales_with_uids($uids);
        if ($clientesLocalesResult['error'] == '') {
            $clientesLocales = $clientesLocalesResult['resultado'];
            $clientesLocalesUIDS = array_column($clientesLocales, 'uid');
            for ($i=0; $i<count($result['resultado']); $i++) {
                $cliente = $result['resultado'][$i];
                $idx = array_search($cliente['UID'], $clientesLocalesUIDS);
                if ($idx !== false) {
                    $result['resultado'][$i]['datosExtras'] = $clientesLocales[$idx];
                }
            }
        }

		return $result;        
    }

	function pdf($data, $headers = true) {
        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            return 'error';
        }

        $access = $accessData['resultado'];

		$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/' . $data['UID'] . '/pdf');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
            'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
            'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
        ));

		$response = curl_exec($ch);
        curl_close($ch);

        if ($headers) {
            header('Cache-Control: public'); 
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="factura.pdf"');
            header('Content-Length: '. strlen($response));
        }

		return $response;
	}


	function xml($data, $headers = true) {
        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            return 'error';
        }

        $access = $accessData['resultado'];

		$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/' . $data['UID'] . '/xml');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
		curl_close($ch);

        if ($headers) {
            header('Cache-Control: public'); 
            header('Content-type: application/xml');
            header('Content-Disposition: attachment; filename="factura.xml"');
            header('Content-Length: '.strlen($response));
        }

		return $response;
	}

    function get_all($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['filtros']['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $search = 'per_page=1000';
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['limite'])) {
                $search = 'per_page=' . $data['filtros']['limite'];
            }
            if (isset($data['filtros']['fecha'])) {
                $search .= '&';
                $date = explode('/', $data['filtros']['fecha']);
                $search .= 'month=' . $date[0];
                $search .= '&year=' . $date[1];
            }
            if (isset($data['filtros']['rfc'])) {
                $search .= '&rfc=' . $data['filtros']['rfc'];
            }
            if (isset($data['filtros']['tipo'])) {
                $search .= '&type_document=' . $data['filtros']['tipo'];
            }
        }

        $result['query'] = $search;

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/list?' . $search);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $facturas = [];

        for ($i=0; $i<count($responseJSON['data']); $i++) {
            if (floatval($responseJSON['data'][$i]['Total']) != 0) {
                $responseJSON['data'][$i]['Emisor'] = $access['nombre'];
                $facturas[] = $responseJSON['data'][$i];
            }
        }

        $result['resultado'] = $facturas;

        for ($i=0; $i<count($result['resultado']); $i++) {
            $facturaResult = $this->get($result['resultado'][$i]['UID']);
            if ($facturaResult['error'] == '') {
                $result['resultado'][$i]['datosExtra'] = $facturaResult['resultado'];
            }
        }

		return $result;
    }

    function get_all_complementos($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['filtros']['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $search = '';
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['fecha'])) {
                $search .= '&';
                $date = explode('/', $data['filtros']['fecha']);
                $search .= 'month=' . $date[0];
                $search .= '&year=' . $date[1];
            }
            if (isset($data['filtros']['rfc'])) {
                $search .= '&rfc=' . $data['filtros']['rfc'];
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/list?per_page=1000&type_document=pago' . $search);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
            'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
            'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $facturas = [];

        for ($i=0; $i<count($responseJSON['data']); $i++) {
            if (floatval($responseJSON['data'][$i]['Total']) == 0) {
                $responseJSON['data'][$i]['Emisor'] = $access['nombre'];
                $facturas[] = $responseJSON['data'][$i];
            }
        }

        $result['resultado'] = $facturas;

        for ($i=0; $i<count($result['resultado']); $i++) {
            $facturaResult = $this->get_complemento($result['resultado'][$i]['UID']);
            if ($facturaResult['error'] == '') {
                $result['resultado'][$i]['datosExtra'] = $facturaResult['resultado'];
            }
        }

        return $result;
    }

    function get_all_access() {
        $result = ['error' => ''];

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'factura_accesos',
                'columns' => ['id', 'nombre'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complementos($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'facturas_complementos',
                'columns' => ['complementoUID', 'cantidad', 'cancelado', 'formaPago', 'fecha'],
                'wheres' => [
                    ['s', 'facturaUID', '=', $data['facturaUID']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_factura_status($uid, $accessData) {
        $result = ['error' => ''];

        $access = $accessData['resultado'];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/' . $uid . '/cancel_status');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $result['resultado'] = $responseJSON['data'];

		return $result;
    }

    function get($uid) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'facturas',
                'columns' => [
                    'facturas' => ['id', 'metodoPago', 'usuarioID', 'facturaUID', 'clienteUID', 'pagos', 'pagoAcumulado', 'pagado'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'facturas' => [
                        ['s', 'facturaUID', '=', $uid]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complemento($uid) {        
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'facturas_complementos',
                'columns' => [
                    'facturas_complementos' => ['id', 'facturaUID', 'complementoUID', 'formaPago', 'cantidad', 'cancelado', 'usuarioID'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'facturas_complementos' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['s', 'complementoUID', '=', $uid]
                    ]
                ]
            ]);            
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_series($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v1/series');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $result['resultado'] = $responseJSON['data'];

		return $result;
    }

    function get_unit($unitStr) {
        $unit = [
            'clave' => 'H87',
            'nombre' => 'Pieza'
        ];
        switch ($unitStr) {
            case 'gramo':
                $unit['clave'] = 'GRM';
                $unit['nombre'] = 'Gramo';
                break;
            case 'kilogramo':
                $unit['clave'] = 'KGM';
                $unit['nombre'] = 'Kilogramo';
                break;
            case 'mililitro':
                $unit['clave'] = 'MLT';
                $unit['nombre'] = 'Mililitro';
                break;
            case 'litro':
                $unit['clave'] = 'LTR';
                $unit['nombre'] = 'Litro';
                break;
            case 'pieza':
                $unit['clave'] = 'H87';
                $unit['nombre'] = 'Pieza';
                break;
            case 'paquete':
                $unit['clave'] = 'XPK';
                $unit['nombre'] = 'Paquete';
                break;
            case 'caja': case 'burbuja':
                $unit['clave'] = 'XBX';
                $unit['nombre'] = 'Caja';
                break;
            case 'costal':
                $unit['clave'] = 'XGY';
                $unit['nombre'] = 'Costal de Yute';
            case 'saco':
                $unit['clave'] = 'XSA';
                $unit['nombre'] = 'Saco';
                break;
            case 'libra':
                $unit['clave'] = 'LBR';
                $unit['nombre'] = 'Libra';
                break;
            case 'maso':
                $unit['clave'] = 'XBH';
                $unit['nombre'] = 'Manojo';
                break;
        }
        return $unit;
    }    

    function facturar($data) {

		$result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

		$conceptos = '';

        $fecha = implode('/', array_reverse(explode('/', $data['fechaFacturacion'])));

		$facturaJSON = [
            'Receptor' => [
                'UID' => $data['clienteUID']
            ],
            'TipoDocumento' => 'factura',
            'Conceptos' => [],
            'UsoCFDI' => $data['usoCFDI'],
            'Serie' => $access['serieF'],
            'FormaPago' => $data['formaPago'],
            'MetodoPago' => $data['metodoPago'],
            'Moneda' => 'MXN',
            'Fecha' => $fecha
        ];

        if (isset($data['comentarios'])) {
            $facturaJSON['Comentarios'] = $data['comentarios'];
        }

        if (!empty($data['relacionados'])) {
            $facturaJSON['CfdiRelacionados'] = [
                'TipoRelacion' => $data['tipoRelacion'],
                'UUID' => []
            ];
            foreach ($data['relacionados'] AS $relacionadoUUID) {
                $facturaJSON['CfdiRelacionados']['UUID'][] = $relacionadoUUID;
            }
        }

		foreach ($data['conceptos'] as $concepto) {
            $unidadObj = $this->get_unit($concepto['unidad']);
            $unidadClave = $unidadObj['clave'];
            $unidadNombre = $unidadObj['nombre'];
            $desCant = floatval($data['descuentoPorcentaje']) * 0.01 * floatval($concepto['precio']);
            $precioDesc = floatval($concepto['precio']) - $desCant;
            $impuesto = 0;
            $impuestoJSON = [];
            $cantidad = round(floatval($concepto['cantidad']) * 1000) / 1000;
            switch ($concepto['impuesto']) {
                case 'ieps8':
                    $impuestoJSON = [
                        'Traslados' => [
                            [
                                'Base' => (floor(floatval($concepto['precio']) * $cantidad * 1000000) / 1000000),
                                'Impuesto' => '003',
                                'TipoFactor' => 'Tasa',
                                'TasaOCuota' => 0.08,
                                'Importe' => (floor($precioDesc * $cantidad * (0.08 * 1000000)) / 1000000)
                            ]
                        ]
                    ];
                    break;
                case 'ieps160iva': 
                    $impuestoJSON = [
                        'Traslados' => [
                            [
                                'Base' => (floor(floatval($concepto['precio']) * $cantidad * 1000000) / 1000000),
                                'Impuesto' => '003',
                                'TipoFactor' => 'Tasa',
                                'TasaOCuota' => 1.6,
                                'Importe' => (floor($precioDesc * $cantidad * (1.6 * 1000000)) / 1000000)
                            ],
                            [
                                'Base' => (floor(floatval($concepto['precio']) * 2.6 * $cantidad * 1000000) / 1000000),
                                'Impuesto' => '002',
                                'TipoFactor' => 'Tasa',
                                'TasaOCuota' => 0.16,
                                'Importe' => (floor($precioDesc * 2.6 * $cantidad * (0.16 * 1000000)) / 1000000)
                            ]
                        ]
                    ];
                    break;
                case 'iva': 
                    $impuestoJSON = [
                        'Traslados' => [
                            [
                                'Base' => (floor(floatval($concepto['precio']) * $cantidad * 1000000) / 1000000),
                                'Impuesto' => '002',
                                'TipoFactor' => 'Tasa',
                                'TasaOCuota' => 0.16,
                                'Importe' => (floor($precioDesc * $cantidad * (0.16 * 1000000)) / 1000000)
                            ]
                        ]
                    ];
                    break;
            }
            $conceptoJSON = [
                'ClaveProdServ' => $concepto['clave'],
                'Cantidad' =>  $cantidad,
                'ClaveUnidad' => $unidadClave,
                'Unidad' =>  $unidadNombre,
                'ValorUnitario' =>  floatval($concepto['precio']),
                'Descripcion' => $concepto['nombre'],
                'Descuento' =>  (floor($desCant * $cantidad * 1000000) / 1000000)
            ];

            if (!empty($impuestoJSON)) {
                $conceptoJSON['Impuestos'] = $impuestoJSON;
            }

            $facturaJSON['Conceptos'][] = $conceptoJSON;
		}

		$ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/create');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_POST, TRUE);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($facturaJSON));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        $responseJson = json_decode($response, true);

        curl_close($ch);

        if (!isset($responseJson['response']) || ($responseJson['response'] != 'success')) {
            $result['error'] = 'FACTURA_ERROR';
            $result['errorLog'] = $responseJson;
            error_log(json_encode($data) . "\n" . json_encode($facturaJSON) . "\n" . json_encode($result), 3, 'factura_error.log');
            return $result;
        }

        $this->save_factura($data['metodoPago'], $responseJson['uid'], $data['clienteUID']);
        
        for ($i=0; $i<count($data['conceptos']); $i++) {
            if ($data['conceptos'][$i]['id'] != 0) {
                $this->update_producto_clavesat($data['conceptos'][$i]['id'], $data['conceptos'][$i]['clave'], $data['conceptos'][$i]['impuesto']);
            }
        }

        $result['resultado'] = $responseJson;

		return $result;
    }

    function add_complemento($data) {

		$result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $fecha = implode('/', array_reverse(explode('/', $data['fecha'])));
        $fechaComplemento = implode('-', array_reverse(explode('/', $data['fecha']))) . 'T12:00:00';

		$facturaJSON =
			'{
				"TipoDocumento": "pago",
				"Serie": "' . ($access['serieCP']) . '",
                "Fecha": "' . $fecha . '",
                "Subtotal": 0,
                "Total": 0,
				"Moneda": "XXX",
				"UsoCFDI": "P01",
				"Receptor": {
					"UID": "' . $data['clienteUID'] . '"
				},
				"Conceptos": [{
                    "ClaveProdServ": "84111506",
                    "Cantidad": 1,
                    "ClaveUnidad": "ACT",
                    "Descripcion": "Pago",
                    "ValorUnitario": 0,
                    "Importe": 0,
                    "Complemento": [{
                        "typeComplement": "pagos",
                        "FechaPago": "' . $fechaComplemento . '",
                        "FormaDePagoP": "' . $data['formaPago'] . '",
                        "MonedaP": "MXN",
                        "Monto": "' . $data['cantidad'] . '",
                        "relacionados": [{
                            "IdDocumento": "' . $data['facturaUUID'] . '",
                            "MonedaDR": "MXN",
                            "MetodoDePagoDR": "PPD",
                            "NumParcialidad": "' . $data['numeroParcialidad'] . '",
                            "ImpSaldoAnt": "' . $data['restoAnterior'] . '",
                            "ImpPagado": "' . $data['cantidad'] . '",
                            "ImpSaldoInsoluto": "' . (floatval($data['restoAnterior']) - floatval($data['cantidad'])) . '"
                        }]
                    }]
                }]
			}';
			
		$ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/complemento/pagos/create');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_POST, TRUE);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $facturaJSON);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        $responseJson = json_decode($response, true);

        curl_close($ch);

        if (!isset($responseJson['response']) || ($responseJson['response'] != 'success')) {
            $result['error'] = 'FACTURA_ERROR';
            $result['errorLog'] = $responseJson;
            return $result;
        }

        $this->update_factura_pagos($data['facturaUID'], $data['cantidad']);
        $this->save_complemento($data['facturaUID'], $responseJson['uid'], $data['cantidad'], $data['formaPago'], $data['fecha']);

        $result['resultado'] = $responseJson;

		return $result;
    }

    function add_complemento_multiples($data) {

		$result = ['error' => ''];

        $accesoResult = $this->get_access($data['accesoID']);

        if ($accesoResult['error'] != '') {
            $result['error'] = $accesoResult['error'];
            return $result;
        }

        $accesoData = $accesoResult['resultado'];

        $fecha = implode('/', array_reverse(explode('/', $data['fecha'])));
        $fechaComplemento = implode('-', array_reverse(explode('/', $data['fechaPago']))) . 'T12:00:00';

        $relacionados = [];

        foreach ($data['facturas'] AS $factura) {
            $relacionados[] = [
                'IdDocumento' => $factura['uuid'],
                'MonedaDR' => 'MXN',
                'MetodoDePagoDR' => 'PPD',
                'NumParcialidad' => $factura['numeroParcialidad'],
                'ImpSaldoAnt' =>  $factura['saldoAnterior'],
                'ImpPagado' => $factura['importe'],
                'ImpSaldoInsoluto' => (floatval($factura['saldoAnterior']) - floatval($factura['importe']))
            ];
        }

        if (count($relacionados) == 0) {
            $result['error'] = 'FACTURAS_EMPTY';
            return $result;
        }

        $facturaJSON = [
            'TipoDocumento' => 'pago',
            'Serie' => $accesoData['serieCP'],
            'Fecha' => $fecha,
            'Subtotal' => 0,
            'Total' => 0,
            'Moneda' => 'XXX',
            'UsoCFDI' => 'P01',
            'Receptor' => [
                'UID' => $data['clienteUID']
            ],
            'Conceptos' => [[
                'ClaveProdServ' => '84111506',
                'Cantidad' => 1,
                'ClaveUnidad' => 'ACT',
                'Descripcion' => 'Pago',
                'ValorUnitario' => 0,
                'Importe' => 0,
                'Complemento' => [[
                    'typeComplement' => 'pagos',
                    'FechaPago' => $fechaComplemento,
                    'FormaDePagoP' =>  $data['formaPago'],
                    'MonedaP' => 'MXN',
                    'Monto' => $data['montoTotal'],
                    'relacionados' => $relacionados
                ]]
            ]]
        ];

		$ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/complemento/pagos/create');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_POST, TRUE);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($facturaJSON));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $accesoData['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $accesoData['apiSecretKey'])
		));

		$response = curl_exec($ch);
        $responseJson = json_decode($response, true);

        curl_close($ch);

        if (!isset($responseJson['response']) || ($responseJson['response'] != 'success')) {
            $result['error'] = 'FACTURA_ERROR';
            $result['errorLog'] = $responseJson;
            error_log("\n" . json_encode($data) . "\n" . json_encode($facturaJSON) . "\n" . json_encode($result), 3, 'factura_error.log');
            return $result;
        }

        foreach ($data['facturas'] AS $factura) {
            $this->update_factura_pagos($factura['uid'], $factura['importe']);
            $this->save_complemento($factura['uid'], $responseJson['uid'], $factura['importe'], $data['formaPago'], $data['fecha']);
        }

        $result['resultado'] = $responseJson;

		return $result;
    }

    function update_factura_pagos($facturaUID, $cantidad) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'facturas',
                'values' => [
                    ['i', 'pagos', 1, 'pagos + ?'],
                    ['d', 'pagoAcumulado', $cantidad, 'pagoAcumulado + ?'],
                    ['i', 'pagado', 1]
                ],
                'wheres' => [
                    ['s', 'facturaUID', '=', $facturaUID]
                ]
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function reduce_factura_pagos($facturaUID, $cantidad) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'facturas',
                'values' => [
                    ['d', 'pagoAcumulado', $cantidad, 'pagoAcumulado - ?']
                ],
                'wheres' => [
                    ['s', 'facturaUID', '=', $facturaUID]
                ]
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function update_producto_clavesat($id, $clave, $impuesto) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['s', 'claveSAT', $clave],
                    ['s', 'impuesto', $impuesto]
                ],
                'wheres' => [
                    ['i', 'id', '=', $id],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;        
    }
    
    function save_factura($metodoPago, $uid, $clientUID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'facturas',
                'values' => [
                    ['s', 'metodoPago', $metodoPago],
                    ['s', 'facturaUID', $uid],
                    ['s', 'clienteUID', $clientUID],
                    ['i', 'pagos', 0],
                    ['d', 'pagoAcumulado', 0],
                    ['i', 'pagado', 0],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
            error_log(date('d-m-Y G:i:s', time()) . "\n" . json_encode($result) . "\n" . $metodoPago . "\n" . $uid . "\n" . $clientUID . "\n\n", 3, 'factura_save_error.log');
        }
        return $result;
    }
    
    function save_complemento($facturaUID, $uid, $cantidad, $formaPago, $fecha) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'facturas_complementos',
                'values' => [
                    ['s', 'facturaUID', $facturaUID],
                    ['s', 'complementoUID', $uid],
                    ['s', 'formaPago', $formaPago],
                    ['d', 'cantidad', $cantidad],
                    ['i', 'cancelado', 0],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                    ['s', 'fecha', date('Y-m-d', strtotime(str_replace('/', '-', $fecha)))]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
    }

    function cancel($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $facturaJSON = [
            'cfdi_uid' => $data['uid'],
            'motivo' => $data['motivo']
        ];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/' . $data['uid'] . '/cancel');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($facturaJSON));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'CANCEL_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

		return $result;
    }

    function complemento_cancel($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $facturaJSON = [
            'cfdi_uid' => $data['uid'],
            'motivo' => $data['motivo']
        ];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v3/cfdi33/' . $data['uid'] . '/cancel');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($facturaJSON));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['response']) || $responseJSON['response'] != 'success') {
            $result['error'] = 'CANCEL_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $complementoResult = $this->get_complemento($data['uid']);
        if ($complementoResult['error'] == '') {
            $complementoData = $complementoResult['resultado'];
            foreach ($complementoData AS $complemento) {
                $this->reduce_factura_pagos($complemento['facturaUID'], $complemento['cantidad']);
            }
        }

        $GLOBALS['DB']->prepareUpdate([
            'table' => 'facturas_complementos',
            'values' => [
                ['i', 'cancelado', 1]
            ],
            'wheres' => [
                ['s', 'complementoUID', '=', $data['uid']]
            ]
        ]);

		return $result;
    }

    function email($data) {
        require_once('libs/phpmailer/Exception.php');
        require_once('libs/phpmailer/SMTP.php');
        require_once('libs/phpmailer/PHPMailer.php');

        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

		$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->isSMTP();

		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Port = $access['emailPort'];
		$mail->Host = $access['emailHost'];
        $mail->Username = $access['emailDireccion'];
        $mail->Password = $access['emailContraseña'];
        $mail->AllowEmpty = true;
        //$mail->SMTPDebug  = 4;

        $pdfData = $this->pdf($data, false);
        $xmlData = $this->xml($data, false);
		try {
			$destinatarios = explode(',', $data['destinatarios']);
			$mail->CharSet = 'UTF-8';
			for ($d=0; $d<count($destinatarios); $d++) {
				$mail->AddAddress(trim($destinatarios[$d]));
            }
            if ($data['ccs'] != '') {
                $ccs = explode(',', $data['ccs']);
                for ($c=0; $c<count($ccs); $c++) {
                    $mail->AddCC(trim($ccs[$c]));
                }
            }
			$mail->SetFrom($access['emailDireccion'], $access['emailNombre'] . ' ' . 'Factura');
			$mail->AddStringAttachment($pdfData, 'Factura-' . $data['UID'] . '.pdf');
			$mail->AddStringAttachment($xmlData, 'Factura-' . $data['UID'] . '.xml');
			$mail->Subject = 'Factura ' . $access['emailNombre'];
			$mail->Body = $data['mensaje'];
			$mail->Send();
		} catch (phpmailerException $e) {
			$result['error'] = $e->errorMessage();
		} catch (Exception $e) {
			$result['error'] = $e->getMessage();
		}
		return $result;
    }    

    function add_client($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $emails = explode(',', $data['emails']);
        for ($i=0; $i<count($emails); $i++) {
            $emails[$i] = trim($emails[$i]);
        }

        $fields = [
            'nombre' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'email' => $emails[0],
            'telefono' => $data['telefono'],
            'razons' => $data['razonSocial'],
            'rfc' => strtoupper($data['rfc']),
            'calle' => $data['calle'],
            'numero_exterior' => $data['numeroExterior'],
            'numero_interior' => $data['numeroInterior'],
            'codpos' => $data['codigoPostal'],
            'colonia' => $data['colonia'],
            'estado' => $data['estado'],
            'ciudad' => $data['ciudad']
        ];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v1/clients/create');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['status']) || $responseJSON['status'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $result['resultado'] = $responseJSON['Data'];

        $this->add_cliente_local($result['resultado']['UID'], $data);

		return $result;        
    }

    function edit_client($data) {
        $result = ['error' => ''];

        $accessData = $this->get_access($data['accesoID']);

        if ($accessData['error'] != '') {
            $result['error'] = $accessData['error'];
            return $result;
        }

        $access = $accessData['resultado'];

        $emails = explode(',', $data['emails']);
        for ($i=0; $i<count($emails); $i++) {
            $emails[$i] = trim($emails[$i]);
        }

        $fields = [
            'nombre' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'email' => $emails[0],
            'telefono' => $data['telefono'],
            'razons' => $data['razonSocial'],
            'rfc' => strtoupper($data['rfc']),
            'calle' => $data['calle'],
            'numero_exterior' => $data['numeroExterior'],
            'numero_interior' => $data['numeroInterior'],
            'codpos' => $data['codigoPostal'],
            'colonia' => $data['colonia'],
            'estado' => $data['estado'],
            'ciudad' => $data['ciudad']
        ];

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ($GLOBALS['config']->facturaDevMode == true ? 'http://devfactura.in/' : 'https://factura.com/') . 'api/v1/clients/' . $data['uid'] . '/update');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        if (!$this->verifySSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
            'F-PLUGIN: 9d4095c8f7ed5785cb14c0e3b033eeb8252416ed',
			'F-API-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devApiKey : $access['apiPublicKey']),
			'F-SECRET-KEY: ' . ($GLOBALS['config']->facturaDevMode == true ? $this->devSecretKey : $access['apiSecretKey'])
		));

		$response = curl_exec($ch);
        curl_close($ch);
        
        $responseJSON = json_decode($response, true);

        if (!isset($responseJSON['status']) || $responseJSON['status'] != 'success') {
            $result['error'] = 'GET_DATA_ERROR';
            $result['errorLog'] = $responseJSON;
            return $result;
        }

        $result['resultado'] = $responseJSON['Data'];

        $this->update_cliente_local($data['uid'], $data);

		return $result;        
    }

    /* GET ALL GLOBAL */

    function get_global_clientes() {
        $result = ['error' => ''];

        $accesosResult = $this->get_all_access();
        if ($accesosResult['error'] != '') {
            $result['error'] = 'ACCESOS_GET:' . $accesosResult['error'];
            return $result;
        }
        $accesosData = $accesosResult['resultado'];

        $clientes = [];

        for ($i=0; $i<count($accesosData); $i++) {
            $acceso = $accesosData[$i];
            
            $clientesResult = $this->get_all_clients(['accesoID' => $acceso['id']]);
            if ($clientesResult['error'] == '') {
                $clientes = array_merge($clientes, $clientesResult['resultado']);
            }            
        }

        $result['resultado'] = $clientes;

        return $result;
    }

    function get_global_facturas($data) {
        $result = ['error' => ''];

        $facturas = [];

        $acceosIDs = [];

        $monthsDiff = 0;
        $dateStartArr = null;

        $dateStartComp = '';
        $dateEndComp = '';

        if (strpos($data['fecha'], ' - ') === FALSE) {
            $dateArr = explode('/', $data['fecha']);
            switch (count($dateArr)) {
                case 1:
                    $data['fecha'] = '01/01/' . $dateArr[0] . ' - 31/12/' . $dateArr[0];
                    break;
                case 2:
                    $data['fecha'] = '01/' . $dateArr[0] . '/' . $dateArr[1] . ' - 31/' . $dateArr[0] . '/' . $dateArr[1];
                    break;
                case 3:
                    $dateStartArr = $dateArr;
                    $dateStartComp = implode('-', array_reverse($dateStartArr));
                    break;
            }
        }

        if (strpos($data['fecha'], ' - ')) {
            $dateArr = explode(' - ', $data['fecha']);
            $dateStartArr = explode('/', $dateArr[0]);
            $dateEndArr = explode('/', $dateArr[1]);
            $monthsDiff = ($dateEndArr[1] + ($dateEndArr[2] * 12)) - ($dateStartArr[1] + ($dateStartArr[2] * 12));
            $dateStartComp = implode('-', array_reverse($dateStartArr));
            $dateEndComp = implode('-', array_reverse($dateEndArr));
            
        }

        if (!isset($data['emisor'])) {
            $accesosResult = $this->get_all_access();
            if ($accesosResult['error'] != '') {
                $result['error'] = 'ACCESOS_GET:' . $accesosResult['error'];
                return $result;
            }
            foreach ($accesosResult['resultado'] AS $acceso) {
                $acceosIDs[] = $acceso['id'];
            }
        }
        else {
            $acceosIDs[] = $data['emisor'];
        }

        foreach ($acceosIDs AS $accesoID) {
            for ($i=0; $i<=$monthsDiff; $i++) {
                $monthRaw = $dateStartArr[1] + $i;
                $dateStr = strval((($monthRaw - 1) % 12) + 1) . '/' . strval($dateStartArr[2] + floor(($monthRaw - 1) / 12));
                $facturaResult = $this->get_all([
                    'filtros' => [
                        'fecha' => $dateStr,
                        'accesoID' => $accesoID
                    ]
                ]);
                foreach ($facturaResult['resultado'] AS $factura) {
                    if ($dateEndComp != '') {
                        if ($factura['FechaTimbrado'] >= $dateStartComp && $factura['FechaTimbrado'] <= $dateEndComp) {
                            $facturas[] = $factura;
                        }
                    }
                    else if ($factura['FechaTimbrado'] == $dateStartComp) {
                        $facturas[] = $factura;
                    }
                }
            }
        }

        $result['resultado'] = $facturas;

        return $result;
    }

    /* END OF GET ALL GLOBAL  */

    function add_cliente_local($uid, $data) {
        $result = ['error' => ''];        

        $emails = explode(',', $data['emails']);
        for ($i=0; $i<count($emails); $i++) {
            $emails[$i] = trim($emails[$i]);
        }
        $emails = implode(', ', $emails);

        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'facturas_clientes',
                'values' => [
                    ['s', 'emails', $emails],
                    ['s', 'usoCFDI', ''],
                    ['s', 'formaPago', ''],
                    ['s', 'uid', $uid]
                ]
            ]);

            $result['resultado'] = $preparedResult;
        }
        catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function update_cliente_local($uid, $data) {
        $result = ['error' => ''];

        $clienteLocalResult = $this->get_cliente_local($uid);
        if ($clienteLocalResult['error'] == 'NOT_FOUND') {
            $result = $this->add_cliente_local($uid, $data);
            return $result;
        }

        $emails = explode(',', $data['emails']);
        for ($i=0; $i<count($emails); $i++) {
            $emails[$i] = trim($emails[$i]);
        }
        $emails = implode(', ', $emails);

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'facturas_clientes',
                'values' => [
                    ['s', 'emails', $emails]
                ],
                'wheres' => [
                    ['s', 'uid', '=', $uid]
                ]
            ]);

            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function update_cliente_local_facturacion($uid, $data) {
        $result = ['error' => ''];        

        $emails = explode(',', $data['emails']);
        for ($i=0; $i<count($emails); $i++) {
            $emails[$i] = trim($emails[$i]);
        }
        $emails = implode(', ', $emails);

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'facturas_clientes',
                'values' => [
                    ['s', 'usoCFDI', $data['usoCFDI']],
                    ['s', 'formaPago', $data['formaPago']]
                ],
                'wheres' => [
                    ['s', 'uid', '=', $uid]
                ]
            ]);

            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function get_cliente_local($uid) {
        $result = ['error' => ''];

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'facturas_clientes',
                'columns' => ['id', 'emails', 'usoCFDI', 'formaPago'],
                'wheres' => [
                    ['s', 'uid', '=', $uid]
                ]
            ]);

            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function get_clientes_locales_with_uids($uids) {
        $result = ['error' => ''];

        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'facturas_clientes',
                'columns' => ['id', 'uid', 'emails', 'usoCFDI', 'formaPago'],
                'wheres' => [
                    ['s', 'uid', 'IN', $uids]
                ]
            ]);

            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            $result['resultado'] = $preparedResult;
        }
        catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

}

?>