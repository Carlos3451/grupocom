<?php

class Conexion {

	public $mysqli;

	function __construct() {
		$tries = 15;
		$user = $GLOBALS['config']->baseDatos['usuario'];
		$pass = $GLOBALS['config']->baseDatos['contraseña'];
		$database = $GLOBALS['config']->baseDatos['nombre'];
		$this->mysqli = new mysqli('localhost', $user, $pass, $database);
		while ($this->mysqli->connect_errno) {
			sleep(0.5);
			$this->mysqli = new mysqli('localhost', $user, $pass, $database);
			$tries -= 1;
			if ($tries == 0) {
				echo 'ERROR AL CONECTAR CON LA BASE DE DATOS: ' . $this->mysqli->connect_error;
				exit;
			}
		}
		$this->mysqli->set_charset('utf8');
		if (date('I')) {
			$query = $this->mysqli->query('SET time_zone="-6:00"');
		}
		else {
			$query = $this->mysqli->query('SET time_zone="-7:00"');
		}
	}

	function prepareInsertInto($object) {

		$queryTable = $object['table'];
		$queryValues = $object['values'];

		$result = [];

		$bindParamTypes = '';
		$bindParamArray = [];
		$bindParams = [];

		$columnsStr = '';
		$valuesStr = '';

		if (is_array($queryValues[0][0])) {
			for ($i=0; $i<count($queryValues[0]); $i++) {
				$columnsStr .= $queryValues[0][$i][1] . ', ';	
			}
			foreach ($queryValues as $section) {
				foreach ($section as $values) {
					$valuesStr .= '?, ';
					$bindParamTypes .= $values[0];
					$bindParamArray[] = $values[2];
				}
				$valuesStr = rtrim($valuesStr, ', ');
				$valuesStr .= '), (';
			}
			$valuesStr = rtrim($valuesStr, '), (');
		}
		else {
			foreach ($queryValues as $values) {
				$columnsStr .= $values[1] . ', ';
				$valuesStr .= '?, ';
				$bindParamTypes .= $values[0];
				$bindParamArray[] = $values[2];
			}
			$valuesStr = rtrim($valuesStr, ', ');
		}
		$columnsStr = rtrim($columnsStr, ', ');

		$bindParams[] = $bindParamTypes;
		for ($i=0; $i<count($bindParamArray); $i++) {
			$bindParams[] = &$bindParamArray[$i];
		}

		$queryStr = 'INSERT INTO ' . $queryTable . ' (' . $columnsStr . ') VALUES (' . $valuesStr . ')';

		$stmt = $this->mysqli->prepare($queryStr);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }
		
		call_user_func_array(array($stmt, 'bind_param'), $bindParams);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->execute();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$result['id'] = $this->mysqli->insert_id;
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->close();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		return $result;

	}

	function prepareUpdate($object) {

		$queryTable = $object['table'];
		$queryValues = $object['values'];
		$queryWheres = isset($object['wheres']) ? $object['wheres'] : null;

		$result = [];

		$bindParamTypes = '';
		$bindParamArray = [];
		$bindParams = [];

		$setStr = '';
		$whereStr = '';

		foreach ($queryValues as $value) {
			if (isset($value[3])) {
				$setStr .= $value[1] . ' = ' . $value[3] . ', ';
				$bindParamTypes .= $value[0];
				$bindParamArray[] = $value[2];
			}
			else {
				$setStr .= $value[1] . ' = ?, ';
				$bindParamTypes .= $value[0];
				$bindParamArray[] = $value[2];
			}
		}

		$setStr = rtrim($setStr, ', ');
		if (!empty($queryWheres)) {
			$nextParam = 'AND';
			$whereStr = ' WHERE';
			foreach ($queryWheres as $table => $values) {
				if (array_values($queryWheres) === $queryWheres) {
					if ($values[2]=='IN') {
						$inStr = '';
						foreach ($values[3] as $inValue) {
							$inStr .= '?, ';
							$bindParamTypes .= $values[0];
							$bindParamArray[] = $inValue;
						}
						$inStr = rtrim($inStr, ', ');
						$nextParam = 'AND';
						if (isset($values[4])) {
							$nextParam = $values[4];
						}
						$whereStr .= ' ' . $values[1] . ' IN (' . $inStr . ') ' . $nextParam;
					}
					else {
						$nextParam = 'AND';
						if (isset($values[4])) {
							$nextParam = $values[4];
						}
						if ($values[3]==NULL) {
							$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' NULL ' . $nextParam;
						}
						else {
							$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' ? ' . $nextParam;
							$bindParamTypes .= $values[0];
							$bindParamArray[] = $values[3];
						}
					}
				}
				else {
					foreach ($values as $value) {
						if ($value[2]=='in') {
							$inStr = '';
							foreach ($value[3] as $inValue) {
								$inStr .= '?,';
								$bindParamTypes .= $value[0];
								$bindParamArray[] = $inValue;
							}
							$inStr = rtrim($inStr, ',');
							$nextParam = 'AND';
							if (isset($value[4])) {
								$nextParam = $value[4];
							}
							$whereStr .= ' ' . $table . '.' . $value[1] . ' IN (' . $inStr . ') ' . $nextParam;
						}
						else {
							$hasLPAD = strpos($value[1], 'LPAD(') === FALSE ? FALSE : TRUE;
							$nextParam = 'AND';
							if (isset($value[4])) {
								$nextParam = $value[4];
							}
							if ($hasLPAD) {
								$inside = str_replace('LPAD(', '', $value[1]);
								$whereStr .= ' LPAD(' . $table . '.' . $inside . ' ' . $value[2] . ' ? ' . $nextParam;
								$bindParamTypes .= $value[0];
								$bindParamArray[] = $value[3];
							}
							else {
								if ($value[3]===NULL) {
									$whereStr .= ' ' . $table . '.' . $value[1] . ' ' . $value[2] . ' NULL ' . $nextParam;
								}
								else {
									$whereStr .= ' ' . $table . '.' . $value[1] . ' ' . $value[2] . ' ? ' . $nextParam;
									$bindParamTypes .= $value[0];
									$bindParamArray[] = $value[3];
								}
							}
						}
					}
				}
			}
			$whereStr = rtrim($whereStr, ' ' . $nextParam);
		}

		$bindParams[] = $bindParamTypes;
		for ($i=0; $i<count($bindParamArray); $i++) {
			$bindParams[] = &$bindParamArray[$i];
		}

		$queryStr = 'UPDATE ' . $queryTable . ' SET ' . $setStr . $whereStr;

		$stmt = $this->mysqli->prepare($queryStr);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }
		
		call_user_func_array(array($stmt, 'bind_param'), $bindParams);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->execute();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$result['affected_rows'] = $this->mysqli->affected_rows;

		return $result;

	}

	function prepareWheres($queryWheres, &$bindParamTypes, &$bindParamArray) {
		$nextParam = 'AND';
		$whereStr = '';
		foreach ($queryWheres as $table => $values) {
			if (strpos($table, 'GROUP') !== FALSE) {
				$whereStr .= '(' . $GLOBALS['DB']->prepareWheres($values, $bindParamTypes, $bindParamArray) . ') AND';
			}
			else if (is_numeric($table)) {
				$nextParam = 'AND';
				if (isset($values[4])) {
					$nextParam = $values[4];
				}
				if ($values[2]=='IN') {
					$inStr = '';
					foreach ($values[3] as $inValue) {
						$inStr .= '?, ';
						$bindParamTypes .= $values[0];
						$bindParamArray[] = $inValue;
					}
					$inStr = rtrim($inStr, ', ');
					$whereStr .= ' ' . $values[1] . ' IN (' . $inStr . ') ' . $nextParam;
				}
				else {
					if ($values[3]==NULL) {
						$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' NULL ' . $nextParam;
					}
					else {
						$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' ? ' . $nextParam;
						$bindParamTypes .= $values[0];
						$bindParamArray[] = $values[3];
					}
				}
			}
			else {
				foreach ($values as $value) {
					$columnValue = $table . '.' . $value[1];
					$nextParam = 'AND';
					$hasTemplate = strpos($value[1], '*.') === FALSE ? FALSE : TRUE;
					if ($hasTemplate) {
						$columnValue = str_replace('*.', $table . '.', $value[1]);
					}
					if (isset($value[4])) {
						$nextParam = $value[4];
					}
					if ($value[2]=='IN') {
						$inStr = '';
						foreach ($value[3] as $inValue) {
							$inStr .= '?,';
							$bindParamTypes .= $value[0];
							$bindParamArray[] = $inValue;
						}
						$inStr = rtrim($inStr, ',');
						$whereStr .= ' ' . $columnValue . ' IN (' . $inStr . ') ' . $nextParam;
					}
					else {
						if ($value[3]===NULL) {
							$whereStr .= ' ' . $columnValue . ' ' . $value[2] . ' NULL ' . $nextParam;
						}
						else {
							$whereStr .= ' ' . $columnValue . ' ' . $value[2] . ' ? ' . $nextParam;
							$bindParamTypes .= $value[0];
							$bindParamArray[] = $value[3];
						}
					}
				}
			}
		}
		$whereStr = rtrim($whereStr, ' ' . $nextParam);
		return $whereStr;
	}

	function prepareDelete($object) {

		$queryTable = $object['table'];
		$queryWheres = isset($object['wheres']) ? $object['wheres'] : null;

		$result = [];

		$bindParamTypes = '';
		$bindParamArray = [];
		$bindParams = [];

		$whereStr = '';
		
		if (!empty($queryWheres)) {
			$nextParam = 'AND';
			$whereStr .= ' WHERE';
			foreach ($queryWheres as $table => $values) {
				if (array_values($queryWheres) === $queryWheres) {
					if ($values[2]=='IN') {
						$inStr = '';
						foreach ($values[3] as $inValue) {
							$inStr .= '?, ';
							$bindParamTypes .= $values[0];
							$bindParamArray[] = $inValue;
						}
						$inStr = rtrim($inStr, ', ');
						$nextParam = 'AND';
						if (isset($values[4])) {
							$nextParam = $values[4];
						}
						$whereStr .= ' ' . $values[1] . ' IN (' . $inStr . ') ' . $nextParam;
					}
					else {
						$nextParam = 'AND';
						if (isset($values[4])) {
							$nextParam = $values[4];
						}
						if (isset($values[5]) && $values[5] == 'GROUP_START') {
							$whereStr .= ' (';
						}
						if ($values[3]==NULL) {
							$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' NULL ' . $nextParam;
						}
						else {
							$whereStr .= ' ' . $values[1] . ' ' . $values[2] . ' ? ' . $nextParam;
							$bindParamTypes .= $values[0];
							$bindParamArray[] = $values[3];
						}
						if (isset($values[5]) && $values[5] == 'GROUP_END') {
							$whereStr = rtrim($whereStr, ' ' . $nextParam);
							$whereStr .= ') ' . $nextParam;
						}
					}
				}
				else {
					foreach ($values as $value) {
						if ($value[2]=='in') {
							$inStr = '';
							foreach ($value[3] as $inValue) {
								$inStr .= '?,';
								$bindParamTypes .= $value[0];
								$bindParamArray[] = $inValue;
							}
							$inStr = rtrim($inStr, ',');
							$nextParam = 'AND';
							if (isset($value[4])) {
								$nextParam = $value[4];
							}
							$whereStr .= ' ' . $table . '.' . $value[1] . ' IN (' . $inStr . ') ' . $nextParam;
						}
						else {
							$hasLPAD = strpos($value[1], 'LPAD(') === FALSE ? FALSE : TRUE;
							$nextParam = 'AND';
							if (isset($value[4])) {
								$nextParam = $value[4];
							}
							if (isset($value[5]) && $value[5] == 'GROUP_START') {
								$whereStr .= ' (';
							}
							if ($hasLPAD) {
								$inside = str_replace('LPAD(', '', $value[1]);
								$whereStr .= ' LPAD(' . $table . '.' . $inside . ' ' . $value[2] . ' ? ' . $nextParam;
								$bindParamTypes .= $value[0];
								$bindParamArray[] = $value[3];
							}
							else {
								if ($value[3]===NULL) {
									$whereStr .= ' ' . $table . '.' . $value[1] . ' ' . $value[2] . ' NULL ' . $nextParam;
								}
								else {
									$whereStr .= ' ' . $table . '.' . $value[1] . ' ' . $value[2] . ' ? ' . $nextParam;
									$bindParamTypes .= $value[0];
									$bindParamArray[] = $value[3];
								}
							}
							if (isset($value[5]) && $value[5] == 'GROUP_END') {
								$whereStr = rtrim($whereStr, ' ' . $nextParam);
								$whereStr .= ') ' . $nextParam;
							}
						}
					}
				}
			}
			$whereStr = rtrim($whereStr, ' ' . $nextParam);
		}

		$bindParams[] = $bindParamTypes;
		for ($i=0; $i<count($bindParamArray); $i++) {
			$bindParams[] = &$bindParamArray[$i];
		}

		$queryStr = 'DELETE FROM ' . $queryTable . $whereStr;

		$stmt = $this->mysqli->prepare($queryStr);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }
		
		call_user_func_array(array($stmt, 'bind_param'), $bindParams);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->execute();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$result['affected_rows'] = $this->mysqli->affected_rows;

		$result['id'] = $this->mysqli->insert_id;
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		return $result;

	}

	function prepareSelectFrom($object) {

		$queryTable = $object['table'];
		$queryColumns = $object['columns'];
		$queryWheres = isset($object['wheres']) ? $object['wheres'] : null;
		$queryLeftJoins = isset($object['leftJoins']) ? $object['leftJoins'] : null;
		$queryInnerJoins = isset($object['innerJoins']) ? $object['innerJoins'] : null;
		$queryOrders = isset($object['orders']) ? $object['orders'] : null;
		$queryLimit = isset($object['limit']) ? $object['limit'] : null;
		$queryGroups = isset($object['groups']) ? $object['groups'] : null;

		$result = [];

		$tmpResult = [];
		$resultArray = [];

		$bindParamTypes = '';
		$bindParamArray = [];
		$bindParams = [];

		$queryStr = 'SELECT';
		
		foreach ($queryColumns as $table => $columns) {
			if (array_values($queryColumns) === $queryColumns) {
				$columnBreak = explode(' AS ', $columns);
				$queryStr .= ' ' . $columns . ',';
				$resultArray[] = &$tmpResult[$columnBreak[count($columnBreak) - 1]];
			}
			else {
				foreach ($columns as $column) {
					$hasCount = strpos($column, 'COUNT(') === FALSE ? FALSE : TRUE;
					if ($hasCount) {
						$column = str_replace('COUNT(', '', $column);
						$queryStr .= ' COUNT(' . $table . '.' . $column . ',';
						$column = str_replace(')', '', $column);
					}
					else {
						if (strpos($column, '*.')) {
							$queryStr .= ' ' . str_replace('*.', $table . '.', $column) . ',';
							$column = str_replace('*.', '', $column);
						}
						else {
							$queryStr .= ' ' . $table . '.' . $column . ',';
						}
					}
					$columnBreak = explode(' AS ', $column);
					$resultArray[] = &$tmpResult[$columnBreak[count($columnBreak) - 1]];
				}
			}
		}
		$queryStr = rtrim($queryStr, ',');

		$queryStr .= ' FROM ' . $queryTable;
		
		if ($queryLeftJoins!==null) {
			foreach ($queryLeftJoins as $table => $values) {
				$aliases = explode(' ', $table);
				$joinTable = $queryTable;
				if (strpos($values[1], '.') !== FALSE) {
					$joinArr = explode('.', $values[1]);
					$joinTable = $joinArr[0];
					$values[1] = $joinArr[1];
				}
				if (count($aliases) == 2) {
					$queryStr .= ' LEFT JOIN ' . $table . ' ON ' . $joinTable . '.' . $values[1] . ' = ' . $aliases[1] . '.' . $values[0];
				}
				else {
					$queryStr .= ' LEFT JOIN ' . $table . ' ON ' . $joinTable . '.' . $values[1] . ' = ' . $table . '.' . $values[0];
				}
			}
		}
		
		if ($queryInnerJoins!==null) {
			foreach ($queryInnerJoins as $table => $values) {
				$aliases = explode(' ', $table);
				$joinTable = $queryTable;
				if (strpos($values[1], '.') !== FALSE) {
					$joinArr = explode('.', $values[1]);
					$joinTable = $joinArr[0];
					$values[1] = $joinArr[1];
				}
				if (count($aliases) == 2) {
					$queryStr .= ' INNER JOIN ' . $table . ' ON ' . $joinTable . '.' . $values[1] . ' = ' . $aliases[1] . '.' . $values[0];
				}
				else {
					$queryStr .= ' INNER JOIN ' . $table . ' ON ' . $joinTable . '.' . $values[1] . ' = ' . $table . '.' . $values[0];
				}
			}
		}
		
		if (!empty($queryWheres)) {
			$queryStr .= ' WHERE ';
			$queryStr .= $GLOBALS['DB']->prepareWheres($queryWheres, $bindParamTypes, $bindParamArray);
		}
		
		if ($queryGroups!==null) {
			$queryStr .= ' GROUP BY';
			foreach ($queryGroups as $table => $values) {
				if (array_values($queryGroups) === $queryGroups) {
					$queryStr .= ' ' . $values . ',';
				}
				else {
					foreach ($values as $value) {
						$queryStr .= ' ' . $table . '.' . $value . ',';
					}
				}
			}
			$queryStr = rtrim($queryStr, ',');
		}
		
		if ($queryOrders!==null) {
			$queryStr .= ' ORDER BY';
			foreach ($queryOrders as $table => $values) {
				if (array_values($queryOrders) === $queryOrders) {
					$queryStr .= ' ' . $values[0] . ' ' . $values[1] . ',';
				}
				else {
					foreach ($values as $value) {
						$hasCount = strpos($value[0], 'COUNT(') === FALSE ? FALSE : TRUE;
						$hasIsNull = strpos($value[0], 'IS NULL') === FALSE ? FALSE : TRUE;
						if ($hasIsNull) {
							$column = str_replace(' IS NULL', '', $value[0]);
							$queryStr .= ' ' . $table . '.' . $column . ' IS NULL, ' . $table . '.' . $column . ' ' . $value[1] . ',';
						}
						else if ($hasCount) {
							$column = str_replace('COUNT(', '', $value[0]);
							$queryStr .= ' COUNT(' . $table . '.' . $column . ' ' . $value[1] . ',';
						}
						else {
							$queryStr .= ' ' . $table . '.' . $value[0] . ' ' . $value[1] . ',';
						}
					}
				}
			}
			$queryStr = rtrim($queryStr, ',');
		}
		
		if ($queryLimit!==null) {
			$queryStr .= ' LIMIT ';
			if (is_array($queryLimit)) {
				if (count($queryLimit)>1) {
					$queryStr .= $queryLimit[1] . ',' . $queryLimit[0];
				}
				else {
					$queryStr .= $queryLimit[0];
				}
			}
			else {
				$queryStr .= $queryLimit;				
			}
		}
		
		if ($bindParamTypes!='') {
			$bindParams[] = $bindParamTypes;

			for ($i=0; $i<count($bindParamArray); $i++) {
				$bindParams[] = &$bindParamArray[$i];
			}
		}

		$queryStr = rtrim($queryStr) . ';';

		$stmt = $this->mysqli->prepare($queryStr);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		if (count($bindParams)!=0) {
			call_user_func_array(array($stmt, 'bind_param'), $bindParams);
			if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }
		}

		$stmt->execute();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		call_user_func_array(array($stmt, 'bind_result'), $resultArray);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		while($stmt->fetch()) {
			$tmp = [];
			foreach ($tmpResult as $key=>$value) {
				$tmp[$key] = $value;
			}
			$result[] = $tmp;
		}
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		return $result;

	}

	function prepareGetRows($object) {

		$queryTable = $object['table'];
		$queryWheres = isset($object['wheres']) ? $object['wheres'] : null;

		$result = [];

		$tmpResult = [];
		$resultArray = [];

		$bindParamTypes = '';
		$bindParamArray = [];
		$bindParams = [];

		$queryStr = 'SELECT Count(id) FROM ' . $queryTable;
		
		if ($queryWheres!==null) {
			$queryStr .= ' WHERE';
			foreach ($queryWheres as $values) {
				if ($values[2]=='IN') {
					$inStr = '';
					foreach ($values[3] as $inValue) {
						$inStr .= '?,';
						$bindParamTypes .= $values[0];
						$bindParamArray[] = $inValue;
					}
					$inStr = rtrim($inStr, ',');
					$queryStr .= ' ' . $values[1] . ' IN (' . $inStr . ') AND';					
				}
				else {
					$queryStr .= ' ' . $values[1] . ' ' . $values[2] . ' ? AND';
					$bindParamTypes .= $values[0];
					$bindParamArray[] = $values[3];
				}
			}
			array_unshift($bindParamArray, $bindParamTypes);
			$queryStr = rtrim($queryStr, ' AND');
		}
		
		$bindParams[] = $bindParamTypes;
		
		for ($i=0; $i<count($bindParamArray); $i++) {
			$bindParams[] = &$bindParamArray[$i];
		}

		$queryStr = rtrim($queryStr) . ';';

		$stmt = $this->mysqli->prepare($queryStr);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		if (count($bindParamArray)!=0) {
			call_user_func_array(array($stmt, 'bind_param'), $bindParamArray);
			if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }
		}

		$stmt->execute();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->bind_result($result['filas']);
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		$stmt->fetch();
		if ($this->mysqli->error) { throw new Exception($this->mysqli->error); }

		return $result;

	}

}

?>