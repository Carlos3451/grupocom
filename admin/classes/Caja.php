<?php

include_once('classes/Notificacion.php');

class Caja {

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'cajas',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function open($data) {
        $result = ['error' => ''];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 2 : $folioResult['resultado']['folio'] + 1;
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'cajas',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['s', 'usuarioID', $GLOBALS['usuario']['id']],
                        ['d', 'dineroApertura', $data['dineroApertura']],
                        ['d', 'dineroCierre', 0],
                        ['i', 'abierta', 1],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                        ['s', 'fechaOpen', date('Y-m-d G:i:s', $data['fecha'])]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                $this->notificacion->create('caja.open', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'dineroApertura' => $data['dineroApertura']
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function close($data) {
        $result = ['error' => ''];
        $cajaResult = $this->get(['id' => $data['id']]);
        if ($cajaResult['error'] != '') {
            $result['error'] = 'CAJA_GET:' . $cajaResult['error'];
            return $result;
        }
        $cajaData = $cajaResult['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'cajas',
                'values' => [
                    ['d', 'dineroCierre', $data['dineroCierre']],
                    ['i', 'abierta', 0],
                    ['s', 'fechaClose', date('Y-m-d G:i:s', $data['fecha'])]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('caja.close', [
                'id' => $cajaData['id'],
                'folio' => $cajaData['folio'],
                'dineroCierre' => $data['dineroCierre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'cajas',
                'columns' => ['id', 'folio', 'dineroApertura', 'dineroCierre'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            $date = explode('/', $data['filtros']['fecha']);
            switch (count($date)) {
                case '1':
                    $wheres['cajas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                    break;
                case '2':
                    $wheres['cajas'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                    $wheres['cajas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                    break;
                case '3':
                    $wheres['cajas'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                    $wheres['cajas'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                    $wheres['cajas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                    break;
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['cajas'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else {            
            if (!empty($data['filtros']['usuario'])) {
                $wheres['cajas'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'cajas',
                'columns' => [
                    'cajas' => ['id', 'folio', 'dineroApeartura', 'dineroCierre'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'cajas' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'cajas' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'cajas' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'cajas' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

}

?>