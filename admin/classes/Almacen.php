<?php

include_once('classes/Notificacion.php');

class Almacen {    

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'almacenes',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            if (is_string($data['productos'])) { $data['productos'] = json_decode($data['productos'], true); }
            $productsEncoded = $this->encode_products($data['productos']);
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'almacenes',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['s', 'productosIDs', $productsEncoded['productosIDs']],
                        ['s', 'cantidadesIniciales', $productsEncoded['cantidadesIniciales']],
                        ['s', 'cantidadesComputadas', $productsEncoded['cantidadesComputadas']],
                        ['s', 'cantidadesFinales', $productsEncoded['cantidadesFinales']],
                        ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]);
                $result['resultado'] = $preparedResult;
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function encode_products($products) {
        $productsEncoded = [
            'productosIDs' => '',
            'cantidadesIniciales' => '',
            'cantidadesComputadas' => '',
            'cantidadesFinales' => ''
        ];
        $productsIDs = [];
        $qtyStart = [];
        $qtyComputed = [];
        $qtyFinal = [];
        for ($i=0; $i<count($products); $i++) {
            $productsIDs[] = $products[$i]['id'];
            $qtyStart[] = $products[$i]['cantidadInicial'];
            $qtyComputed[] = $products[$i]['cantidadActual'];
            $qtyFinal[] = $products[$i]['cantidadFinal'];
        }
        $productsEncoded['productosIDs'] = implode(',', $productsIDs);
        $productsEncoded['cantidadesIniciales'] = implode(',', $qtyStart);
        $productsEncoded['cantidadesComputadas'] = implode(',', $qtyComputed);
        $productsEncoded['cantidadesFinales'] = implode(',', $qtyFinal);
        return $productsEncoded;
    }

    function decode_products($data) {
        $products = [];
        $productsIDs = explode(',', $data['productosIDs']);
        $qtyStart = explode(',', $data['cantidadesIniciales']);
        $qtyComputed = explode(',', $data['cantidadesComputadas']);
        $qtyFinal = explode(',', $data['cantidadesFinales']);
        for ($i=0; $i<count($productsIDs); $i++) {
            $productData = $this->get_product($productsIDs[$i]);
            if ($productData['error'] == '') {
                $product = $productData['resultado'];
                $product['cantidadInicial'] = $qtyStart[$i];
                $product['cantidadComputada'] = $qtyComputed[$i];
                $product['cantidadFinal'] = $qtyFinal[$i];
                $products[] = $product;
            }
        }
        return $products;
    }

    function get_product($productID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['id', 'folio', 'nombre', 'unidad'],
                'wheres' => [
                    ['i', 'id', '=', $productID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'almacenes',
                'columns' => [
                    'almacenes' => ['id', 'folio', 'usuarioID', 'productosIDs', 'cantidadesIniciales', 'cantidadesComputadas', 'cantidadesFinales'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'almacenes' => [
                        ['i', 'id', '=', $data['id']],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $preparedResult[0]['productos'] = $this->decode_products($preparedResult[0]);
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['almacenes'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['almacenes'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['almacenes'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['almacenes'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['almacenes'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['almacenes'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['almacenes'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['almacenes'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 4) {
            $wheres['almacenes'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else {
            if (!empty($data['filtros']['usuario'])) {
                $wheres['almacenes'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'almacenes',
                'columns' => [
                    'almacenes' => ['id', 'folio', 'productosIDs', 'cantidadesIniciales', 'cantidadesComputadas', 'cantidadesFinales', 'fechaCreate'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;        
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'almacenes' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'almacenes' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'almacenes' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'almacenes' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

}

?>