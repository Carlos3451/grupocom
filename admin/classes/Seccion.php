<?php

class Seccion {

    function add($data, $files) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'secciones',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'claveGerente', $data['claveGerente']],
                    ['d', 'lat', $data['lat']],
                    ['d', 'lng', $data['lng']],
                    ['i', 'facturacion', 0]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];

            $this->add_productos_existencias($preparedResult['id']);

            if (isset($result['resultado']['id'])) {
                $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/secciones/' . $result['resultado']['id'];
                mkdir($dirPath);
                if ($_FILES['imagen']['size'] != 0) {
                    $tmpFilename = $_FILES['imagen']['tmp_name'];
                    $filename = $_FILES['imagen']['name'];
                    if (getimagesize($tmpFilename)[0] != 0) {
                        move_uploaded_file($tmpFilename, $dirPath . '/' . $filename);
                    }
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit($data, $files) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'secciones',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'claveGerente', $data['claveGerente']],
                    ['d', 'lat', $data['lat']],
                    ['d', 'lng', $data['lng']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            if ($_FILES['imagen']['size'] != 0) {
                $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/secciones/' . $data['id'];
                if (!file_exists($dirPath)) {
                    mkdir($dirPath);
                }
                $tmpFilename = $_FILES['imagen']['tmp_name'];
                $filename = $_FILES['imagen']['name'];
                if (getimagesize($tmpFilename)[0] != 0) {
                    $this->delete_image($data['id']);
                    move_uploaded_file($tmpFilename, $dirPath . '/' . $filename);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        if (!in_array(1, $data['ids'])) {
            try {
                $preparedResult = $GLOBALS['DB']->prepareDelete([
                    'table' => 'secciones',
                    'wheres' => [
                        ['i', 'id', 'IN', $data['ids']],
                    ]
                ]);
                if ($preparedResult['affected_rows'] == -1) {
                    $result['error'] = 'NOT_FOUND';
                    return $result;
                }
                for ($i=0; $i<count($data['ids']); $i++) {
                    $this->delete_directory($data['ids'][$i]);
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'DELETE_BASE';
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id', 'nombre', 'claveGerente', 'lat', 'lng'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['imagen'] = $this->get_image($preparedResult[0]);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $orders = $this->get_order($data['metodo'], $data['orden']);
        $where = $this->get_where($data['busqueda']);
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id', 'nombre', 'claveGerente', 'lat', 'lng'],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['imagen'] = $this->get_image($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_image($data) {
        $imgsArray = [];
        $dirPath = '/secciones/' . $data['id'];
        $imgPath = '/secciones/0/default.png';
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirPath)) {
            foreach (array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $dirPath), array('..', '.')) as $img) {
                $imgsArray[] = $img;
            }
            if (count($imgsArray)!=0) {
                $imgPath = $dirPath . '/' . $imgsArray[0];
            }
        }
        return $imgPath;
    }

    function delete_image($id) {
        $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/secciones/' . $id;
        foreach (array_diff(scandir($dirPath), array('.', '..')) as $image) {
            unlink($dirPath . '/' . $image);
        }
    }

    function delete_directory($id) {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/secciones/' . $id)) {
            $this->delete_image($id);
            $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/secciones/' . $id;
            rmdir($dirPath);
        }
    }

    function get_where($search) {
        $where = [];
        if ($search != '') {
            $search = '%' . $search . '%';
            $where = [
                ['s', 'LPAD(id, 4, "0")', 'LIKE', $search, 'OR'],
                ['s', 'nombre', 'LIKE', $search, 'OR']
            ];
        }
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'id':
                $orderArray = [
                    ['id', $orderValue]
                ];
                break;
            case 'nombre':
                $orderArray = [
                    ['nombre', $orderValue]
                ];
        }
        return $orderArray;
    }

    // ADD PRODUCTOS EXISTENCIAS

    function get_all_productos() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['id']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    
    function add_productos_existencias($seccionID) {
        $result = ['error' => ''];

        $productosResult = $this->get_all_productos();
        if ($productosResult['error'] != '') {
            $result['error'] = 'GET_PRODUCTOS:' . $productosResult['error'];
        }
        $productosData = $productosResult['resultado'];

        $values = [];
        foreach ($productosData AS $producto) {
            $values[] = [
                ['d', 'existencia', 0],
                ['i', 'productoID', $producto['id']],
                ['i', 'seccionID', $seccionID]
            ];
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'productos_existencias',
                'values' => $values
            ]);
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>