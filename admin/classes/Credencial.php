<?php

include_once('classes/Notificacion.php');

class Credencial {

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function add($data, $files) {
        $result = ['error' => ''];
        if ($data['contraseña'] == $data['confirmarContraseña']) {
            $passwordHashed = password_hash($data['contraseña'], PASSWORD_DEFAULT);
            $sections = $GLOBALS['usuario']['seccionActual']['id'];
            if (!empty($data['secciones'])) {
                $sections = $this->get_sanatized_sections($data['secciones']);
            }
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'usuarios',
                    'values' => [
                        ['s', 'nombre', $data['nombre']],
                        ['s', 'telefono', $data['telefono']],
                        ['s', 'email', $data['email']],
                        ['s', 'calle', $data['calle']],
                        ['s', 'colonia', $data['calle']],
                        ['s', 'codigoPostal', $data['codigoPostal']],
                        ['s', 'numeroExterior', $data['numeroExterior']],
                        ['s', 'numeroInterior', $data['numeroInterior']],
                        ['s', 'usuario', $data['usuario']],
                        ['s', 'contraseña', $passwordHashed],
                        ['s', 'privilegios', $data['privilegios']],
                        ['i', 'privilegiosNivel', max(0, min($GLOBALS['usuario']['privilegiosNivel'], $data['privilegiosNivel']))],
                        ['s', 'secciones', $sections],
                        ['i', 'seccionActual', $GLOBALS['usuario']['seccionActual']['id']],
                        ['s', 'cookie', '']
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];

                if ($result['resultado']['id'] != '') {
                    if ($_FILES['imagen']['size'] != 0) {
                        $tmpFilename = $_FILES['imagen']['tmp_name'];
                        $filename = $_FILES['imagen']['name'];
                        if (getimagesize($tmpFilename)[0] != 0) {
                            $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/usuarios/' . $result['resultado']['id'];
                            if (!file_exists($dirPath)) {
                                mkdir($dirPath);
                            }
                            if (getimagesize($tmpFilename)[0] != 0) {
                                move_uploaded_file($tmpFilename, $dirPath . '/' . $filename);
                            }
                        }
                    }
                }

                $this->notificacion->create('credencial.add', [
                    'id' => $result['resultado']['id'],
                    'nombre' => $data['nombre']
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'PASSWORD_NOT_MATCH';
        }
        return $result;
    }

    function edit($data, $files) {
        $result = ['error' => ''];
        $sections = $GLOBALS['usuario']['seccionActual']['id'];
        if (isset($data['secciones'])) {
            $sections = $this->get_sanatized_sections($data['secciones']);
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'usuarios',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'telefono', $data['telefono']],
                    ['s', 'email', $data['email']],
                    ['s', 'calle', $data['calle']],
                    ['s', 'colonia', $data['calle']],
                    ['s', 'codigoPostal', $data['codigoPostal']],
                    ['s', 'numeroExterior', $data['numeroExterior']],
                    ['s', 'numeroInterior', $data['numeroInterior']],
                    ['s', 'privilegios', $data['privilegios']],
                    ['i', 'privilegiosNivel', max(0, min($GLOBALS['usuario']['privilegiosNivel'], $data['privilegiosNivel']))],
                    ['s', 'secciones', $sections]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%']
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }

            if ($_FILES['imagen']['size'] != 0) {
                $tmpFilename = $_FILES['imagen']['tmp_name'];
                $filename = $_FILES['imagen']['name'];
                if (getimagesize($tmpFilename)[0] != 0) {
                    $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/usuarios/' . $data['id'];
                    if (!file_exists($dirPath)) {
                        mkdir($dirPath);
                    }
                    $this->delete_image($data['id']);
                    move_uploaded_file($tmpFilename, $dirPath . '/' . $filename);
                }
            }

            $this->notificacion->create('credencial.edit', [
                'id' => $data['id'],
                'nombre' => $data['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function change_user_data($data) {
        $result = ['error' => ''];
        $credencial = $this->get(['id' => $data['id']])['resultado'];
        if ($data['contraseña'] == $data['confirmarContraseña']) {
            $passwordHashed = password_hash($data['contraseña'], PASSWORD_DEFAULT);
            $sections = $GLOBALS['usuario']['seccionActual']['id'];
            if ($data['id'] != 1 || $GLOBALS['usuario']['id'] == 1) {
                try {
                    $wheres = [
                        ['i', 'id', '=', $data['id']]
                    ];
                    if ($data['id'] != 1) {
                        $wheres[] = ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%'];
                    }
                    $preparedResult = $GLOBALS['DB']->prepareUpdate([
                        'table' => 'usuarios',
                        'values' => [
                            ['s', 'usuario', $data['usuario']],
                            ['s', 'contraseña', $passwordHashed]
                        ],
                        'wheres' => $wheres
                    ]);
                    if ($preparedResult['affected_rows'] == -1) {
                        $result['error'] = 'NOT_FOUND';
                        return $result;
                    }
                    $this->notificacion->create('credencial.changepassword', [
                        'id' => $data['id'],
                        'nombre' => $credencial['nombre']
                    ]);
                }
                catch (Exception $err) {
                    $result['error'] = $err->getMessage();
                }
            }
            else {
                $result['error'] = 'NOT_ADMIN';
            }
        }
        else {
            $result['error'] = 'PASSWORD_NOT_MATCH';
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        if (!in_array(1, $data['ids'])) {
            $credenciales = [];
            for ($i=0; $i<count($data['ids']); $i++) {
                $credenciales[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
            }
            try {
                $preparedResult = $GLOBALS['DB']->prepareDelete([
                    'table' => 'usuarios',
                    'wheres' => [
                        ['i', 'id', 'IN', $data['ids']],
                        ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%']
                    ]
                ]);
                if ($preparedResult['affected_rows'] == -1) {
                    $result['error'] = 'NOT_FOUND';
                    return $result;
                }
                for ($i=0; $i<count($data['ids']); $i++) {
                    $this->delete_directory($data['ids'][$i]);
                    $this->notificacion->create('credencial.delete', [
                        'id' => $data['ids'][$i],
                        'nombre' => $credenciales[$i]['nombre']
                    ]);
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = 'No se puede eliminar al Administrador.';
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id', 'nombre', 'telefono', 'email', 'calle', 'colonia', 'codigoPostal', 'numeroExterior', 'numeroInterior', 'privilegios', 'privilegiosNivel', 'secciones'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $sections = explode(',', $preparedResult[0]['secciones']);            
            if (in_array($GLOBALS['usuario']['seccionActual']['id'], $sections) || $preparedResult[0]['id'] == 1) {
                $preparedResult[0]['imagen'] = $this->get_image($preparedResult[0]);
                $result['resultado'] = $preparedResult[0];
            }
            else {
                $result['error'] = 'NOT_FOUND';
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all() {
        $result = ['error' => ''];
        $orders = $this->get_order($_POST['metodo'], $_POST['orden']);
        $where = $this->get_where($_POST['busqueda']);
        if ($GLOBALS['usuario']['id'] != 1) {
            $where[] = ['i', 'id', '>', '1'];
        }
        try {
            $users = [];
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id', 'nombre', 'secciones', 'privilegios', 'privilegiosNivel', 'usuario'],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['imagen'] = $this->get_image($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_dealers() {
        $result = ['error' => ''];
        try {
            $users = [];
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id', 'nombre', 'secciones', 'privilegios', 'privilegiosNivel', 'usuario'],
                'orders' => [
                    ['id', 'ASC']
                ],
                'wheres' => [
                    ['i', 'id', '>', '1'],
                    ['s', 'privilegios', 'LIKE', '%entregas%'],
                    ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_image($data) {
        $imgPath = '/usuarios/0/default.jpg';
        $imgsArray = [];
        $dirPath = '/usuarios/' . $data['id'];
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirPath)) {
            foreach (array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $dirPath), array('..', '.')) as $img) {
                $imgsArray[] = $img;
            }
            if (count($imgsArray)!=0) {
                $imgPath = $dirPath . '/' . $imgsArray[0];
            }
        }
        return $imgPath;
    }

    function delete_image($id) {
        $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/usuarios/' . $id;
        foreach (array_diff(scandir($dirPath), array('.', '..')) as $image) {
            unlink($dirPath . '/' . $image);
        }
    }

    function delete_directory($id) {
        $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/usuarios/' . $id;
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $dirPath)) {
            $this->delete_image($id);
            rmdir($dirPath);
        }
    }

    function get_where($search) {
        $search = '%' . $search . '%';
        $where = [
            'GROUP_1' => [ 
                ['i', 'id', '=', '1', 'OR'],
                ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%']
            ],
            'GROUP_2' => [ 
                ['s', 'LPAD(id, 4, "0")', 'LIKE', $search, 'OR'],
                ['s', 'nombre', 'LIKE', $search]
            ]
        ];
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'id':
                $orderArray = [
                    ['id', $orderValue]
                ];
                break;
            case 'nombre':
                $orderArray = [
                    ['nombre', $orderValue]
                ];
        }
        return $orderArray;
    }

    function get_sanatized_sections($sectionsStr) {
        $sectionsRaw = explode(',', $sectionsStr);
        $sections = [];
        for ($i=0; $i<count($GLOBALS['usuario']['secciones']); $i++) {
            if (in_array($GLOBALS['usuario']['secciones'][$i]['id'], $sectionsRaw)) {
                $sections[] = $GLOBALS['usuario']['secciones'][$i]['id'];
            }
        }
        $sections = implode(',', $sections);
        if ($sections == '') {
            $section = $GLOBALS['usuario']['seccionActual']['id'];
        }
        return $sections;
    }

}

?>