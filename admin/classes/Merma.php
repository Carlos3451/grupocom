<?php

include_once('classes/Notificacion.php');

class Merma {

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'mermas',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'mermas',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                if (isset($data['productos'])) {
                    foreach ($data['productos'] AS $producto) {
                        $this->add_merma_producto($result['resultado']['id'], $producto);
                    }
                }
                $this->notificacion->create('merma.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $mermaResult = $this->get(['id' => $data['id']]);
        if ($mermaResult['error'] != '') {
            $result['error'] = 'GET_MERMA:' . $mermaResult['error'];
            return $result;
        }
        $mermaData = $mermaResult['resultado'];
        if (isset($data['productos'])) {
            foreach ($data['productos'] AS $producto) {
                $this->add_merma_producto($data['id'], $producto);
            }
        }
        $this->notificacion->create('merma.edit', [
            'id' => $data['id'],
            'folio' => $mermaData['folio']
        ]);
        return $result;
    }

    function add_merma_producto($mermaID, $data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'mermas_productos',
                'values' => [
                    ['i', 'mermaID', $mermaID],
                    ['i', 'productoID', $data['productoID']],
                    ['d', 'cantidad', $data['cantidad']]
                ]
            ]);
            $this->subtract_existence($data['productoID'], $data['cantidad']);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $mermasData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $mermaResult = $this->get(['id' => $data['ids'][$i]]);
            if ($mermaResult['error'] != '') {
                $result['error'] = 'MERMA_GET:' . $mermaResult['error'];
                return $result;
            }
            $mermasData[] = $mermaResult['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'mermas',
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $merma = $mermasData[$i];
                if ($merma['productos']['error'] == '') {
                    foreach ($merma['productos']['resultado'] AS $producto) {
                        $this->add_existence($producto['productoID'], $producto['cantidad']);
                    }
                }
                $this->notificacion->create('merma.delete', [
                    'id' => $data['ids'][$i],
                    'folio' => $merma['folio']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'mermas',
                'columns' => ['id', 'folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['productos'] = $this->get_merma_productos($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['mermas'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['mermas'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['mermas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['mermas'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['mermas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['mermas'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['mermas'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['mermas'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['mermas'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else {            
            if (!empty($data['filtros']['usuario'])) {
                $wheres['mermas'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'mermas',
                'columns' => [
                    'mermas' => ['id', 'folio', 'fechaCreate'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_merma_productos($mermaID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'mermas_productos',
                'columns' => [
                    'mermas_productos' => ['id', 'productoID', 'cantidad'],
                    'productos' => ['nombre', 'unidad']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'mermas_productos' => [
                        ['i', 'mermaID', '=', $mermaID]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_search($search) {
        $where = [
            'mermas' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ]
        ];
        if ($search != '') {
            $search = '%' . $search . '%';
            $where['GROUP_SEARCH'] = [
                'mermas' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ];
        }
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'mermas' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'mermas' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function add_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $quantity, 'existencia + ?']
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function subtract_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $quantity, 'existencia - ?']
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

}

?>