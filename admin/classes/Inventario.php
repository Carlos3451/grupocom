<?php

include_once('classes/Notificacion.php');

class Inventario {

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'inventarios',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'inventarios',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                if (isset($data['inventarioProductos'])) {
                    foreach ($data['inventarioProductos'] AS $inventarioProducto) {
                        $this->add_inventory_product($result['resultado']['id'], $inventarioProducto);
                    }
                }
                $this->notificacion->create('inventario.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $inventoryData = $this->get(['id' => $data['id']])['resultado'];
        $this->notificacion->create('inventario.edit', [
            'id' => $data['id'],
            'folio' => $inventoryData['folio']
        ]);
        if (isset($data['inventarioProductos'])) {
            foreach ($data['inventarioProductos'] AS $inventarioProducto) {
                $this->add_inventory_product($data['id'], $inventarioProducto);
            }
        }
        return $result;
    }

    function add_inventory_product($inventoryID, $data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'inventarios_productos',
                'values' => [
                    ['i', 'inventarioID', $inventoryID],
                    ['i', 'productoID', $data['productoID']],
                    ['d', 'existenciaNueva', $data['existenciaNueva']],
                    ['d', 'existenciaAnterior', $data['existenciaAnterior']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
            $this->set_existence($data['productoID'], $data['existenciaNueva']);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $inventoriesData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $inventoriesData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'inventarios',
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('inventario.delete', [
                    'id' => $data['ids'][$i],
                    'folio' => $inventoriesData[$i]['folio']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function set_existence($productID, $existence) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $existence]
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'inventarios',
                'columns' => ['id', 'folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['inventario'] = $this->get_inventory_productos($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['inventarios'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['inventarios'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['inventarios'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['inventarios'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['inventarios'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['inventarios'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['inventarios'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['inventarios'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        if (!empty($data['filtros']['usuario'])) {
            $wheres['inventarios'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
        }
        if (!empty($data['filtros']['cliente'])) {
            $wheres['inventarios'][] = ['i', 'clienteID', '=', $data['filtros']['cliente']];
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['inventarios'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        else {            
            if (!empty($data['filtros']['usuario'])) {
                $wheres['inventarios'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'inventarios',
                'columns' => [
                    'inventarios' => ['id', 'folio', 'fechaCreate'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_inventory_productos($inventoryID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'inventarios_productos',
                'columns' => [
                    'inventarios_productos' => ['id', 'productoID', 'existenciaAnterior', 'existenciaNueva'],
                    'productos' => ['nombre', 'unidad']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'inventarios_productos' => [
                        ['i', 'inventarioID', '=', $inventoryID]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'inventarios' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'inventarios' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'inventarios' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'inventarios' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }


}

?>