<?php

include_once('classes/Notificacion.php');

class Ticket {

    function __construct() {
        $this->notificacion = new Notificacion();
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];

        $clientResult = $this->get_client($data['clienteID']);
        if ($clientResult['error'] != '') {
            $result['error'] = 'CLIENT_GET:' . $clientResult['error'];
            return $result;
        }
        $clientData = $clientResult['resultado'];

        $data['subtotal'] = isset($data['subtotal']) ? $data['subtotal'] : $data['total'];
        $data['descuento'] = isset($data['descuento']) ? $data['descuento'] : 0;

        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            if (!isset($data['domicilio'])) {
                $data['domicilio'] = 0;
            }
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'tickets',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                        ['i', 'clienteID', $data['clienteID']],
                        ['d', 'subtotal', $data['subtotal']],
                        ['d', 'descuento', $data['descuento']],
                        ['d', 'total', $data['total']],
                        ['d', 'pago', $data['pago']],
                        ['d', 'cambio', $data['cambio']],
                        ['s', 'tipo', $data['tipo']],
                        ['i', 'domicilio', $data['domicilio']],
                        ['i', 'cancelado', $data['cancelado']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                        ['s', 'fechaCreate', date('Y-m-d G:i:s', $data['fecha'])]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                $result['resultado']['folio'] = $folio;

                if (isset($data['ticketProductos'])) {
                    if (is_string($data['ticketProductos'])) { $data['ticketProductos'] = json_decode($data['ticketProductos'], true); }
                    foreach ($data['ticketProductos'] AS $ticketProducto) {
                        $this->add_ticket_product($result['resultado']['id'], $ticketProducto, $data['descuento'], $data['fecha']);
                        $this->subtract_existence($ticketProducto['productoID'], $ticketProducto['cantidad']);
                    }
                }

                $this->notificacion->create('ticket.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'total' => $data['total'],
                    'tipo' => $data['tipo'],
                    'clienteFolio' => $clientData['folio'],
                    'clienteNombre' => $clientData['nombre']
                ]);

                if ($data['tipo'] == 'credito') {
                    if ($data['cancelado'] == 0) {
                        $this->add_debt($data['clienteID'], $data['total']);
                    }
                    else {
                        $this->notificacion->create('ticket.cancel', [
                            'id' => $result['resultado']['id'],
                            'folio' => $folio,
                            'total' => $data['total'],
                            'tipo' => $data['tipo'],
                            'clienteFolio' => $clientData['folio'],
                            'clienteNombre' => $clientData['nombre']
                        ]);
                    }
                }
                else {
                    if ($data['cancelado'] == 0) {
                        $this->add_payment($result['resultado']['id'], null, $data['metodo'], $data['total'], $data['fecha']);
                    }
                    else {
                        $this->notificacion->create('ticket.cancel', [
                            'id' => $result['resultado']['id'],
                            'folio' => $folio,
                            'total' => $data['total'],
                            'tipo' => $data['tipo'],
                            'clienteFolio' => $clientData['folio'],
                            'clienteNombre' => $clientData['nombre']
                        ]);
                    }
                }
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $ticketResponse = $this->get(['id' => $data['id']]);
        if ($ticketResponse['error'] != '') {
            $result['error'] = $ticketResponse['error'];
            return $result;
        }
        $ticketData = $ticketResponse['resultado'];
        $clientData = $this->get_client($data['clienteID'])['resultado'];
        if (!isset($data['domicilio'])) {
            $data['domicilio'] = 0;
        }

        $data['subtotal'] = isset($data['subtotal']) ? $data['subtotal'] : $data['total'];
        $data['descuento'] = isset($data['descuento']) ? $data['descuento'] : 0;

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'tickets',
                'values' => [
                    ['i', 'folio', $ticketData['folio']],
                    ['i', 'clienteID', $data['clienteID']],
                    ['d', 'subtotal', $data['subtotal']],
                    ['d', 'descuento', $data['descuento']],
                    ['d', 'total', $data['total']],
                    ['d', 'pago', $data['pago']],
                    ['d', 'cambio', $data['cambio']],
                    ['s', 'tipo', $data['tipo']],
                    ['i', 'domicilio', $data['domicilio']],
                    ['i', 'cancelado', $data['cancelado']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                ]
            ]);

            $this->delete_ticket_products($data['id']);
            $this->delete_payments($data['id']);

            if ($ticketData['tipo'] == 'credito') {
                if ($ticketData['cancelado'] == 0) {
                    $this->subtract_debt($ticketData['clienteID'], $ticketData['total']);
                }
            }

            $this->notificacion->create('ticket.edit', [
                'id' => $data['id'],
                'folio' => $ticketData['folio'],
                'total' => $data['total'],
                'tipo' => $data['tipo'],
                'clienteFolio' => $clientData['folio'],
                'clienteNombre' => $clientData['nombre']
            ]);

            if ($data['tipo'] == 'credito') {
                if ($data['cancelado'] == 0) {
                    $this->add_debt($data['clienteID'], $data['total']);
                }
                else {
                    $this->notificacion->create('ticket.cancel', [
                        'id' => $data['id'],
                        'folio' => $ticketData['folio'],
                        'total' => $data['total'],
                        'tipo' => $data['tipo'],
                        'clienteFolio' => $clientData['folio'],
                        'clienteNombre' => $clientData['nombre']
                    ]);
                }
            }
            else {
                if ($data['cancelado'] == 0) {
                    $this->add_payment($data['id'], null, $data['metodo'], $data['total'], $data['fecha']);
                }
                else {
                    $this->notificacion->create('ticket.cancel', [
                        'id' => $data['id'],
                        'folio' => $ticketData['folio'],
                        'total' => $data['total'],
                        'tipo' => $data['tipo'],
                        'clienteFolio' => $clientData['folio'],
                        'clienteNombre' => $clientData['nombre']
                    ]);
                }
            }
            if (isset($data['ticketProductos'])) {
                foreach ($data['ticketProductos'] AS $ticketProducto) {
                    $this->add_ticket_product($data['id'], $ticketProducto, $data['descuento'], $data['fecha']);
                    $this->subtract_existence($ticketProducto['productoID'], $ticketProducto['cantidad']);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel($data) {
        $result = ['error' => ''];
        $ticketsData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $ticketsData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'tickets',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $ticketsProducts = $this->get_ticket_productos($data['ids'][$i]);
                $this->notificacion->create('ticket.cancel', [
                    'id' => $data['ids'][$i],
                    'folio' => $ticketsData[$i]['folio'],
                    'total' => $ticketsData[$i]['total'],
                    'tipo' => $ticketsData[$i]['tipo'],
                    'clienteFolio' => $ticketsData[$i]['clienteFolio'],
                    'clienteNombre' => $ticketsData[$i]['clienteNombre']
                ]);
                if ($ticketsProducts['error'] == '') {
                    foreach ($ticketsProducts['resultado'] AS $ticketProducto) {
                        $this->add_existence($ticketProducto['productoID'], $ticketProducto['cantidad']);
                    }
                }
                if ($ticketsData[$i]['tipo'] == 'credito') {
                    $this->subtract_debt($ticketsData[$i]['clienteID'], $ticketsData[$i]['total']);
                }
                else {
                    $this->cancel_payment($ticketsData[$i]['id']);
                }
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_ticket_product($ticketID, $data, $discount, $date) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'tickets_productos',
                'values' => [
                    ['i', 'ticketID', $ticketID],
                    ['i', 'productoID', $data['productoID']],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'unidad', $data['unidad']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['d', 'precio', $data['precio']],
                    ['d', 'costo', isset($data['costo']) ? $data['costo'] : 0],
                    ['d', 'descuento', $discount],
                    ['d', 'total', $data['total']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_ticket_products($ticketID) {
        $result = ['error' => ''];
        try {
            $ticketProducts = $this->get_ticket_productos($ticketID);
            if ($ticketProducts['error'] != '') {
                $result['error'] = $ticketProducts['error'];
                return $result;
            }
            foreach ($ticketProducts['resultado'] as $ticketProduct) {
                $this->add_existence($ticketProduct['productoID'], $ticketProduct['cantidad']);
            }
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'tickets_productos',
                'wheres' => [
                    ['i', 'ticketID', '=', $ticketID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_client($clientID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'clientes',
                'columns' => ['folio', 'nombre', 'credito', 'deuda'],
                'wheres' => [
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['id', 'folio', 'clienteID', 'subtotal', 'descuento', 'total', 'pago', 'cambio', 'tipo', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                    'tickets_pagos' => ['metodo']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'tickets_pagos' => ['ticketID', 'id']
                ],
                'wheres' => [
                    'tickets' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['productos'] = $this->get_ticket_productos($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_print($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['id', 'folio', 'clienteID', 'subtotal', 'descuento', 'total', 'pago', 'cambio', 'tipo', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre', 'telefono AS clienteTelefono', 'calle AS clienteCalle', 'numeroExterior AS clienteNumeroExterior', 'numeroInterior AS clienteNumeroInterior', 'colonia AS clienteColonia', 'codigoPostal AS clienteCodigoPostal', 'ciudad AS clienteCiudad', 'estado AS clienteEstado'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'tickets' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['productos'] = $this->get_ticket_productos($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_facturar($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['id', 'folio', 'clienteID', 'subtotal', 'descuento', 'total', 'pago', 'cambio', 'tipo', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                    'tickets_pagos' => ['metodo']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'tickets_pagos' => ['ticketID', 'id']
                ],
                'wheres' => [
                    'tickets' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                        ['i', 'id', '=', $data['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $preparedResult[0]['productos'] = $this->get_ticket_productos($preparedResult[0]['id']);
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_ticket_productos($ticketID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets_productos',
                'columns' => [
                    'tickets_productos' => ['id', 'productoID', 'nombre', 'cantidad', 'precio', 'descuento', 'total', 'costo', 'unidad'],
                    'productos' => ['folio AS productoFolio', 'claveSAT', 'impuesto']
                ],
                'leftJoins' => [
                    'productos' => ['id', 'productoID']
                ],
                'wheres' => [
                    'tickets_productos' => [
                        ['i', 'ticketID', '=', $ticketID],
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    
    function add_debt($clientID, $quantity) {
        $result = ['error' => ''];
        $clientData = $this->get_client($clientID)['resultado'];
        try {
            $newDebt = $clientData['deuda'] + $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['d', 'deuda', $newDebt]
                ],
                'wheres' => [
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function pay($data) {
        $result = ['error' => ''];
        $result = $this->add_payment(null, $data['clienteID'], $data['metodo'], $data['cantidad'], $data['fecha']);
        if ($result['error'] == '') {
            $clientData = $this->get_client($data['clienteID'])['resultado'];
            $this->notificacion->create('ticket.payment', [
                'id' => $result['resultado']['id'],
                'metodo' => $data['metodo'],
                'cantidad' => $data['cantidad'],
                'clienteFolio' => $clientData['folio'],
                'clienteNombre' => $clientData['nombre']
            ]);
            if ($data['cancelado'] == 1) {
                $this->cancel_payment($result['resultado']['id']);
                $this->notificacion->create('ticket.paymentcancel', [
                    'id' => $result['resultado']['id'],
                    'metodo' => $data['metodo'],
                    'cantidad' => $data['cantidad'],
                    'clienteFolio' => $clientData['folio'],
                    'clienteNombre' => $clientData['nombre']
                ]);
            }
            else {
                $this->subtract_debt($data['clienteID'], $data['cantidad'])['error'];
            }
        }
        return $result;
    }

    function pay_cancel($data) {
        $result = ['error' => ''];
        $payment = $this->get_payment($data['id'])['resultado'];
        $result = $this->cancel_payment($data['id']);
        if ($result['error'] == '') {
            $this->add_debt($payment['clienteID'], $payment['cantidad']);
            $this->notificacion->create('ticket.paymentcancel', [
                'id' => $data['id'],
                'metodo' => $data['metodo'],
                'cantidad' => $payment['cantidad'],
                'clienteFolio' => $payment['clienteFolio'],
                'clienteNombre' => $payment['clienteNombre']
            ]);
        }
        return $result;
    }

    function subtract_debt($clientID, $quantity) {
        $result = ['error' => ''];
        $clientData = $this->get_client($clientID)['resultado'];
        try {
            $newDebt = $clientData['deuda'] - $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'clientes',
                'values' => [
                    ['d', 'deuda', max($newDebt, 0)]
                ],
                'wheres' => [
                    ['i', 'id', '=', $clientID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    
    function subtract_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $productData = $this->get_existence($productID)['resultado'];
            $newExistence = $productData['existencia'] - $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $newExistence]
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($productData['existencia'] >= $productData['inventarioMinimo'] && $newExistence < $productData['inventarioMinimo']) {
                $this->notificacion->create('producto.low', [
                    'id' => $productID,
                    'folio' => $productData['folio'],
                    'nombre' => $productData['nombre']
                ]);
            }
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add_payment($ticketID, $clientID, $method, $quantity, $date) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'tickets_pagos',
                'values' => [
                    ['i', 'ticketID', $ticketID],
                    ['i', 'clienteID', $clientID],
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['d', 'cantidad', $quantity],
                    ['s', 'metodo', $method],
                    ['i', 'cancelado', 0],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']],
                    ['s', 'fechaCreate', date('Y-m-d G:i:s', $date)]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel_payment($ticketID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'tickets_pagos',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'ticketID', '=', $ticketID],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_payments($ticketID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'tickets_pagos',
                'wheres' => [
                    ['i', 'ticketID', '=', $ticketID],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    
    function get_ticket_payment($ticketID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets_pagos',
                'columns' => [
                    'tickets_pagos' => ['cantidad', 'clienteID', 'metodo'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID']
                ],
                'wheres' => [
                    'tickets_pagos' => [
                        ['i', 'ticketID', '=', $ticketID]
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }
    
    function get_payment($paymentID) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets_pagos',
                'columns' => [
                    'tickets_pagos' => ['cantidad', 'clienteID', 'metodo'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID']
                ],
                'wheres' => [
                    'tickets_pagos' => [
                        ['i', 'id', '=', $paymentID]
                    ]
                ]
            ]);
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }
        return $result;
    }

    function add_existence($productID, $quantity) {
        $result = ['error' => ''];
        try {
            $productData = $this->get_existence($productID)['resultado'];
            $newExistence = $productData['existencia'] + $quantity;
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'productos',
                'values' => [
                    ['d', 'existencia', $newExistence]
                ],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_existence($productID) {
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'productos',
                'columns' => ['folio', 'existencia', 'inventarioMinimo', 'nombre'],
                'wheres' => [
                    ['i', 'id', '=', $productID]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        $wheres['tickets'][] = ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']];
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['fecha'])) {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['tickets'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
            if ($GLOBALS['usuario']['id'] != 1 && !in_array('reportes', $GLOBALS['usuario']['privilegios']) && $GLOBALS['usuario']['privilegiosNivel'] < 2 && !isset($data['filtros']['repartidorID'])) {
                $wheres['tickets'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
            }
            else {
                if (!empty($data['filtros']['usuario'])) {
                    $wheres['tickets'][] = ['i', 'usuarioID', '=', $data['filtros']['usuario']];
                }
            }
            if (!empty($data['filtros']['tipo'])) {
                $wheres['tickets'][] = ['s', 'tipo', '=', $data['filtros']['tipo']];
            }
            if (!empty($data['filtros']['cliente'])) {
                $wheres['tickets'][] = ['i', 'clienteID', '=', $data['filtros']['cliente']];
            }
            if (isset($data['filtros']['entrega']) && $data['filtros']['entrega'] != -1) {
                    $wheres['tickets'][] = ['i', 'domicilio', '=', $data['filtros']['entrega']];
            }
            if (!empty($data['filtros']['repartidorID'])) {
                $wheres['tickets'][] = ['i', 'domicilio', '=', '1'];
                if ($data['filtros']['repartidorID'] == -1) {
                    $wheres['tickets'][] = ['i', 'repartidorID', 'IS', NULL];
                }
                else if ($data['filtros']['repartidorID'] > 0) {
                    $wheres['tickets'][] = ['i', 'repartidorID', '=', $data['filtros']['repartidorID']];
                }
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['id', 'folio', 'folio', 'subtotal', 'descuento', 'total', 'tipo', 'domicilio', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre'],
                    'usuarios' => ['nombre AS usuarioNombre'],
                    'repartidor' => ['COALESCE(*.nombre, "S/D") AS repartidorNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID'],
                    'usuarios repartidor' => ['id', 'repartidorID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_facturar($data) {
        $result = ['error' => ''];
        $wheres = [
            ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ['i', 'cancelado', '=', '0']
        ];
        if (strpos($data['fecha'], '-')) {
            $dateArray = explode(' - ', $data['fecha']);
            $startDate = explode('/', $dateArray[0]);
            $endDate = explode('/', $dateArray[1]);
            $wheres[] = ['s', 'DATE(fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
            $wheres[] = ['s', 'DATE(fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
        }
        else {
            $date = explode('/', $data['fecha']);
            switch (count($date)) {
                case '1':
                    $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[0]];
                    break;
                case '2':
                    $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[0]];
                    $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[1]];
                    break;
                case '3':
                    $wheres[] = ['s', 'DAY(fechaCreate)', '=', $date[0]];
                    $wheres[] = ['s', 'MONTH(fechaCreate)', '=', $date[1]];
                    $wheres[] = ['s', 'YEAR(fechaCreate)', '=', $date[2]];
                    break;
            }
        }
        if (isset($data['clienteID'])) {
            $wheres[] = ['i', 'clienteID', '=', $data['clienteID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => ['id', 'folio', 'fechaCreate'],
                'wheres' => $wheres,
                'orders' => [
                    ['folio', 'ASC']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['productos'] = $this->get_ticket_productos($preparedResult[$i]['id']);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function set_delivery($data) {
        $result = ['error' => ''];
        $ticketsData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $ticketsData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        $deliveryData = $this->get_user(['id' => $data['repartidorID']])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'tickets',
                'values' => [
                    ['i', 'repartidorID', $data['repartidorID']],
                ],
                'wheres' => [
                    ['i', 'id', 'IN', $data['ids']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($ticketsData); $i++) {
                $this->notificacion->create('ticket.delivery', [
                    'id' => $data['ids'][$i],
                    'folio' => $ticketsData[$i]['folio'],
                    'clienteID' => $ticketsData[$i]['clienteID'],
                    'clienteFolio' => $ticketsData[$i]['clienteFolio'],
                    'clienteNombre' => $ticketsData[$i]['clienteNombre'],
                    'repartidorID' => $deliveryData['id'],
                    'repartidorNombre' => $deliveryData['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_news($data) {
        $result = ['error' => ''];
        $wheres = $this->get_filter_search($data['busqueda']);
        if (isset($data['filtros']['fecha'])) {
            $date = explode('/', $data['filtros']['fecha']);
            switch (count($date)) {
                case '1':
                    $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                    break;
                case '2':
                    $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                    $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                    break;
                case '3':
                    $wheres['tickets'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                    $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                    $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                    break;
            }
        }
        if ($GLOBALS['usuario']['id'] != 1 && $GLOBALS['usuario']['privilegiosNivel'] < 2) {
            $wheres['tickets'][] = ['i', 'usuarioID', '=', $GLOBALS['usuario']['id']];
        }
        if ($data['ultimoID'] > 0) {
            $wheres['tickets'][] = ['i', 'id', '>', $data['ultimoID']];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['MAX(*.id) AS idMax', 'COUNT(id) AS nuevos'],
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres
            ]);
            if ($preparedResult[0]['nuevos'] == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        } catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_search($search) {
        $where = [
            'tickets' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ]
        ];
        if ($search != '') {
            $search = '%' . $search . '%';
            $where['GROUP_SEARCH'] = [
                'tickets' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'clientes' => [
                    ['s', 'nombre', 'LIKE', $search, 'OR']
                ],
                'usuarios' => [
                    ['s', 'nombre', 'LIKE', $search]
                ]
            ];

        }
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'tickets' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'tickets' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function get_user($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'usuarios',
                'columns' => ['id', 'nombre'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['s', 'CONCAT(secciones, ",")', 'LIKE', '%' . $GLOBALS['usuario']['seccionActual']['id'] . ',%']
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }
    

    function get_all_desktop($data) {
        $result = ['error' => ''];
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        $wheres = [];
        $wheres['tickets'][] = ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']];
        if (isset($data['filtros'])) {
            if (isset($data['filtros']['fecha'])) {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['tickets'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['tickets'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['tickets'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'tickets',
                'columns' => [
                    'tickets' => ['id', 'folio', 'clienteID', 'usuarioID', 'pago', 'cambio', 'domicilio', 'subtotal', 'descuento', 'total', 'tipo', 'cancelado', 'fechaCreate'],
                    'clientes' => ['folio AS clienteFolio', 'nombre AS clienteNombre', 'calle', 'numeroExterior', 'numeroInterior', 'colonia', 'codigoPostal', 'ciudad', 'estado'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'clientes' => ['id', 'clienteID'],
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['clienteDireccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
                $preparedResult[$i]['productos'] = $this->get_ticket_productos($preparedResult[$i]['id']);
                $ticketPayment = $this->get_ticket_payment($preparedResult[$i]['id']);
                $method = '';
                if ($ticketPayment['error'] == '') {
                    $method = $ticketPayment['resultado']['metodo'];
                }
                $preparedResult[$i]['metodo'] = $method;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complete_address($data) {
        $street = [];
        if ($data['calle'] != '') { $street[] = $data['calle']; }
        if ($data['numeroExterior'] != '') { $street[] = '#' . $data['numeroExterior']; }
        if ($data['numeroInterior'] != '') { $street[] = 'Int. ' . $data['numeroInterior']; }
        $street = implode(' ', $street);
        $address = [];
        if ($street != '') { $address[] = $street; }
        if ($data['colonia'] != '') { $address[] = $data['colonia']; }
        if ($data['codigoPostal'] != '') {$address[] = $data['codigoPostal']; }
        if ($data['ciudad'] != '') { $address[] = $data['ciudad']; }
        if ($data['estado'] != '') { $address[] = $data['estado']; }
        $address = implode(', ', $address);
        return $address;
    }

}

?>