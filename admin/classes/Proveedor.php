<?php

include_once('classes/Notificacion.php');

class Proveedor {

    function __construct() {
        $this->notificacion = new Notificacion();
        
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'proveedores',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 2 : $folioResult['resultado']['folio'] + 1;
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'proveedores',
                    'values' => [
                        ['i', 'folio', $folio],
                        ['s', 'nombre', $data['nombre']],
                        ['s', 'telefono', $data['telefono']],
                        ['s', 'email', $data['email']],
                        ['s', 'calle', $data['calle']],
                        ['s', 'colonia', $data['colonia']],
                        ['s', 'ciudad', $data['ciudad']],
                        ['s', 'estado', $data['estado']],
                        ['s', 'codigoPostal', $data['codigoPostal']],
                        ['s', 'numeroExterior', $data['numeroExterior']],
                        ['s', 'numeroInterior', $data['numeroInterior']],
                        ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                $result['resultado']['folio'] = $folio;
                $this->notificacion->create('proveedor.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'nombre' => $data['nombre']
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $providerData = $this->get(['id' => $data['id']])['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'proveedores',
                'values' => [
                    ['s', 'nombre', $data['nombre']],
                    ['s', 'telefono', $data['telefono']],
                    ['s', 'email', $data['email']],
                    ['s', 'calle', $data['calle']],
                    ['s', 'colonia', $data['colonia']],
                    ['s', 'ciudad', $data['ciudad']],
                    ['s', 'estado', $data['estado']],
                    ['s', 'codigoPostal', $data['codigoPostal']],
                    ['s', 'numeroExterior', $data['numeroExterior']],
                    ['s', 'numeroInterior', $data['numeroInterior']],
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('proveedor.edit', [
                'id' => $data['id'],
                'folio' => $providerData['folio'],
                'nombre' => $data['nombre']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete($data) {
        $result = ['error' => ''];
        $ids = implode(',', $data['ids']);
        $providersData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $providersData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'proveedores',
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('proveedor.delete', [
                    'id' => $providersData[$i]['id'],
                    'folio' => $providersData[$i]['folio'],
                    'nombre' => $providersData[$i]['nombre']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'proveedores',
                'columns' => ['id', 'folio', 'nombre', 'telefono', 'email', 'calle', 'colonia', 'ciudad', 'estado', 'codigoPostal', 'numeroExterior', 'numeroInterior'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all() {
        $result = ['error' => ''];
        $orders = $this->get_order($_POST['metodo'], $_POST['orden']);
        $where = $this->get_where($_POST['busqueda']);
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'proveedores',
                'columns' => ['id', 'folio', 'nombre', 'telefono', 'email', 'calle', 'colonia', 'ciudad', 'estado', 'codigoPostal', 'numeroExterior', 'numeroInterior'],
                'orders' => $orders,
                'wheres' => $where
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            for ($i=0; $i<count($preparedResult); $i++) {
                $preparedResult[$i]['direccionCompleta'] = $this->get_complete_address($preparedResult[$i]);
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_complete_address($data) {
        $street = [];
        if ($data['calle'] != '') { $street[] = $data['calle']; }
        if ($data['numeroExterior'] != '') { $street[] = '#' . $data['numeroExterior']; }
        if ($data['numeroInterior'] != '') { $street[] = 'Int. ' . $data['numeroInterior']; }
        $street = implode(' ', $street);
        $address = [];
        if ($street != '') { $address[] = $street; }
        if ($data['colonia'] != '') { $address[] = $data['colonia']; }
        if ($data['codigoPostal'] != '') {$address[] = $data['codigoPostal']; }
        if ($data['ciudad'] != '') { $address[] = $data['ciudad']; }
        if ($data['estado'] != '') { $address[] = $data['estado']; }
        $address = implode(', ', $address);
        return $address;
    }

    function get_where($search) {
        $search = '%' . $search . '%';
        $where = [
            'GROUP_1' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id'], 'OR'],
                ['i', 'id', '=', 1],
            ],
            'GROUP_2' => [
                ['s', 'LPAD(folio, 4, "0")', 'LIKE', $search, 'OR'],
                ['s', 'nombre', 'LIKE', $search, 'OR'],
                ['s', 'telefono', 'LIKE', $search, 'OR'],
                ['s', 'email', 'LIKE', $search, 'OR'],
                ['s', 'calle', 'LIKE', $search, 'OR'],
                ['s', 'CONCAT("#", numeroExterior)', 'LIKE', $search, 'OR'],
                ['s', 'CONCAT("Int. ", numeroInterior)', 'LIKE', $search, 'OR'],
                ['s', 'colonia', 'LIKE', $search, 'OR'],
                ['s', 'ciudad', 'LIKE', $search, 'OR'],
                ['s', 'estado', 'LIKE', $search, 'OR']
            ]
        ];
        return $where;
    }

    function get_order($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    ['folio', $orderValue]
                ];
                break;
            case 'nombre':
                $orderArray = [
                    ['nombre', $orderValue]
                ];
                break;
            case 'direccion':
                $orderArray = [
                    ['calle', $orderValue],
                    ['numeroInterior', $orderValue],
                    ['numeroExterior', $orderValue],
                    ['colonia', $orderValue],
                    ['ciudad', $orderValue],
                    ['estado', $orderValue],
                    ['codigoPostal', $orderValue]
                ];
                break;
        }
        return $orderArray;
    }

}

?>