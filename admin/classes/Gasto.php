<?php

include_once(__DIR__ . '/Notificacion.php');

class Gasto {

    function __construct() {
        $this->notificacion = new Notificacion();        
    }

    function get_last_folio() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos',
                'columns' => ['MAX(folio) AS folio'],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]                
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function add($data) {
        $result = ['error' => ''];
        
        $values = [
            ['i', 'usuarioID', $GLOBALS['usuario']['id']],
            ['s', 'razon', $data['razon']],
            ['s', 'comentario', $data['comentario']],
            ['d', 'cantidad', $data['cantidad']],
            ['i', 'cancelado', 0],
            ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
        ];

        if (isset($data['fecha'])) {
            $values[] = ['s', 'fechaCreate', implode('-', array_reverse(explode('/', $data['fecha'])))];
        }

        $folioResult = $this->get_last_folio();
        if ($folioResult['error'] == '' || $folioResult['error'] == 'EMPTY') {
            $folio = $folioResult['error'] == 'EMPTY' ? 1 : $folioResult['resultado']['folio'] + 1;
            $values[] = ['i', 'folio', $folio];
            try {
                $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                    'table' => 'gastos',
                    'values' => $values
                ]);
                $result['resultado']['id'] = $preparedResult['id'];
                $this->notificacion->create('gasto.add', [
                    'id' => $result['resultado']['id'],
                    'folio' => $folio,
                    'razon' => $data['razon'],
                    'cantidad' => $data['cantidad']
                ]);
            }
            catch (Exception $err) {
                $result['error'] = $err->getMessage();
            }
        }
        else {
            $result['error'] = $folioResult['error'];
        }
        return $result;
    }

    function edit($data) {
        $result = ['error' => ''];
        $gastoResult = $this->get(['id' => $data['id']]);
        if ($gastoResult['error'] != '') {
            $result['error'] = $gastoResult['error'];
        }
        $gasto = $gastoResult['resultado'];

        $values = [
            ['s', 'razon', $data['razon']],
            ['s', 'comentario', $data['comentario']],
            ['d', 'cantidad', $data['cantidad']]
        ];

        if (isset($data['fecha'])) {
            $values[] = ['s', 'fechaCreate', implode('-', array_reverse(explode('/', $data['fecha'])))];
        }

        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'gastos',
                'values' => $values,
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $this->notificacion->create('gasto.edit', [
                'id' => $data['id'],
                'folio' => $gasto['folio'],
                'razon' => $data['razon'],
                'cantidad' => $data['cantidad']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit_cantidad($data) {
        $result = ['error' => ''];
        $gastoResult = $this->get(['id' => $data['id']]);
        if ($gastoResult['error'] != '') {
            $result['error'] = $gastoResult['error'];
        }
        $gasto = $gastoResult['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'gastos',
                'values' => [
                    ['d', 'cantidad', $data['cantidad']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']],
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $this->notificacion->create('gasto.editcantidad', [
                'id' => $data['id'],
                'folio' => $gasto['folio'],
                'razon' => $gasto['razon'],
                'cantidad' => $data['cantidad']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos',
                'columns' => ['folio', 'usuarioID', 'razon', 'comentario', 'cantidad', 'cancelado'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function cancel($data) {
        $result = ['error' => ''];
        $gastosData = [];
        for ($i=0; $i<count($data['ids']); $i++) {
            $gastosData[] = $this->get(['id' => $data['ids'][$i]])['resultado'];
        }
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'gastos',
                'values' => [
                    ['i', 'cancelado', 1]
                ],
                'wheres' => [
                    ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
                    ['i', 'cancelado', '=', '0'],
                    ['i', 'id', 'IN', $data['ids']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            for ($i=0; $i<count($data['ids']); $i++) {
                $this->notificacion->create('gasto.cancel', [
                    'id' => $data['ids'][$i],
                    'folio' => $gastosData[$i]['folio'],
                    'razon' => $gastosData[$i]['razon'],
                    'cantidad' => $gastosData[$i]['cantidad']
                ]);
            }
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all($data) {
        $result = ['error' => ''];
        $wheres = [];
        if (isset($data['busqueda'])) {
            $wheres = $this->get_filter_search($data['busqueda']);
        }
        if (isset($data['filtros']['fecha'])) {
            if (strpos($data['filtros']['fecha'], '-')) {
                $dateArray = explode(' - ', $data['filtros']['fecha']);
                $startDate = explode('/', $dateArray[0]);
                $endDate = explode('/', $dateArray[1]);
                $wheres['gastos'][] = ['s', 'DATE(*.fechaCreate)', '>=', $startDate[2] . '-' . $startDate[1] . '-' . $startDate[0]];
                $wheres['gastos'][] = ['s', 'DATE(*.fechaCreate)', '<=', $endDate[2] . '-' . $endDate[1] . '-' . $endDate[0]];
            }
            else {
                $date = explode('/', $data['filtros']['fecha']);
                switch (count($date)) {
                    case '1':
                        $wheres['gastos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[0]];
                        break;
                    case '2':
                        $wheres['gastos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[0]];
                        $wheres['gastos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[1]];
                        break;
                    case '3':
                        $wheres['gastos'][] = ['s', 'DAY(*.fechaCreate)', '=', $date[0]];
                        $wheres['gastos'][] = ['s', 'MONTH(*.fechaCreate)', '=', $date[1]];
                        $wheres['gastos'][] = ['s', 'YEAR(*.fechaCreate)', '=', $date[2]];
                        break;
                }
            }
        }
        $orders = $this->get_filter_orders($data['metodo'], $data['orden']);
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos',
                'columns' => [
                    'gastos' => ['id', 'folio', 'razon', 'comentario', 'cantidad', 'cancelado', 'fechaCreate'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => $wheres,
                'orders' => $orders
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_filter_search($search) {
        $search = '%' . $search . '%';
        $where = [
            'gastos' => [
                ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']],
            ],
            'GROUP' => [
                'gastos' => [
                    ['s', 'LPAD(*.folio, 4, "0")', 'LIKE', $search, 'OR']
                ],
                'gastos' => [
                    ['s', 'razon', 'LIKE', $search]
                ]
            ]
        ];
        return $where;
    }

    function get_filter_orders($method, $order) {
        $orderArray = [];
        $orderValue = 'DESC';
        switch ($order) {
            case 'asc':
                $orderValue = 'ASC';
                break;
            case 'desc':
                $orderValue = 'DESC';
                break;
        }
        switch ($method) {
            case 'folio':
                $orderArray = [
                    'gastos' => [
                        ['folio', $orderValue]
                    ]
                ];
                break;
            case 'fecha':
                $orderArray = [
                    'gastos' => [
                        ['fechaCreate', $orderValue]
                    ]
                ];
                break;
            case 'cantidad':
                $orderArray = [
                    'gastos' => [
                        ['cantidad', $orderValue]
                    ]
                ];
                break;
        }
        return $orderArray;
    }

    function add_gastoprogramado($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareInsertInto([
                'table' => 'gastos_programados',
                'values' => [
                    ['i', 'usuarioID', $GLOBALS['usuario']['id']],
                    ['s', 'razon', $data['razon']],
                    ['s', 'comentario', $data['comentario']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['s', 'programacion', $data['programacion']],
                    ['i', 'seccionID', $GLOBALS['usuario']['seccionActual']['id']]
                ]
            ]);
            $result['resultado']['id'] = $preparedResult['id'];
            $this->notificacion->create('gastoprogramado.add', [
                'id' => $result['resultado']['id'],
                'razon' => $data['razon'],
                'cantidad' => $data['cantidad'],
                'programacion' => $data['programacion']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function edit_gastoprogramado($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareUpdate([
                'table' => 'gastos_programados',
                'values' => [
                    ['s', 'razon', $data['razon']],
                    ['s', 'comentario', $data['comentario']],
                    ['d', 'cantidad', $data['cantidad']],
                    ['s', 'programacion', $data['programacion']]
                ],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            $result['resultado']['id'] = $data['id'];
            $this->notificacion->create('gastoprogramado.edit', [
                'id' => $result['resultado']['id'],
                'razon' => $data['razon'],
                'cantidad' => $data['cantidad'],
                'programacion' => $data['programacion']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_gastoprogramado($data) {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos_programados',
                'columns' => ['usuarioID', 'razon', 'comentario', 'cantidad', 'programacion'],
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult[0];
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function delete_gastoprogramado($data) {
        $result = ['error' => ''];
        $gastoResult = $this->get_gastoprogramado(['id' => $data['id']]);
        if ($gastoResult['error'] != '') {
            $result['error'] = 'GASTOPROGRAMADO_GET:' . $gastoResult['error'];
            return $result;
        }
        $gastoData = $gastoResult['resultado'];
        try {
            $preparedResult = $GLOBALS['DB']->prepareDelete([
                'table' => 'gastos_programados',
                'wheres' => [
                    ['i', 'id', '=', $data['id']]
                ]
            ]);
            if ($preparedResult['affected_rows'] == -1) {
                $result['error'] = 'NOT_FOUND';
                return $result;
            }
            $this->notificacion->create('gastoprogramado.eliminado', [
                'id' => $data['id'],
                'razon' => $gastoData['razon'],
                'cantidad' => $gastoData['cantidad'],
                'programacion' => $gastoData['programacion']
            ]);
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_gastosprogramados() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'gastos_programados',
                'columns' => [
                    'gastos_programados' => ['id', 'razon', 'comentario', 'cantidad', 'programacion', 'usuarioID'],
                    'usuarios' => ['nombre AS usuarioNombre']
                ],
                'leftJoins' => [
                    'usuarios' => ['id', 'usuarioID']
                ],
                'wheres' => [
                    'gastos_programados' => [
                        ['i', 'seccionID', '=', $GLOBALS['usuario']['seccionActual']['id']]
                    ]
                ],
                'orders' => [
                    'gastos_programados' => [
                        ['id', 'ASC']
                    ]
                ]
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function get_all_sections() {
        $result = ['error' => ''];
        try {
            $preparedResult = $GLOBALS['DB']->prepareSelectFrom([
                'table' => 'secciones',
                'columns' => ['id', 'nombre']
            ]);
            if (count($preparedResult) == 0) {
                $result['error'] = 'EMPTY';
                return $result;
            }
            $result['resultado'] = $preparedResult;
        }
        catch (Exception $err) {
            $result['error'] = $err->getMessage();
        }
        return $result;
    }

    function process_programados() {
        $seccionesResult = $this->get_all_sections();
        if ($seccionesResult['error'] != '') {
            error_log($seccionesResult['error']);
            return;
        }
        $secciones = $seccionesResult['resultado'];
        foreach ($secciones AS $seccion) {
            $GLOBALS['usuario']['seccionActual'] = $seccion;
            $gastosProgramadosResult = $this->get_all_gastosprogramados();
            if ($gastosProgramadosResult['error'] == '') {
                $gastosProgramados = $gastosProgramadosResult['resultado'];
                foreach ($gastosProgramados AS $gastoProgramado) {
                    $GLOBALS['usuario']['id'] = $gastoProgramado['usuarioID'];
                    $canAdd = false;
                    switch ($gastoProgramado['programacion']) {
                        case 'mensual':
                            if (intval(date('j')) == 1) {
                                $canAdd = true;
                            }
                            break;
                        case 'quincenal':
                            if (intval(date('j')) == 15 || intval(date('j')) == intval(date('t'))) {
                                $canAdd = true;
                            }
                            break;
                        case 'finesdesemana':
                            if (intval(date('N')) == 6 || intval(date('N')) == 7) {
                                $canAdd = true;
                            }
                            break;
                        case 'entresemana':
                            if (intval(date('N')) >= 0 && intval(date('N')) <= 5) {
                                $canAdd = true;
                            }
                            break;
                        case 'lunes':
                            if (intval(date('N')) == 1) {
                                $canAdd = true;
                            }
                            break;
                        case 'martes':
                            if (intval(date('N')) == 2) {
                                $canAdd = true;
                            }
                            break;
                        case 'miercoles':
                            if (intval(date('N')) == 3) {
                                $canAdd = true;
                            }
                            break;
                        case 'jueves':
                            if (intval(date('N')) == 4) {
                                $canAdd = true;
                            }
                            break;
                        case 'viernes':
                            if (intval(date('N')) == 5) {
                                $canAdd = true;
                            }
                            break;
                        case 'sabado':
                            if (intval(date('N')) == 6) {
                                $canAdd = true;
                            }
                            break;
                        case 'domingo':
                            if (intval(date('N')) == 7) {
                                $canAdd = true;
                            }
                            break;
                        case 'diario':
                            $canAdd = true;
                            break;
                    }
                    if ($canAdd) {
                        $this->add([
                            'razon' => $gastoProgramado['razon'],
                            'comentario' => $gastoProgramado['comentario'],
                            'cantidad' => $gastoProgramado['cantidad']
                        ]);
                    }
                }
            }
            else {
                error_log($gastosProgramadosResult['error']);
            }
        }
    }

}

?>