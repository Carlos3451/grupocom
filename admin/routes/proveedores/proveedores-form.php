<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proveedores - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/proveedores.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.fullscreen.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/prettycheckbox.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/proveedores/proveedores-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/proveedores.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Proveedor</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre *</p>
                <input class="inp-required" type="text" name="nombre">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Teléfono / Celular</p>
                <input type="text" name="telefono">
            </div>
            <div class="input-container">
                <p>Email</p>
                <input type="text" name="email">
            </div>
        </div>
        <br>
        <h3>Dirección</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Calle</p>
                <input type="text" name="calle">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Numero Exterior</p>
                <input type="text" name="numeroExterior">
            </div>
            <div class="input-container">
                <p>Numero Interior</p>
                <input type="text" name="numeroInterior">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Colonia</p>
                <input type="text" name="colonia">
            </div>
            <div class="input-container">
                <p>Código Postal</p>
                <input type="text" name="codigoPostal">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Ciudad</p>
                <input type="text" name="ciudad">
            </div>
            <div class="input-container">
                <p>Estado</p>
                <input type="text" name="estado">
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/PrettyCheckbox.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/proveedores/proveedores-form.js"></script>
</body>
</html>
