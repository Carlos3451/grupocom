<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ventas Mostrador - Viendo</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/ventas.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/ventas/ventas-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/ventas.png">
        <div class="title-text">
            <p>VIENDO</p>
            <h3>Ventas Mostrador</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cliente</p>
                <input class="inp-icon inp-user inp-required" type="text" name="clienteID" disabled transparent>
            </div>
        </div>
        <br>
        <h3>Venta</h3>
        <div class="ticket-table-overflow">
            <div class="ticket-table">
                <div class="ticket-table-header">
                    <div class="ticket-table-column">
                        <p>Producto</p>
                    </div>
                    <div class="ticket-table-column">
                        <p>Cant</p>
                    </div>
                    <div class="ticket-table-column">
                        <p>Precio</p>
                    </div>
                    <div class="ticket-table-column">
                        <p>Total</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="accounting-container">
            <div class="totals-container">
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Subtotal</p>
                        <input class="inp-icon inp-price text-align-right" name="subtotal" type="text" disabled transparent>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Descuento</p>
                        <input class="inp-icon inp-percent text-align-right" name="descuento" type="text" disabled transparent>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Total</p>
                        <input class="inp-icon inp-price text-align-right" name="total" type="text" disabled transparent>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Metodo de Pago</p>
                        <input class="text-align-right" name="metodo" type="text" disabled transparent>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Pago con</p>
                        <input class="inp-icon inp-price text-align-right" name="pago" type="text" disabled transparent>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Cambio</p>
                        <input class="inp-icon inp-price text-align-right" name="cambio" type="text" disabled transparent>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <button class="form-cancel" type="button">Volver</button>
        </div>
    </form>
    <script>
        var formMode = 'VIEW';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
        var privileges = '<?php echo implode(',', $GLOBALS['usuario']['privilegios']); ?>'.split(',');
        var privilegesLevel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/ventas/ventas-form.js"></script>
</body>
</html>
