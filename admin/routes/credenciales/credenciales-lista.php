<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Credenciales - Lista</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/credenciales.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/list.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/sortcontroller.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/credenciales/credenciales-lista.css">
</head>
<body>
    <div class="changepassword-overlay">
        <div class="changepassword-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Usuario</p>
                    <input class="inp-required" type="text" name="usuario">
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Nueva Contraseña</p>
                    <input class="inp-required" type="password" togglePassword="hidden" name="contraseña">
                </div>
                <div class="input-container">
                    <p>Confirmar Contraseña</p>
                    <input class="inp-required" type="password" togglePassword="hidden" name="confirmarContraseña">
                </div>
            </div>
            <div class="actions-container">
                <button class="changepassword-confirm">Confirmar</button>
                <button class="changepassword-cancel">Cancelar</button>
            </div>
        </div>
    </div>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/sortcontroller.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/credenciales.png">
        <h3>Credenciales</h3>
        <div class="actions-container">
            <a href="credenciales/agregar" tooltip-hint="Agregar Credencial"><img src="/admin/imgs/icons/actions/add.png"></a>
        </div>
    </div>
    <?php include_once('templates/listactions.php'); ?>
    <div class="list-overflow">
        <div class="list-container">
            <div class="list-filler">
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
            </div>
            <div class="list-header">
                <div class="list-column"></div>
                <div class="list-column"><p>Folio</p></div>
                <div class="list-column"><p>Nombre</p></div>
                <div class="list-column"><p>Privilegios</p></div>
                <div class="list-column"><p>Nivel</p></div>
            </div>
        </div>
    </div>
    <script>
        privilegios = JSON.parse('<?php echo json_encode($GLOBALS['usuario']['privilegios']); ?>');
        privilegiosNivel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SortController.js"></script>
    <script src="/admin/js/plugins/personal/TogglePassword.js"></script>
    <script src="/admin/js/global/Lista.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/credenciales/credenciales-lista.js"></script>
</body>
</html>