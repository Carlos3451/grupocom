<?php
    $_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 0;
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Credenciales - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/credenciales.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/prettycheckbox.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/credenciales/credenciales-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/credenciales.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Credencial</h3>
        </div>
    </div>
    <form id="credencial-form">
        <h3>Fotografía</h3>
        <div class="image-container">
            <img src="/admin/imgs/icons/camara.png">
            <input class="hidden" type="file" name="imagen" accept=".png, .jpg, .jpeg, .bmp, .gif">
        </div>
        <br>
        <h3>Contacto</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre *</p>
                <input class="inp-required" type="text" name="nombre">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Teléfono / Celular</p>
                <input type="text" name="telefono">
            </div>
            <div class="input-container">
                <p>Email</p>
                <input type="text" name="email">
            </div>
        </div>
        <br>
        <h3>Dirección</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Calle</p>
                <input type="text" name="calle">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Numero Exterior</p>
                <input type="text" name="numeroExterior">
            </div>
            <div class="input-container">
                <p>Numero Interior</p>
                <input type="text" name="numeroInterior">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Colonia</p>
                <input type="text" name="colonia">
            </div>
            <div class="input-container">
                <p>Código Postal</p>
                <input type="text" name="codigoPostal">
            </div>
        </div>
        <br>
        <?php if ($_GET['mode'] == 'ADD') { ?>
        <h3>Credenciales</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Usuario *</p>
                <input class="inp-required" type="text" name="usuario">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Contraseña *</p>
                <input class="inp-required" type="password" togglePassword="hidden" name="contraseña">
            </div>
            <div class="input-container">
                <p>Confirmar Contraseña *</p>
                <input class="inp-required" type="password" togglePassword="hidden" name="confirmarContraseña">
            </div>
        </div>
        <br>
        <?php } ?>
        <h3 <?php if ($_GET['mode'] == 'EDIT' && $_GET['id'] == 1) { echo 'style="display:none;"'; } ?>>Privilegios</h3>
        <div class="privileges-containers" <?php if ($_GET['mode'] == 'EDIT' && $_GET['id'] == 1) { echo 'style="display:none;"'; } ?>>
            <div class="privilege-container" value="credenciales" tooltip-hint="Administrar credenciales">
                <img class="privilege-image" src="/admin/imgs/icons/credenciales.png">
            </div>
            <div class="privilege-container" value="clientes" tooltip-hint="Administrar clientes">
                <img class="privilege-image" src="/admin/imgs/icons/clientes.png">
            </div>
            <div class="privilege-container" value="proveedores" tooltip-hint="Administrar proveedores">
                <img class="privilege-image" src="/admin/imgs/icons/proveedores.png">
            </div>
            <div class="privilege-container" value="rutas" tooltip-hint="Administrar rutas">
                <img class="privilege-image" src="/admin/imgs/icons/rutas.png">
            </div>
            <div class="privilege-container" value="productos" tooltip-hint="Administrar productos">
                <img class="privilege-image" src="/admin/imgs/icons/productos.png">
            </div>
            <div class="privilege-container" value="pedidos" tooltip-hint="Administrar pedidos">
                <img class="privilege-image" src="/admin/imgs/icons/pedidos.png">
            </div>
            <div class="privilege-container" value="compras" tooltip-hint="Administrar compras">
                <img class="privilege-image" src="/admin/imgs/icons/compras.png">
            </div>
            <div class="privilege-container" value="gastos" tooltip-hint="Administrar gastos">
                <img class="privilege-image" src="/admin/imgs/icons/gastos.png">
            </div>
            <div class="privilege-container" value="reportes" tooltip-hint="Reportes">
                <img class="privilege-image" src="/admin/imgs/icons/reportes.png">
            </div>
            <?php if ($GLOBALS['usuario']['seccionActual']['facturacion'] == 1) { ?>
            <div class="privilege-container" value="facturacion" tooltip-hint="Facturación">
                <img class="privilege-image" src="/admin/imgs/icons/facturas.png">
            </div>
            <?php } ?>
            <input class="hidden" type="text" name="privilegios">
        </div>
        <div class="inputs-containers" <?php if ($_GET['mode'] == 'EDIT' && $_GET['id'] == 1) { echo 'style="display:none;"'; } ?>>
            <div class="input-container">
                <p>Nivel de privilegios</p>
                <select name="privilegiosNivel">
                    <option value="1">Crear</option>
                    <?php if ($GLOBALS['usuario']['privilegiosNivel'] > 1) { ?>
                    <option value="2">Crear, Editar</option>
                    <?php } ?>
                    <?php if ($GLOBALS['usuario']['privilegiosNivel'] > 2) { ?>
                    <option value="3">Crear, Editar, Eliminar</option>
                    <?php } ?>
                    <?php if ($GLOBALS['usuario']['privilegiosNivel'] > 3) { ?>
                    <option value="4">Superusuario</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <br>
        <?php if ($_GET['id'] != 1) { ?>
        <h3 <?php if (count($GLOBALS['usuario']['secciones']) == 1) { echo 'class="hidden"'; } ?>>Secciones</h3>
        <div class="sections-containers<?php if (count($GLOBALS['usuario']['secciones']) == 1) { echo ' hidden'; } ?>">
        <?php
            $sections = $GLOBALS['usuario']['secciones'];
            for ($i=0; $i<count($sections); $i++) {
                $contextMode = 'image';
                $contextData = $sections[$i]['imagen'];
                if ($contextData == '') {
                    $nameArray = explode(' ', $sections[$i]['nombre']);
                    $name = strtoupper(substr($nameArray[0], 0, 1));
                    $contextData = $name;
                    $contextMode = 'text';
                }
        ?>
            <input data-hint="<?php echo $sections[$i]['nombre']; ?>" data-sectionID="<?php echo $sections[$i]['id'] ?>" data-context-mode="<?php echo $contextMode ?>" data-context-data="<?php echo $contextData ?>" type="checkbox" prettycheckbox>
        <?php
            }
        ?>
        </div>
        <br>
        <?php } ?>
        <div class="buttons-container">
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <button class="form-cancel" type="button">Cancelar</button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo $_GET['id']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/PrettyCheckbox.js"></script>
    <script src="/admin/js/plugins/personal/TogglePassword.js"></script>
    <script src="/admin/js/plugins/personal/DropImage.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/credenciales/credenciales-form.js"></script>
</body>
</html>