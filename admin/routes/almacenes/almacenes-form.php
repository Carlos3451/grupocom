<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Almacenes - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/almacenes.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/almacenes/almacenes-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/almacenes.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Almacenes</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Almacen</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Usuario</p>
                <input type="text" class="inp-icon inp-user" name="usuario" disabled>
            </div>
        </div>
        <div class="inventario-table-overflow">
            <div class="inventario-table" <?php if ($_GET['mode'] == 'VIEW') {  } ?>>
                <div class="inventario-table-header">
                    <div class="inventario-table-column">
                        <p>Producto</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Inicial</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Computada</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Final</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/almacenes/almacenes-form.js"></script>
</body>
</html>
