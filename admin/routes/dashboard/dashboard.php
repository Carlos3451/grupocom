<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/dashboard.png">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/graph.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/piegraph.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/dashboard/dashboard.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div id="content">
        <!-- <div class="latest-sales-container">
            <h3>Ultimas Ventas</h3>
            <div class="latest-sales-graph"></div>
        </div>
        <div class="fecha-selector-container">
            <h3>Rendimiento</h3>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Fecha:</p>
                    <input name="fecha">
                </div>
                <div class="input-container" <?php if ($GLOBALS['usuario']['privilegiosNivel'] != 4) { echo 'style="display:none;"'; } ?>>
                    <p>Vendedor:</p>
                    <input name="usuarioID">
                </div>
            </div>
        </div>
        <div class="dashboard-containers">
            <div class="dashboard-container payments-container">
                <h3>Pagos</h3>
                <div class="payments-graph"></div>
            </div>
            <div class="dashboard-container">
                <h3>Ventas</h3>
                <div class="sales-containers">
                    <div class="sales-container">
                        <p class="title">Ventas totales:</p>
                        <p class="description-big total-sales"></p>
                    </div>
                    <div class="sales-container">
                        <p class="subtitle">Costos:</p>
                        <p class="description total-costs"></p>
                    </div>
                    <div class="sales-container">
                        <p class="title">Ganancias:</p>
                        <p class="description-big total-earnings"></p>
                    </div>
                    <div class="sales-container">
                        <p class="subtitle">Margen de utlidad:</p>
                        <p class="description total-margin"></p>
                    </div>
                    <div class="sales-container">
                        <p class="subtitle">Pagos:</p>
                        <p class="description total-pagos"></p>
                    </div>
                    <div class="sales-container">
                        <p class="subtitle">Gastos:</p>
                        <p class="description total-gastos"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-containers">
            <div class="dashboard-container products-container">
                <h3>Productos más vendidos (pedidos)</h3>
                <div class="top-table">
                    <div class="top-header">   
                        <div class="top-column"><p>#</p></div>
                        <div class="top-column"><p>Nombre</p></div>
                        <div class="top-column"><p class="text-align-right">Ventas</p></div>
                    </div>
                </div>
            </div>
            <div class="dashboard-container products-container">
                <h3>Productos más vendidos (mostrador)</h3>
                <div class="top-mostrador-table">
                    <div class="top-header">   
                        <div class="top-column"><p>#</p></div>
                        <div class="top-column"><p>Nombre</p></div>
                        <div class="top-column"><p class="text-align-right">Ventas</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-containers" <?php if ($GLOBALS['usuario']['privilegiosNivel'] != 4) { echo 'style="display:none;"'; }; ?>>
            <div class="dashboard-container payments-container">
                <h3>Ventas de vendedores</h3>
                <div class="salesmans-table">
                    <div class="salesmans-header">
                        <div class="salesmans-column"><p>#</p></div>
                        <div class="salesmans-column"><p>Vendedor</p></div>
                        <div class="salesmans-column"><p class="text-align-right">Ventas</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-containers" <?php if ($GLOBALS['usuario']['privilegiosNivel'] != 4) { echo 'style="display:none;"'; }; ?>>
            <div class="dashboard-container payments-container">
                <h3>Ventas de repartidores</h3>
                <div class="deliveries-table">
                    <div class="deliveries-header">
                        <div class="deliveries-column"><p>#</p></div>
                        <div class="deliveries-column"><p>Repartidor</p></div>
                        <div class="deliveries-column"><p class="text-align-right">Ventas</p></div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <script>
        privilegesLevel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/Graph.js"></script>
    <script src="/admin/js/plugins/personal/PieGraph.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <!-- <script src="/admin/js/dashboard/dashboard.js"></script> -->
</body>
</html>