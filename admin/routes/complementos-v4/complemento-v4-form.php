<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Complemento de pago V4 - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/complementos.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/global/claves-sat.css">
    <link rel="stylesheet" href="/admin/css/global/factura-v4/email-factura-v4.css">
    <link rel="stylesheet" href="/admin/css/global/tinyproducto.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/complementos-v4/complemento-v4-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/factura-v4/email-factura-v4.php'); ?>
    <div class="facturadder-overlay">
        <div class="facturadder-container">
            <h3>Facturas</h3>
            <br>
            <div class="facturadder-table">
                <div class="facturadder-table-header">
                    <div class="facturadder-table-column"></div>
                    <div class="facturadder-table-column">
                        <p>Fecha</p>
                    </div>
                    <div class="facturadder-table-column">
                        <p>Folio</p>
                    </div>
                    <div class="facturadder-table-column">
                        <p>Saldo Restante</p>
                    </div>
                    <div class="facturadder-table-column">
                        <p>Total</p>
                    </div>
                </div>
            </div>
            <div class="actions-container">
                <button class="facturadder-add-btn">Agregar</button>
                <button class="facturadder-close-btn">Cerrar</button>
            </div>
        </div>
    </div>
    <div class="title-container">
        <img src="/admin/imgs/icons/facturas.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Complemento de pago V4</h3>
        </div>
    </div>
    <form id="data-form">
        <div class="inputs-containers">
            <div class="input-container">
                <p>Emisor</p>
                <select name="emisorID" enterfocus="1"></select>
            </div>
            <div class="input-container">
                <p>Fecha de Emisión</p>
                <input type="text" name="fechaFacturacion" enterfocus="1">
            </div>
        </div>
        <h3>Datos Cliente</h3>
        <br>
        <div class="client-actions-container">
            <p>Cliente</p>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <div class="client-input-container">
                    <input class="inp-icon inp-user inp-required" type="text" name="clienteUID">
                    <div class="client-toggle"></div>
                </div>
            </div>
        </div>
        <div class="client-data-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Nombre(s)</p>
                    <input type="text" name="clienteNombres" disabled>
                </div>
                <div class="input-container">
                    <p>Apellidos</p>
                    <input type="text" name="clienteApellidos" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Regimen Fiscal</p>
                    <select name="clienteRegimenFiscal" enterfocus="2" disabled ></select>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Email</p>
                    <input type="text" name="clienteEmail" disabled>
                </div>
                <div class="input-container">
                    <p>Teléfono</p>
                    <input type="text" name="clienteTelefono" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>RFC</p>
                    <input type="text" name="clienteRFC" disabled>
                </div>
                <div class="input-container">
                    <p>Razón Social</p>
                    <input type="text" name="clienteRazonSocial" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Calle</p>
                    <input type="text" name="clienteCalle" disabled>
                </div>
                <div class="input-container">
                    <p>Código Postal</p>
                    <input type="text" name="clienteCodigoPostal" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Número Exterior</p>
                    <input type="text" name="clienteNumeroExterior" disabled>
                </div>
                <div class="input-container">
                    <p>Número Interior</p>
                    <input type="text" name="clienteNumeroInterior" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Colonia</p>
                    <input type="text" name="clienteColonia" disabled>
                </div>
                <div class="input-container">
                    <p>Ciudad</p>
                    <input type="text" name="clienteCiudad" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Estado</p>
                    <input type="text" name="clienteEstado" disabled>
                </div>
                <div class="input-container"></div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Uso de CFDI por defecto</p>
                    <select name="clienteUsoCFDI" disabled></select>
                </div>
                <div class="input-container">
                    <p>Forma de pago por defecto</p>
                    <select name="clienteFormaPago" disabled></select>
                </div>
            </div>
        </div>
        <br>
        <h3>Datos Complemento de Pago</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Fecha de Pago</p>
                <input type="text" name="fechaPago" enterfocus="1">
            </div>
            <div class="input-container">
                <p>Forma de Pago</p>
                <select name="formaPago" enterfocus="1"></select>
            </div>
        </div>
        <h3>Facturas</h3>
        <?php if ($_GET['mode'] != 'VIEW') { ?>
        <button class="add-factura-btn" type="button">Agregar factura</button>
        <?php } ?>
        <div class="custom-table-overflow">
            <div class="custom-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                <div class="custom-table-header">
                    <div class="custom-table-column">
                        <p>Fecha</p>
                    </div>
                    <div class="custom-table-column">
                        <p>Folio</p>
                    </div>
                    <div class="custom-table-column">
                        <p>Total</p>
                    </div>
                    <div class="custom-table-column">
                        <p>Saldo Anterior</p>
                    </div>
                    <div class="custom-table-column">
                        <p>Importe</p>
                    </div>
                    <div class="custom-table-column"></div>
                </div>
            </div>
        </div>
        <br>
        <div class="accounting-container">
            <div class="totals-container">
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Monto Total</p>
                        <input class="inp-icon inp-price text-align-right" name="montoTotal" type="text" disabled>
                    </div>
                    <div class="input-container"></div>
                    <div class="input-container"></div>
                    <div class="input-container"></div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button">Aceptar</button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
        var privileges = '<?php echo implode(',', $GLOBALS['usuario']['privilegios']); ?>'.split(',');
        var privilegesLevel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/global/factura-v4/EmailFacturaV4.js?v=170420230215"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/complementos-v4/complemento-v4-form.js?v=250420231033"></script>
</body>
</html>
