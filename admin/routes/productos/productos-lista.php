<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos - Listado</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/productos.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/list.css">
    <link rel="stylesheet" href="/admin/css/global/editorprecios.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/sortcontroller.css">
    <link rel="stylesheet" href="/admin/css/productos/productos-lista.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/sortcontroller.php'); ?>
    <?php include_once('templates/editorprecios.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/productos.png">
        <h3>Productos</h3>
        <div class="actions-container">
            <a href="productos/agregar" tooltip-hint="Agregar Producto"><img src="/admin/imgs/icons/actions/add.png"></a>
            <a class="print-products" target="_blank" tooltip-hint="Imprimir Productos"><img src="/admin/imgs/icons/actions/printer.png"></a>
        </div>
    </div>
    <?php include_once('templates/listactions.php'); ?>
    <div class="list-overflow">
        <div class="list-container">
            <div class="list-filler">
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
            </div>
            <div class="list-header">
                <div class="list-column">
                </div>
                <div class="list-column">
                    <p>Folio</p>
                </div>
                <div class="list-column">
                    <p>Nombre</p>
                </div>
                <div class="list-column">
                    <p>Proveedor</p>
                </div>
                <div class="list-column">
                    <p>Categoría</p>
                </div>
                <div class="list-column">
                    <p>Precios</p>
                </div>
                <div class="list-column">
                    <p>Costo</p>
                </div>
                <div class="list-column">
                    <p>Existencia</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        privilegios = JSON.parse('<?php echo json_encode($GLOBALS['usuario']['privilegios']); ?>');
        privilegiosNivel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SortController.js"></script>
    <script src="/admin/js/global/Lista.js"></script>
    <script src="/admin/js/global/EditorPrecios.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/productos/productos-lista.js"></script>
</body>
</html>