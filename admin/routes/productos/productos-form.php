<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/productos.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/global/claves-sat.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/checkswitch.css">
    <link rel="stylesheet" href="/admin/css/productos/productos-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php if ($GLOBALS['usuario']['seccionActual']['facturacion'] == 1) { include_once('templates/claves-sat.php'); } ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/productos.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Producto</h3>
        </div>
    </div>
    <form id="producto-form">
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre</p>
                <input class="inp-required" type="text" name="nombre">
            </div>
            <div class="input-container">
                <p>Codigo de barras</p>
                <input class="inp-icon inp-barcode" type="text" name="codigo">
            </div>
            <div class="input-container">
                <p>Proveedor</p>
                <input type="text" name="proveedorID">
            </div>
        </div>
        <div class="separator"></div>
        <div class="category-container">
            <p>Categoría</p>
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <div class="actions-container">
                <img class="category-add" tooltip-hint="Agregar categoría" src="/admin/imgs/icons/actions/add.png">
                <img class="category-edit disabled" tooltip-hint="Editar categoría" src="/admin/imgs/icons/actions/edit.png">
                <img class="category-delete disabled" tooltip-hint="Eliminar categoría" src="/admin/imgs/icons/actions/delete.png">
            </div>
            <?php } ?>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <select type="text" name="categoriaID">
                    <option value="0">Ninguna</option>
                </select>
            </div>
        </div>
        <div class="separator"></div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Unidad</p>
                <select type="text" name="unidad">
                    <option value="pieza">Pieza (pza)</option>
                    <option value="paquete">Paquete (paq)</option>
                    <option value="caja">Caja (caj)</option>
                    <option value="burbuja">Burbuja (burb)</option>
                    <option value="costal">Costal (cost)</option>
                    <option value="maso">Maso (maso)</option>
                    <option value="gramo">Gramo (g)</option>
                    <option value="kilogramo">Kilogramo (kg)</option>
                    <option value="libra">Libra (lb)</option>
                    <option value="mililitro">Mililitro (ml)</option>
                    <option value="litro">Litro (l)</option>
                    <option value="unidad">Unidad (ud)</option>
                </select>
            </div>
            <div class="input-container">
                <p>Inventario Minimo</p>
                <input class="inp-required inp-unit inp-pieza" type="text" name="inventarioMinimo">
            </div>
            <div class="input-container">
                <p>Costo</p>
                <input class="inp-required inp-icon inp-price" type="text" name="costo" numeric>
            </div>
        </div>
        <div class="separator"></div>
        <h3>Precios</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Precio Menudeo</p>
                <input class="inp-required inp-icon inp-price" type="text" name="precio1" numeric>
            </div>
            <div class="input-container">
                <p>Margen de ganancia</p>
                <input class="inp-required inp-icon inp-percent" type="text" name="margen1" numeric>
            </div>
            <div class="input-container"></div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Precio Medio Mayoreo</p>
                <input class="inp-icon inp-price" type="text" name="precio2" numeric>
            </div>
            <div class="input-container">
                <p>Margen de ganancia</p>
                <input class="inp-icon inp-percent" type="text" name="margen2" numeric>
            </div>
            <div class="input-container"></div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Precio Mayoreo</p>
                <input class="inp-icon inp-price" type="text" name="precio3" numeric>
            </div>
            <div class="input-container">
                <p>Margen de ganancia</p>
                <input class="inp-icon inp-percent" type="text" name="margen3" numeric>
            </div>
            <div class="input-container"></div>
        </div>
        <div class="separator"></div>
        <h3>Facturación</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Clave SAT</p>
                <div class="clave-input-container">
                    <input type="text" name="claveSAT">
                    <img class="action-clave-search" src="imgs/icons/buscador-azul.png">
                </div>
            </div>
            <div class="input-container">
                <p>Tipo de Impuesto</p>
                <div class="clave-input-container">
                    <select name="impuesto">
                        <option value="tasacero">Tasa Cero (0%)</option>
                        <option value="iva">IVA (16%)</option>
                        <option value="ieps8">IEPS (8%)</option>
                        <option value="ieps160iva">IEPS (160%) e IVA (16%)</option>
                        <option value="exento">Exento</option>
                    </select>
                </div>
            </div>
            <div class="input-container"></div>
        </div>
        <div class="separator"></div>
        <h3>Venta por diferentes unidades</h3>
        <div class="productosunidades-container">
            <button class="add-productounidad-btn" type="button">Agregar unidad</button>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var facturacion = <?php echo $GLOBALS['usuario']['seccionActual']['facturacion']; ?>;
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/DropImage.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/CheckSwitch.js"></script>
    <script src="/admin/js/global/ClavesSAT.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/productos/productos-form.js"></script>
</body>
</html>