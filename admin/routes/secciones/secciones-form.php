<?php
    $_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 0;
?>
<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Secciones - <?php echo $_GET['mode'] == 'ADD' ? 'Agregar' : 'Editar'; ?></title>
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/secciones/secciones-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/secciones.png">
        <div class="title-text">
            <p><?php echo $_GET['mode'] == 'ADD' ? 'NUEVA' : 'EDITANDO'; ?></p>
            <h3>Sección</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Fotografía</h3>
        <div class="image-container">
            <img src="/admin/imgs/icons/camara.png">
            <input class="hidden" type="file" name="imagen" accept=".png, .jpg, .jpeg, .bmp, .gif">
        </div>
        <br>
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre</p>
                <input class="inp-required" type="text" name="nombre">
            </div>
            <div class="input-container">
                <p>Clave de Gerente</p>
                <input type="text" name="claveGerente">
            </div>
        </div>
        <br>
        <h3>Ubicación</h3>
        <div class="map-container">            
            <input class="hidden" type="text" name="lat">
            <input class="hidden" type="text" name="lng">
            <div class="map"></div>
        </div>
        <br>
        <div class="buttons-container">
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <button class="form-cancel" type="button">Cancelar</button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo $_GET['id']; ?>;
    </script>
    <script src="/admin/js/plugins/external/leaflet.js"></script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/DropImage.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/secciones/secciones-form.js?v=270320230115"></script>
</body>
</html>