<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pedidos - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/pedidos.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/pedidos/pedidos-form.css?v=270320230115">
    <link rel="stylesheet" href="/admin/css/global/payment.css">
    <link rel="stylesheet" href="/admin/css/global/tinyproducto.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/payment.php'); ?>
    <?php include_once('templates/tinyproducto.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/pedidos.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Pedido</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Cliente</p>
                <input class="inp-icon inp-user inp-required" type="text" name="clienteID">
            </div>
            <div class="input-container">
                <p>Fecha</p>
                <input class="inp-icon inp-date" type="text" name="fecha">
            </div>
        </div>
        <br>
        <h3>Pedido</h3>
        <?php if ($_GET['mode'] != 'VIEW') { ?>
        <button class="new-producto-btn" type="button">Nuevo producto</button>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Buscar producto</p>
                <input name="producto-buscador" type="text">
            </div>
        </div>
        <?php } ?>
        <div class="pedido-table-overflow">
            <div class="pedido-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                <div class="pedido-table-header">
                    <div class="pedido-table-column">
                        <p>Producto</p>
                    </div>
                    <div class="pedido-table-column">
                        <p>Unidad</p>
                    </div>
                    <div class="pedido-table-column">
                        <p>Cant</p>
                    </div>
                    <div class="pedido-table-column">
                        <p>Precio</p>
                    </div>
                    <div class="pedido-table-column">
                        <p>Total</p>
                    </div>
                    <div class="pedido-table-column">
                        <p>Acciones</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="accounting-container">
            <div class="totals-container">
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Subtotal</p>
                        <input class="inp-icon inp-price text-align-right" name="subtotal" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers discount-container">
                    <div class="input-container">
                        <p>Descuento</p>
                        <input class="inp-icon inp-percent" name="descuentoPorcentaje" type="text" autocomplete="off" numeric>
                    </div>
                    <div class="input-container">
                        <p>-</p>
                        <input class="inp-icon inp-price text-align-right" name="descuentoCantidad" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Total</p>
                        <input class="inp-icon inp-price text-align-right" name="total" type="text" disabled>
                    </div>
                </div>
            </div>
            <div class="payments-container">
                <div class="payments-table-overflow">
                    <div class="payments-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                        <div class="payments-table-header">
                            <div class="payments-table-column">
                                <p>Fecha</p>
                            </div>
                            <div class="payments-table-column">
                                <p>Cantidad</p>
                            </div>
                            <div class="payments-table-column"></div>
                        </div>
                    </div>
                </div>
                <?php if ($_GET['mode'] != 'VIEW') { ?>
                <button class="payment-add" type="button">Añadir pago</button>
                <?php } ?>
            </div>
            <div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Pagado</p>
                        <input class="inp-icon inp-price text-align-right" name="pagado" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Resto</p>
                        <input class="inp-icon inp-price text-align-right" name="resto" type="text" disabled>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
        var privileges = '<?php echo implode(',', $GLOBALS['usuario']['privilegios']); ?>'.split(',');
        var privilegesLevel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/Payment.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/global/TinyProducto.js"></script>
    <script src="/admin/js/pedidos/pedidos-form.js?v=280320230108"></script>
</body>
</html>
