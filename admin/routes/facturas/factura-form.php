<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facturas - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/facturas.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/global/claves-sat.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/checkswitch.css">
    <link rel="stylesheet" href="/admin/css/facturas/factura-form.css">
    <link rel="stylesheet" href="/admin/css/global/emailfactura.css">
    <link rel="stylesheet" href="/admin/css/global/tinyproducto.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/claves-sat.php'); ?>
    <?php include_once('templates/emailfactura.php'); ?>
    <?php include_once('templates/tinyproducto.php'); ?>
    <div class="facturadder-overlay">
        <div class="facturadder-container">
            <h3>CFDIs</h3>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Fecha</p>
                    <input class="facturadder-fecha" type="text">
                </div>
            </div>
            <br>
            <div class="facturadder-table">
                <div class="facturadder-table-header">
                    <div class="facturadder-table-column"></div>
                    <div class="facturadder-table-column">
                        <p>Fecha</p>
                    </div>
                    <div class="facturadder-table-column">
                        <p>Folio</p>
                    </div>
                    <div class="facturadder-table-column">
                        <p>UUID</p>
                    </div>
                </div>
            </div>
            <div class="actions-container">
                <button class="facturadder-add-btn">Agregar</button>
                <button class="facturadder-close-btn">Cerrar</button>
            </div>
        </div>
    </div>
    <div class="title-container">
        <img src="/admin/imgs/icons/facturas.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVA';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Factura</h3>
        </div>
    </div>
    <div class="client-adder-overlay">
        <form class="client-adder-form">
            <h3>Registrar cliente</h3>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>RFC*</p>
                    <input class="inp-required" type="text" name="rfc" enterfocus="2" >
                </div>
                <div class="input-container">
                    <p>Razón Social*</p>
                    <input class="inp-required" type="text" name="razonSocial" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Nombre(s)</p>
                    <input type="text" name="nombres" enterfocus="2" >
                </div>
                <div class="input-container">
                    <p>Apellidos</p>
                    <input type="text" name="apellidos" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Emails* (separados por coma)</p>
                    <input class="inp-required" type="text" name="emails" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Teléfono</p>
                    <input type="text" name="telefono" enterfocus="2" >
                </div>
                <div class="input-container"></div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Calle</p>
                    <input type="text" name="calle" enterfocus="2" >
                </div>
                <div class="input-container">
                    <p>Código Postal*</p>
                    <input class="inp-required" type="text" name="codigoPostal" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Número Exterior</p>
                    <input type="text" name="numeroExterior" enterfocus="2" >
                </div>
                <div class="input-container">
                    <p>Número Interior</p>
                    <input type="text" name="numeroInterior" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Colonia</p>
                    <input type="text" name="colonia" enterfocus="2" >
                </div>
                <div class="input-container">
                    <p>Ciudad</p>
                    <input type="text" name="ciudad" enterfocus="2" >
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Estado</p>
                    <input type="text" name="estado" enterfocus="2" >
                </div>
                <div class="input-container"></div>
            </div>
            <div class="actions-container">
                <button class="client-adder-accept" type="button">Registrar</button>
                <button class="client-adder-close" type="button">Cerrar</button>
            </div>
        </form>
    </div>
    <form id="data-form">
        <div class="inputs-containers">
            <div class="input-container">
                <p>Facturar a nombre de</p>
                <select name="emisorID" enterfocus="1"></select>
            </div>
        </div>
        <h3>Datos Cliente</h3>
        <br>
        <div class="client-actions-container">
            <p>Cliente</p>
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <div class="actions-container">
                <img class="client-add" tooltip-hint="Registrar cliente" src="/admin/imgs/icons/actions/add.png">
                <img class="client-edit disabled" tooltip-hint="Editar cliente" src="/admin/imgs/icons/actions/edit.png">
            </div>
            <?php } ?>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <div class="client-input-container">
                    <input class="inp-icon inp-user inp-required" type="text" name="clienteUID">
                    <div class="client-toggle"></div>
                </div>
            </div>
        </div>
        <div class="client-data-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Nombre(s)</p>
                    <input type="text" name="clienteNombres" disabled>
                </div>
                <div class="input-container">
                    <p>Apellidos</p>
                    <input type="text" name="clienteApellidos" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Email(s)</p>
                    <input type="text" name="clienteEmails" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Teléfono</p>
                    <input type="text" name="clienteTelefono" disabled>
                </div>
                <div class="input-container"></div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>RFC</p>
                    <input type="text" name="clienteRFC" disabled>
                </div>
                <div class="input-container">
                    <p>Razón Social</p>
                    <input type="text" name="clienteRazonSocial" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Calle</p>
                    <input type="text" name="clienteCalle" disabled>
                </div>
                <div class="input-container">
                    <p>Código Postal</p>
                    <input type="text" name="clienteCodigoPostal" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Número Exterior</p>
                    <input type="text" name="clienteNumeroExterior" disabled>
                </div>
                <div class="input-container">
                    <p>Número Interior</p>
                    <input type="text" name="clienteNumeroInterior" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Colonia</p>
                    <input type="text" name="clienteColonia" disabled>
                </div>
                <div class="input-container">
                    <p>Ciudad</p>
                    <input type="text" name="clienteCiudad" disabled>
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Estado</p>
                    <input type="text" name="clienteEstado" disabled>
                </div>
                <div class="input-container"></div>
            </div>
        </div>
        <br>
        <h3>Datos Facturación</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Fecha de Facturación</p>
                <input type="text" name="fechaFacturacion" enterfocus="1">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Uso del CFDI</p>
                <select name="usoCFDI" enterfocus="1"></select>
            </div>
            <div class="input-container">
                <p>Forma de Pago</p>
                <select name="formaPago" enterfocus="1"></select>
            </div>
            <div class="input-container">
                <p>Método de Pago</p>
                <select name="metodoPago" enterfocus="1"></select>
            </div>
            <div class="input-container">
                <p>Activar CFDIs Relacionados</p>
                <input type="checkbox" name="activarRelacionados">
            </div>
            <div class="input-container" style="display:none;">
                <p>Aumentar Impuesto al precio</p>
                <input type="checkbox" name="aumentarImpuesto">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Observaciones</p>
                <input type="text" name="comentarios" enterfocus="1">
            </div>
        </div>
        <div class="relacionados-container" style="display:none;">
            <h3>CFDIs Relacionados</h3>
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Tipo de Relación</p>
                    <select name="tipoRelacion" enterfocus="1"></select>
                </div>
                <div class="input-container"></div>
            </div>
            <button class="add-relacionado-btn" type="button">Agregar CFDIs</button>
            <div class="relacionados-table-overflow">
                <div class="relacionados-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                    <div class="relacionados-table-header">
                        <div class="relacionados-table-column">
                            <p>Fecha</p>
                        </div>
                        <div class="relacionados-table-column">
                            <p>Folio</p>
                        </div>
                        <div class="relacionados-table-column">
                            <p>UUID</p>
                        </div>
                        <div class="relacionados-table-column"></div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <h3>Conceptos</h3>
        <?php if ($_GET['mode'] != 'VIEW') { ?>
        <button class="new-producto-btn" type="button">Nuevo producto</button>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Buscar producto</p>
                <input name="producto-buscador" type="text">
            </div>
        </div>
        <?php } ?>
        <div class="factura-table-overflow">
            <div class="factura-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                <div class="factura-table-header">
                    <div class="factura-table-column">
                        <p>Cláve SAT</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Concepto</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Unidad</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Cant</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Precio</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Total</p>
                    </div>
                    <div class="factura-table-column">
                        <p>Impuestos</p>
                    </div>
                    <div class="factura-table-column"></div>
                </div>
            </div>
        </div>
        <br>
        <div class="accounting-container">
            <div class="totals-container">
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Gran Total</p>
                        <input class="inp-icon inp-price text-align-right" name="granTotal" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers discount-container">
                    <div class="input-container">
                        <p>Descuento</p>
                        <input class="inp-icon inp-percent" name="descuentoPorcentaje" type="text" autocomplete="off" numeric>
                    </div>
                    <div class="input-container">
                        <p>-</p>
                        <input class="inp-icon inp-price text-align-right" name="descuentoCantidad" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Subtotal</p>
                        <input class="inp-icon inp-price text-align-right" name="subtotal" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Impuestos</p>
                        <input class="inp-icon inp-price text-align-right" name="impuestos" type="text" disabled>
                    </div>
                </div>
                <div class="inputs-containers">
                    <div class="input-container">
                        <p>Total</p>
                        <input class="inp-icon inp-price text-align-right" name="total" type="text" disabled>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button">Facturar</button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
        var privileges = '<?php echo implode(',', $GLOBALS['usuario']['privilegios']); ?>'.split(',');
        var privilegesLevel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/plugins/personal/CheckSwitch.js"></script>
    <script src="/admin/js/global/TinyProducto.js"></script>
    <script src="/admin/js/global/ClavesSAT.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/global/EmailFactura.js"></script>
    <script src="/admin/js/facturas/factura-form.js"></script>
</body>
</html>
