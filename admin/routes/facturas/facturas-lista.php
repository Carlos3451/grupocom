<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Facturas - Listado</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/facturas.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/list.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/sortcontroller.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/facturas/facturas-lista.css">
    <link rel="stylesheet" href="/admin/css/global/payment.css">
    <link rel="stylesheet" href="/admin/css/global/emailfactura.css">
    <link rel="stylesheet" href="/admin/css/global/complementospago.css">
    <link rel="stylesheet" href="/admin/css/global/facturaload.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/payment.php'); ?>
    <?php include_once('templates/sortcontroller.php'); ?>
    <?php include_once('templates/complementospago.php'); ?>
    <?php include_once('templates/emailfactura.php'); ?>
    <?php include_once('templates/facturaload.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/facturas.png">
        <h3>Facturas</h3>
        <div class="actions-container">
            <a href="facturas/agregar" tooltip-hint="Crear Factura"><img src="/admin/imgs/icons/actions/add.png"></a>
        </div>
    </div>
    <?php include_once('templates/listactions.php'); ?>
    <div class="list-overflow">
        <div class="list-container">
            <div class="list-header">
                <div class="list-column"></div>
                <div class="list-column">
                    <p>Folio</p>
                </div>
                <div class="list-column">
                    <p>Fecha</p>
                </div>
                <div class="list-column">
                    <p>Cliente</p>
                </div>
                <div class="list-column">
                    <p>Emisor</p>
                </div>
                <div class="list-column">
                    <p>Usuario</p>
                </div>
                <div class="list-column">
                    <p>Estatus</p>
                </div>
                <div class="list-column">
                    <p>Pagado</p>
                </div>
                <div class="list-column">
                    <p>Tipo</p>
                </div>
                <div class="list-column">
                    <p>Total</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        privilegios = JSON.parse('<?php echo json_encode($GLOBALS['usuario']['privilegios']); ?>');
        privilegiosNivel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/external/jszip.min.js"></script>
    <script src="/admin/js/plugins/external/xlsx.full.min.js"></script>
    <script src="/admin/js/plugins/external/pdf-lib.min.js" ></script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/Payment.js"></script>
    <script src="/admin/js/plugins/personal/SortController.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/global/Lista.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/global/EmailFactura.js"></script>
    <script src="/admin/js/global/ComplementosPago.js"></script>
    <script src="/admin/js/global/FacturaLoad.js"></script>
    <script src="/admin/js/facturas/facturas-lista.js" type="module"></script>
</body>
</html>