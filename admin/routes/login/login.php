<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/login/login.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
</head>
<body>
    <div id="content">
        <form id="login-form">
            <img src="/config/logotipo.png">
            <h3 class="text-color-primary text-align-center">¡Bienvenido!</h3>
            <p class="text-color-primary text-align-center">Ingrese su usuario y contraseña para continuar</p>
            <div class="inputs-containers">
                <div class="input-container">
                    <p class="text-color-ltgray">Usuario</p>
                    <input type="text" name="usuario">
                </div>
            </div>
            <div class="inputs-containers">
                <div class="input-container">
                    <p class="text-color-ltgray">Contraseña</p>
                    <input type="password" name="contraseña" togglePassword="hidden">
                </div>
            </div>
            <br>
            <button class="button-align-center">Iniciar Sesión</button>
        </form>
    </div>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/TogglePassword.js"></script>
    <script src="/admin/js/login/login.js"></script>
</body>
</html>