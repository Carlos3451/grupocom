<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rutas - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/rutas.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.fullscreen.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.markercluster.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.markercluster.default.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/prettycheckbox.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/rutas/rutas-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="clients-searcher-overlay">
        <div class="clients-searcher-container">
            <div class="searcher-actions-container">
                <input class="inp-icon inp-search clients-searcher-inp">
            </div>
            <div class="searcher-table-overflow">
                <div class="searcher-table">
                    <div class="searcher-table-header">
                        <div class="searcher-table-column"></div>
                        <div class="searcher-table-column">
                            <p>Folio</p>
                        </div>
                        <div class="searcher-table-column">
                            <p>Nombre</p>
                        </div>
                        <div class="searcher-table-column">
                            <p>Dirección</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="searcher-actions">
                <button class="searcher-add" disabled>Agregar</button>
                <button class="searcher-cancel">Cancelar</button>
            </div>
        </div>
    </div>
    <div class="title-container">
        <img src="/admin/imgs/icons/rutas.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Ruta</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Ruta</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre *</p>
                <input class="inp-icon inp-route inp-required" type="text" name="nombre">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Vendedor</p>
                <input class="inp-icon inp-user" type="text" name="usuarioID">
            </div>
        </div>
        <br>
        <div class="clients-header">
            <h3>Clientes</h3>
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <div class="actions-container">
                <img tooltip-hint="Agregar Cliente" class="client-add" src="/admin/imgs/icons/actions/add.png">
                <img tooltip-hint="Quitar" class="client-remove" src="/admin/imgs/icons/actions/delete.png">
            </div>
            <?php } ?>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Mostrar clientes de</p>
                <select class="client-filter-route-days">
                    <option value="-1">Todos los días</option>
                    <option value="0">Sin asignar</option>
                    <option value="1">Lunes</option>
                    <option value="2">Martes</option>
                    <option value="4">Miercoles</option>
                    <option value="8">Jueves</option>
                    <option value="16">Viernes</option>
                    <option value="32">Sabado</option>
                    <option value="64">Domingo</option>
                </select>
            </div>
        </div>
        <div class="clients-table-overflow">
            <div class="clients-table" <?php if ($_GET['mode'] == 'VIEW') { echo 'view'; } ?>>
                <div class="clients-table-header">
                    <div class="clients-table-column"></div>
                    <div class="clients-table-column">
                        <p>Folio</p>
                    </div>
                    <div class="clients-table-column">
                        <p>Nombre</p>
                    </div>
                    <div class="clients-table-column">
                        <p>Dirección</p>
                    </div>
                    <div class="clients-table-column">
                        <p>Días de ruta</p>
                    </div>
                </div>
            </div>
        </div>
        <h3>Ubicaciones</h3>
        <div class="map-container">            
            <input class="hidden" type="text" name="lat">
            <input class="hidden" type="text" name="lng">
            <div class="map"> 
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var seccionLat = <?php echo $GLOBALS['usuario']['seccionActual']['lat']; ?>;
        var seccionLng = <?php echo $GLOBALS['usuario']['seccionActual']['lng']; ?>;
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/external/leaflet.js"></script>
    <script src="/admin/js/plugins/external/leaflet.fullscreen.min.js"></script>
    <script src="/admin/js/plugins/external/leaflet.markercluster.min.js"></script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/PrettyCheckbox.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/rutas/rutas-form.js"></script>
</body>
</html>
