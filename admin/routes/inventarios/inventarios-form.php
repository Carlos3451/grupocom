<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inventario - <?php
        switch ($_GET['mode']) { 
            case 'ADD':
                echo 'Agregar';
                break;
            case 'EDIT':
                echo 'Editando';
                break;
            case 'VIEW':
                echo 'Viendo';
                break;
        }
    ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/inventario.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/inventarios/inventarios-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/inventario.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Inventario</h3>
        </div>
    </div>
    <form id="data-form">
        <h3>Inventario</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Categoría</p>
                <select name="categoriaID"></select>
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Busqueda</p>
                <input class="inp-icon inp-search" type="text" name="busqueda">
            </div>
        </div>
        <div class="inventario-table-overflow">
            <div class="inventario-table" <?php if ($_GET['mode'] == 'VIEW') {  } ?>>
                <div class="inventario-table-header">
                    <div class="inventario-table-column">
                        <p>Producto</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Nueva</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Anterior</p>
                    </div>
                    <div class="inventario-table-column">
                        <p>Exist. Diferencia</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/inventarios/inventarios-form.js"></script>
</body>
</html>
