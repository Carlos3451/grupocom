<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/reportes/reporte.css">
</head>
<body>
    <object src="" type="application/pdf" title="title.pdf">
        <embed src="" type="application/pdf" title="title.pdf">
    </object>
    <script src="/admin/js/plugins/external/jspdf.min.js"></script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/reportes/imprimir.js"></script>
</body>
</html>