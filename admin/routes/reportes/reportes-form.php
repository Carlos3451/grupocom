<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reportes</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/reportes.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/prettycheckbox.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/selectinp.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/datepicker.css">
    <link rel="stylesheet" href="/admin/css/reportes/reportes-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/reportes.png">
        <div class="title-text">
            <h3>Reportes</h3>
        </div>
    </div>
    <form id="data-form">
        <div class="inputs-containers">
            <div class="input-container">
                <p>Tipo de reporte</p>
                <select name="tipo">
                    <option value="productos-vendidos">Productos vendidos</option>
                    <option value="pedidos-realizados">Pedidos realizados</option>
                    <option value="ventas-mostrador">Ventas de mostrador</option>
                    <option value="gastos">Gastos</option>
                    <option value="compras">Compras</option>
                    <option value="facturas">Facturas</option>
                    <option value="ganancias">Ganancias</option>
                    <!--<option value="pagos-generados">Pagos generados</option>-->
                </select>
            </div>
            <div class="input-container">
                <p>Desde:</p>
                <input type="text" name="fechaDesde">
            </div>
            <div class="input-container">
                <p>Hasta:</p>
                <input type="text" name="fechaHasta">
            </div>
        </div>
        <div class="productos-vendidos-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Tipo de venta</p>
                    <select name="tipoVenta">
                        <option value="">Todos</option>
                        <option value="pedidos">Pedidos</option>
                        <option value="mostrador">Mostrador</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Listar productos</p>
                    <select name="tipoProducto">
                        <option value="">Todos</option>
                        <option value="solo">Solo</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Vendedor</p>
                    <select name="vendedor">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
                <div class="products-list"></div>
            </div>
        </div>
        <div class="pedidos-realizados-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Metodo de pago</p>
                    <select name="metodoPago">
                        <option value="">Todos</option>
                        <option value="credito">Crédito</option>
                        <option value="contado">Contado</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Estado de los pedidos</p>
                    <select name="estadoPedido">
                        <option value="">Todos</option>
                        <option value="pagado">Pagado</option>
                        <option value="sinpagar">Sin pagar</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Cliente</p>
                    <select name="cliente">
                        <option value="">Todos</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Vendedor</p>
                    <select name="vendedor">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
            </div>
        </div>
        <div class="ventas-mostrador-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Metodo de pago</p>
                    <select name="metodoPago">
                        <option value="">Todos</option>
                        <option value="efectivo">Efectivo</option>
                        <option value="tarjeta">Tarjeta</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Tipo de pago</p>
                    <select name="tipoPago">
                        <option value="">Todos</option>
                        <option value="contado">Contado</option>
                        <option value="credito">Crédito</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Tipo de venta</p>
                    <select name="tipoVenta">
                        <option value="">Todos</option>
                        <option value="mostrador">Mostrador</option>
                        <option value="domicilio">Domicilio</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Vendedor</p>
                    <select name="vendedor">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
            </div>
        </div>
        <div class="pagos-generados-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Tipo de venta</p>
                    <select name="tipoVenta">
                        <option value="todos">Todos</option>
                        <option value="pedidos">Pedidos</option>
                        <option value="mostrador">Mostrador</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Metodos de pagos</p>
                    <select name="metodoPago">
                        <option value="">Todos</option>
                        <option value="efectivo">Efectivo</option>
                        <option value="cheque">Cheque</option>
                        <option value="tarjeta">Tarjeta</option>
                        <option value="transferencia">Transferencia</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Vendedor</p>
                    <select name="vendedor">
                        <option value="todos">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
            </div>
        </div>
        <div class="ganancias-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Tipo de venta</p>
                    <select name="tipoVenta">
                        <option value="">Todos</option>
                        <option value="pedidos">Pedidos</option>
                        <option value="mostrador">Mostrador</option>
                    </select>
                </div>
                <div class="input-container">
                    <p>Vendedor</p>
                    <select name="vendedor">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
            </div>
        </div>
        <div class="facturas-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Cliente</p>
                    <input type="text" name="clienteRFC">
                </div>
                <div class="input-container">
                    <p>Emisor</p>
                    <select name="emisor">
                        <option value="">Todos</option>
                    </select>
                </div>
            </div>
            <div class="just-products-container" style="display:none;">
                <div class="inputs-containers">
                    <div class="input-container">
                        <input type="text" name="producto">
                    </div>
                </div>
            </div>
        </div>
        <div class="compras-container reporte-container">
            <div class="inputs-containers">
                <div class="input-container">
                    <p>Proveedor</p>
                    <select name="proveedor" disabled><option>Cargando...</option></select>
                </div>
            </div>
        </div>
        <br>
        <div class="buttons-container">
            <button class="act-print" type="button">Imprimir</button>
            <button class="act-csv" type="button">Descargar CSV</button>
        </div>
    </form>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/PrettyCheckbox.js"></script>
    <script src="/admin/js/plugins/personal/SelectInp.js"></script>
    <script src="/admin/js/plugins/personal/DatePicker.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/reportes/reportes-form.js"></script>
</body>
</html>
