<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientes - <?php echo $_GET['mode'] == 'ADD' ? 'Agregar' : 'Editar'; ?></title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/clientes.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/form.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.css">
    <link rel="stylesheet" href="/admin/css/plugins/external/leaflet.fullscreen.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/prettycheckbox.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/togglepassword.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/checkswitch.css">
    <link rel="stylesheet" href="/admin/css/clientes/clientes-form.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/clientes.png">
        <div class="title-text">
            <p><?php
                switch ($_GET['mode']) {
                    case 'ADD':
                        echo 'NUEVO';
                        break;
                    case 'EDIT':
                        echo 'EDITAR';
                        break;
                    case 'VIEW':
                        echo 'VIENDO';
                        break;
                }
                ?></p>
            <h3>Cliente</h3>
        </div>
    </div>
    <form id="cliente-form">
        <h3>Datos</h3>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Nombre *</p>
                <input class="inp-required" type="text" name="nombre">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Teléfono / Celular</p>
                <input type="text" name="telefono">
            </div>
            <div class="input-container">
                <p>Email</p>
                <input type="text" name="email">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Credito *</p>
                <input class="inp-icon inp-price inp-required" type="text" name="credito">
            </div>
            <div class="input-container">
                <p>Ruta</p>
                <select class="inp-icon inp-route" type="text" name="rutaID">
                    <option value="0">Ninguna</option>
                </select>
            </div>
        </div>
        <br>
        <div class="routedays-outside">
            <p>Días de ruta</p>
            <div class="routedays-container">
                <input data-route-day="lunes" data-context="LUN" type="checkbox" prettycheckbox>
                <input data-route-day="martes" data-context="MAR" type="checkbox" prettycheckbox>
                <input data-route-day="miercoles" data-context="MIE" type="checkbox" prettycheckbox>
                <input data-route-day="jueves" data-context="JUE" type="checkbox" prettycheckbox>
                <input data-route-day="viernes" data-context="VIE" type="checkbox" prettycheckbox>
                <input data-route-day="sabado" data-context="SAB" type="checkbox" prettycheckbox>
                <input data-route-day="domingo" data-context="DOM" type="checkbox" prettycheckbox>
            </div>
        </div>
        <h3>Dirección</h3>
        <?php if ($_GET['mode'] != 'VIEW') { ?>
        <button class="map-geocoding" type="button">Obtener ubicación con dirección</button>
        <?php } ?>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Calle</p>
                <input type="text" name="calle">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Numero Exterior</p>
                <input type="text" name="numeroExterior">
            </div>
            <div class="input-container">
                <p>Numero Interior</p>
                <input type="text" name="numeroInterior">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Colonia</p>
                <input type="text" name="colonia">
            </div>
            <div class="input-container">
                <p>Código Postal</p>
                <input type="text" name="codigoPostal">
            </div>
        </div>
        <div class="inputs-containers">
            <div class="input-container">
                <p>Ciudad</p>
                <input type="text" name="ciudad">
            </div>
            <div class="input-container">
                <p>Estado</p>
                <input type="text" name="estado">
            </div>
        </div>
        <br>
        <h3>Ubicación</h3>
        <?php if ($_GET['mode'] != 'VIEW') { ?>
        <button class="map-reverse-geocoding" type="button">Obtener dirección con ubicación</button>
        <?php } ?>
        <div class="map-container">            
            <input class="hidden" type="text" name="lat">
            <input class="hidden" type="text" name="lng">
            <div class="map"></div>
        </div>
        <br>
        <div class="buttons-container">
            <?php if ($_GET['mode'] != 'VIEW') { ?>
            <button class="form-submit" type="button"><?php echo $_GET['mode'] == 'ADD' ? 'Registrar' : 'Guardar'; ?></button>
            <?php } ?>
            <button class="form-cancel" type="button"><?php echo $_GET['mode'] == 'VIEW' ? 'Volver' : 'Cancelar'; ?></button>
        </div>
    </form>
    <script>
        var formMode = '<?php echo $_GET['mode']; ?>';
        var editID = <?php echo isset($_GET['id']) ? $_GET['id'] : 0; ?>;
    </script>
    <script src="/admin/js/plugins/external/leaflet.js"></script>
    <script src="/admin/js/plugins/external/leaflet.fullscreen.min.js"></script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/PrettyCheckbox.js"></script>
    <script src="/admin/js/plugins/personal/CheckSwitch.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/clientes/clientes-form.js"></script>
</body>
</html>
