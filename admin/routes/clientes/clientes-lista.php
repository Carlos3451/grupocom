<!DOCTYPE html>
<html lang="es-MX">
<head>
	<base href="<?php echo BASE_URL;?>">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientes - Lista</title>
    <link rel="icon" type="image/png" href="/admin/imgs/icons/clientes.png">
    <link rel="stylesheet" href="/admin/css/global/animations.css">
    <link rel="stylesheet" href="/admin/css/global/fonts.css">
    <link rel="stylesheet" href="/admin/css/global/global.css">
    <link rel="stylesheet" href="/admin/css/global/menu.css">
    <link rel="stylesheet" href="/admin/css/global/list.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/tooltip.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/checkswitch.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/messages.css">
    <link rel="stylesheet" href="/admin/css/plugins/personal/sortcontroller.css">
    <link rel="stylesheet" href="/admin/css/clientes/clientes-lista.css">
</head>
<body>
    <?php include_once('templates/menu.php'); ?>
    <?php include_once('templates/sortcontroller.php'); ?>
    <div class="title-container">
        <img src="/admin/imgs/icons/clientes.png">
        <h3>Clientes</h3>
        <div class="actions-container">
            <a href="clientes/agregar" tooltip-hint="Agregar Cliente"><img src="/admin/imgs/icons/actions/add.png"></a>
        </div>
    </div>
    <?php include_once('templates/listactions.php'); ?>
    <div class="list-overflow">
        <div class="list-container">
            <div class="list-filler">
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
                <div class="list-column"></div>
            </div>
            <div class="list-header">
                <div class="list-column">
                </div>
                <div class="list-column">
                    <p>Folio</p>
                </div>
                <div class="list-column">
                    <p>Nombre</p>
                </div>
                <div class="list-column">
                    <p>Ruta</p>
                </div>
                <div class="list-column">
                    <p>Días de Ruta</p>
                </div>
                <div class="list-column">
                    <p>Teléfono</p>
                </div>
                <div class="list-column">
                    <p>Deuda</p>
                </div>
                <div class="list-column">
                    <p>Dirección</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        privilegios = JSON.parse('<?php echo json_encode($GLOBALS['usuario']['privilegios']); ?>');
        privilegiosNivel = <?php echo $GLOBALS['usuario']['privilegiosNivel']; ?>;
    </script>
    <script src="/admin/js/plugins/personal/Tools.js"></script>
    <script src="/admin/js/plugins/personal/messages.js"></script>
    <script src="/admin/js/plugins/personal/ToolTip.js"></script>
    <script src="/admin/js/plugins/personal/CheckSwitch.js"></script>
    <script src="/admin/js/plugins/personal/SortController.js"></script>
    <script src="/admin/js/global/Lista.js"></script>
    <script src="/admin/js/global/menu.js"></script>
    <script src="/admin/js/clientes/clientes-lista.js"></script>
</body>
</html>